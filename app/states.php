<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class states extends Model
{
    protected $table="states";
    public function user(){
      
        return $this->hasMany('App/User');
    }
    public function kyc(){
        return $this->hasOne('App\kyc');
    }
}
