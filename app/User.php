<?php

namespace App;

use App\Notifications\PasswordReset;
use App\Notifications\SendGenericNotificationToUsers;
use App\Notifications\VerifyEmail as VerifyEmailNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Str;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'firstname',
        'lastname',
        'email',
        'pwd',
        'phone',
        'username',
        'status',
        'currency',
        'referal',
        'reg_date',
        'act_code',
        'pass_thru',
        'two_factor_code',
        'two_factor_expires_at',
        'google_id',
    ];

    protected $with = [
        'files',
        'deposits',
        'withdrawals',
        'kyc',
        'activities',
        'funding_subscription',

        'fundings',
        'walletAddresses'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pwd',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $dates = [
        'updated_at',
        'created_at',
        'two_factor_expires_at',
        'verification_token_expires_at',
    ];

    public function getAuthPassword()
    {
        return $this->pwd;
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {

        $this->generateVerificationToken();
        $this->setVerificationToken();
        $this->notify(new VerifyEmailNotification);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }

    protected function setVerificationToken()
    {
        $this->verification_token = $this->generateVerificationToken();
        $this->verification_token_expires_at = now()->addMinutes(60);
        $this->save();
    }

    /**
     * Generate a unique verification token.
     *
     * @return string
     */
    public function generateVerificationToken()
    {
        return sha1($this->getEmailForVerification());
    }

    public function notifyUser(array $data)
    {
        $reqProps = ['message'];
        if (!empty(array_diff($reqProps, array_keys($data)))) {
            throw new \InvalidArgumentException("Required properties missing");
        }
        $notifiData = [
            'action' => $data['action'] ?? 'View', // title of the notificatin button
            'message' => $data['message'],
            'greeting' => $data['greeting'] ?? "Hello! {$this->name}",
            'message_type' => $data['message_type'] ?? 'info', // 'info', 'success', 'error', 'warning
            'sub_message' => $data['sub_message'] ?? '',
            'subject' => $data['subject'] ?? "Notification from " . config('app.name'),
            'action_url' => $data['action_url'] ?? "",
        ];
        $this->notify(new SendGenericNotificationToUsers($notifiData));
    }

    /**
     * Create a new user.
     *
     * @param array $data
     * @return \App\User
     */
    public static function createUser($data)
    {
        $user = new User();
        $user->firstname = $data['firstname'];
        $user->lastname = $data['lastname'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        $user->pwd = '';
        $user->username = $data['username'];
        $user->status = 1;
        $user->google_id = $data['google_id'] ?? null;
        $user->email_verified_at = now();
        $user->referal = $data['referal'] ?? null;
        $user->img = $data['img'] ?? null;
        $user->save();
        return $user;

    }

    public function getNameAttribute()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function getVerificationToken()
    {

        return $this->verification_token;
    }

    public function ticket()
    {
        return $this->hasMany('App\ticket', 'user_id');
    }
    public function kyc()
    {
        return $this->hasOne('App\kyc');
    }
    public function activities()
    {
        return $this->hasMany('App\activities');
    }
    public function country()
    {
        return $this->hasOne('App\country');
    }
    public function state()
    {
        return $this->belongsTo('App\state');
    }
    public function comments()
    {
        return $this->hasMany('App\comments', 'sender_id');
    }
    public function investments()
    {
        return $this->hasMany('App\investment', 'user_id');
    }
    public function deposits()
    {
        return $this->hasMany('App\deposits');
    }
    public function referals()
    {
        return $this->hasMany('App\ref');
    }
    public function withdrawals()
    {
        return $this->hasMany('App\withdrawal');
    }
    public function getFullNameAttribute()
    {
        return "{$this->firstname} {$this->lastname}";
    }
    public function generateTwoFactorCode()
    {
        $this->timestamps = false;
        $this->two_factor_code = rand(100000, 999999);
        $this->two_factor_expires_at = now()->addMinutes(10);
        $this->save();
    }
    public function resetTwoFactorCode()
    {
        $this->timestamps = false;
        $this->two_factor_code = null;
        $this->two_factor_expires_at = null;
        $this->save();
    }

    public function files()
    {
        return $this->hasMany('App\File', 'user_id');
    }

    public function loginSecurity()
    {
        return $this->hasOne(LoginSecurity::class);
    }

    public function funding_subscription()
    {
        return $this->hasOne(FundingSubscription::class);
    }

    public function fundings()
    {
        return $this->hasMany(Funding::class, 'user_id');
    }
    public function jointInvestments()
    {
        return $this->hasMany(JointInvestment::class);
    }

    public function walletAddresses()
    {
        return $this->hasMany(WalletAddress::class);
    }

    /**
     * Check if the given username is unique.
     *
     * @param string $username
     * @param int|null $ignoreId
     * @return bool
     */
    public static function isUsernameUnique($username, $ignoreId = null)
    {
        $query = self::where('username', $username);

        if (!is_null($ignoreId)) {
            $query->where('id', '!=', $ignoreId);
        }

        return $query->count() === 0;
    }

    /**
     * Generate a unique username by suffixing numbers if needed.
     *
     * @param string $username
     * @param int|null $ignoreId
     * @return string
     */
    public static function generateUniqueUsername($username, $ignoreId = null)
    {
        $baseUsername = $username;
        $suffix = rand(1, 1000);

        while (!self::isUsernameUnique($username, $ignoreId)) {
            $username = $baseUsername . $suffix;
            $suffix++;
        }

        return $username;
    }
}
