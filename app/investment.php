<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class investment extends Model
{
    protected $table = "invest";
    public $with = ['package'];

    protected $fillable = [
        'user_id',
        'package_id',
        'capital',
        'daily_interest',
        'status',
        'date_invested',
        'end_date',
        'usn',
        'i_return',
        'w_amt',
        'last_wd',
        'created_at',
        'days_interval',
        'currency',
        'period',
        'interest',
    ];
    protected $dates = [
        'created_at',
        // 'date_invested',
        // 'end_date'
    ];

    public function package()
    {
        return $this->belongsTo('App\packages', );
    }

    // public function setEndDateAttribute($value) {
    //     return $value->format('Y-m-d');
    // }

    // public function setDateInvestedAttribute($value) {
    //     return $value->format('Y-m-d');
    // }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function jointHolders()
    {
        return $this->hasMany(JointInvestment::class);
    }

}
