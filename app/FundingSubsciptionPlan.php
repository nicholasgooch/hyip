<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FundingSubsciptionPlan extends Model
{
    public function subscription()
    {
        return $this->hasOne(User::class);
    }
}
