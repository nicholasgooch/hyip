<?php

namespace App\Http\Middleware;

use Closure;

class FundingOnboarded
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->user()->funding_onboarded){
            return redirect(route('funding.onboarding'));
        }
        return $next($request);
    }
}
