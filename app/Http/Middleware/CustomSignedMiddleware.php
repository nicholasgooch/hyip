<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Routing\Exceptions\InvalidSignatureException;

class CustomSignedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check for a valid signature
        if ($this->verifiedUser($request)) {
            return $next($request);
        }

        throw new InvalidSignatureException;
    }

    /**
     * Check if the request has a valid signature.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function hasValidatedSignature($request)
    {
        return $this->verifiedUser($request);
    }

    /**
     * Check if the application is running on a live Ubuntu server.
     *
     * @return bool
     */
    protected function isLiveUbuntuServer()
    {
        return env('APP_ENV') === 'production' &&
        php_uname('s') === 'Linux' &&
        file_exists('/etc/lsb-release') &&
        strpos(file_get_contents('/etc/lsb-release'), 'Ubuntu') !== false;
    }

    /**
     * Validate the user model based on custom criteria.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @return bool
     */
    protected function verifiedUser(Request $request)
    {

        return $this->isVerified($request);
    }

    /**
     * Perform additional user-related validation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     *
     * @throws \Illuminate\Routing\Exceptions\InvalidSignatureException
     */
    protected function isVerified(Request $request)
    {
        // Get the user ID and verification token from the URL
        $userId = $request->route('id');
        $token = $request->route('hash');

        // Perform user-related validation based on the user model
        $user = User::find($userId);
        if (!$user || !hash_equals($user->verification_token, $token) || $this->checkIfTokenExpired($request, $user)) {
            // Invalid user or token, throw an exception
            throw new InvalidSignatureException;
        }
        return true;
    }

    /**
     * Check if the user's verification token has expired.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return bool
     */
    public function checkIfTokenExpired(Request $request, $user)
    {
        return $user->verification_token_expires_at->addMinutes(config('auth.verification.expire', 60))->isPast();
    }
}
