<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class TwoFactor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = auth()->user();
        \Log::info('TwoFactor middleware', ['user' => $user]);

        if (Auth::guard('web')->check() && $user->two_factor_code) {

            if ($user->two_factor_expires_at->lt(now())) {
                $user->resetTwoFactorCode();
                // auth()->logout();

                return redirect()->route('login')
                    ->withMessage('The two factor code has expired. Please login again.');
            } else {
                return redirect()->route('otp.index');
            }
        }

        return $next($request);
    }
}
