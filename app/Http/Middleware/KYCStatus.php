<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class KYCStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('web')->user()->kyc_status == false) {
            return redirect(route('v2.kyc.page'));

        }
        return $next($request);
    }
}
