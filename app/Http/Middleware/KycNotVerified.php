<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class KycNotVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('web')->user()->kyc_status == true) {
            return redirect(route('v2.index'));

        }
        return $next($request);
    }
}
