<?php

namespace App\Http\Middleware;

use Closure;

class MustApplytoFunding
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->fundings->isEmpty()){
            session()->flash('error', 'You must apply to a funding before you can access this page');
            return redirect()->route('funding.apply');
        }

        return $next($request);
    }
}
