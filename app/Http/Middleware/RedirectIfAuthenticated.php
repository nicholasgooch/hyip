<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $isAdminRoute = $request->is('admin') || $request->is('admin/*') || $request->is('back-end/*') || $request->is('back-end');
        if ($isAdminRoute) {
            if (Auth::guard('admin')->check()) {
                return redirect()->route('admin.dashboard');
            }
        } else {
            if (Auth::guard('web')->check()) {
                return redirect()->route('v2.index');
            }
        }

        return $next($request);
    }
}
