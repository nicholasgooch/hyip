<?php

namespace App\Http\Livewire;

use App\investment;
use App\JointInvestment;
use App\User;
use Auth;
use Livewire\Component;

class JointInvestmentManager extends Component
{
    public $userQuery = '';
    public $selectedUsers = [];
    public $remainingShare = 100;
    public $user;
    public $investment;
    public $investmentUsers = [];
    protected $minOwnerShare = 20;
    public $availableShare;
    public function mount($investment)
    {
        $this->user = Auth::guard('web')->user();
        $this->investment = is_numeric($investment) ? Investment::find($investment) : $investment;
        $this->loadInvestmentUsers();
    }
    public function render()
    {
        $searchResults = [];
        if (strlen($this->userQuery) >= 2) {
            $searchResults = User::where(function ($query) {
                // Group the search conditions to ensure they are evaluated correctly
                $query->where('firstname', 'like', '%' . $this->userQuery . '%')
                    ->orWhere('lastname', 'like', '%' . $this->userQuery . '%')
                    ->orWhere('email', 'like', '%' . $this->userQuery . '%');
            })
                ->take(5)
                ->get();
        }
        return view('livewire.joint-investment-manager', [
            'searchResults' => $searchResults,
        ]);
    }
    public function addUser($userId, $userName, $email)
    {
        if (!$this->validateUserAddition($userId, $email)) {
            return;
        }

        if (!array_key_exists($userId, $this->selectedUsers)) {
            $this->selectedUsers[$userId] = ['name' => $userName, 'email' => $email, 'share_percentage' => 0];
            $this->userQuery = ''; // Clear the search query to reset the search input
            $this->calculateRemainingShare(); // Recalculate the remaining share
            $this->emitSelf('userAdded'); // Optional: Emit an event if you want to trigger any UI response
        }

    }

    protected function validateUserAddition($userId, $email)
    {
        if ($userId == $this->user->id) {
            $this->addError('error', 'You cannot add yourself to a joint investment.');
            $this->userQuery = '';
            return false;
        }

        if (array_key_exists($email, $this->investmentUsers)) {
            $this->addError('error', 'This user is already added.');
            $this->userQuery = '';
            return false;
        }

        return true;
    }

    protected function calculateRemainingShare()
    {

        $totalShareAllocated = array_sum(array_column($this->selectedUsers, 'share_percentage'));
        $this->remainingShare = max(100 - $totalShareAllocated, $this->minOwnerShare);

        // Ensure the owner's share is not below the threshold
        if ($this->remainingShare < $this->minOwnerShare) {
            $this->error('The owner\'s share cannot drop below the minimum threshold.');
            // Consider adjusting shares or notifying the user to adjust manually
        }

    }
    public function updateShare($userId, $share)
    {

        if ($userId == $this->user->id) {
            $this->addError('error', 'You cannot add yourself to a joint investment.');
            return;
        }

        // Ensure the requested share is a number and within the bounds.
        $share = max(0, min(100, intval($share)));

        // First, let's update the share of the user if it exists in selectedUsers.
        if (isset($this->selectedUsers[$userId])) {
            // Temporarily update the user's share to calculate the total.
            $this->selectedUsers[$userId]['share_percentage'] = $share;
        } else {
            // If user not in selectedUsers and trying to set share, abort.
            $this->addError('error', 'Invalid user selected.');
            return;
        }
        $investmentUsers = $this->investmentUsers;
        unset($investmentUsers[$this->user->email]);
        $totalInvestmentUserShares = array_sum(array_column($investmentUsers, 'share_percentage'));

        // Calculate the total used share excluding the owner.
        $totalUsedShare = array_sum(array_column($this->selectedUsers, 'share_percentage'));
        $ownerShareWithNewUser = 100 - $totalUsedShare;

        // Check if the update breaches the minimum threshold for the owner's share.
        if (($ownerShareWithNewUser - $totalInvestmentUserShares) < $this->minOwnerShare) {
            $this->addError('error', "Updating this user's share would reduce the owner's share below the minimum threshold of " . $this->minOwnerShare . "%.");
            // Revert the share update due to error.
            $this->selectedUsers[$userId]['share_percentage'] = $this->availableShare; // Consider calculating the original share instead of resetting to 0
            return;
        }

        // Now, update the owner's share in investmentUsers (if the owner is part of this).
        if (isset($this->investmentUsers[$this->user->email])) {
            $this->investmentUsers[$this->user->email]['share_percentage'] = $ownerShareWithNewUser - $totalInvestmentUserShares;
        }

        //update other investmen users share also

        // Recalculate and update the remaining share to ensure UI reflects the current state accurately.
        $this->calculateRemainingShare();
    }
    public function removeUser($userId)
    {
        unset($this->selectedUsers[$userId]);
        $this->calculateRemainingShare();
    }

    protected function loadInvestmentUsers()
    {

        $investmentModel = $this->investment instanceof Investment ? $this->investment : Investment::find($this->investment);

        // Include the investment owner with an initial 100% share
        $owner = $investmentModel->user; // Fetch the owner
        if ($owner) {
            $this->investmentUsers[$owner->email] = [
                'id' => $owner->id,
                'name' => $owner->firstname . ' ' . $owner->lastname,
                'share_percentage' => 100, // Start with 100% share
                'share_amount' => $investmentModel->capital,
            ];
        }
        if ($investmentModel && $investmentModel->jointHolders) {
            // Load joint holders and adjust shares
            $totalJointShare = 0;

            foreach ($investmentModel->jointHolders as $jointHolder) {

                $user = $jointHolder->user;

                $this->investmentUsers[$user->email] = [
                    'id' => $user->id,
                    'name' => $jointHolder->user->firstname . ' ' . $jointHolder->user->lastname,
                    'share_percentage' => $jointHolder->share_percentage,
                    'share_amount' => ($jointHolder->share_percentage / 100) * $investmentModel->capital,
                ];

                $totalJointShare += $jointHolder->share_percentage;
            }

            // Adjust the owner's share based on the total share allocated to joint holders
            if ($owner) {
                $this->investmentUsers[$owner->email]['share_percentage'] = max(100 - $totalJointShare, 0);
                $this->investmentUsers[$owner->email]['share_amount'] = ($this->investmentUsers[$owner->email]['share_percentage'] / 100) * $investmentModel->capital;
            }
        }
    }
    public function availableShareForNewUsers()
    {
        //remove current user from investment users array
        $investmentUsers = $this->investmentUsers;
        unset($investmentUsers[$this->user->email]);
        $currentTotalShares = array_sum(array_column($investmentUsers, 'share_percentage'));
        $availableShare = 100 - $currentTotalShares - $this->minOwnerShare; // Reserve MIN_OWNER_SHARE_THRESHOLD for the owner
        $this->availableShare = max($availableShare, 0); // Ensure it doesn't return a negative number
        return max($availableShare, 0);
    }
    public function saveJointInvestments()
    {
        if (!$this->investment || count($this->selectedUsers) === 0) {
            $this->error('No investment selected or no users to add.');
            return;
        }

        foreach ($this->selectedUsers as $userId => $details) {
            // Assuming $details includes 'share' as the share percentage
            JointInvestment::updateOrCreate(
                [

                    'investment_id' => $this->investment->id,
                    'user_id' => $userId,
                ],
                [
                    'share_percentage' => $details['share_percentage'],
                ]
            );
            //add user to investment users array
            $this->investmentUsers[$details['email']] = [
                'id' => $userId,
                'name' => $details['name'],
                'share_percentage' => $details['share_percentage'],
                'share_amount' => ($details['share_percentage'] / 100) * $this->investment->capital,
            ];
        }

        // Optional: Reset state or redirect after saving
        $this->reset(['selectedUsers', 'userQuery']);
        $this->emit('jointInvestmentsSaved'); // Notify other components or for UI updates

    }
}
