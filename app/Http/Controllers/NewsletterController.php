<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactEmail;
use Newsletter;

class NewsletterController extends Controller
{
    public function store(Request $request)
    {
        $this->subscribeMailChimp($request->email);

        return back()->with('jsAlert', 'Updated!');
    }

    public function contactEmailAlert(Request $request)
    {
        $content = $request->all();

        $request->validate([
            'g-recaptcha-response' => 'required|captcha'
        ]);

        $this->subscribeMailChimp($request->email);

        try {
            Mail::to(config('app.admin_email'))->send(new ContactEmail($content));
            $suc = true;
            return back()->with('contactAlert', 'Updated!');
        } catch (\Exception $e) {
            return $e;
        }


    }

    private function subscribeMailChimp($email)
    {
        try {
            if (!Newsletter::isSubscribed($email)) {
                return Newsletter::subscribe($email);
            }
        } catch (\Exception $e) {
            return $e;
        }

    }

}
