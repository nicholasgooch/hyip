<?php

namespace App\Http\Controllers\Exchange;

use App\ExWallet;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('exchange.wallets');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExWallet  $exWallet
     * @return \Illuminate\Http\Response
     */
    public function show(ExWallet $exWallet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExWallet  $exWallet
     * @return \Illuminate\Http\Response
     */
    public function edit(ExWallet $exWallet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExWallet  $exWallet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExWallet $exWallet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExWallet  $exWallet
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExWallet $exWallet)
    {
        //
    }
}
