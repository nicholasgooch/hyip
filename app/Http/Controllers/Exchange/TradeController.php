<?php


namespace App\Http\Controllers\Exchange;

use App\ExTrade;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class TradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('exchange.trades');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExTrade  $exTrade
     * @return \Illuminate\Http\Response
     */
    public function  trade_page( )
    {
     return view('exchange.trade');   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExTrade  $exTrade
     * @return \Illuminate\Http\Response
     */
    public function edit(ExTrade $exTrade)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExTrade  $exTrade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExTrade $exTrade)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExTrade  $exTrade
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExTrade $exTrade)
    {
        //
    }
}
