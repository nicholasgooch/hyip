<?php

namespace App\Http\Controllers;

use App\banks;
use App\CollateralType;
use App\deposits;
use App\Funding;
use App\FundingSubsciptionPlan;
use App\FundingSubscription;
use App\FundingType;
use App\FundingWithdrawal;
use App\Mail\Admin\FundingApplicationNotification as FundingApplicationNotificationAdmin;
use App\Mail\Admin\FundingWithdrawalNotificationAdmin;
use App\Mail\FundingApplicationNotification;
use App\Mail\FundingWithdrawalNotificationUser;
use App\Repayment;
use App\site_settings;
use App\User;
use App\WalletAddress;
use App\withdrawal;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FundingController extends Controller
{
    private $st;
    private $supportEmail;

    public function __construct()
    {
        $this->st = site_settings::first();
        $this->supportEmail = config('app.support_email');
        $this->middleware(['auth:web']);
    }
    public function dashboard()
    {
        $user = Auth::guard('web')->user();

        $funding = Funding::where('user_id', $user->id)->first();
        $my_deposits = deposits::where('user_id', $user->id)->orderBy('created_at', 'desc')->get()->take(5)
            ->map(function ($deposit) {
                $deposit->type = 'deposit';
                return $deposit;
            });

        $my_withdrawals = withdrawal::where('user_id', $user->id)->get()->take(5)
            ->map(function ($withdrawal) {
                $withdrawal->type = 'withdrawal';
                //generate random ref
                $ref = generateRandomTrxRf();
                $withdrawal->ref = $ref;
                return $withdrawal;
            });

        return view('funding.dashboard')->with(compact(['funding', 'my_deposits', 'my_withdrawals']));

    }

    public function applicationPage()
    {

        return view('funding.apply');
    }

    public function transactions_page()
    {
        $user = Auth::guard('web')->user();

        $funding = Funding::where('user_id', $user->id)->first();
        $my_deposits = deposits::where('user_id', $user->id)->orderBy('created_at', 'desc')->get()
            ->map(function ($deposit) {
                $deposit->type = 'deposit';
                return $deposit;
            });

        $my_withdrawals = withdrawal::where('user_id', $user->id)->get()
            ->map(function ($withdrawal) {
                $withdrawal->type = 'withdrawal';
                //generate random ref
                $ref = generateRandomTrxRf();
                $withdrawal->ref = $ref;
                return $withdrawal;
            });

        //combine deposits and withdrawals into transactions var
        $my_transactions = $my_deposits->merge($my_withdrawals)->paginate(10);

        return view('funding.transactions')->with(compact(['funding', 'my_transactions']));

    }

    public function applicationDetails()
    {
        $application = Funding::where('user_id', auth()->user()->id)->first();

        return view('funding.funding_details')->with(compact(['application']));
    }

    public function applicationPost(Request $request)
    {

        $user = Auth::guard('web')->user();
        $funding = Funding::updateOrCreate(['user_id' => $user->id], [
            'user_id' => $user->id,
            'amount' => $request->form['amount'],
            'funding_type_id' => $request->form['funding_type'],
            'collateral_type_id' => $request->form['collateral_type'],
            'collateral_amount' => $request->form['collateral_amount'],
            'interest_rate' => $request->form['interest'],
            'funding_status_id' => 2, //'under-review',
            'term' => $request->form['term'],
            'available_to_withdraw' => 0,
            'collateral_paid' => 0,
            'repayment_frequency' => $request->form['repayment_schedule'],
            'purpose_of_funding' => $request->form['purpose_of_funding'],
            'comments' => $request->form['comments'],
        ]);

        $repayments = [];
        foreach ($request->repayments as $repaymentData) {
            $repayment = new Repayment([
                'funding_id' => $funding->id,
                'payment_number' => $repaymentData['payment_number'],
                'due_date' => Carbon::parse($repaymentData['due_date']),
                'payment_amount' => $repaymentData['payment_amount'],
                'status' => $repaymentData['status'],
            ]);
            $funding->repayments()->save($repayment);
            $repayments[] = $repayment;
        }
        $data = [
            'fundingApplication' => $funding,
            'status' => "under-review",
            'type' => $funding->type->slug,
            'subject' => "{$funding->type->name} Application Received",
        ];

        Mail::to($user->email)->send(new FundingApplicationNotification($data));

        Mail::to($this->supportEmail)->send(new FundingApplicationNotificationAdmin($funding));

        return response()->json(['id' => $funding->id, $repayments]);

    }

    public function applications()
    {
        $user = Auth::guard()->user();
        $applications = Funding::where('user_id', $user->id)->get()
            ->map(function ($application) {
                $application->ref = generateRandomTrxRf();
                return $application;
            });
        return view('funding.applications')->with(compact(['applications']));
    }

    public function getCollateralTypes()
    {
        $collateral_types = CollateralType::all();
        return response()->json($collateral_types);
    }

    public function getFundingTypes()
    {
        $funding_types = FundingType::all();
        return response()->json($funding_types);
    }

    public function onboardPage()
    {
        $id = auth()->user()->id;
        $user = User::find($id);
        if ($user->funding_onboarded === 0) {
            return view('funding.onboarding');

        } else {
            return redirect(route('funding.dashboard'));
        }

    }
    public function onboard()
    {
        $id = auth()->user()->id;

        $user = User::find($id);

        if ($user->funding_onboarded === 0) {
            $user->funding_onboarded = 1;
            $user->save();

        } else {
            return redirect(route('funding.apply'));
        }

        return redirect(route('funding.apply'));
    }

    public function createFundingSubscription($user_id)
    {
        $subscription = new FundingSubscription();
        $subscription->user_id = $user_id;
        $subscription->plan_1_amount = 600000;
        $subscription->plan_2_amount = 60000;
        $subscription->plan_3_amount = 5000;
        $subscription->save();
    }

    public function fundingTypesPage()
    {
        return view('funding.funding_types');
    }

    public function deposit()
    {
        $user = auth()->user();
        $deposit_category = '';
        $plan = '';
        $amount = 0;

        //get deposit category
        if (isset($_GET['c'])) {
            $deposit_category = $_GET['c'];
        } else {
            $deposit_category = '';
        }

        //if plan isset, get plan
        if (isset($_GET['plan'])) {
            $plan = $_GET['plan'];

            $subscription = $user->funding_subscription;

            $plan = FundingSubsciptionPlan::where('name', $plan)->first();

            if ($plan->id == 1) {
                $amount = $subscription->plan_1_amount;
            } elseif ($plan->id == 2) {
                $amount = $subscription->plan_2_amount;
            } elseif ($plan->id == 3) {
                $amount = $subscription->plan_3_amount;
            }
        } else {
            $plan = '';
        }

        $deposit_type = 'funding';

        return view('funding.deposit')->with(compact(['deposit_category', 'plan', 'deposit_type', 'amount']));
    }

    public function subscriptionPlansPage()
    {
        //get all sub type
        $sub_types = FundingSubsciptionPlan::all();
        //get user sub plan if activated for sub by admin
        $user = auth()->user()->load('funding_subscription');

        if (!$user->funding_subscription) {
            $this->createFundingSubscription($user->id);

        }

        $user_subscription = $user->funding_subscription;

        $plans = collect();
        foreach ($sub_types as $type) {

            if ($type->id == 1) {
                $type->setAttribute('amount', $user_subscription->plan_1_amount);
            } elseif ($type->id == 2) {
                $type->setAttribute('amount', $user_subscription->plan_2_amount);
            } elseif ($type->id == 3) {
                $type->setAttribute('amount', $user_subscription->plan_3_amount);
            }
            $plans->push($type);
        }

        return view('funding.subscription_plans')->with(compact('plans'));
    }

    public function withdrawPage()
    {
        return view('funding.withdraw');
    }

    public function saveWithdrawalMethod(Request $request)
    {

        $type = $request->type;
        $user = auth()->user();
        $message = '';
        $data = '';
        if ($type === 'bank') {
            $bank = new banks();
            $bank->user_id = $user->id;
            $bank->Account_number = $request->account_number ?? '';
            $bank->Routing_number = $request->routing_number ?? '';
            $bank->Account_name = $request->account_name ?? '';
            $bank->Bank_name = $request->bank_name ?? '';
            $bank->save();
            $data = $bank;
            $message = 'You have successfully added a new bank account';
        } elseif ($type === 'crypto') {
            $wallet = new WalletAddress();
            $wallet->user_id = $user->id;
            $wallet->wallet_provider = $request->wallet_provider ?? '';
            $wallet->wallet_nickname = $request->wallet_nickname ?? '';
            $wallet->wallet_address = $request->wallet_provider ?? '';
            $wallet->asset_type = $request->wallet_provider ?? '';
            $wallet->save();
            $data = $wallet;
            $message = 'You have successfully added a new crypto wallet';
        } else {
            $message = 'Invalid withdrawal method type';
        }
        return response()->json([
            'status' => 'success',
            'data' => $data,
            'message' => $message,
        ], 200);
    }

    public function getWithdrawalMethods($type)
    {
        $response = '';
        $user = auth()->user();
        if ($type == 'bank') {
            $response = banks::where('user_id', $user->id)->get();
        } elseif ($type == 'crypto') {
            $response = WalletAddress::where('user_id', $user->id)->get();
        }

        return response()->json($response);
    }

    public function submitWithdrawal(Request $request)
    {
        $user = auth()->user();
        $coin_amount = '';
        $message = '';
        $status = '';
        // try{
        $wd = new FundingWithdrawal();
        $wd->user_id = $user->id;
        $wd->wallet_address_id = $request->method_crypto;
        $wd->bank_id = $request->method_bank;
        $wd->amount = $request->amount;
        $wd->withdrawal_method = $request->method_type;
        $wd->category = $request->category;
        $wd->ref = generateRandomTrxRf();

        if ($request->method_type === 'crypto') {
            $wallet = WalletAddress::find($wd->wallet_address_id);
            if ($wallet->asset_type == 'BTC') {
                $coin_amount = toBTC($request->amount);
            } elseif ($wallet->asset_type == 'ETH') {
                $coin_amount = toETH($request->amount);
            }
        }
        $wd->save();
        $data = [
            'user' => $user,
            'amount' => $request->amount,
            'currency' => $this->st->currency,
            'status' => 'Submitted',
        ];

        Mail::to($user->email)->send(new FundingWithdrawalNotificationUser($data));
        $admin_data = [
            'user' => $user,
            'withdrawal' => $wd,
            'approve_url' => ' $approveUrl',
            'decline_url' => '$declineUrl',
        ];
        Mail::to($this->supportEmail)->send(new FundingWithdrawalNotificationAdmin($admin_data));
        $status = 'success';
        $message = 'Your withdrawal request has been submitted and will be processed within the next 24 hours. You will receive an email confirmation once your withdrawal has been processed.';
        // }catch(\Exception $e){
        //     $status = 'error';
        //     $message = 'Oops! An error occurred while processing your request. Please try again later or contact support for assistance.';
        // }

        return response()->json([
            'status' => $status,
            'data' => $wd,
            'message' => $message,
        ], 200);
    }

    public function getSettings()
    {
        $settings = site_settings::first();
        return response()->json($settings);
    }

    public function getApplication($id)
    {
        $application = Funding::where('id', $id)->first();

        return response()->json($application);
    }

}
