<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use App\Notifications\TwoFactorCode;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\Admin\NewUserNotification;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'app/overview';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $messages = [
            'cf-turnstile-response.required' => 'The CAPTCHA field is required',
            'cf-turnstile-response.turnstile' => 'The CAPTCHA verification failed. Please try again.',
        ];
        $validator = validator()->make($request->all(), [
            'email' => 'required|string',
            'password' => 'required|string',
        ], $messages);

        if (App::environment('production')) {
            $validator->sometimes('cf-turnstile-response', 'required|turnstile', function () {
                return true;
            });
        }
        return $validator;
    }

    /**
     * Summary of handleSocialCallback
     * @param mixed $provider
     * @return mixed|\Illuminate\Http\RedirectResponse
     */
    public function handleSocialCallback($provider)
    {

        $socialiteUser = Socialite::driver($provider)->user();
        $ref = session('ref');

        $username = explode("@", $socialiteUser->email)[0];

        $name = explode(" ", $socialiteUser->name);
        $firstname = $name[0];
        $lastname = $name[1] ?? '';
        $existingUser = User::where('email', $socialiteUser->email)->first();
        if ($existingUser) {

            auth()->login($existingUser, true);
        } else {

            $username = User::generateUniqueUsername($username);
            $user = User::createUser([
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $socialiteUser->email,
                'username' => $username,
                'google_id' => $socialiteUser->id,
                'img' => $socialiteUser->avatar_original ?? $socialiteUser->avatar,
                'referal' => $ref ?? '',
            ]);
            auth()->login($user, true);

            $supportEmail = config('app.support_email');

            Mail::to($supportEmail)->send(new NewUserNotification($user));

        }
        return redirect()->route('v2.index', $username);
    }

    /**
     * Summary of redirectToSocial
     * @param mixed $provider
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToSocial($provider)
    {
        // Flash the referral data to the session
        session()->flash('ref', request()->get('ref'));

        return Socialite::driver($provider)->redirect();
    }

    /**
     * Summary of authenticated
     * @param \Illuminate\Http\Request $request
     * @param mixed $user
     * @return mixed|\Illuminate\Http\RedirectResponse
     */
    protected function authenticated(Request $request, $user)
    {
        \Log::info('User authenticated', ['user' => $user]);
        $user->generateTwoFactorCode();
        $user->notify(new TwoFactorCode());
        return redirect()->intended($this->redirectPath());
    }

    /**
     * Show the application's login form.
     *

     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        $credentials = $request->only('email', 'password');

        if (Auth::guard('web')->attempt($credentials)) {
            //authenticate user
            return $this->authenticated($request, Auth::guard('web')->user());
        }

        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request)
    {
        Auth::guard('web')->logout();
        Session::flush();
        return Redirect::to('/');
    }

}
