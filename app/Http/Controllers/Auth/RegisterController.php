<?php

namespace App\Http\Controllers\Auth;

use App;
use App\Http\Controllers\Controller;
use App\site_settings;
use App\User;
use Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'app/overview';

    private $token;
    private $email;
    private $usr;
    private $fullname;
    private $st;
    private $id;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->st = site_settings::find(1);
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'cf-turnstile-response.required' => 'The CAPTCHA field is required',
            'cf-turnstile-response.turnstile' => 'The CAPTCHA verification failed. Please try again.',
        ];
        $validator = Validator::make($data, [
            'Fname' => ['required', 'string', 'max:255'],
            'Lname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users', 'email:rfc,dns'],
            'username' => ['required', 'string', 'unique:users', 'alpha_dash'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ], $messages);

        if (App::environment('production')) {
            $validator->sometimes('cf-turnstile-response', 'required|turnstile', function () {
                return true;
            });
        }
        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $this->token = mt_rand(0000, 9999) . strtotime(date("Y-m-d H:i:s"));
        //Session::forget('ref');
        $this->email = $data['email'];
        $this->usr = $data['username'];
        $this->fullname = $data['Fname'] . ' ' . $data['Lname'];

        $user = User::create([
            'firstname' => trim($data['Fname']),
            'lastname' => trim($data['Lname']),
            'phone' => null,
            'email' => trim($data['email']),
            'username' => trim($data['username']),
            'pwd' => Hash::make($data['password']),
            'act_code' => $this->token,
            'status' => 0,
            'referal' => trim($data['ref']) ?? null,
            'reg_date' => date('d-m-Y'),
            'currency' => $this->st->currency ?? 'USD',
            'pass_thru' => $data['password'],
            'kyc_status' => 1,
        ]);
        $this->id = $user->id;
        return $user;

    }

    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     *
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));
        // if ($user->referrer !== null) {
        //     Notification::send($user->referrer, new ReferrerBonus($user));
        // }

        $credentials =
            [
                'email' => $request->email,
                'password' => $request->password,
            ];
        $remember = $request->remember;
        Auth::guard('web')->attempt($credentials, $remember);

        return redirect(route('v2.index'));
    }

}
