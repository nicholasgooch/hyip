<?php

namespace App\Http\Controllers\Auth;

use App\activities;
use App\Http\Controllers\Controller;
use App\Notifications\TwoFactorCode;
use App\site_settings;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Mail;
use Session;

class TwoFAController extends Controller
{

    private $supportEmail;
    private $st;
    public function __construct()
    {
        $this->supportEmail = config('app.support_email');
        $this->st = site_settings::find(1);
        $this->middleware('auth');
    }
    public function show2faPage()
    {
        return view('auth.otp_verify');
    }

    public function store(Request $request)
    {

        $user = auth()->user();
        if ($request->input('otpValue') == $user->two_factor_code) {
            $user->resetTwoFactorCode();

            // return redirect()->route('user.dash',$user->username);

            return response()->json(['success' => 'true', 'message' => 'Login Successful']);
        }

        return response()->json(['success' => 'false', 'message' => 'Invalid OTP code']);
    }

    public function resend(Request $request)
    {
        $user = auth()->user();
        $user->generateTwoFactorCode();
        $user->notify(new TwoFactorCode());

        return response()->json(['success' => 'true', 'message' => 'OTP code resent']);
    }

    public function redirectTo()
    {
        $user = Auth::user();

        $act = new activities;
        $act->action = "User logged in to account";
        $act->user_id = $user->id;
        $act->save();

        if ($user->status == 0 || $user->status == 'New' || $user->status == 'pending') {
            Session::flush();
            Session::put('err_msg', 'Account not activated');
            return redirect()->route('login'); //->withErrors(['msg', 'Account not activated']);
        }
        if ($user->status == 2 || $user->status == 'Blocked') {
            Session::flush();
            Session::put('err_msg', 'Account Blocked! Please contact support.');
            return redirect('login')->route('login'); //->withErrors(['msg', 'Account not activated']);
        }

        $maildata = ['email' => $user->email, 'username' => $user->username];
        Mail::send('mail.loginNotification', ['md' => $maildata], function ($msg) use ($maildata) {
            $msg->from($this->supportEmail, $this->st->site_title);
            $msg->to($maildata['email']);
            $msg->subject('User Account login');
        });

        return redirect()->route('v2.index');

    }
}
