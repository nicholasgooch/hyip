<?php

namespace App\Http\Controllers\Admin;

use App\Currencies;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExchangeController extends Controller
{
    public function ex_currencies_page()
    {
        return view('admin.exchange.currency.index');
    }

    public function ex_currency_fiat_page()
    {
        return view('admin.exchange.currency.fiat');
    }
    public function ex_currency_crypto_page()
    {
        return view('admin.exchange.currency.crypto');
    }

    public function ex_currency_create()
    {
        return view('admin.exchange.currency.create');
    }
    public function ex_currency_create_post(Request $request)
    {

        // $request->validate([
        //     'icon'               => 'required_if:type,2|image|mimes:png,jpg,PNG,jpeg',
        //     'curr_name'          => 'required',
        //     'code'               => 'required|max:4',
        //     'symbol'             => 'required|unique:currencies',
        //     'rate'               => 'required|gt:0',
        //     'type'               => 'required|in:1,2',
        //     'default'            => 'required_if:type,1|in:1,0',
        //     'status'             => 'required|in:1,0',
        //     'deposit_charge'     => 'required_if:type,2|lt:100',
        //     'withdraw_charge'    => 'required_if:type,2|lt:100',
        //     'withdraw_limit_min' => 'required_if:type,2|gt:0',
        //     'withdraw_limit_max' => 'required_if:type,2|gt:0',
        // ],
        // [
        //     'curr_name.required'              =>'Currency name is required.',
        //     'withdraw_limit_min.required_if'  =>'Withdraw minimum limit is required when currency type is crypto.',
        //     'withdraw_limit_max.required_if'  =>'Withdraw maximum limit is required when currency type is crypto.',
        //     'icon.required_if'                =>'Icon is required when currency type is crypto.',
        // ]);

        $data = $request->only('icon', 'curr_name', 'code', 'symbol', 'rate', 'type', 'default', 'status');

        if ($request->default && $request->type != 2) {
            $default = Currency::where('default', 1)->firstOrFail();
            $default->default = 0;
            $default->save();

        } else {
            $data['default'] = 0;
            $data['charges'] = [
                'deposit_charge' => $request->deposit_charge,
                'withdraw_charge' => $request->withdraw_charge,
                'withdraw_limit_min' => $request->withdraw_limit_min,
                'withdraw_limit_max' => $request->withdraw_limit_max,
            ];
        }
        // $data['icon'] = $request->icon ? MediaHelper::handleMakeImage($request->icon) : null;
        Currencies::create($data);
        return back()->with('success', 'New currency has been added');
    }
}
