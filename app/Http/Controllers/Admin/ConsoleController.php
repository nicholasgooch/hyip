<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Response;
use Symfony\Component\Process\Exception\ProcessFailedException;

class ConsoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display the console page
     *
     * @return Response|RedirectResponse
     */
    public function index()
    {
        $adm = Auth::guard('admin')->user();

        if ($adm->role != 'admin') {
            session()->flash('error', 'You are not authorized to access this page');
            return redirect('/admin/overview');
        }
        return view('admin.console.index')->with('adm', $adm);
    }

    /**
     * Execute a system command
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function execute(Request $request)
    {
        $command = $request->input('command');

        // Basic security check - you might want to enhance this
        if (empty($command) || !is_string($command)) {
            return response()->json(['error' => 'Invalid command'], 400);
        }

        try {
            $process = Process::fromShellCommandline($command);
            $process->setWorkingDirectory(base_path());
            $process->setTimeout(60); // 1 minute timeout
            $process->run();

            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            return response()->json([
                'output' => $process->getOutput(),
                'success' => true
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'output' => $process->getErrorOutput() ?? '',
                'success' => false
            ]);
        }
    }
}