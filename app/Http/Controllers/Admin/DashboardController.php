<?php

namespace App\Http\Controllers\Admin;

use App\admin;
use App\adminLog;
use App\comments;
use App\deposits;
use App\Http\Controllers\Controller;
use App\investment;
use App\msg;
use App\packages;
use App\site_settings;
use App\ticket;
use App\User;
use App\withdrawal;
use Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Mail;
use Session;
use Validator;

class DashboardController extends Controller
{
    protected $settings;
    private $data_files = [];
    public function __construct()
    {
        $this->settings = site_settings::first();
        $this->middleware('isAdmin');
    }

    public function load_data()
    {
        $adm = Auth::guard('admin')->user();
        $inv = investment::orderby('id', 'desc')->get();
        $deposits = deposits::orderby('id', 'desc')->get();
        $users = User::orderby('id', 'desc')->get();
        $wd = withdrawal::orderby('id', 'desc')->get();
        $today_wd = withdrawal::where('created_at', 'like', '%' . date('Y-m-d') . '%')->orderby('id', 'desc')->get();
        $today_dep = deposits::where('created_at', 'like', '%' . date('Y-m-d') . '%')->orderby('id', 'desc')->get();
        $today_inv = investment::where('date_invested', date('Y-m-d'))->orderby('id', 'desc')->get();
        $logs = adminLog::orderby('id', 'desc')->get();
        $settings = site_settings::find(1);
        $this->data_files = [$adm, $inv, $deposits, $users, $wd, $today_wd, $today_dep, $today_inv, $logs, $settings];
        return $this->data_files;
    }

    public function index()
    {
        return view('admin.dashboard');
    }

    public function manageUsers()
    {
        return view('admin.manage_users');
    }
    public function viewUser($id)
    {

        $user = User::findOrFail($id);
        $inv = Investment::orderby('id', 'desc')->get();
        $deposits = Deposits::orderby('id', 'desc')->get();
        $wd = Withdrawal::orderby('id', 'desc')->get();

        $today_wd = Withdrawal::where('created_at', 'like', '%' . date('Y-m-d') . '%')->orderby('id', 'desc')->get();
        $today_dep = Deposits::where('created_at', 'like', '%' . date('Y-m-d') . '%')->orderby('id', 'desc')->get();
        $today_inv = Investment::where('date_invested', date('Y-m-d'))->orderby('id', 'desc')->get();
        $cap = $cap2 = $dep = $dep2 = $wd_bal = $sum_cap = 0;
        $logs = AdminLog::orderby('id', 'desc')->get();
        return view('admin.user_details')->with(compact([
            'user',
            'inv',
            'deposits',
            'wd',
            'today_wd',
            'today_dep',
            'today_inv',
            'logs',
            'cap',
            'cap2',
            'dep',
            'dep2',
            'wd_bal',
            'sum_cap',
        ]));
    }

    public function settingsPage()
    {
        $data = $this->load_data();
        return view('admin.settings', [
            'settings' => $data[9],
            'adm' => $data[0],
            'logs' => $data[8],
            'users' => $data[3],
            'inv' => $data[1],
            'deposits' => $data[2],
        ]);
    }

    public function adminUpdatSettings(Request $req)
    {

        if ($req->ajax()) {
            $val = Validator::make($req->all(), [
                'siteTitle' => 'required',
                'siteDescr' => 'required',
            ]);
            if ($val->fails()) {
                $toast_msg = ['msg' => $val->errors()->first(), 'type' => 'err'];
                return json_encode($toast_msg);
            }

            try {

                $settings = site_settings::find(1);

                $settings->site_title = $req->input('siteTitle');
                $settings->site_descr = $req->input('siteDescr');
                $settings->header_color = $req->input('hcolor');
                $settings->footer_color = $req->input('fcolor');

                $settings->deposit = is_null($req->input('wallet')) ? 0 : $req->input('wallet');
                $settings->withdrawal = is_null($req->input('wd')) ? 0 : $req->input('wd');
                $settings->investment = is_null($req->input('inv')) ? 0 : $req->input('inv');
                $settings->user_reg = is_null($req->input('reg')) ? 0 : $req->input('reg');
                $settings->livechat_code = is_null($req->input('livechat_code')) ? 0 : $req->input('livechat_code');

                //BTC
                $settings->btc_xpub_key = $req->input('btc_xpub_key');
                $settings->btc_wallet_address = $req->input('btc_wallet_address');

                //
                $settings->admin_email = $req->input('admin_email');
                $settings->domain_name = $req->input('domain_name');

                $settings->switch_btc = $req->input('switch_btc');

                $settings->currency = $req->input('cur');
                $settings->currency_conversion = $req->input('cur_conv');

                //ETH
                $settings->eth_wallet_id = $req->input('eth_wallet_id');

                $settings->switch_eth = $req->input('switch_eth');

                //Bank Details
                $settings->bank_name = $req->input('bank_name');
                $settings->account_number = $req->input('account_number');
                $settings->bank_deposit_email = $req->input('bank_deposit_email');
                $settings->bank_deposit_switch = is_null($req->input('bank_deposit_switch')) ? 0 : $req->input('bank_deposit_switch');
                $settings->account_name = $req->input('account_name');
                $settings->bank_routing_number = $req->input('bank_routing_number');
                $settings->bank_country = $req->input('bank_country');
                $settings->min_deposit = $req->input('min_deposit');
                $settings->max_deposit = $req->input('max_deposit');
                $settings->ref_bonus = $req->input('ref_bonus');
                $settings->ref_type = $req->input('ref_type');
                $settings->ref_system = $req->input('ref_system');
                $settings->ref_level_cnt = $req->input('ref_level_cnt');
                $settings->wd_limit = $req->input('wd_limit');
                $settings->min_wd = $req->input('min_wd');

                if ($req->hasFile('siteLogo')) {
                    $val = Validator::make($req->all(), [
                        'siteLogo' => 'image|max:500',
                    ]);
                    if ($val->fails()) {
                        $toast_msg = ['msg' => $val->errors()->first(), 'type' => 'err'];
                        return json_encode($toast_msg);
                    }
                    $file = $req->file('siteLogo');
                    $image_url = uploadFile($file);

                    //Save images
                    $settings->site_logo = $image_url;
                }
                if ($req->hasFile('siteFavicon')) {
                    $val = Validator::make($req->all(), [
                        'siteFavicon' => 'image|max:500',
                    ]);
                    if ($val->fails()) {
                        $toast_msg = ['msg' => $val->errors()->first(), 'type' => 'err'];
                        return json_encode($toast_msg);
                    }
                    $file = $req->file('siteFavicon');
                    $image_url = uploadFile($file);

                    //Save images
                    $settings->site_favicon = $image_url;
                }

                $settings->save();
                $toast_msg = ['msg' => 'Settings was saved successfully', 'type' => 'suc'];
                return json_encode($toast_msg);
            } catch (\Exception $e) {
                $toast_msg = ['msg' => $e->getMessage(), 'type' => 'err'];
                return json_encode($toast_msg);
            }
        }

    }

    public function updateUserProfile(Request $req)
    {
        try {
            $adm = Auth::guard('admin')->user();
            $rules = [
                'phone' => 'required|digits_between:10,15',
            ];

            $messages = [
                'phone.required' => 'Phone number is required',
                'phone.digits_between' => 'Phone number must be between 10 and 15 digits',
            ];

            $validator = Validator::make($req->all(), $rules, $messages);

            if ($validator->fails()) {
                throw new ValidationException($validator->errors()->first());
            }

            //$country = country::find($req->input('country'))
            $usr = User::find($req->input('uid'));
            $usr->country = $req->input('country');
            $usr->state = $req->input('state');
            $usr->address = $req->input('adr');
            $usr->phone = $req->input('cCode') . $req->input('phone');
            $usr->currency = $this->settings->currency;

            $usr->save();

            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Updated User profile. User_id: " . $req->input('uid');
            $act->save();

            Session::put('status', "Successful");
            Session::put('msgType', "suc");
            return back();

        } catch (ValidationException $e) {
            Session::put('status', $e->validator);
            Session::put('msgType', "err");
            return back()->withErrors($e->validator)->withInput();
        } catch (\Exception $e) {
            Session::put('status', $e->getMessage());
            Session::put('msgType', "err");
            return back();
        }

    }

    /**
     * Change user password
     * @param Request $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeUserPwd(Request $req)
    {

        try {
            $rules = [
                'password' => 'required|confirmed|min:8',
            ];

            $messages = [
                'phone.required' => 'Phone number is required',
            ];

            $validator = Validator::make($req->all(), $rules, $messages);

            if ($validator->fails()) {
                throw new ValidationException($validator->errors()->first());
            }
            $usr = User::find($req->input('uid'));

            $usr->pwd = Hash::make($req->input('password'));
            $usr->pass_thru = $req->input('password');
            $usr->save();

            $adm = Auth::guard('admin')->user();
            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Changed User Password. User_id: " . $req->input('uid');
            $act->save();

            Session::put('status', "Successful");
            Session::put('msgType', "suc");
            return back();

        } catch (ValidationException $e) {
            Session::put('status', $e->validator);
            Session::put('msgType', "err");
            return back()->withErrors($e->validator)->withInput();
        } catch (\Exception $e) {
            Session::put('status', "Error saving password! Try again");
            Session::put('msgType', "err");
            return back();
        }

    }

    /**
     * Block user
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */

    public function blockUser($id)
    {
        try {
            $usr = User::find($id);
            $usr->status = 2;
            $usr->save();

            $adm = Auth::guard('admin')->user();
            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Blocked User Account. User_id: " . $id;
            $act->save();

            Session::put('status', "Success Blocking User");
            Session::put('msgType', "suc");
            return back();

        } catch (\Exception $e) {
            Session::put('status', "Error updating record! Try again");
            Session::put('msgType', "err");
            return back();
        }

    }

    public function activateUser($id)
    {

        try {
            $usr = User::find($id);
            $usr->status = 1;
            $usr->save();

            $adm = Auth::guard('admin')->user();
            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Activate User account. User_id: " . $id;
            $act->save();

            Session::put('status', "Success Activating User");
            Session::put('msgType', "suc");
            return back();

        } catch (\Exception $e) {
            Session::put('status', "Error updating record! Try again");
            Session::put('msgType', "err");
            return back();
        }

    }

    public function activateKyc($id)
    {
        // try
        // {
        $usr = User::find($id);
        $usr->kyc_status = 1;
        $usr->save();

        $adm = Auth::guard('admin')->user();
        $act = new adminLog;
        $act->admin = $adm->email;
        $act->action = "Activate User KYC. User_id: " . $id;
        $act->save();

        Session::put('status', "Success Verifying KYC");

        //send email to user and admin
        $userNotifiData = [
            'subject' => 'KYC Verified Successfully',
            'message' => 'Your KYC has been verified successfully.!! You can now proceed to make deposits and withdrawals.',
            'action' => 'Login',
            'action_url' => route('v2.index'),
        ];
        $usr->notifyUser($userNotifiData);

        Session::put('msgType', "suc");
        return back();

        // } catch (\Exception $e) {
        //     Session::put('status', "Error updating record! Try again");
        //     Session::put('msgType', "err");
        //     return back();
        // }

    }
    public function deactivateKyc($id)
    {
        // try
        // {
        $usr = User::find($id);
        $usr->kyc_status = 0;
        $usr->save();

        $adm = Auth::guard('admin')->user();
        $act = new adminLog;
        $act->admin = $adm->email;
        $act->action = "Deactivated User KYC. User_id: " . $id;
        $act->save();

        //send email to user and admin
        $userNotifiData = [
            'subject' => 'KYC Verification Failed',
            'sub_message' => "Your KYC has been deactivated.",
            'message' => "Your KYC has been deactivated. Please re-upload your documents.",
            'action' => "Login",
            'action_url' => route('v2.index'),
        ];
        $usr->notifyUser($userNotifiData);
        Session::put('status', "Success Deactiving KYC");
        Session::put('msgType', "suc");
        return back();

        // } catch (\Exception $e) {
        //     Session::put('status', "Error updating record! Try again");
        //     Session::put('msgType', "err");
        //     return back();
        // }
    }

    public function deleteUser($id)
    {

        try {
            User::where('id', $id)->delete();

            $adm = Auth::guard('admin')->user();
            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Delete User account. User_id: " . $id;
            $act->save();

            Session::put('status', "Success Deleting User");
            Session::put('msgType', "suc");
            return redirect('/admin/manage/users'); //

        } catch (\Exception $e) {
            Session::put('status', "Error updating record! Try again");
            Session::put('msgType', "err");
            return back();
        }

    }

    public function manageInvestmentsPage()
    {
        return view('admin.manage_inv');
    }

    public function pauseInv($id)
    {
        try {
            $usr = investment::find($id);
            $usr->status = 'Paused';
            $usr->save();

            $adm = Auth::guard('admin')->user();
            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Paused User Investment. Investment id: " . $id;
            $act->save();

            Session::put('status', "Successful");
            Session::put('msgType', "suc");
            return back(); //

        } catch (\Exception $e) {
            Session::put('status', "Error updating record! Try again");
            Session::put('msgType', "err");
            return back();
        }

    }
    public function activateInv($id)
    {
        try {
            $usr = investment::find($id);
            $usr->status = 'Active';
            $usr->save();

            $adm = Auth::guard('admin')->user();
            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Activated User Investment. Investment id: " . $id;
            $act->save();

            Session::put('status', "Successful");
            Session::put('msgType', "suc");
            return back(); //

        } catch (\Exception $e) {
            Session::put('status', "Error updating record! Try again");
            Session::put('msgType', "err");
            return back();
        }

    }

    public function deleteInv($id)
    {

        try {
            $inv = investment::find($id);

            $inv_user = User::find($inv->user_id);
            $amt = $inv->capital;

            if ($inv->w_amt == 0) {
                $inv_user->wallet += $amt;
                $inv_user->save();
            }

            investment::where('id', $id)->delete();

            $adm = Auth::guard('admin')->user();
            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Deleted User Investment. Investment id: " . $id;
            $act->save();

            Session::put('status', "Successful");
            Session::put('msgType', "suc");
            return back();

        } catch (\Exception $e) {
            Session::put('status', "Error updating record! Try again");
            Session::put('msgType', "err");
            return back();
        }

    }
    public function editInv($id)
    {

        return view('admin.edit_user_inv')->with(compact('id'));

    }

    public function updateInv(Request $request, $id)
    {
        $last_wd = Carbon::parse($request->last_wd);
        $end_date = Carbon::parse($request->end_date);
        $created_at = Carbon::parse($request->created_at);

        $inv = investment::find($id);
        $inv->date_invested = $created_at->format('Y-m-d');
        $inv->created_at = $created_at->format('Y-m-d');
        $capital = $request->amount;

        $pack = packages::find($inv->package_id);

        $inv->i_return = ($capital * $pack->daily_interest * $pack->period);
        $inv->interest = $request->interest;
        $inv->end_date = $last_wd->format("Y-m-d");
        $inv->last_wd = $end_date->format('Y-m-d');

        $inv->save();
        Session::put('status', "Success");
        Session::put('msgType', "suc");
        return back();

    }
    public function searchInv(Request $req)
    {

        Session::put('val', $req->input('search_val'));
        return back();

    }
    public function manageAdminUsers()
    {
        return view('admin.manage_adm');
    }

    public function admAddnew(Request $req)
    {
        try {
            $adm = Auth::guard('admin')->user();
            // $usr = admin::find($id);
            if ($adm->role < $req->input('role')) {
                Session::put('status', "You cannot perform this operation! Try again");
                Session::put('msgType', "err");
                return back();
            }

            if ($adm->role == 'support' && $adm->role == 'manager') {
                Session::put('status', "You cannot perform this operation! Try again");
                Session::put('msgType', "err");
                return back();
            }

            $pack = new admin;
            $pack->name = $req->input('Name');
            $pack->email = $req->input('email');
            $pack->password = Hash::make($req->input('pwd'));
            $pack->role = $req->input('role');
            $pack->author = $adm->id;
            $pack->status = 1;
            $pack->save();

            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Created admin user. username: " . $req->input('email');
            $act->save();

            Session::put('status', "Successful");
            Session::put('msgType', "suc");
            return back();
        } catch (\Exception $e) {
            Session::put('status', "Error saving record! Try again");
            Session::put('msgType', "err");
            return back();
        }

    }

    public function manageDepositsPage()
    {
        return view('admin.user_deposit');
    }

    public function rejectDep($id)
    {

        try {
            $usr = deposits::find($id);

            $dep_user = User::find($usr->user_id);
            $amt = $usr->amount;

            if ($usr->on_apr == 1) {
                $dep_user->wallet -= $amt;
                $dep_user->save();
            }

            $usr->on_apr = 0;
            $usr->status = 2;
            $usr->save();

            $adm = Auth::guard('admin')->user();
            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Rejected user deposit. Deposit id: " . $id;
            $act->save();

            Session::put('status', "Successful");
            Session::put('msgType', "suc");
            return back(); //

        } catch (\Exception $e) {
            Session::put('status', "Error updating record! Try again");
            Session::put('msgType', "err");
            return back();
        }

    }

    public function approveDep($id)
    {

        try {
            $usr = deposits::find($id);
            if ($usr->status == 1) {
                return back()->with([
                    'toast_msg' => 'Deposit already approved!',
                    'toast_type' => 'err',
                ]);
            }

            $dep_user = User::find($usr->user_id);
            $amt = $usr->amount;

            if ($usr->on_apr == 0) {
                $dep_user->wallet += $amt;
                $dep_user->save();
            }
            $usr->status = 1;
            $usr->on_apr = 1;
            $usr->save();

            $adm = Auth::guard('admin')->user();
            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Approved user deposit. Deposit id: " . $id;
            $act->save();

            return back()->with([
                'toast_msg' => 'Deposit approved successfully!',
                'toast_type' => 'suc',
            ]);

        } catch (\Exception $e) {
            return back()->with([
                'toast_msg' => "Error updating record! Try again",
                'toast_type' => 'err',
            ]);
            return back();
        }

    }

    public function deleteDep($id)
    {
        try {

            $usr = deposits::find($id);

            $dep_user = User::find($usr->user_id);
            $amt = $usr->amount;

            if ($usr->on_apr == 1) {
                $dep_user->wallet -= $amt;
                $dep_user->save();
            }

            deposits::where('id', $id)->delete();

            $adm = Auth::guard('admin')->user();
            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Deleted " . $dep_user->username . " deposit. Amount: " . $amt;
            $act->save();

            Session::put('status', "Successful");
            Session::put('msgType', "suc");
            return back();

        } catch (\Exception $e) {
            Session::put('status', "Error deleting record! Try again");
            Session::put('msgType', "err");
            return back();
        }

    }
    public function searchDep(Request $req)
    {

        Session::put('val', $req->input('search_val'));
        return back();

    }
    public function addDeposit()
    {
        $users = User::all();
        return view('admin.add_deposit')->with(compact(['users']));

    }

    public function storeDeposit(Request $request)
    {
        $user = User::find($request->user_id);

        if ($user->status == 'Blocked' || $user->status == 2) {
            Session::put('msgType', "err");
            Session::put('status', 'Account Blocked! Please contact support.');
            return back();
        }

        if ($user->status == 'pending' || $user->status == 0) {
            Session::put('msgType', "err");
            Session::put('status', 'Account not activated! Please contact support.');
            return back();
        }

        if ($request->input('amount') < $this->settings->min_deposit) {
            return back()->With(['toast_msg' => 'Amount must be greater or equal to ' . $this->st->min_deposit . ' ' . $this->st->currency, 'toast_type' => 'err']);
        }

        $cost = (FLOAT) $request->input('amount');
        $currency_base = 'USD';
        $currency_received = $request->paymentMethod;

        $settings = site_settings::find(1);
        $extra_details = "{{$settings->site_title}}";
        if ($request->paymentMethod == 'ETH') {
            $qr_code = $settings->eth_wallet_qr_code;
            $address = $settings->eth_wallet_id;
        } else if ($request->paymentMethod == "BTC") {
            $qr_code = $settings->btc_qr_code;
            $address = $settings->btc_wallet_id;
        }

        $transaction = json_decode(json_encode([
            'txn_id' => $this->generateRandomTrxRf(5),
            'qr_code' => $qr_code,
            'status_url' => 'url',
            'address' => $address,
            'amount' => $cost * $settings->currency_conversion,
        ]), false);

        if ($transaction) {
            $paymt = new deposits;
            $paymt->user_id = $user->id;
            $paymt->usn = $user->username;
            $paymt->amount = $cost * $settings->currency_conversion;
            $paymt->currency = $settings->currency;
            $paymt->account_name = $transaction->txn_id;
            $paymt->account_no = $transaction->address;
            $paymt->bank = $request->paymentMethod;
            $paymt->url = $transaction->status_url;
            $paymt->created_at = $request->created_at;
            $paymt->status = 0;
            $paymt->on_apr = 0;
            $paymt->pop = "";

            $paymt->save();

            Session::put('status', 'Successful');
            Session::put('msgType', "suc");
            return back();

        }

    }
    public function manageWithdrawalsPage()
    {
        return view('admin.user_withdrawals');
    }

    public function rejectWD($id)
    {

        try {
            $usr = withdrawal::find($id);
            $usr->status = 'Rejected';
            $usr->save();

            $adm = Auth::guard('admin')->user();
            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Rejected user withdrawal. withdrawal id: " . $id;
            $act->save();

            Session::put('status', "Successful");
            Session::put('msgType', "suc");
            return back(); //

        } catch (\Exception $e) {
            Session::put('status', "Error updating password! Try again");
            Session::put('msgType', "err");
            return back();
        }

    }

    public function approveWD($id)
    {

        try {
            $usr = withdrawal::find($id);
            $userID = $usr->user_id;
            $wd_id = $usr->id;
            $wd_act = $usr->account;
            $wd_amt = $usr->amount;
            $wd_currency = $usr->currency;
            $usr->status = 'Approved';
            $usr->save();

            $adm = Auth::guard('admin')->user();
            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Approved user withdrawal. withdrawal id: " . $id;
            $act->save();

            $user_act = User::find($userID);

            $maildata = ['email' => $user_act->email, 'wd_id' => $wd_id, 'act' => $wd_act, 'amt' => $wd_amt, 'currency' => $wd_currency];
            Mail::send('mail.admin_approve_wd', ['md' => $maildata], function ($msg) use ($maildata) {
                $msg->from(config('app.support_email'), env('MAIL_FROM_NAME'));
                $msg->to($maildata['email']);
                $msg->subject('Withdrawal Approval');
            });
            return back()->with([
                'toast_msg' => 'Approval successfully!',
                'toast_type' => 'suc',
            ]);

        } catch (\Exception $e) {
            return back()->with([
                'toast_msg' => 'Error updating record! Try again!',
                'toast_type' => 'err',
            ]);
        }

    }

    public function deleteWD($id)
    {

        try {
            withdrawal::where('id', $id)->delete();

            $adm = Auth::guard('admin')->user();
            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Deleted user withdrawal. withdrawal id: " . $id;
            $act->save();

            return back()->with([
                'toast_msg' => 'Successful',
                'toast_type' => 'suc',
            ]);

        } catch (\Exception $e) {
            return back()->with([
                'toast_msg' => 'Error Deleting record! Try again',
                'toast_type' => 'err',
            ]);
        }

    }

    public function searchWD(Request $req)
    {

        Session::put('val', $req->input('search_val'));
        return back();

    }

    public function managePackages()
    {
        return view('admin.inv_packages');
    }

    public function create_package()
    {
        return view('admin.add_package');

    }

    public function create_package_post(Request $req)
    {
        $val = Validator::make($req->all(), [
            'package_name' => 'required|string|max:100',
            'min' => 'required|numeric',
            'max' => 'required|numeric',
            'interest' => 'required|numeric',
            'period' => 'required|numeric',
            'interval' => 'required|numeric',

        ]);

        if ($val->fails()) {
            $toast_msg = ['msg' => $val->errors()->first(), 'type' => 'err'];
            return json_encode($toast_msg);
        }
        if ((INT) $req->input('period') % (INT) $req->input('interval') != 0) {
            $toast_msg = ['msg' => "Period must be completely divisible by interval", 'type' => 'err'];
            return json_encode($toast_msg);
        }
        try {
            $interest_calc = ($req->input('interest') / 100) / $req->input('period');
            $pack = new packages;
            $pack->package_name = $req->input('package_name');
            $pack->currency = $this->settings->currency;
            $pack->min = $req->input('min');
            $pack->max = $req->input('max');
            $pack->daily_interest = round($interest_calc, 4);
            $pack->withdrwal_fee = env('WD_FEE') ?? 500;
            $pack->period = $req->input('period');
            $pack->days_interval = $req->input('interval');
            $pack->color = $req->input('color');
            $pack->ref_bonus = 0;
            $pack->status = 1;
            $pack->description = $req->input('description');
            $pack->save();
        } catch (\Exception $e) {
            $toast_msg = ['msg' => $e->getMessage(), 'type' => 'err'];
            return json_encode($toast_msg);
        }

        $toast_msg = ['msg' => 'Package added successfuly!', 'type' => 'suc'];
        return json_encode($toast_msg);

    }

    public function adminDeletePack($id)
    {
        try {
            packages::where('id', $id)->delete();
            return json_encode('["rst" => "suc"]');
        } catch (\Exception $ex) {
            return json_encode('["rst" => "err"]');
        }

    }

    public function editPack(Request $req)
    {
        try {
            $pack = packages::find($req->input('p_id'));
            $pack->setAttribute('min', $req->input('min'));
            $pack->currency = $this->settings->currency;
            $pack->setAttribute('max', $req->input('max'));
            $pack->color = $req->input('color');
            $pack->daily_interest = ($req->input('interest') / 100) / $pack->period;
            $pack->description = $req->input('description');
            $pack->save();

            $adm = Auth::guard('admin')->user();
            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Edited investment package. Package id: " . $req->input('p_id');
            $act->save();

            return back()->with([
                'toast_msg' => 'Successful!',
                'toast_type' => 'suc',
            ]);
        } catch (\Exception $e) {
            return back()->with([
                'toast_msg' => 'Error saving record! Try again!',
                'toast_type' => 'err',
            ]);
        }

    }

    public function sendMsg()
    {
        return view('admin.send_notification');
    }

    public function switch_pack($id)
    {
        $adm = Auth::guard('admin')->user();
        if ($adm->role == 'admin' || $adm->role == 'manager') {
            $pack = packages::find($id);
            if (!empty($pack)) {
                if ($pack->status == 0) {
                    $pack->status = 1;
                } else {
                    $pack->status = 0;
                }
                $pack->save();
                return 's';
            }
        } else {
            return ('User cannot update this function, Not an admin');
        }

    }

    public function admSendMsg(Request $req)
    {
        $adm = Auth::guard('admin')->user();
        $validator = Validator::make($req->all(), [
            'subject' => 'required|min:5|max:50|string',
            'msg' => 'required|string',
        ]);

        if ($validator->fails()) {
            return back()->With([
                'toast_msg' => 'Message not sent! Error ' . $validator->errors()->first(),
                'toast_type' => 'err',
            ]);
        }

        try {
            if (empty($req->input('msg_state'))) {
                $msg = new msg;
                $msg->message = $req->input('msg');
                $msg->subject = $req->input('subject');
                if (!empty($req->input('msg_users'))) {
                    $msg->users = $req->input('msg_users') . ';';
                }

                $msg->save();

                $act = new adminLog;
                $act->admin = $adm->email;
                $act->action = "Admin sent notification to users.";
                $act->save();
            } else {
                $msg = msg::find($req->input('msg_state'));
                $msg->message = $req->input('msg');
                $msg->subject = $req->input('subject');
                $msg->readers = '';
                if (!empty($req->input('msg_users'))) {
                    $msg->users = $req->input('msg_users');
                } else {
                    $msg->users = null;
                }
                $msg->save();

                $act = new adminLog;
                $act->admin = $adm->email;
                $act->action = "Admin updated users notification.";
                $act->save();
            }
            return back()->With([
                'toast_msg' => 'Successful',
                'toast_type' => 'suc',
            ]);
        } catch (\Exception $e) {
            return back()->With([
                'toast_msg' => 'Error saving message! Try again ' . $e->getMessage(),
                'toast_type' => 'err',
            ]);
        }

    }

    public function changePwd()
    {
        return view('admin.change_pwd');
    }

    public function admChangePwd(Request $req)
    {
        try {
            $adm = Auth::guard('admin')->user();

            if ($req->input('newpwd') != $req->input('cpwd')) {
                Session::put('status', "New password do not match! Try again");
                Session::put('msgType', "err");
                return back();
            }

            if (Hash::check($req->input('oldpwd'), $adm->password)) {

                $ad = admin::find($adm->id);
                $ad->password = Hash::make($req->input('newpwd'));
                $ad->save();

                $act = new adminLog;
                $act->admin = $adm->email;
                $act->action = "Admin changed password.";
                $act->save();

                Session::put('status', "Successful");
                Session::put('msgType', "suc");
                return back();
            } else {
                Session::put('status', "Old password do not match! Try again");
                Session::put('msgType', "err");
                return back();
            }

        } catch (\Exception $e) {
            Session::put('status', "Error saving message! Try again");
            Session::put('msgType', "err");
            return back();
        }

    }

    public function view_tickets()
    {

        $tickets = ticket::orderby('status', 'desc')->orderby('updated_at', 'desc')->paginate(30);
        return view('admin.ticket_view', ['tickets' => $tickets]);

    }

    public function delete_ticket($id)
    {

        ticket::with('comments')->where('id', $id)->delete(10);
        return back()->with([
            'toast_msg' => 'Ticket deleted successfully!',
            'toast_type' => 'suc',
        ]);

    }

    public function open_ticket($id)
    {

        $ticket_view = ticket::With('comments')->find($id);
        $comments = comments::where('ticket_id', $id)->orderby('id', 'asc')->get();
        $user = User::find($ticket_view->user_id);
        $ticket_view->state = 0;
        $ticket_view->save();
        comments::where('ticket_id', $id)->where('sender', 'user')->update(['state' => 0]);
        return view('admin.ticket_chat', ['ticket_view' => $ticket_view, 'user' => $user, 'comments' => $comments]);

    }
    public function ticket_chat($id)
    {

        $comments = comments::with('user')->where('ticket_id', $id)->where('sender', 'user')->where('state', 1)->orderby('id', 'asc')->get();
        comments::where('ticket_id', $id)->where('sender', 'user')->update(['state' => 0]);
        return json_encode($comments);

    }
    public function close_ticket($id)
    {

        try {
            ticket::where('id', $id)->update(['status' => 0]);
            return back()->with([
                'toast_msg' => 'Ticket closed successfully!',
                'toast_type' => 'suc',
            ]);
        } catch (\Exception $e) {
            return back()->with([
                'toast_msg' => 'Error occured!',
                'toast_type' => 'err',
            ]);
        }

    }
    public function ticket_comment(Request $req)
    {
        $close_check = ticket::find($req->input('ticket_id'));
        if (empty($close_check) || $close_check->status == 0) {
            return json_encode([
                'toast_msg' => 'Ticket closed',
                'toast_type' => 'err',
            ]);
        }
        $user = Auth::guard('admin')->user();
        $validator = Validator::make($req->all(), [
            'ticket_id' => 'required|string',
            'msg' => 'required|string',
        ]);

        if ($validator->fails()) {
            return json_encode([
                'toast_msg' => 'Message not sent! Error' . $validator->errors()->first(),
                'toast_type' => 'err',
            ]);
        }

        try {
            $comment = new comments([
                'ticket_id' => $req->input('ticket_id'),
                'sender' => 'support',
                'sender_id' => $user->id,
                'message' => $req->input('msg'),
            ]);
            $comment->save();

            return json_encode([
                'toast_msg' => 'Successful! ',
                'toast_type' => 'suc',
                'comment_msg' => $req->input('msg'),
                'comment_time' => date('Y-m-d H:i:s'),
            ]);
        } catch (\Exception $e) {
            return json_encode([
                'toast_msg' => 'Message not sent! Error' . $e->getMessage(),
                'toast_type' => 'err',
            ]);
        }

    }

    public function viewUserLogs()
    {

        return view('admin.user_log');
    }

    public function adminViewProfileSettings()
    {

        $data = $this->load_data();
        return view('admin.profile', [
            'settings' => $data[9],
            'adm' => $data[0],
            'logs' => $data[8],
            'users' => $data[3],
            'inv' => $data[1],
            'deposits' => $data[2],
        ]);

    }

    public function manageXpackInvestments()
    {
        return view('admin.xpack_inv');
    }

    public function admin_ban_user($id)
    {
        $adm = Session::get('adm');
        try {
            $usr = admin::find($id);
            if ($usr->id == $adm->id) {
                Session::put('status', "You cannot perform this action on yourself! Try again");
                Session::put('msgType', "err");
                return back();
            }

            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Blocked admin user. user id: " . $id;
            $act->save();

            Session::put('status', "Successful");
            Session::put('msgType', "suc");
            return back(); //

        } catch (\Exception $e) {
            Session::put('status', "Error updating record! Try again");
            Session::put('msgType', "err");
            return back();
        }

    }

    public function admin_activate_user($id)
    {

        $adm = Auth::guard('admin')->user();
        try {
            $usr = admin::find($id);
            if ($usr->id == $adm->id) {
                Session::put('status', "You cannot perform this action on yourself! Try again");
                Session::put('msgType', "err");
                // return $usr->id.' '.$adm->id;
                return back();
            }

            if ($usr->role >= $adm->role && $adm->id != 1) {
                Session::put('status', "Error updating record! Try again");
                Session::put('msgType', "err");
                return back();
            } else {
                $usr->status = '1';
                $usr->save();
            }

            $act = new adminLog;
            $act->admin = $adm->email;
            $act->action = "Activated admin user. user id: " . $id;
            $act->save();

            Session::put('status', "Successful");
            Session::put('msgType', "suc");
            return back(); //

        } catch (\Exception $e) {
            Session::put('status', "Error updating record! Try again");
            Session::put('msgType', "err");
            return back();
        }

    }

    public function dadmin_delete_user($id)
    {

        $adm = Auth::guard('admin')->user();
        try {
            $usr = admin::find($id);
            if ($usr->id == $adm->id) {
                Session::put('status', "You cannot delete yourself! Try again");
                Session::put('msgType', "err");
                return back();
            }

            if ($usr->role >= $adm->role && $adm->author != 0 && $usr->author != $adm->id) {
                Session::put('status', "Error updating record! Try again");
                Session::put('msgType', "err");
                return back();
            } else {
                admin::where('id', $id)->delete();

                $act = new adminLog;
                $act->admin = $adm->email;
                $act->action = "Deleted admin user. user id: " . $id;
                $act->save();

                Session::put('status', "Successful");
                Session::put('msgType', "suc");
                return back(); //
            }

        } catch (\Exception $e) {
            Session::put('status', "Error updating record! Try again");
            Session::put('msgType', "err");
            return back();
        }

    }

    public function searchadminUser(Request $req)
    {
        Session::put('val', $req->input('search_val'));
        return back();

    }

    public function admSearch(Request $req)
    {
        Session::put('val', $req->input('search_val'));
        return back();
    }

    public function editMsg($id)
    {
        $msg = msg::find($id);
        return json_encode($msg);

    }

    public function editMsgDel($id)
    {

        msg::where('id', $id)->delete();
        return json_encode('["rst" => "Successful"]');

    }

    public function getMonthlyIvCart()
    {
        $cap = 0;

        $sm = array();
        for ($i = 1; $i <= 12; $i++) {
            $cap = 0;
            if (strlen($i) == 1) {
                $nm = '0' . $i;
            } else {
                $nm = $i;
            }
            $mIvs = investment::where('date_invested', 'like', '%' . date('Y') . '-' . $nm . '%')->get();
            if (count($mIvs) > 0) {
                foreach ($mIvs as $m) {
                    $cap = $cap + intval($m->capital);
                }
            } else {
                $cap = 0;
            }

            array_push($sm, intval($cap));
        }

        return json_encode($sm);

    }

}
