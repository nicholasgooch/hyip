<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LandrickController extends Controller
{
    public function index(){
        return view('landrick.index');
    }

    public function aboutPage(){
        return view('landrick.about');
    }

    public function cloudMinningPage()
    {
        return view('landrick.minning');
    }

    public function faqPage()
    {
        return view('landrick.faq');
    }

    public function contact()
    {
        return view('landrick.contact');
    }
    
    public function buyBitcoinPage()
    {
        return view('landrick.buy-bitcoin');
    }
    public function howItWorks()
    {
        return view('landrick.how-it-works');
    }
    public function privacyPolicy()
    {
        return view('landrick.privacy-policy');
    }
    public function termsCondition()
    {
        return view('landrick.terms&conditions');
    }
    public function privateEquity()
    {
        return view('landrick.private-equity');
    }
    public function InvestmentPlans()
    {
        return view('landrick.investment-plans');
    }

    public function oilgasPage(){
        return view('landrick.plans.oil&gas');
    }
    public function goldInv(){
        return view('landrick.plans.gold-investment');
    }
    public function realEstate(){
        return view('landrick.plans.real-estate');
    }
    public function agriInfra(){
        return view('landrick.plans.agri-infra');
    }
    public function forex(){
        return view('landrick.plans.forex-investments');
    }
    public function crypto(){
        return view('landrick.plans.crypto-investments');
    }
    public function dictionary(){
        return view('landrick.dictionary');
    }
    public function security(){
        return view('landrick.security');
    }
    public function business(){
        return view('landrick.business');
    }
    public function affiliates(){
        return view('landrick.affiliates');
    }
}
