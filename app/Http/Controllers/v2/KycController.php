<?php

namespace App\Http\Controllers\v2;

use App\activities;
use App\Http\Controllers\Controller;
use App\kyc;
use App\site_settings;
use App\User;
use Auth;
use Exception;
use Illuminate\Http\Request;
use Mail;
use Session;

class KycController extends Controller
{
    private $settings;
    private $supportEmail;
    public function __construct()
    {
        $this->settings = site_settings::first();
        $this->supportEmail = config('app.support_email');
        $this->middleware(['auth', 'kyc_not_verified', 'authenicator2fa', 'twofactor', 'verified']);
    }
    public function kyc_page()
    {
        $user = User::find(Auth::guard('web')->id());
        if ($user->kyc !== null && $user->kyc_status == false) {
            return redirect(route('v2.kyc.proccess'));
        }

        return view('v2.kyc.intro');
    }

    public function kyc_proccessing_page()
    {

        return view('v2.kyc.process');
    }
    public function Kyc_form()
    {

        return view('v2.kyc.form');
    }
    public function Kyc_form_store(Request $request)
    {

        $this->validate(
            $request,
            [
                'dob' => 'required',
                'ssn' => 'required',
                'address_1' => 'required',
                'state' => 'required',
                'zip' => 'required',
                'id_type' => 'required',
                'tc-agree' => 'required',
                'info-assure' => 'required',
            ],
            [
                'id_type.required' => 'Pick a document type',
                'address_1.required' => 'Address field is required',
                'tc-agree.required' => 'Terms and Condtion is required',
                'info-assure.required' => '',
            ]
        );
        $address = strval($request->address_1) . ' ' . strval($request->address_2);
        try {
            $user = Auth::guard('web')->user();

            kyc::firstOrCreate(
                ['user_id' => $user->id],
                [
                    'user_id' => $user->id,
                    'font_pics' => $request->front_pics,
                    'back_pics' => $request->back_pics,
                    'id_type' => $request->id_type,
                    'ssn' => $request->ssn,
                    'address_1' => $request->address_1,
                    'address_2' => $request->address_2,
                    'city' => $request->city,
                    'state_id' => $request->state,
                    'country_id' => $request->country,
                    'zip' => $request->zip,
                    'dob' => $request->dob,
                ]
            );

            $user = User::find($user->id);
            $user->ssn = $request->ssn;
            $user->dob = $request->dob;
            $user->id_type = $request->id_type;
            $user->address = $address;
            $user->state = $request->state;
            $fullname = $user->Fname . ' ' . $user->Lname;
            $user->country = $request->country;
            $user->phone = $request->phone;
            $user->save();

            $act = new activities;
            $act->action = $user->id_type != null ? "KYC Documents Uploaded" : "User Updated profile";
            $act->user_id = $user->id;
            $act->save();

            Session::put('status', 'Operation Successful! KYC Proccessing');
            Session::put('msgType', "suc");
            $maildata = ['email' => $user->email, 'username' => $user->username];
            Mail::send('mail.kyc_submitted', ['md' => $maildata], function ($msg) use ($maildata) {
                $msg->to($maildata['email']);
                $msg->subject('KYC Successfully Uploaded');
            });

            $admin_maildata = ['email' => $user->email, 'usr' => $user->username, 'fullname' => $fullname, 'id' => $user->id];

            Mail::send('mail.admin.new_kyc', ['md' => $admin_maildata], function ($msg) use ($admin_maildata) {
                $msg->to($this->supportEmail);
                $msg->subject("KYC Form Uploaded: {$admin_maildata['fullname']}");
            });

            return redirect(route('v2.kyc.proccess'));
        } catch (Exception $e) {
            Session::put('status', "Error saving your data. Please try again!");
            Session::put('msgType', "err");
            return back();
        }
    }

}
