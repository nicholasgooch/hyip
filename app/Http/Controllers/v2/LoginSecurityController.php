<?php

namespace App\Http\Controllers\v2;

use App\activities;
use App\Http\Controllers\Controller;
use App\LoginSecurity;
use App\site_settings;
use Auth;
use Exception;
use Hash;
use Illuminate\Http\Request;
use Session;

class LoginSecurityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $st;
    public function __construct()
    {
        $this->st = site_settings::find(1);
        $this->middleware('auth');
    }
    public function show2faDisableForm()
    {
        $user = Auth::user();
        $data = array(
            'user' => $user,

        );
        return view('v2.2fa_settings')->with('data', $data);
    }

    /**
     * Show 2FA Setting form
     */
    public function show2faForm(Request $request)
    {
        try {
            $user = Auth::user();
            $google2fa_url = "";
            $secret_key = "";

            if ($user->loginSecurity()->exists()) {
                $google2fa = (new \PragmaRX\Google2FAQRCode\Google2FA());
                $google2fa_url = $google2fa->getQRCodeInline(
                    $this->st->site_title,
                    $user->email,
                    $user->loginSecurity->google2fa_secret
                );
                $secret_key = $user->loginSecurity->google2fa_secret;
            }

            $data = array(
                'user' => $user,
                'secret' => $secret_key,
                'google2fa_url' => $google2fa_url,
            );

            return view('v2.2fa_settings')->with('data', $data);
        } catch (Exception $e) {
            Session::put('status', 'An Error Occured');
            Session::put('msgType', "err");
        }
    }

    /**
     * Generate 2FA secret key
     */
    public function generate2faSecret(Request $request)
    {
        try {
            $user = Auth::user();
            // Initialise the 2FA class
            $google2fa = (new \PragmaRX\Google2FAQRCode\Google2FA());

            // Add the secret key to the registration data
            $login_security = LoginSecurity::firstOrNew(array('user_id' => $user->id));
            $login_security->user_id = $user->id;
            $login_security->google2fa_enable = 0;
            $login_security->google2fa_secret = $google2fa->generateSecretKey();
            $login_security->save();

            $act = new activities;
            $act->action = $user->full_name . "Two Factor Auth: 2FA Secret key is generated.";
            $act->user_id = $user->id;
            $act->save();

            Session::put('status', 'Operation Successful! 2FA Secret key is generated.');
            Session::put('msgType', "suc");

            return redirect()->route('v2.2fa.page')->with('success', "Secret key is generated.");

        } catch (Exception $e) {
            Session::put('status', 'An Error Occured');
            Session::put('msgType', "err");
        }
    }

    /**
     * Enable 2FA
     */
    public function enable2fa(Request $request)
    {
        try {

            $user = Auth::user();
            $google2fa = (new \PragmaRX\Google2FAQRCode\Google2FA());

            $secret = $request->input('secret');
            $valid = $google2fa->verifyKey($user->loginSecurity->google2fa_secret, $secret);

            if ($valid) {
                $user->loginSecurity->google2fa_enable = 1;
                $user->loginSecurity->save();

                $act = new activities;
                $act->action = $user->full_name . "Two Factor Auth: 2FA is enabled successfully.";
                $act->user_id = $user->id;
                $act->save();

                Session::put('status', 'Operation Successful! 2FA is enabled successfully.');
                Session::put('msgType', "suc");

                return redirect()->route('v2.2fa.page')->with('success', "2FA is enabled successfully.");
            } else {
                Session::put('status', 'Invalid verification Code, Please try again.');
                Session::put('msgType', "err");

                return redirect()->route('v2.2fa.page')->with('error', "Invalid verification Code, Please try again.");
            }
        } catch (Exception $e) {
            Session::put('status', 'Invalid verification Code, Please try again.');
            Session::put('msgType', "err");
        }
    }

    /**
     * Disable 2FA
     */
    public function disable2fa(Request $request)
    {
        try {
            if (!(Hash::check($request->get('current-password'), Auth::user()->pwd))) {
                // The passwords matches
                Session::put('status', 'The password does not match with your account password. Please try again.');
                Session::put('msgType', "err");

                return redirect()->back()->with("error", "The password does not match with your account password. Please try again.");
            }

            $validatedData = $request->validate([
                'current-password' => 'required',
            ]);
            $user = Auth::user();
            $user->loginSecurity->google2fa_enable = 0;
            $user->loginSecurity->save();

            $act = new activities;
            $act->action = $user->full_name . "Two Factor Auth: 2FA was disabled.";
            $act->user_id = $user->id;
            $act->save();

            Session::put('status', '2FA is now disabled.');
            Session::put('msgType', "suc");

            return redirect()->route('v2.2fa.page')->with('success', "2FA is now disabled.");
        } catch (Exception $e) {
            Session::put('status', 'Invalid verification Code, Please try again.');
            Session::put('msgType', "err");
        }
    }
}
