<?php

namespace App\Http\Controllers\v2;

use App\activities;
use App\admin;
use App\banks;
use App\comments;
use App\deposits;
use App\fund_transfer;
use App\Http\Controllers\Controller;
use App\investment;
use App\Mail\Admin\DepositNotification as AdminDepositNotification;
use App\Mail\UserDepositNotification;
use App\msg;
use App\packages;
use App\site_settings;
use App\ticket;
use App\User;
use App\withdrawal;
use Auth;
use Exception;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Mail;
use Session;
use Validator;

class UserController extends Controller
{

    protected $user;
    protected $st;

    public function __construct()
    {

        $this->user = Auth::User();
        $this->st = site_settings::find(1);
        $this->middleware(['auth', 'kyc_verified', 'authenicator2fa', 'verified', 'twofactor']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalEarning = 0;
        $currentEarning = 0;
        $workingDays = 0;
        $runInv = new Collection;

        $user = Auth::User();
        $actInv = investment::where('user_id', $user->id)->where('status', 'Active')->orderby('id', 'desc')->get();

        foreach ($actInv as $inv) {
            $totalElapse = getDays(date('y-m-d'), $inv->end_date);
            if ($totalElapse == 0) {
                $lastWD = $inv->last_wd;
                $enddate = ($inv->end_date);

                $getDays = getDays($lastWD, $enddate);
                $currentEarning += $getDays * $inv->interest * $inv->capital;

            } else {
                $runInv->push($inv);
                $sd = $inv->last_wd;
                $ed = date('Y-m-d');

                $getDays = getDays($sd, $ed);

                foreach ($runInv as $run) {
                    $qd = $run->last_wd;
                    $ed = date('Y-m-d');

                    $getDays = getDays($qd, $sd);

                    //profits for running inves
                    $currentEarning += $run->i_return;
                }

            }
        }

        return view('v2.index')->with(compact(['totalEarning', 'currentEarning', 'workingDays', 'runInv']));
    }

    /**
     * Add wallet
     *
     * @param Request $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addWallet(Request $req)
    {
        $user = Auth::User();

        $validate = $req->validate([
            'asset_type' => 'required',
            'coin_host' => 'required',
            'coin_wallet' => 'required|unique:bank,account_number',
        ]);
        $bank = new banks;
        $bank->user_id = $user->id;
        $bank->Account_name = $req->input('asset_type') == 'btc' ? 'BTC' : 'ETH';
        $bank->Account_number = $req->input('coin_wallet') ?? '';
        $bank->Bank_Name = $req->input('coin_host') ?? '';
        $bank->Routing_number = $req->input('routing_number') ?? '';
        $bank->type = $req->input('asset_type') ?? '';
        $bank->save();

        $act = new activities;
        $act->action = "User added" . $bank->Account_number . " wallet";
        $act->user_id = $user->id;
        $act->save();

        Session::put('status', 'Operation Successful! Wallet saved successfully');
        Session::put('msgType', "suc");

        return back();

    }

    /**
     * Add bank
     *
     * @param Request $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addBank(Request $req)
    {
        $user = Auth::User();

        $req->validate([
            'account_name' => 'required',
            'bank_name' => 'required',
            'account_number' => 'required|unique:bank,account_number',
        ]);
        $bank = new banks;
        $bank->user_id = $user->id;
        $bank->Account_name = $req->input('account_name') ?? '';
        $bank->Account_number = $req->input('account_number') ?? '';
        $bank->Bank_Name = $req->input('bank_name') ?? '';
        $bank->Routing_number = $req->input('routing_number') ?? '';
        $bank->type = 'bank';
        $bank->save();

        $act = new activities;
        $act->action = "User added" . $bank->Account_number . " wallet";
        $act->user_id = $user->id;
        $act->save();

        Session::put('status', 'Operation Successful! Wallet saved successfully');
        Session::put('msgType', "suc");

        return back();

    }

    /**
     * Affiliates
     *
 
     */
    public function affiliates()
    {
        return view('v2.affiliates');
    }

    /**
     * Tickets
     *

     */
    public function ticket()
    {
        $user = Auth::user();
        $tickets = ticket::where('user_id', $user->id)
            ->orderby('status', 'desc')->orderby('updated_at', 'desc')->paginate(10);
        return view('v2.tickets', ['tickets' => $tickets]);

    }

    /**
     * Open a ticket
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ticket_open($id)
    {
        $ticket_view = ticket::With('comments')->find($id);
        $comments = comments::where('ticket_id', $id)->orderby('id', 'asc')->get();
        comments::where('ticket_id', $id)->where('sender', 'support')->update(['state' => 0]);
        return view('v2.ticket_chat', ['ticket_view' => $ticket_view, 'comments' => $comments]);

    }

    /**
     * Load chat for a ticket
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function load_chat(Request $request)
    {
        $user = Auth::User();
        $comments = comments::where('ticket_id', $request->id)
            ->where('sender', 'support')->where('state', 1)
            ->orderby('id', 'asc')->get();
        comments::where('ticket_id', $request->id)->where('sender', 'support')->update(['state' => 0]);
        return response()->json($comments);

    }

    /**
     * Close a ticket
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function close_ticket(Request $request, $id)
    {
        try {
            ticket::where('id', $id)->update(['status' => 0]);

            Session::put('status', 'Operation Successful! Ticket closed successfully');
            Session::put('msgType', "suc");
            return back();
        } catch (Exception $e) {
            Session::put('status', 'Ticket not created! Error' . $e->getMessage());
            Session::put('msgType', "err");
            return back();
        }

    }

    /**
     * Create a new ticket
     * @param Request $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create_ticket(Request $req)
    {
        $user = Auth::User();
        $validator = Validator::make($req->all(), [
            'title' => 'string|max:499',
            'msg' => 'string',
        ]);

        if ($validator->fails()) {
            Session::put('status', 'Ticket not created! Error' . $validator->errors()->first());
            Session::put('msgType', "err");
            return back();
        }
        try {
            $ticket = new ticket([
                'ticket_id' => $user->id . strtotime(date('Y-m-d H:i:s')),
                'user_id' => $user->id,
                'title' => $req->input('title'),
                'msg' => $req->input('msg'),
                'status' => 1,
            ]);
            $ticket->save();

            // $tickets = ticket::find($user->id);
            Session::put('status', 'Ticket submitted successfully! Admin will attend to you shortly');
            Session::put('msgType', "suc");
            return back();
        } catch (Exception $e) {
            Session::put('status', 'Ticket not created! Error' . $e->getMessage());
            Session::put('msgType', "err");
            return back();

        }

    }

    /**
     * Comment on ticket
     * @param Request $req
     * @return \Illuminate\Http\JsonResponse
     */
    public function ticket_comment(Request $req)
    {
        $user = Auth::User();

        $close_check = ticket::find($req->input('ticket_id'));
        if (empty($close_check) || $close_check->status == 0) {
            return response()->json([
                'toast_msg' => 'Ticket closed',
                'toast_type' => 'err',
            ]);
        }

        $validator = Validator::make($req->all(), [
            'ticket_id' => 'required|string',
            'msg' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'toast_msg' => 'Message not sent! Error' . $validator->errors()->first(),
                'toast_type' => 'err',
            ]);
        }

        try {
            $comment = new comments([
                'ticket_id' => $req->input('ticket_id'),
                'sender' => 'user',
                'sender_id' => $user->id,
                'message' => $req->input('msg'),
            ]);
            $comment->save();

            return response()->json([
                'toast_msg' => 'Successful! ',
                'toast_type' => 'suc',
                'comment_msg' => $req->input('msg'),
                'comment_time' => date('Y-m-d H:i:s'),
                'user_img' => $user->img,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'toast_msg' => 'Message not sent! Error' . $e->getMessage(),
                'toast_type' => 'err',
            ]);
        }

    }

    /**
     * Profile page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile()
    {
        $user = Auth::user();
        return view('v2.profile')->with(compact(['user']));
    }

    /**
     * Notifications page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function notifications()
    {

        $msgs = msg::orderby('id', 'desc')->get();
        return view('v2.notifications', ['msgs' => $msgs]);

    }

    /**
     * Deposit page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function deposit_page()
    {

        if (isset($_GET['c'])) {
            $deposit_category = $_GET['c'];
        } else {
            $deposit_category = '';
        }
        $deposit_type = 'invest';
        return view('v2.deposit')->with(compact(['deposit_category', 'deposit_type']));
    }

    /**
     * Transactions page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function transactions()
    {
        $user = Auth::user();

        // Get deposits and add type
        $deposits = deposits::where('user_id', $user->id)
            ->select('*')
            ->selectRaw("'deposits' as type")
            ->get();

        // Get withdrawals and add type 
        $withdrawals = withdrawal::where('user_id', $user->id)
            ->select('*')
            ->selectRaw("'withdrawals' as type")
            ->get();

        // Calculate investment stats
        $totalInvested = investment::where('user_id', $user->id)
            ->sum('capital');

        $invs = investment::where('user_id', $user->id)->get();

        // Merge and paginate collections
        $transactions = collect()
            ->concat($deposits)
            ->concat($withdrawals)
            ->sortByDesc('created_at')
            ->values(); // Reset array keys

        $perPage = 10;
        $transactions = new \Illuminate\Pagination\LengthAwarePaginator(
            $transactions->forPage(\Request::get('page', 1), $perPage),
            $transactions->count(),
            $perPage,
            \Request::get('page', 1),
            ['path' => \Request::url()]
        );

        return view('v2.transactions', compact('transactions', 'totalInvested', 'invs'));
    }

    /**
     * Investment page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function invest_page()
    {
        $choosen_plan = '';
        if (isset($_GET['plan'])) {
            $plan_id = $_GET['plan'];
            $choosen_plan = packages::find($plan_id);
        }

        return view('v2.invest')->with(compact(['choosen_plan']));
    }

    /**
     * Withdrawal page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function withdraw_page()
    {
        return view('v2.withdrawal');
    }

    /**
     * Deposit funds via XHR
     * @param Request $req
     * @return \Illuminate\Http\JsonResponse
     */
    public function deposit_funds_xhr(Request $req)
    {

        $user = Auth::User();

        //check pending trx
        $last_trx = deposits::where('user_id', $user->id)->where('status', 0)->latest()->first();

        //to track emails
        if (!$last_trx) {
            return response()->json($this->save_deposit($req, $user));
        } else {
            //0 pending
            if ($last_trx->status === 0) {
                $last_trx->pending_message = true;
                $last_trx->address = $this->st->btc_wallet_address;

                $status = strtolower($last_trx->deposit_status);
                //send email to user
                Mail::to($user->email)->send(new UserDepositNotification($user, $last_trx, $status));

                return response()->json($last_trx);
            } else {

                return response()->json($this->save_deposit($req, $user));
            }
        }

    }
    /**
     * Save deposit
     * @param Request $req
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save_deposit(Request $req, $user)
    {
        //  dd($req->deposit_type);
        if ($req->input('amount') < $this->st->min_deposit) {
            throw new Exception('Amount must be greater or equal to ' . $this->st->min_deposit . ' ' . $this->st->currency);
        }
        try {

            $cost = (FLOAT) $req->input('amount');
            $currency_base = 'USD';

            $st = site_settings::first();

            switch ($req->method) {
                case 'ETH':
                    $transaction = json_decode(json_encode([
                        'txn_id' => generateRandomTrxRf(5),
                        'qr_code' => $st->eth_wallet_qr_code,
                        'account_name' => 'Ethereum',
                        'address' => $st->eth_wallet_id,
                        'amount' => $cost * $st->currency_conversion,
                        'coin_amount' => toETH($cost),
                    ]), false);
                    break;
                case 'BTC':
                    $transaction = json_decode(json_encode([
                        'txn_id' => generateRandomTrxRf(5),
                        'qr_code' => $st->btc_qr_code,
                        'account_name' => 'Bitcoin',
                        'address' => $st->btc_wallet_address,
                        'amount' => $cost * $st->currency_conversion,
                        'coin_amount' => toBTC($cost),
                    ]), false);
                    break;
                case 'BANK':
                    $transaction = json_decode(json_encode([
                        'txn_id' => generateRandomTrxRf(5),
                        'address' => $st->bank_name . ' ' . $st->account_number,
                        'account_name' => 'Bank Transfer',
                        'amount' => $cost * $st->currency_conversion,
                        'coin_amount' => 0.00,
                    ]), false);
                    break;
                default:
                    throw new Exception('Invalid deposit method');
            }
            if ($transaction) {
                $paymt = new deposits;
                $paymt->user_id = $user->id;
                $paymt->usn = $user->username;
                $paymt->amount = $cost * $st->currency_conversion;
                $paymt->currency = $st->currency;
                $paymt->account_name = $transaction->txn_id;
                $paymt->account_no = $transaction->address;
                $paymt->bank = $req->method;
                $paymt->deposit_category = $req->deposit_category ?? 'deposit';
                //write a tenary operator that checks if deposit type is empty, if yes change to 'invest'
                $paymt->deposit_type = $deposit_type = $req->deposit_type ?? 'invest';
                $paymt->ref = $transaction->txn_id;
                $paymt->url = $transaction->address ?? "";
                $paymt->status = 0;
                $paymt->on_apr = 0;
                $paymt->coin_amount = $transaction->coin_amount;
                $paymt->pop = "";

                $paymt->save();
                $paymt->type = 'deposit';

                $act = new activities;
                $act->action = $user->username . " deposited " . $user->currency == 'USD' ? '$' : $user->currency . intval($req->input('amount')) . " through " . strtoupper($req->method);
                $act->user_id = $user->id;
                $act->save();

                //to track emails
                $status = 'initiated';

                //send email to user
                Mail::to($user->email)->send(new UserDepositNotification($user, $paymt, $status));

                // Send notification email to admin...
                $admin = admin::first();

                //admin email routes
                $approveUrl = route('admin.deposit.approve', $paymt->id);
                $declineUrl = route('admin.deposit.decline', $paymt->id);

                //declare admin_email var, check if env is set, if not use db value
                $admin_email = env('MAIL_FROM_ADDRESS', $admin->email);
                Mail::to($admin_email)->send(new AdminDepositNotification($user, $paymt, $approveUrl, $declineUrl));

                return $transaction;
            }
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
        return response()->json(['error' => 'An error occurred'], 500);
    }

    /**
     * Withdraw funds from user's wallet
     * @param Request $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function user_wallet_wd(Request $req)
    {
        $user = Auth::User();

        if ($this->st->withdrawal != 1) {
            Session::put('msgType', "err");
            Session::put('status', 'Withdrawal disabled! Please contact support.');
            return back();
        }

        if ($user->status == 'Blocked' || $user->status == 2) {
            Session::put('msgType', "err");
            Session::put('status', 'Account Blocked! Please contact support.');
            return back();
        }

        if ($user->status == 'pending' || $user->status == 0) {
            Session::put('msgType', "err");
            Session::put('status', 'Account not activated! Please contact support.');
            return back();
        }

        if (intval($req->input('amt')) > intval($user->wallet) || intval($req->input('amt')) == 0) {
            Session::put('msgType', "err");
            Session::put('status', 'Invalid withdrawal amount. Amount must be greater than zero(0) and not more than wallet balance');
            return back();
        }

        if (intval($req->input('amt')) > $this->st->wd_limit) {
            Session::put('msgType', "err");
            Session::put('status', $this->st->currency . ' ' . $this->st->wd_limit . ' Withdrawal limit exceeded!');
            return back();
        }

        if (!empty($user)) {

            try {

                $usr = User::find($user->id);
                // $usr->wallet -= intval($req->input('amt'));
                $usr->save();

                $wd = new withdrawal;
                $wd->user_id = $user->id;
                $wd->usn = $user->username;

                //no use for package
                $wd->package = 'Wallet';

                $bank_id = $req->input('bank');

                $wd->bank_id = $bank_id;
                $bank = banks::find($bank_id);
                $wd->account = $bank->Account_name . ' ' . $bank->Account_number . ' ' . $bank->Bank_Name;

                //no use for invest_id
                $wd->invest_id = $user->id;
                $wd->amount = intval($req->input('amt'));
                $wd->w_date = date('Y-m-d');
                $wd->currency = $user->currency;
                $wd->charges = $charge = intval($req->input('amt')) * $this->st->wd_fee;
                $wd->recieving = intval($req->input('amt')) - $charge;
                $wd->save();

                $act = new activities;
                $act->action = "User requested withdrawal from wallet to bank ";
                $act->user_id = $user->id;
                $act->save();

                $maildata = ['email' => $user->email, 'username' => $user->username];
                $wd_fee = $this->st->wd_fee * 100;

                Session::put('status', 'Withdrawal Successful! Please Allow up to 5 to 10 Business Days for Payment Processing. Note: Withdrawal attracts processing ' . $wd_fee . '% fee');
                Session::put('msgType', "suc");

                Mail::send('mail.wd_notification', ['md' => $maildata], function ($msg) use ($maildata) {
                    $msg->from(config('app.support_email'), env('APP_NAME'));
                    $msg->to($maildata['email'], 'LedgerStocks Admin');
                    $msg->subject('User Withdrawal Notification');
                });

                return back();
            } catch (Exception $e) {
                Session::put('status', 'Error');
                Session::put('msgType', "err");
                return back();
            }

        } else {
            return redirect('/');
        }
    }

    /**
     * Settings page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function settings()
    {
        $user = Auth::user();
        $data = array(
            'user' => $user,

        );
        return view('v2.settings')->with(compact('data'));
    }

    /**
     * Activities page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function activities()
    {
        return view('v2.activities');
    }

    /**
     * Funds transfer page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function funds_transfer()
    {
        return view('v2.funds_transfer');
    }

    /**
     * Send funds to another user
     * @param Request $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function user_send_fund(Request $req)
    {
        $user = Auth::User();

        $validator = Validator::make($req->all(), [
            'usn' => 'required|string',
            's_amt' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            Session::put('err_send', $validator->errors()->first());
            Session::put('status', $validator->errors()->first());
            Session::put('msgType', "err");
            return back();
        }

        if ($user->username == $req->input('usn')) {
            Session::put('err_send', "You cannot send fund to yourself");
            Session::put('status', 'You cannot send fund to yourself');
            Session::put('msgType', "err");
            return back();
        }

        if ($user->wallet < 10) {
            Session::put('err_send', "Wallet balance is less than minimum!");
            Session::put('status', 'Wallet balance is less than minimum!');
            Session::put('msgType', "err");
            return back();
        }

        if ($user->wallet < intval($req->input('s_amt'))) {
            Session::put('err_send', "Wallet balance is lower than input amount!");
            Session::put('status', 'Wallet balance is lower than input amount!');
            Session::put('msgType', "err");
            return back();
        }

        try {
            $rec = User::where('username', trim($req->input('usn')))->get();
            if (count($rec) < 1) {
                Session::put('err_send', "Username record not found!");
                Session::put('status', 'User record not found!');
                Session::put('msgType', "err");
                return back();
            }

            $amt = intval($req->input('s_amt'));

            $rec[0]->wallet += $amt;
            $rec[0]->save();

            $user->wallet -= intval($req->input('s_amt'));
            $user->save();

            $rc = new fund_transfer;
            $rc->from_usr = $user->username;
            $rc->to_usr = trim($req->input('usn'));
            $rc->amt = intval($req->input('s_amt'));
            $rc->save();

            $act = new activities;
            $act->action = "User send fund of " . $user->currency . intval($req->input('s_amt')) . " to " . trim($req->input('usn'));
            $act->user_id = $user->id;
            $act->save();

            //send email to sender.
            $sender_email = $user->email;

            //send email to reciver
            $reciver_email = $rec[0]->email;
            //send email to admin

            Session::put('status', 'Your transaction was successful');
            Session::put('msgType', "suc");
            return back();
        } catch (Exception $e) {
            Session::put('err_send', "Error sending funds to another user!");
            Session::put('status', 'Error sending funds to another user!');
            Session::put('msgType', "err");
            return back();
        }

    }

    /**
     * Change password for user
     * @param Request $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePwd(Request $req)
    {
        $user = Auth::User();
        if ($req->input('newpwd') == $req->input('cpwd')) {
            if ($req->input('newpwd') == $req->input('oldpwd')) {
                Session::put('status', "You cannot use same Old password! Please use a different password.");
                Session::put('msgType', "err");
                return back();
            }
            try {
                $usr = User::find($user->id);
                if (Hash::check($req->input('oldpwd'), $user->pwd)) {
                    $usr->pwd = Hash::make($req->input('newpwd'));
                    $usr->save();

                    $act = new activities;
                    $act->action = "User changed password";
                    $act->user_id = $user->id;
                    $act->save();

                    Session::put('status', "Password Successfully Changed");
                    Session::put('msgType', "suc");
                    return back();
                } else {
                    Session::put('status', "Old Password invalid! Try again");
                    Session::put('msgType', "err");
                    return back();
                    // return back();
                }
            } catch (Exception $e) {
                Session::put('status', "Error saving password! Try again");
                Session::put('msgType', "err");
                return back();
            }
        } else {
            Session::put('status', "New Password does match");
            Session::put('msgType', "err");
            return back();
        }

    }

    public function addwithmethods()
    {
        return view('v2.with_method');
    }
}
