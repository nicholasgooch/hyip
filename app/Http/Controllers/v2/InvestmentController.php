<?php

namespace App\Http\Controllers\v2;

use App\activities;
use App\Http\Controllers\Controller;
use App\investment;
use App\Mail\UserInvestmentNotification;
use App\packages;
use App\ref;
use App\site_settings;
use App\User;
use Auth;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Mail;

class InvestmentController extends Controller
{
    private $st;

    public function __construct()
    {
        $this->st = site_settings::find(1);
        $this->middleware(['auth', 'authenicator2fa', 'twofactor', 'verified']);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalEarning = 0;
        $currentEarning = 0;
        $workingDays = 0;
        $runInv = new Collection;
        $endInv = new Collection;
        $total_daily_ern = 0;

        $user = Auth::User();
        $actInv = investment::where('user_id', $user->id)
            ->where('status', 'Active')->orderby('id', 'desc')->get();
        $total_run_profit = 0;
        foreach ($actInv as $inv) {
            $totalElapse = getDays(date('y-m-d'), $inv->end_date);
            if ($totalElapse == 0) {
                $endInv->push($inv);
                $lastWD = $inv->last_wd;
                $enddate = ($inv->end_date);
                $getDays = getDays($lastWD, $enddate);
                $currentEarning += $getDays * $inv->interest * $inv->capital;
                $total_run_profit += 0;
                $total_daily_ern += 0;
            } else {
                $runInv->push($inv);
                $sd = $inv->last_wd;
                $ed = date('Y-m-d');
                $getDays = getDays($sd, $ed);
                $currentEarning += $getDays * $inv->interest * $inv->capital;

                foreach ($runInv as $run) {
                    $qd = $run->last_wd;
                    $ed = date('Y-m-d');

                    $getDays = getDays($qd, $sd);

                    //profits for running inves
                    $total_run_profit += $getDays * $run->interest * $run->capital;

                    //daily earning for running inves
                    $total_daily_ern += round($run->i_return / $run->days_interval, 6);
                }

            }
        }
        return view('v2.my_investments')
            ->with(compact(['runInv', 'endInv',
                'totalEarning', 'total_daily_ern',
                'currentEarning', 'workingDays',
                'total_run_profit']));
    }

    public function invest(Request $req)
    {
        $user = Auth::User();

        if ($this->st->investment != 1) {
            throw new Exception("Investment disabled! You will be notified when it is available.", 1);
        }

        $pack = packages::find($req->input('p_id'));

        $capital = (int) $req->input('amount');

        if ($user->wallet < $capital) {
            throw new Exception("Your wallet balance is lower than capital.", 1);
        }

        if ($user->wallet < $pack->min) {
            throw new Exception("Wallet balance is lower than minimum investment.", 1);
        }

        if ($capital > $pack->max) {
            throw new Exception("Input capital is greater than maximum investment.", 1);
        }

        if ($capital < $pack->min) {
            throw new Exception("Input Capital is less than minimum investment.", 1);
        }
        try {

            $inv = new investment;
            $inv->capital = $capital;
            $inv->user_id = $user->id;
            $inv->usn = $user->username;
            $inv->date_invested = date("Y-m-d");
            $inv->period = $pack->period;
            $inv->days_interval = $pack->days_interval;
            $inv->i_return = ($capital * $pack->daily_interest * $pack->period);
            $inv->interest = $pack->daily_interest;
            // $no = 0;
            $dt = strtotime(date('Y-m-d'));
            $days = $pack->period;

            while ($days > 0) {
                $dt += 86400;
                $actualDate = date('Y-m-d', $dt);
                $days--;
            }

            $inv->package_id = $pack->id;
            $inv->currency = $this->st->currency;
            $inv->end_date = $actualDate;
            $inv->last_wd = date("Y-m-d");
            $inv->status = 'Active';

            $user->wallet -= $capital;
            $user->save();

            $inv->save();

            if (!empty($user->referal)) {
                $ref_bonus = $capital * $this->st->ref_bonus;

                if ($this->st->ref_type == 'Once') {
                    $ref_cnt = $this->st->ref_level_cnt;
                    $new_ref_user = $user->referal;
                    $itr_cnt = 1;
                    $refExist = ref::where('user_id', $user->id)->get();
                    if (count($refExist) < 1) {
                        $ref = new ref;
                        $ref->user_id = $user->id;
                        $ref->username = $user->referal;
                        // $ref->referral = 0;
                        $ref->wdr = 0;
                        $ref->currency = $this->st->currency;
                        $ref->amount = $ref_bonus;
                        $ref->save();

                        while ($itr_cnt <= $ref_cnt) {
                            $refUser = User::where('username', $new_ref_user)->get();
                            if (count($refUser) > 0) {
                                $refUser[0]->ref_bal += $ref_bonus / $itr_cnt;
                                $refUser[0]->save();
                                $new_ref_user = $refUser[0]->username;
                            }
                            $itr_cnt += 1;
                            if ($this->st->ref_system == 'Single_level') {
                                break;
                            }
                        }

                    }

                }
                if ($this->st->ref_type == 'Continous') {

                    $ref_cnt = $this->st->ref_level_cnt;
                    $new_ref_user = $user->referal;
                    $ref_bonus = $capital * $this->st->ref_bonus;
                    $itr_cnt = 1;
                    $refExist = ref::where('user_id', $user->id)->get();

                    if (count($refExist) < 1) {
                        $ref = new ref;
                        $ref->user_id = $user->id;
                        $ref->username = $user->referal;
                        // $ref->referral = 0;
                        $ref->wdr = 0;
                        $ref->currency = $this->st->currency;
                        $ref->amount = $ref_bonus;
                        $ref->save();
                    }

                    while ($itr_cnt <= $ref_cnt) {
                        $refUser = User::where('username', $new_ref_user)->get();

                        if (count($refUser) > 0) {
                            $refUser[0]->ref_bal += $ref_bonus / $itr_cnt;
                            $refUser[0]->save();
                            $new_ref_user = $refUser[0]->username;
                        }
                        $itr_cnt += 1;
                        if ($this->st->ref_system == 'Single_level') {
                            break;
                        }

                    }
                }
            }
            $act = new activities;
            $act->action = "User Invested " . $capital . " in " . $pack->package_name . " package";
            $act->user_id = $user->id;
            $act->save();

            // Assuming you have a $user instance after investment and a $investment variable for investment details
            Mail::to($user->email)->send(new UserInvestmentNotification($user, $inv));

            return response()->json($inv);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 1);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inv = investment::find($id);
        $totalEarning = 0;
        $currentEarning = 0;
        $workingDays = 0;
        $enddate = 0;
        $dailyErn = 0;
        $withdrawable = 0;
        $ern = 0;
        $Edays = 0;
        $ended = '';
        $totalElapse = getDays(date('Y-m-d'), $inv->end_date);
        if ($totalElapse == 0) {
            $lastWD = $inv->last_wd;
            $enddate = ($inv->end_date);
            $Edays = getDays($lastWD, $enddate);
            $ern = $Edays * $inv->interest * $inv->capital;
            $withdrawable = $ern;
            $totalDays = getDays($inv->date_invested, $inv->end_date);
            $date_invested = \Carbon\Carbon::createFromFormat('Y-m-d', $inv->date_invested);
            $enddate = \Carbon\Carbon::createFromFormat('Y-m-d', $inv->end_date);
            $ended = "Yes";

            $getDays = getDays($lastWD, $enddate);
            $currentEarning += $getDays * $inv->interest * $inv->capital;

            $dailyErn = round($inv->i_return / $inv->days_interval, 6);
            $totalEarning = $inv->period * $dailyErn;

        } else {
            $lastWD = $inv->last_wd;
            $enddate = (date('Y-m-d'));
            $Edays = getDays($lastWD, $enddate);
            $ern = $Edays * $inv->interest * $inv->capital;
            $withdrawable = 0;

            $getDays = getDays($lastWD, $enddate);
            $currentEarning += $getDays * $inv->interest * $inv->capital;

            if ($Edays >= $inv->days_interval) {
                $withdrawable = $inv->days_interval * $inv->interest * $inv->capital;
            }
            $totalDays = getDays($inv->date_invested, date('Y-m-d'));
            $date_invested = \Carbon\Carbon::createFromFormat('Y-m-d', $inv->date_invested);
            $enddate = \Carbon\Carbon::createFromFormat('Y-m-d', $inv->end_date);
            $ended = "No";
            $dailyErn = round($inv->i_return / $inv->days_interval, 6);
            $currentEarning = $dailyErn * $totalDays;
        }
        return view('v2.single_investment')
            ->with(compact(['inv', 'ern',
                'Edays', 'ended', 'date_invested',
                'currentEarning', 'enddate', 'dailyErn',
                'totalEarning', 'withdrawable',
                'totalElapse', 'totalDays']));
    }

    public function investmentPlans()
    {
        $invs = packages::orderBy('id', 'desc')->get();
        return view('v2.investment_plans')->with(compact('invs'));
    }
}
