<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;

class AdminApiController extends Controller
{
    function editUserBalance(Request $request, $id){
        $user = User::find($id);
        $user->wallet = $request->amount;

        $user->save();
        Session::put('status', "Successful");
        Session::put('msgType', "suc");
        return response()->json($user);

    }
}
