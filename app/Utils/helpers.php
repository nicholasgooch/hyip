<?php
use App\Utils\HD;
use Illuminate\Support\Facades\Http;

if (!function_exists('toBTC')) {
    function toBTC($amount)
    {
        // Use curl to perform the currency conversion using Blockchain.info's currency conversion API
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://blockchain.info/tobtc?currency=USD&value=" . $amount);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $conversion = curl_exec($ch);
        return $conversion;
    }
}
if (!function_exists('toETH')) {
    function toETH($amount)
    {
        $getrate = "https://min-api.cryptocompare.com/data/price?fsym=USD&tsyms=ETH";

        $price = file_get_contents($getrate);
        $result = json_decode($price, true);

        // ETH in USD
        $result = $result['ETH'];

        $quantity = $amount;
        $value = $quantity * $result;
        return $value;
    }
}
if (!function_exists('toUSD')) {
    function toUSD($btc)
    {
        $getrate = "https://api.alternative.me/v2/ticker/?convert=USD";

        $price = file_get_contents($getrate);
        $result = json_decode($price, true);

        // BTC in USD
        $result = $result['data'][1]['quotes']['USD']['price'];

        $quantity = $btc;
        $value = $quantity * $result;
        return $value;
    }
}

if (!function_exists('GetMkCap')) {
    function GetMkCap()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.nomics.com/v1/currencies/highlights?key=629d2fad9b46ad401c61eecd2a39ea4a4242b0f2&currency=BTC");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $conversion = curl_exec($ch);
        return $conversion;
    }
}

if (!function_exists('uploadFile')) {
    function uploadFile($file)
    {
        $image = cloudinary()->upload($file->getRealPath())->getSecurePath();
        return $image;
    }
}

if (!function_exists('getWorkingDays')) {

    function getWorkingDays($startDate, $endDate)
    {
        $begin = strtotime($startDate) + 86400;
        $end = strtotime($endDate);
        if ($begin > $end) {
            // echo "startdate is in the future! <br />";
            return 0;
        } else {
            $no_days = 0;
            $weekends = 0;
            while ($begin <= $end) {
                $no_days++; // no of days in the given interval
                $what_day = date("N", $begin);
                if ($what_day > 5) { // 6 and 7 are weekend days
                    $weekends++;
                    // echo $what_day;
                }
                ;
                $begin += 86400; // +1 day
            }
            ;
            $working_days = $no_days - $weekends;
            return $working_days;
        }
    }

}

if (!function_exists('getDays')) {
    function getDays($startDate, $endDate)
    {
        $begin = strtotime($startDate) + 86400;
        $end = strtotime($endDate);
        if ($begin > $end) {
            // echo "startdate is in the future! <br />";
            return 0;
        } else {
            $no_days = 0;
            $weekends = 0;
            while ($begin <= $end) {
                $no_days++; // no of days in the given interval
                $what_day = date("N", $begin);
                if ($what_day > 5) { // 6 and 7 are weekend days
                    $weekends++;
                    // echo $what_day;
                }
                ;
                $begin += 86400; // +1 day
            }
            ;
            // $working_days = $no_days - $weekends;
            return $no_days;
        }
    }
    if (!function_exists('generateRandomTrxRf')) {
        function generateRandomTrxRf($length = 8)
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }
    }
    if (!function_exists('getBtcTrxData')) {
        function getBtcTrxData($address)
        {

            $transaction_list = array();
            $satoshi = 100000000;
            $txnlist = file_get_contents("https://blockchain.info/rawaddr/" . $address);
            if ($txnlist) {
                $txnlist = json_decode($txnlist, true);
                if ($txnlist && isset($txnlist['txs']) && $txnlist['txs']) {
                    $txns = $txnlist['txs'];
                    foreach ($txns as $txn) {
                        $amount = $txn['result'] / $satoshi;
                        $time = $txn['time'];
                        $hash = $txn['hash'];
                        $transaction_list[] = array(
                            'amount' => $amount,
                            'hash' => $hash,
                            'time' => $time,
                        );
                    }
                }
                $data['address'] = $txnlist['address'];
                $data['total_txn'] = $txnlist['n_tx'];
                $data['total_received'] = $txnlist['total_received'] / $satoshi;
                $data['total_sent'] = $txnlist['total_sent'] / $satoshi;
                $data['final_balance'] = $txnlist['final_balance'] / $satoshi;
                $data['transaction_list'] = $transaction_list;
            }
            return $data;

        }
    }
    if (!function_exists('getBtcWalletAddress')) {
        function getBtcWalletAddress($xpub)
        {
            $file = fopen(storage_path('wallet_count.txt'), 'r');
            $i = fgets($file);
            $count = intval($i) + 1;
            $hd = new HD();

            $path = $count . '/0';

            file_put_contents(storage_path('wallet_count.txt'), $count);
            $hd->set_xpub($xpub);
            $address = $hd->address_from_master_pub($path);
            $result = [
                'address' => $address,
                'prefix' => $count,
            ];
            return $address;
        }

    }
    /*
     * Check if email is valid by checking the mx records
     */
    if (!function_exists('isEmailValidWithDebounce')) {

        function isEmailValidWithDebounce($email): bool
        {
            $apiKey = env('DEBOUNCE_API_KEY');
            $isValid = false;
            try {
                $response = Http::withHeaders([
                    'accept' => "application/json",
                ])->get('https://api.debounce.io/v1/', [
                    'api' => $apiKey,
                    'email' => $email,
                ]);

                if ($response->successful()) {
                    $responseObject = json_decode(json_encode($response->json()));

                    if ($responseObject->debounce->result === 'Safe to Send') {
                        $isValid = true;
                    }
                }
            } catch (\Exception $e) {
                // Handle exception if needed
                dd($e);
                Log::error($e->getMessage());
                return false;
            }
            return $isValid;
        }

    }
}
