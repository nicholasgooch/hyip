<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserInvestmentNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $investment;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$investment)
    {
        $this->user = $user;
        $this->investment = $investment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Investment Confirmation')
        ->view('mail.user.investment_notification')
        ->with([
            'user' => $this->user,
            'investment' => $this->investment,
        ]);
    }
}
