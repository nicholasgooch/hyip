<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Funding;

class FundingApplicationNotification extends Mailable
{
    use Queueable, SerializesModels;

    
    use Queueable, SerializesModels;

    /**
     * The funding application instance.
     *
     * @var Funding
     */
    public $fundingApplication;

    /**
     * Create a new message instance.
     *
     * @param Funding $fundingApplication
     */
    public function __construct(Funding $fundingApplication)
    {
        $this->fundingApplication = $fundingApplication;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New Funding Application')
        ->markdown('mail.admin.funding_application_notification')
        ->with(['fundingApplication'=>$this->fundingApplication]);
    }
}
