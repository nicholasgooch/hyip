<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\deposits;

class DepositNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $deposit;
    public $approveUrl;
    public $declineUrl;

    /**
     * Create a new message instance.
     *
     * @param  User  $user
     * @param  deposits  $deposit
     * @param  string  $approveUrl
     * @param  string  $declineUrl
     * @return void
     */
    public function __construct($user, $deposit, $approveUrl, $declineUrl)
    {
        $this->user = $user;
        $this->deposit = $deposit;
        $this->approveUrl = $approveUrl;
        $this->declineUrl = $declineUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.admin.deposit_notification')
            ->subject('New deposit notification')->with([
                    'approve_url' => $this->approveUrl,
                    'decline_url' => $this->declineUrl
                ]);
    }

}
