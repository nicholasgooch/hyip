<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FundingWithdrawalNotificationAdmin extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('app.support_email'))
            ->subject('New Loan Withdrawal Request')
            ->markdown('mail.admin.funding_withdrawal_notification_admin')
            ->with([
                'user' => $this->data['user'],
                'withdrawal' => $this->data['withdrawal'],
                'approve_url' => $this->data['approve_url'],
                'decline_url' => $this->data['decline_url'],
            ]);
    }
}
