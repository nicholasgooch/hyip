<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\FundingApplication;

class FundingApplicationNotification extends Mailable
{
    use Queueable, SerializesModels;

   
    public $data;

    
    public function __construct( $data)
    {
        $this->data = $data;
    }

    

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->data['subject'])
        ->markdown('mail.funding.funding_application_notification')
        ->with([
            'type' => $this->data['type'],
            'status' => $this->data['status'],
            'fundingApplication' => $this->data['fundingApplication'],
            'subject' => $this->data['subject']
        ]);
    }
}
