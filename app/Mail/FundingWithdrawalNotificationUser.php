<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FundingWithdrawalNotificationUser extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    $amount = $this->data['amount'];
    $currency = $this->data['currency'];
    $user = $this->data['user'];
    $status = $this->data['status'];

    return $this->subject('Funding Withdrawal Notification')
                ->markdown('mail.funding.withdrawal_notification_user')
                ->with([
                    'amount' => $amount,
                    'currency' => $currency,
                    'user' => $user,
                    'status' => $status,
                ]);
    }
}
