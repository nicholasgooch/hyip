<?php

namespace App\Mail;

use App\User;
use App\deposits;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class UserDepositNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $deposit;
    public $status;
    public $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, deposits $deposit,String $status)
    {
        $this->user = $user;
        $this->deposit = $deposit;
        $this->status = $status;
        
        switch($this->status){
            case('pending'):
                $this->subject = 'You Have a Pending Deposit';
                break;
            case('initiated'):
                $this->subject = "Deposit Initiated - Awaiting Confirmation";
                break;
            case('declined'):
                $this->subject = "Deposit Declined - Please Contact Support";
                break;

            case('approved'):
                $this->subject = "Deposit Approved - Your Funds are Ready!";
                break;
            default:
                $this->subject = "New Deposit Notification";

        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)->markdown('mail.deposit_notification')
        ->with([
            'user' => $this->user,
            'deposit' => $this->deposit,
            'status' => $this->status
        ]);
    }
}
