<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $content;
    public function __construct(Array $content)
    {
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       // dd($this->content);
        return $this->view('landrick.email.contact')
        ->with([
            'name' => $this->content['name'],
            'email' => $this->content['email'],
            'subject' => $this->content['subject'],
            'comments' => $this->content['comments'],
        ]);
    }
}
