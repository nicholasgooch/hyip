<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

class ReferralInvestmentNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $investment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $investment)
    {
        $this->user = $user;
        $this->investment = $investment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Investment Confirmation')
            ->view('mail.user.user_investment_referral_notification')
            ->with([
                'user' => $this->user,
                'investment' => $this->investment,
            ]);
    }
}
