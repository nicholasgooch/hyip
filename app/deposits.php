<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class deposits extends Model
{
    // Define the possible values for the status attribute
    const PENDING = 0;
    const APPROVED = 1;
    const DECLINED = 2;

    protected $table = "deposits";
    protected $fillable = ['user_id',
        'amount',
        'status',
        'on_apr',
        'bank',
        'coin_amount',
        'account_name',
        'currency',
        'account_no',
        'url', 'ref',
        'usn',
        'created_at',
        'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    // Define an accessor for the status attribute that returns a string
    public function getDepositStatusAttribute($value)
    {
        switch ($value) {
            case self::PENDING:
                return 'Pending';
            case self::APPROVED:
                return 'Approved';
            case self::DECLINED:
                return 'Declined';
            default:
                return 'Unknown';
        }
    }
}
