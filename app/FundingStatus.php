<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class FundingStatus extends Model
{
    protected $fillable = [
        'name',
        'slug',
    ];

    public function fundings()
    {
        $this->hasMany('App\Funding');
    }

    public static function createOrUpdateStatus($data)
    {
        $types = [];
        foreach ($data as $name) {
            $slug = Str::slug($name);
            $types[] = FundingStatus::updateOrCreate([
                'slug' => $slug,
            ], [
                'name' => $name,
                'slug' => $slug,
            ]);
        }

        return $types;
    }
}
