<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendGenericNotificationToUsers extends Notification
{
    use Queueable;

    protected $data;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $mail = (new MailMessage);

        if (isset($this->data['subject'])) {
            $mail->subject($this->data['subject']);
        }

        if (isset($this->data['greeting'])) {
            $mail->greeting($this->data['greeting']);
        }

        if (isset($this->data['message_type'])) {
            $mail->level($this->data['message_type']);
        }

        if (isset($this->data['sub_message'])) {
            $mail->line($this->data['sub_message']);
        }

        if (isset($this->data['message'])) {
            $mail->line($this->data['message']);
        }

        if (isset($this->data['action'])) {
            $mail->action($this->data['action'], url($this->data['action_url']));
        }
        $mail->line('Thank you for using our application!');

        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
