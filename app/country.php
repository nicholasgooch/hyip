<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class country extends Model
{
    protected $table="countries";

    protected $fillable = ['sortname','name','phonecode' ];

    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App/User', 'country');
    }
    public function kyc(){
        return $this->hasOne('App\kyc');
    }

}
