<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class withdrawal extends Model
{
    protected $table="withdrawals";

    protected $with = ['bank'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function bank(){
        return $this->belongsTo('App\banks');
    }
}
