<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class FundingType extends Model
{
    public $fillable = [
        'name', 'slug', 'user_type',
    ];

    public function funding()
    {
        return $this->hasOne('App\Funding');
    }

    public static function createOrUpdateType($data)
    {
        $types = [];
        foreach ($data as $name) {
            $slug = Str::slug($name);
            $types[] = FundingType::updateOrCreate([
                'slug' => $slug,
            ], [
                'name' => $name,
                'slug' => $slug,
            ]);
        }

        return $types;
    }

}
