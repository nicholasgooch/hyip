<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JointInvestment extends Model
{

    protected $fillable = ['investment_id', 'user_id', 'share_percentage'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function investment()
    {
        return $this->belongsTo(Investment::class);
    }
}
