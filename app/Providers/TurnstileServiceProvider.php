<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;

class TurnstileServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Validator::extend('turnstile', function ($attribute, $value, $parameters, $validator) {
            $response = Http::asForm()->post('https://challenges.cloudflare.com/turnstile/v0/siteverify', [
                'secret' => config('turnstile.secret_key'),
                'response' => $value,
                'remoteip' => request()->ip(),
            ]);

            return $response->successful() && $response->json('success');
        });

        Validator::replacer('turnstile', function ($message, $attribute, $rule, $parameters) {
            return 'The CAPTCHA verification failed. Please try again.';
        });
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/turnstile.php',
            'turnstile'
        );
    }
}