<?php

namespace App\Providers;

use App\activities;
// use Illuminate\Support\ServiceProvider;
use App\adminLog;
use App\banks;
use App\deposits;
use App\investment;
use App\packages;
use App\site_settings;
use App\User;
use App\withdrawal;
use Auth;
use Doctrine\DBAL\Types\Type;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Check if the ENUM type is already registered to avoid a runtime error
        if (!Type::hasType('enum')) {
            Type::addType('enum', 'Doctrine\DBAL\Types\StringType');
        }

        Schema::defaultStringLength(191);
        if ($this->app->environment('production')) {
            \URL::forceScheme('https');
        }

        //collection paginate
        /**
         * Paginate a standard Laravel Collection.
         *
         * @param int $perPage
         * @param int $total
         * @param int $page
         * @param string $pageName
         * @return array
         */
        Collection::macro('paginate', function ($perPage, $total = null, $page = null, $pageName = 'page') {
            $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);
            $paginator = new LengthAwarePaginator(
                $this->forPage($page, $perPage),
                $total ?: $this->count(),
                $perPage,
                $page,
                [
                    'path' => LengthAwarePaginator::resolveCurrentPath(),
                    'pageName' => $pageName,
                ]
            );
            return $paginator->toArray();
        });
        // Using Closure based composers...
        View::composer('*', function ($view) {

            $adm = Auth::guard('admin')->user();
            $settings = site_settings::find(1);
            $packages = packages::all();
            //check if path is /admin

            if (!request()->is('admin/*') && Auth::guard('web')->check()) {
                $user = Auth::guard('web')->user();
                $myInv = investment::where('user_id', $user->id)->orderby('id', 'asc')->get();
                $actInv = investment::where('user_id', $user->id)->where('status', 'Active')->orderby('id', 'desc')->get();
                $endedInv = investment::where('user_id', $user->id)->where('status', 'Pending')->orderby('id', 'desc')->get();
                $refs = User::where('referal', $user->username)->orderby('id', 'DESC')->get();
                $wd = withdrawal::where('user_id', $user->id)->orderby('id', 'asc')->get();
                $deposits = deposits::where('user_id', $user->id)->orderby('id', 'asc')->get();
                $logs = activities::where('user_id', $user->id)->orderby('id', 'DESC')->take(20)->paginate(5);
                $mybanks = banks::where('user_id', $user->id)->where('Account_name', '!=', 'BTC')->orderby('id', 'DESC')->get();
                $my_wallets = banks::where('user_id', $user->id)->where('Account_name', 'BTC')->orderby('id', 'DESC')->get();

                $totalInvested = 0.00;
                $invs = investment::where('user_id', $user->id)->get();
                foreach ($invs as $key => $inv) {
                    $totalInvested += (float) $inv->capital;
                }

                $view->with(['user' => $user,
                    'myInv' => $myInv, 'actInv' => $actInv, 'refs' => $refs,
                    'wd' => $wd, 'logs' => $logs,
                    'mybanks' =>
                    $mybanks, 'my_wallets' =>
                    $my_wallets,
                    'settings' => $settings,
                    'packages' => $packages,
                    'deposits' => $deposits,
                    'totalInvested' => $totalInvested,
                    'endedInv' => $endedInv,
                ]);
            } else {
                $inv = investment::orderby('id', 'desc')->get();
                $deposits = deposits::orderby('id', 'desc')->get();
                $users = User::orderby('id', 'desc')->get();
                $wd = withdrawal::orderby('id', 'desc')->paginate(20);
                $today_wd = withdrawal::where('created_at', 'like', '%' . date('Y-m-d') . '%')->orderby('id', 'desc')->get();
                $today_dep = deposits::where('created_at', 'like', '%' . date('Y-m-d') . '%')->orderby('id', 'desc')->get();
                $today_inv = investment::where('date_invested', date('Y-m-d'))->orderby('id', 'desc')->get();
                $cap = $cap2 = $dep = $dep2 = $wd_bal = $sum_cap = 0;
                $logs = adminLog::orderby('id', 'desc')->take(5)->get();

                $view->with(['inv' => $inv, 'deposits' => $deposits, 'users' => $users, 'wd' => $wd, 'today_wd' => $today_wd, 'today_dep' => $today_dep, 'today_inv' => $today_inv,
                    'settings' => $settings,
                    'logs' => $logs,
                    'cap' => $cap,
                    'cap2' => $cap2,
                    'dep' => $dep,
                    'dep2' => $dep2,
                    'wd_bal' => $wd_bal,
                    'sum_cap' => $sum_cap,
                    'packages' => $packages,
                    'adm' => $adm,
                ]);

            }

        });
    }
}
