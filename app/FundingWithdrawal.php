<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FundingWithdrawal extends Model
{
    protected $fillable = [
        'user_id',
        'funding_withdrawal_method_id',
        'status',
        'ref',
        'amount',
    ];

    public function method(){
        $this->belongsTo(FundingWithdrawalMethod::class, 'funding_withdrawal_method_id');
    }

    
}
