<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class site_settings extends Model
{
    protected $table = "settings";

    protected $fillable = [
        'site_title',
        'site_descr',
        'site_logo',
        'header_color',
        'footer_color',
        'site_favicon',
    ];
}
