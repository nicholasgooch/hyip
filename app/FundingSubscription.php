<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FundingSubscription extends Model
{

    protected $fillable = [
        'user_id',
        'plan_1_amount',
        'plan_2_amount',
        'plan_3_amount',
    ];

    protected $with = ['plan'];
    public function user()
    {
        return $this->belongsTo(User::class,);
    }

    public function plan()
    {
        return $this->belongsTo(FundingSubsciptionPlan::class, 'funding_subscription_plan_id');
    }
}
