<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FundingSubscriptionPlan extends Model
{
    protected $fillable = [
        'user_id',
        'plan_1_amount',
        'plan_2_amount',
        'plan_3_amount',
    ];

    public function subscription()
    {
        return $this->belongsTo(User::class);
    }
}
