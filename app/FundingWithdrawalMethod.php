<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FundingWithdrawalMethod extends Model
{
   protected $fillable = [
        'user_id',
        'account_name',
        'account_number',
        'routing_number',
        'bank_name',
        'bank_country',
        'crypto_wallet_address',
        'crypto_asset_name',
        'crypto_asset_shortcode',
        'crypto_asset_provider'

    ];

    public function withdrawals()
    {
        return $this->hasMany(FundingWithdrawal::class);
    }

    
}
