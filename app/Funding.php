<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funding extends Model
{
    protected $fillable= [
        'user_id',
        'amount',
        'funding_type_id',
        'collateral_amount',
        'collateral_type_id',
        'interest_rate',
        'funding_status_id',
        'term',
        'purpose_of_funding',
        'repayment_frequency',
        'collateral_paid',
        'available_to_withdraw',
        'equity_percentage',
        'business_valuation',
        'comments',

    ];

    protected $with = [
        'type',
        'status'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function repayments()
    {
        return $this->hasMany('App\Repayment');
    }

    public function type(){
        return $this->belongsTo('App\FundingType', 'funding_type_id');
    }

    public function status()
    {
        return $this->belongsTo(FundingStatus::class, 'funding_status_id');
    }
   
}
