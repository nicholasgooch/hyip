<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kyc extends Model
{
    protected $fillable = ['user_id', 'font_pics', 'back_pics', 'id_type', 'ssn', 'address_1', 'address_2', 'city', 'state_id', 'country_id', 'zip', 'dob'];
    protected $with = ['country', 'state'];
    public function user(){
        return belongsTo('App\User');
    }
    public function country(){
        return $this->belongsTo('App\country');
    }
    public function state(){
        return $this->belongsTo('App\states');
    }
}
