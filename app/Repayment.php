<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repayment extends Model
{
    protected $fillable = [
        'funding_id',
        'payment_number',
        'due_date',
        'payment_amount',
        'status',
    ];

}
