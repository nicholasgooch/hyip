<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ref extends Model
{
    protected $table="ref";

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
