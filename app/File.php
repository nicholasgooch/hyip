<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['user_id', 'url', 'category'];

    public function user(){
        return $this->belongsTo('App\User' ,);
    }
}
