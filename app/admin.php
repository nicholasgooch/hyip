<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class admin extends Authenticatable
{
    protected $table = "admin";

    protected $fillable = [
        'email',
        'password',
        'name',
        'role',
        'author',
        'status',
        'img',
    ];
}
