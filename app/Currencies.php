<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currencies extends Model
{
    public function deposits()
    {
        return $this->hasMany(Deposit::class,'currency_id');
    }

    public function withdrawals()
    {
        return $this->hasMany(Withdrawals::class,'currency_id');
        
    }
}
