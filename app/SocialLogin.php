<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialLogin extends Model
{
    protected $table = 'social_login';

    protected $fillable = [
        'user_id', 'provider', 'auth_token',
    ];

    protected $casts = [
        'auth_token' => 'array',
    ];
}
