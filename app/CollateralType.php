<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class CollateralType extends Model
{
    public $fillable = [
        'name',
        'funding_type_id',
        'description',
        'slug',
    ];

    public static function createOrUpdateCollateraType($data)
    {
        $types = [];
        foreach ($data as $arr) {
            $slug = Str::slug($arr['name']);
            $types[] = CollateralType::updateOrCreate([
                'slug' => $slug,
            ], [
                'name' => $arr['name'],
                'slug' => $slug,
                'funding_type_id' => $arr['funding_type_id'],
            ])->toArray();
        }

        return $types;
    }
}
