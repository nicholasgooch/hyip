<?php

namespace App\Console\Commands;

use App\admin;
use Hash;
use Illuminate\Console\Command;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:admin {--email=} {--password=} {--name=} {--role=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new admin user';

    /**
     * Execute the console command.
     *
    
     */
    public function handle()
    {
        $domain = config('app.url');
        $email = $this->option('email') ?? 'admin@' . $domain;
        $password = $this->option('password') ?? 'password';
        $name = $this->option('name') ?? 'Admin';
        $role = $this->option('role') ?? 'admin';

        $hashedPassword = Hash::make($password);

        $admin = new admin();
        $admin->email = $email;
        $admin->password = $hashedPassword;
        $admin->name = $name;
        $admin->role = $role;
        $admin->author = 1;
        $admin->status = 1;
        $admin->img = null;
        $admin->save();

        $this->info('Admin created successfully');
    }
}
