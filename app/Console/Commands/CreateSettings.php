<?php

namespace App\Console\Commands;

use App\CollateralType;
use App\FundingStatus;
use App\FundingType;
use App\site_settings;
use DB;
use Illuminate\Console\Command;

class CreateSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'settings:register {--site-logo=} {--site-favicon=} {--site-title=} {--site-descr=} {--header-color=} {--footer-color=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regsisters initial settings {--site-logo=} {--site-favicon=} --site-title= {--site-descr=} --header-color= {--footer-color=}';

    public function handle()
    {
        $site_logo = $this->option('site-logo') ?? 'https://res.cloudinary.com/demo/image/upload/sample.jpg';
        $site_favicon = $this->option('site-favicon') ?? 'https://res.cloudinary.com/demo/image/upload/sample.jpg';
        $site_title = $this->option('site-title') ?? 'Your Site Title';
        $site_descr = $this->option('site-descr') ?? 'A brief description of your site';
        $header_color = $this->option('header-color') ?? '#FFFFFF';
        $footer_color = $this->option('footer-color') ?? '#000000';

        // Define your default settings
        $defaultSettings = [
            'site_title' => $site_title,
            'site_descr' => $site_descr,
            'site_logo' => $site_logo,
            'header_color' => $header_color,
            'footer_color' => $footer_color,
            'site_favicon' => $site_favicon,
            // Add more settings as needed
        ];

        DB::beginTransaction();

        try {
            site_settings::updateOrCreate(['id' => 1], $defaultSettings);
            $typeData = [
                'Business Funding',
                'Private Equity',
                'Personal Loans',

            ];
            FundingType::createOrUpdateType($typeData);

            $statusData = [
                "Application Received",
                "Under Review",
                "Approved",
                "Funds Disbured",
            ];
            FundingStatus::createOrUpdateStatus($statusData);

            $personalLoans = FundingType::where('slug', 'personal-loans')->first();
            $businessLoans = FundingType::where('slug', 'private-equity')->first();
            $collateralTypes = [
                [
                    "name" => "Crypto currency",
                    'funding_type_id' => $personalLoans->id
                ],
                [
                    "name" => "Bank Transfer",
                    "funding_type_id" => $personalLoans->id
                ],
                [
                    "name" => "Credit Card Payment",
                    "funding_type_id" => $businessLoans->id,
                ],

            ];

            CollateralType::createOrUpdateCollateraType($collateralTypes);

            $this->info('Settings have been created successfully.');
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->error('Failed to create settings: ' . $e->getMessage());
        }
    }
}
