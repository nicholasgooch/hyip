<?php

namespace App\Console\Commands;

use App\packages;
use Illuminate\Console\Command;

class CreateDefaultPackages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:packages {--recreate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Inserts default package records into the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $defaultPackages = [
            [
                'package_name' => 'Oil and Gas',
                'currency' => 'USD',
                'min' => 50000.00,
                'max' => 50000000.00,
                'daily_interest' => 10.8,
                'period' => 45,
                'days_interval' => 30,
                'withdrwal_fee' => 2.00,
                'ref_bonus' => 5.00,
                'status' => 1,
            ],
            [
                'package_name' => 'Agriculture',
                'currency' => 'USD',
                'min' => 20000.00,
                'max' => 2000000.00,
                'daily_interest' => 10.2,
                'period' => 60,
                'days_interval' => 30,
                'withdrwal_fee' => 1.50,
                'ref_bonus' => 4.00,
                'status' => 1,
            ],
            [
                'package_name' => 'Forex',
                'currency' => 'USD',
                'min' => 1000.00,
                'max' => 100000.00,
                'daily_interest' => 10.6,
                'period' => 30,
                'days_interval' => 15,
                'withdrwal_fee' => 3.00,
                'ref_bonus' => 6.00,
                'status' => 1,
            ],
            [
                'package_name' => 'Crypto-Currency',
                'currency' => 'USD',
                'min' => 5000.00,
                'max' => 2000000.00,
                'daily_interest' => 20.0,
                'period' => 40,
                'days_interval' => 30,
                'withdrwal_fee' => 2.50,
                'ref_bonus' => 5.00,
                'status' => 1,
            ],
            [
                'package_name' => 'Stock Trading',
                'currency' => 'USD',
                'min' => 1000.00,
                'max' => 1000000.00,
                'daily_interest' => 10.3,
                'period' => 35,
                'days_interval' => 20,
                'withdrwal_fee' => 1.80,
                'ref_bonus' => 4.50,
                'status' => 1,
            ],
            [
                'package_name' => 'Gold Investments',
                'currency' => 'USD',
                'min' => 3000.00,
                'max' => 5000000.00,
                'daily_interest' => 10.7,
                'period' => 50,
                'days_interval' => 30,
                'withdrwal_fee' => 2.20,
                'ref_bonus' => 5.50,
                'status' => 1,
            ],
            [
                'package_name' => 'Real Estate',
                'currency' => 'USD',
                'min' => 100000.00,
                'max' => 50000000.00,
                'daily_interest' => 10.9,
                'period' => 60,
                'days_interval' => 30,
                'withdrwal_fee' => 3.00,
                'ref_bonus' => 6.00,
                'status' => 1,
            ],
            [
                'package_name' => 'Crypto IRA',
                'currency' => 'USD',
                'min' => 20000.00,
                'max' => 1000000.00,
                'daily_interest' => 20.0,
                'period' => 365,
                'days_interval' => 30,
                'withdrwal_fee' => 1.80,
                'ref_bonus' => 5.00,
                'status' => 1,
            ],
            [
                'package_name' => 'Certificate of Deposit (CD)',
                'currency' => 'USD',
                'min' => 50000.00,
                'max' => 500000.00,
                'daily_interest' => 30.0,
                'period' => 120,
                'days_interval' => 30,
                'withdrwal_fee' => 2.50,
                'ref_bonus' => 4.00,
                'status' => 1,
            ],
        ];

        if ($this->option('recreate')) {
            $this->info('Package will be truncated');
            Packages::truncate();
            $this->info('Package successfully truncated');
        }

        foreach ($defaultPackages as $package) {
            Packages::updateOrCreate(
                ['package_name' => $package['package_name']],
                $package
            );
        }

        $this->info('Default packages have been inserted successfully.');
    }
}
