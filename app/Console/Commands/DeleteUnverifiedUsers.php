<?php

namespace App\Console\Commands;

use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeleteUnverifiedUsers extends Command
{
    protected $signature = 'users:delete 
        {--days=2 : Number of days to look back}
        {--type=suspicious : Type of users to delete (suspicious/unverified/both)} 
        {--force : Force deletion without confirmation} 
        {--filter : Filter users by id}';

    protected $description = 'Deletes unverified users and/or users with suspicious names {--days=2} {--type=suspicious} {--force} {--filter} possible types are suspicious, unverified, both';

    public function __construct()
    {
        parent::__construct();

    }
    /**
     * Handle the command
     * 
     * @return void
     */
    public function handle()
    {
        $days = $this->option('days') ?? 2;
        $type = $this->option('type') ?? 'both';
        $shouldForce = $this->option('force');
        $shouldFilter = $this->option('filter');

        $usersToDelete = collect();

        switch ($type) {
            case 'suspicious':
                $usersToDelete = $this->getSuspiciousUsers($days);
                $this->info("\nLooking for suspicious users...");
                break;
            case 'unverified':
                $usersToDelete = $this->getUnverifiedUsers($days);
                $this->info("\nLooking for unverified users...");
                break;
            case 'both':
                $suspiciousUsers = $this->getSuspiciousUsers($days);
                $unverifiedUsers = $this->getUnverifiedUsers($days);
                $usersToDelete = $suspiciousUsers->merge($unverifiedUsers)->unique('id');
                $this->info("\nLooking for both suspicious and unverified users...");
                break;
            default:
                $this->error("Invalid type. Use 'suspicious', 'unverified', or 'both'");
                return;
        }

        if ($usersToDelete->isEmpty()) {
            $this->info("No users found to delete.");
            return;
        }
        $this->info("\nFound " . $usersToDelete->count() . " users to delete:");

        if ($shouldFilter) {
            // Show users with selection options

            $this->info("Select users to exclude (space to select/unselect, enter to confirm):\n");

            $choices = $usersToDelete->map(function ($user) {
                return [
                    'id' => $user->id,
                    'label' => "ID: {$user->id}, Name: {$user->firstname} {$user->lastname}, Username: {$user->username}, Email: {$user->email}"
                ];
            })->pluck('label', 'id')->toArray();

            $selectedIds = $this->choice(
                'Select users to exclude:',
                $choices,
                null,
                null,
                true // Allow multiple selections
            );

            if (!empty($selectedIds)) {
                // Get IDs of selected users
                $excludeIds = collect($choices)
                    ->filter(function ($label, $id) use ($selectedIds) {
                        return in_array($label, $selectedIds);
                    })
                    ->keys()
                    ->toArray();

                // Filter out excluded users
                $excludedCount = $usersToDelete->whereIn('id', $excludeIds)->count();
                $usersToDelete = $usersToDelete->whereNotIn('id', $excludeIds);

                $this->info("\nExcluded {$excludedCount} users from deletion.");
                $this->info("Remaining users to delete: " . $usersToDelete->count());

                // Show remaining users
                foreach ($usersToDelete as $user) {
                    $this->line("ID: {$user->id}, Name: {$user->firstname} {$user->lastname}, Username: {$user->username}, Email: {$user->email}");
                }

                // Ask to press enter to continue
                $this->info("\nPress Enter to continue with remaining users...");
                fgets(STDIN);
            }

            if ($usersToDelete->isEmpty()) {
                $this->info("No users left to delete after exclusions.");
                return;
            }
        }

        if ($shouldForce) {
            $usersToDelete->each->delete();
            $this->info("Deleted {$usersToDelete->count()} users.");
        } else {
            if ($this->confirm('Do you want to proceed with deletion?')) {
                $usersToDelete->each->delete();
                $this->info("Deleted {$usersToDelete->count()} users.");
            } else {
                $this->info("Operation cancelled.");
            }
        }
    }

    /**
     * Get unverified users
     * 
     * @param int $days
     * @return User[]
     */
    protected function getUnverifiedUsers(int $days)
    {
        return User::where(function ($query) use ($days) {
            $query->whereNull('email_verified_at')
                ->orWhere('status', 0)
                ->where('created_at', '<', Carbon::now()->subDays($days));
        })->get();
    }
    /**
     * Get suspicious users
     * 
     * @param int $days
     * @return User[]
     */
    protected function getSuspiciousUsers(int $days)
    {
        return User::where('created_at', '<', Carbon::now()->subDays($days))
            ->where(function ($query) {
                // Check firstname patterns
                $query->where(function ($q) {
                    $q->where('firstname', 'REGEXP', '^[A-Za-z0-9]{8,}$')
                        ->orWhere('firstname', 'REGEXP', '[A-Z][a-z][A-Z][a-z]')
                        ->orWhere('firstname', 'REGEXP', '[A-Za-z0-9]{20,}')
                        ->orWhere('firstname', 'LIKE', '%xX%')
                        ->orWhere('firstname', 'LIKE', '%Xx%')
                        ->orWhere('firstname', 'LIKE', '%0O%')
                        ->orWhere('firstname', 'LIKE', '%O0%')
                        ->orWhere('firstname', 'REGEXP', '(.)\\1{3,}')
                        ->orWhere('firstname', 'REGEXP', '[0-9]{4,}')
                        // Additional firstname patterns
                        ->orWhere('firstname', 'REGEXP', '[^a-zA-Z\\s-]') // Contains non-letter characters
                        ->orWhere('firstname', 'REGEXP', '^[A-Z]+$') // All uppercase
                        ->orWhere('firstname', 'LIKE', '% %') // Multiple spaces
                        ->orWhere('firstname', 'REGEXP', '^[a-z]') // Starts with lowercase
                        ->orWhere('firstname', 'REGEXP', '[0-9]') // Contains numbers
                        ->orWhere('firstname', 'REGEXP', '[_]') // Contains underscore
                        ->orWhere('firstname', 'REGEXP', '^.{1,2}$') // Too short (1-2 chars)
                        ->orWhere('firstname', 'REGEXP', '^.{30,}$'); // Too long (30+ chars)
                })
                    // Check lastname patterns
                    ->orWhere(function ($q) {
                    $q->where('lastname', 'REGEXP', '^[A-Za-z0-9]{8,}$')
                        ->orWhere('lastname', 'REGEXP', '[A-Z][a-z][A-Z][a-z]')
                        ->orWhere('lastname', 'REGEXP', '[A-Za-z0-9]{20,}')
                        ->orWhere('lastname', 'LIKE', '%xX%')
                        ->orWhere('lastname', 'LIKE', '%Xx%')
                        ->orWhere('lastname', 'LIKE', '%0O%')
                        ->orWhere('lastname', 'LIKE', '%O0%')
                        ->orWhere('lastname', 'REGEXP', '(.)\\1{3,}')
                        ->orWhere('lastname', 'REGEXP', '[0-9]{4,}')
                        // Additional lastname patterns
                        ->orWhere('lastname', 'REGEXP', '[^a-zA-Z\\s-]')
                        ->orWhere('lastname', 'REGEXP', '^[A-Z]+$')
                        ->orWhere('lastname', 'LIKE', '% %')
                        ->orWhere('lastname', 'REGEXP', '^[a-z]')
                        ->orWhere('lastname', 'REGEXP', '[0-9]')
                        ->orWhere('lastname', 'REGEXP', '[_]')
                        ->orWhere('lastname', 'REGEXP', '^.{1,2}$')
                        ->orWhere('lastname', 'REGEXP', '^.{30,}$');
                })
                    // Check username patterns
                    ->orWhere(function ($q) {
                    $q->where('username', 'REGEXP', '^[A-Za-z0-9]{8,}$')
                        // Additional username patterns
                        ->orWhere('username', 'REGEXP', '^[0-9]+$') // Only numbers
                        ->orWhere('username', 'REGEXP', '^admin\\d*$') // Starts with 'admin'
                        ->orWhere('username', 'REGEXP', '^user\\d*$') // Starts with 'user'
                        ->orWhere('username', 'REGEXP', '^test\\d*$') // Starts with 'test'
                        ->orWhere('username', 'REGEXP', '(.)\\1{3,}') // Repeated characters
                        ->orWhere('username', 'REGEXP', '[!@#$%^&*(),.?":{}|<>]') // Special characters
                        ->orWhere('username', 'LIKE', '% %') // Contains spaces
                        ->orWhere('username', 'REGEXP', '^.{1,3}$') // Too short
                        ->orWhere('username', 'REGEXP', '^.{25,}$'); // Too long
                })
                    // Check email patterns
                    ->orWhere(function ($q) {
                    $q->where('email', 'REGEXP', '^[0-9]+@')  // Starts with numbers
                        ->orWhere('email', 'REGEXP', 'temp\\d*@') // Contains 'temp'
                        ->orWhere('email', 'REGEXP', 'test\\d*@') // Contains 'test'
                        ->orWhere('email', 'REGEXP', 'user\\d*@') // Contains 'user'
                        ->orWhere('email', 'REGEXP', 'fake\\d*@') // Contains 'fake'
                        ->orWhere('email', 'LIKE', '%@temp%') // Temporary email domains
                        ->orWhere('email', 'LIKE', '%@example%')
                        ->orWhere('email', 'LIKE', '%@test%')
                        ->orWhere('email', 'REGEXP', '(.)\\1{3,}@'); // Repeated characters
                });
            })
            // Exclude verified and active users
            ->where(function ($query) {
                $query->whereNull('email_verified_at')
                    ->orWhere('status', 0);
            })
            ->get();
    }

}
