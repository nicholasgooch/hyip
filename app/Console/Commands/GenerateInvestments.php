<?php

namespace App\Console\Commands;

use App\deposits;
use App\packages;
use App\site_settings as settings;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GenerateInvestments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:investments
    {--email= : The email of the user}
    {--amount= : The amount of the investment}
    {--count= : The number of investments to be generated}
    {--start= : The number of months to go back from now}
    {--end= : The number of months to go forward from now}
    {--deposits=5 : Amount of deposits to be generated}
    {--add_balance : Add balance to the user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a set of investments for a user
    {--email= : The email of the user}
    {--amount= : The amount of the investment}
    {--count= : The number of investments to be generated}
    {--start= : The number of months to go back from now}
    {--end= : The number of months to go forward from now}
    {--deposits=5 : Amount of deposits to be generated}
    {--add_balance : Add balance to the user}';
    protected $settings;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->settings = settings::first();
        if (!$this->settings) {
            $this->error('Site settings not found');
            return;
        }
        $email = $this->option('email');
        if (!$email) {
            $this->error('Email is required');
            return;
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Invalid email address');
            return;
        }
        $amount = $this->option('amount');
        if (!$amount) {
            $this->error('Amount is required');
            return;
        }
        $number = $this->option('count') ?? 5;

        [$start, $end] = $this->getStartAndEndDates();

        $deposits_count = $this->option('deposits') ?? 2;
        $addBalance = $this->option('add_balance');


        $user = User::where('email', $email)->first();
        if (!$user) {
            $this->error('User not found');
            return;
        }
        $this->info("Found a user with the email {$user->name}");
        try {
            $this->generateInvestments($user, $amount, $number, $start, $end, $deposits_count, $addBalance);

        } catch (\Exception $e) {

            $this->error($e->getMessage());
        }

    }

    protected function generateInvestments($user, $amount, $number, $start, $end, $deposits_count, $addBalance = false)
    {
        $this->info("Investments Generatation process begins....");
        $packages = packages::all()->toArray();

        if (!$packages) {
            $this->error('No packages found...');
            return collect();
        }
        $investments = collect();
        $this->info('Generating amounts for user...');
        $amounts = $this->distributeAmount($amount, $number);
        $this->info
        ('Amounts generated successfully');
        for ($i = 1; $i < $number; $i++) {

            $this->info("Generating investment {$i}...");
            $package = $packages[array_rand($packages)];

            $startDate = $this->randomDateInRange($start, $end);
            $endDate = (new Carbon($startDate))->addDays($package['days_interval'])->format('Y-m-d');

            $investments->push($user->investments()->create([
                'usn' => $user->username,
                'package_id' => $package['id'],
                'capital' => $amounts[$i],
                'currency' => 'USD',
                'last_wd' => $startDate,
                'i_return' => $amounts[$i] * ($package['daily_interest'] / 100) * $package['days_interval'],
                'date_invested' => $startDate,
                'interest' => $package['daily_interest'],
                'period' => $package['days_interval'],
                'days_interval' => $package['days_interval'],
                'start_date' => $startDate,
                'end_date' => $endDate,
                'w_amt' => 0,
                'status' => 'Active',
            ]));
            $this->info("Investment {$i} generated successfully");
        }
        $this->info('Investments generated successfully');

        if ($deposits_count !== "0") {
            $this->info('Generating deposit(s) to the user...');
            $this->generateDeposits($user, rand(1000, $amount));
            $this->info('Deposits added successfully');
        }
        if ($addBalance) {
            $this->info('Adding balance to the user...');
            $user->wallet = $user->wallet + $amount;
            $user->save();
            $this->info('Balance added successfully');
        }
        return $investments;
    }

    protected function randomDateInRange($start, $end)
    {
        $startDate = strtotime($start);
        $endDate = strtotime($end);
        $randomDate = rand($startDate, $endDate);

        return date('Y-m-d', $randomDate);
    }

    protected function distributeAmount($totalAmount, $parts)
    {

        $remaining = $totalAmount;
        $amounts = [];

        for ($i = 0; $i < $parts - 1; $i++) {
            $amount = rand(1, $remaining - ($parts - $i - 1));
            $amounts[] = $amount;
            $remaining -= $amount;
        }

        $amounts[] = $remaining;

        return $amounts;
    }

    protected function generateDeposits($user, $amount)
    {
        $methods = ['BTC', 'ETH', 'Bank Transfer'];
        $rndNumber = rand(5, 10);
        $deposits = collect();
        for ($i = 1; $i < $rndNumber; $i++) {
            $method = $methods[array_rand($methods)];

            $deposits->push($user->deposits()->create([
                'amount' => $amount,
                'status' => deposits::APPROVED,
                'currency' => $this->settings->currency,
                'bank' => $method,
                'ref' => generateRandomTrxRf(5),
                'usn' => $user->username,
                'on_apr' => 1,
                'account_name' => $method == 'Bank Transfer' ? 'Bank Transfer' : 'Crypto Wallet',
                'account_no' => $method == 'BTC'
                    ? $this->settings->btc_wallet_address
                    : $this->settings->eth_wallet_id,
                'url' => $method == 'BTC' ? 'https://blockchain.info' : 'https://etherscan.io',
                'coin_amount' => $method == 'BTC' ? toBTC($amount) : toETH($amount),
                'created_at' => $this->randomDateInRange(now()->subYears(5), now()),
                'updated_at' => $this->randomDateInRange(now()->subYears(5), now()),
            ]));

            $this->info('Deposit ' . $i . ' generated successfully');
        }
        return $deposits->toArray();
    }

    protected function getStartAndEndDates()
    {
        $start = now()->subMonths($this->option('start') ?? 24)->format('Y-m-d');
        $end = now()->addMonths($this->option('end') ?? 4)->format('Y-m-d');
        return [$start, $end];
    }

}
