<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class SeedCountriesAndStates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed:country-state';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seeds the countries and states tables from SQL files.';

    /**
     * Execute the console command.
     *

     */
    public function handle()
    {

        // Check if countries are already seeded
        if ($this->isTableSeeded('countries')) {
            $this->info('Countries are already seeded.');
        } else {
            $this->seedFromSql('countries.sql');
        }

        if ($this->isTableSeeded('states')) {
            $this->info('States are already seeded.');
        } else {
            $this->seedFromSql('states.sql');
        }

    }

    protected function seedFromSql($fileName)
    {
        $filePath = public_path($fileName);
        if (!File::exists($filePath)) {
            $this->error("The file {$fileName} does not exist.");
            return;
        }

        try {
            DB::beginTransaction();
            $sql = File::get($filePath);
            DB::unprepared($sql);
            $this->info("Successfully seeded {$fileName}.");
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->error("Failed to seed {$fileName}: " . $e->getMessage());
        }
    }

    protected function isTableSeeded($tableName)
    {
        return DB::table($tableName)->exists();
    }
}
