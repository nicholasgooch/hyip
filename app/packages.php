<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class packages extends Model
{
    protected $table = "packages";

    protected $fillable = [
        'package_name',
        'currency',
        'min',
        'max',
        'daily_interest',
        'period',
        'days_interval',
        'withdrwal_fee',
        'ref_bonus',
        'status',
    ];
    public function investments()
    {
        return $this->hasMany('App\investment', 'package_id');
    }
}
