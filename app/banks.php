<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class banks extends Model
{
    protected $table="bank";

    public function withdrawals(){
        return $this->hasOne('App\withdrawal');
    }
}
