<?php

return [
    
    'manifest' => [
        'name' => env('APP_NAME', 'My PWA App'),
        'short_name' => 'LDGR',
        'start_url' => '/?source=pwa',
        'background_color' => '#ffffff',
        'theme_color' => '#2f55d4',
        'display' => 'standalone',
        'orientation'=> 'portrait',
        'status_bar'=> 'black', 
        'icons' => [
            '72x72' => [
                'path' => '/landrick/images/pwa/android-launchericon-72-72.png',
                'purpose' => 'any'
            ],
            '96x96' => [
                'path' => '/landrick/images/pwa/android-launchericon-96-96.png',
                'purpose' => 'any'
            ],
            '144x144' => [
                'path' => '/landrick/images/pwa/android-launchericon-144-144.png',
                'purpose' => 'any'
            ],
            '256x256' => [
                'path' => '/landrick/images/pwa/icon-256x256.png',
                'purpose' => 'any'
            ],
            '192x192' => [
                'path' => '/landrick/images/pwa/icon-192x192.png',
                'purpose' => 'maskable'
            ],
            '384x384' => [
                'path' => '/landrick/images/pwa/icon-384x384.png',
                'purpose' => 'maskable'
            ],
            '512x512' => [
                'path' => '/landrick/images/pwa/icon-512x512.png',
                'purpose' => 'maskable'
            ],
        ],
        'splash' => [
            '640x1136' => '/landrick/images/pwa/apple-splash-640-1136.png',
            '750x1334' => '/landrick/images/pwa/apple-splash-750-1334.png',
            '828x1792' => '/landrick/images/pwa/apple-splash-828-1792.png',
            '1125x2436' => '/landrick/images/pwa/apple-splash-1125-2436.png',
            '1242x2208' => '/landrick/images/pwa/apple-splash-1242-2208.png',
            '1242x2688' => '/landrick/images/pwa/apple-splash-1242-2688.png',
            '1536x2048' => '/landrick/images/pwa/apple-splash-1536-2048.png',
            '1668x2224' => '/landrick/images/pwa/apple-splash-1668-2224.png',
            '1668x2388' => '/landrick/images/pwa/apple-splash-1668-2388.png',
            '2048x2732' => '/landrick/images/pwa/apple-splash-2048-2732.png',
            ],
        'shortcuts' => [
           
        ],
        'custom' => []
    ]
];
