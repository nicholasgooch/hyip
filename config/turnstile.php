<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Cloudflare Turnstile Keys
    |--------------------------------------------------------------------------
    |
    | These are the keys used for Cloudflare Turnstile verification. You can get
    | these keys from your Cloudflare dashboard.
    |
    */
    'site_key' => env('TURNSTILE_SITE_KEY', ''),
    'secret_key' => env('TURNSTILE_SECRET_KEY', ''),

    /*
    |--------------------------------------------------------------------------
    | Turnstile Theme
    |--------------------------------------------------------------------------
    |
    | The theme to use for the Turnstile widget.
    | Supported: "light", "dark", "auto"
    |
    */
    'theme' => 'auto',
];