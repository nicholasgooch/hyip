<title>
    Login Account - <?php echo e($settings->site_title); ?></title>

<?php $__env->startSection('content'); ?>
<!-- Loader -->
<div class="back-to-home rounded d-none d-sm-block d-flex justify-content-center ">
    <a href="<?php echo e(env('PORTFOLIO_URL')); ?>" class=" pt-2 btn btn-icon btn-soft-primary text-white">
        <i class="fa fa-home" style="font-size: 20px;"></i></a>
</div>

<!-- Hero Start -->
<section class="h-screen">
    <div class="container-fluid no-margin-container">
        <div class="row no-gutters" id="animatedDiv">
            <?php echo $__env->make('auth.partials.side-hero-slider', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="col-lg-5 col-md-6">


                <div class="card login_page shadow rounded border-0" style="background: url(<?php echo e('v2/assets/images/auth-bg.jpg'); ?>);
                background-repeat: no-repeat;
                  background-size: cover;">
                    <div class="card-body pl-5 pr-5">
                        <div class="card-title text-center">
                            <a class="mt-5 pt-5" href="/">
                                <img class="img-responsive " src="<?php echo e($settings->site_logo); ?>" height="50" alt="">
                            </a>
                            <h4 class="mt-5 mb-5 text-muted"><?php echo e(__('Login In')); ?></h4>
                        </div>
                        <form class="login-form mt-5" method="POST" action="<?php echo e(route('login')); ?>">
                            <div class="row">
                                <input id="csrf" type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                                <?php if(Session::has('err_msg')): ?>
                                    <div class="alert alert-danger">
                                        <?php echo e(Session::get('err_msg')); ?>

                                    </div>
                                    <?php echo e(Session::forget('err_msg')); ?>

                                <?php endif; ?>

                                <?php if(Session::has('regMsg')): ?>
                                    <div class="alert alert-success">
                                        <?php echo e(Session::get('regMsg')); ?>

                                    </div>
                                    <?php echo e(Session::forget('regMsg')); ?>

                                <?php endif; ?>
                                <div class="col-lg-12">
                                    <div class="form-group position-relative">
                                        <label>Your Email <span class="text-danger">*</span></label>
                                        <div class="form-icon position-relative">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-user fea icon-sm icons">
                                                <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                                <circle cx="12" cy="7" r="4"></circle>
                                            </svg>
                                            <input id="email" type="email"
                                                class="p-4 <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?> form-control pl-5"
                                                name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email"
                                                autofocus placeholder="E-Mail Address">
                                            <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                <span class="invalid-feedback" role="alert alert-danger">
                                                    <?php echo e($message); ?>

                                                </span>
                                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group position-relative">
                                        <label for="password"><?php echo e(__('Password')); ?> <span
                                                class="text-danger">*</span></label>
                                        <div class="form-icon position-relative">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-key fea icon-sm icons">
                                                <path
                                                    d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4">
                                                </path>
                                            </svg>
                                            <input id="password" type="password"
                                                class="p-4 <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?> form-control pl-5"
                                                name="password" required autocomplete="current-password"
                                                placeholder="Password">

                                            <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                <span class="invalid-feedback" role="alert alert-danger">
                                                    <?php echo e($message); ?>

                                                </span>
                                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                        </div>


                                    </div>
                                </div>
                                <div class="col-lg-12 d-flex justify-content-center">
                                    <div class="form-group position-relative">
                                        <?php if($errors->has('cf-turnstile-response')): ?>
                                            <span class="help-block">
                                                <strong
                                                    class="text-danger"><?php echo e($errors->first('cf-turnstile-response')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                        <?php if (isset($component)) { $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4 = $component; } ?>
<?php $component = $__env->getContainer()->make(Illuminate\View\AnonymousComponent::class, ['view' => 'components.turnstile','data' => []]); ?>
<?php $component->withName('turnstile'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php if (isset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4)): ?>
<?php $component = $__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4; ?>
<?php unset($__componentOriginalc254754b9d5db91d5165876f9d051922ca0066f4); ?>
<?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-lg-12">

                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value=""
                                                    id="remember <?php echo e(old('flexCheckDefault') ? 'checked' : ''); ?>">
                                                <label class="form-check-label" for="flexCheckDefault">Remember me
                                                </label>
                                            </div>
                                        </div>
                                        <?php if(Route::has('password.request')): ?>
                                            <p class="forgot-pass mt-4 mb-0"><a href="<?php echo e(route('password.request')); ?>"
                                                    class="text-dark font-weight-bold">Forgot Password ?</a></p>
                                        <?php endif; ?>
                                    </div>

                                </div>
                                <div class="col-12 text-center mb-3">
                                    <p class="mb-0 mt-3"><small class="text-dark mr-2">Don't have an account ?</small>
                                        <a href="/register" class="text-dark font-weight-bold">Create An Account</a>
                                    </p>
                                </div>
                                <div class="col-lg-12 mb-0">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        <?php echo e(__('Login')); ?>

                                    </button>
                                </div>
                                <?php if(env('SOCIAL_LOGIN') == true): ?>
                                    <?php echo $__env->make('auth.partials.social-auth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                <?php endif; ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--end col-->

        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->
<!-- Hero End -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script src="<?php echo e(asset('landrick/js/jquery-3.5.1.min.js')); ?>"></script>
<script src="<?php echo e(asset('landrick/js/bootstrap.bundle.min.js')); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.4/min/tiny-slider.js"
    integrity="sha512-j+F4W//4Pu39at5I8HC8q2l1BNz4OF3ju39HyWeqKQagW6ww3ZF9gFcu8rzUbyTDY7gEo/vqqzGte0UPpo65QQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(function () {
        $love = $('.heart');
        for (var i = 0; i < 4; i++) {
            $('.wrapper').append($love.clone());
        }
    });
</script>
<script>
    var slider = tns({
        container: '.tiny-single-item',
        items: 1,
        slideBy: 'page',
        "center": true,
        autoplay: true,
        infinite: true,
        autoplayButtonOutput: false,
        'controls': false,
        nav: true,
        "animateIn": "jello",
        "animateOut": "rollOut",
        "navContainer": false,
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('auth.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mazibuckler/apps/sites/hyip/resources/views/auth/login.blade.php ENDPATH**/ ?>