<div
    class="modal fade"
    id="refEarnModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="modalDefault"
    aria-hidden="true" >
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div  class="modal-body modal-body-md text-center">
                <div class="nk-modal">
                    <div class="" >
                        <div class="" style="">
                            <h3 class="nk-modal-title"><b><?php echo e(__('Referral Withdrawal')); ?></b></h3>
                            <h5 class="text-danger"><b><?php echo e(__('Total Earning:')); ?></b>
                                <?php echo e($settings->currency.' '.$user->ref_bal); ?></h5>
                            <hr>
                        </div>
                        <div id="">
                           <p class="fw-medium text-muted"><?php echo e(__('Enter amount to withdraw and select bank below')); ?></p> 
                            <form id="wd_formssss" action="/user/ref/wd" method="post">
                                <div class="form-group" align="left">
                                    <input type="hidden" class="form-control" name="_token" value="<?php echo e(csrf_token()); ?>">
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend ">
                                            <span class="input-group-text span_bg"><?php echo e($settings->currency); ?></span>
                                        </div>
                                        <input id="ref_amt" type="text" class="form-control" name="amt" required
                                            placeholder="Enter Amount to withdraw">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group" >                   
                                      <div class="input-group-prepend " >
                                        <span class="input-group-text span_bg"><i class="fa fa-home" ></i></span>
                                      </div>
                                      <select name="bank" class="form-control" required  id="withClass">
                                          <option value="">---Select----</option>
                                          <?php 
                                            $banks = App\banks::where('user_id', $user->id)->get();
                                          ?>
                                            <?php if(count($banks) > 0): ?>
                                                <?php $__currentLoopData = $banks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bank): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($bank->id); ?>"><?php echo e($bank->Account_name.' '.$bank->Account_number.' '.$bank->Bank_Name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                            <option value="add" >
                                                &#x2B;  <span class="padding-left 5px;"></span>  Add Bank/Wallet
                                                </option>
                  
                                      </select>
                                    </div>
                                  </div>
                                <div class="form-group">
                                    <br><br>
                                    <button class="collb btn btn-warning"><?php echo e(__('Withdraw')); ?></button>
                                    <span style="">
                                        <a id="ref_wd_close" href="javascript:void(0)"
                                            class="collcc btn btn-danger"><?php echo e(__('Cancel')); ?></a>
                                    </span>
                                    <br>
                                </div>
                            </form>
                        </div>
                        <!-- close btn -->
                        <script type="text/javascript">
                            $('#ref_wd_close').click(function () {
                                $('#refEarnModal').modal('close');
                            });

                        </script>
                        <!-- end close btn -->

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH /Users/mazibuckler/apps/sites/hyip/resources/views/v2/partials/modals/withdraw-ref-earnings.blade.php ENDPATH**/ ?>