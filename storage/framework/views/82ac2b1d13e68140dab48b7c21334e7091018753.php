<div class="col-lg-12 mt-4 text-center">
    <h6>Or Login With</h6>
    <div class="row">
        
        <!--end col-->

        <div class="col-6 mt-3 mx-auto">
            <div class="d-grid">
                <a href="<?php echo e(route('social.login', 'google')); ?>" class="btn btn-light"><i
                        class="mdi mdi-google text-danger"></i>
                    Google</a>
            </div>
        </div>
        <!--end col-->
    </div>
</div>
<?php /**PATH /Users/mazibuckler/apps/sites/hyip/resources/views/auth/partials/social-auth.blade.php ENDPATH**/ ?>