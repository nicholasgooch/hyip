<?php $__env->startSection('title', 'Overview'); ?>
<?php $__env->startSection('content'); ?>
<div class="nk-content nk-content-lg nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head">
                    <div class="nk-block-between-md g-3">
                        <div class="nk-block-head-content">
                            <div class="nk-block-head-sub"><span>Welcome!</span></div>
                            <div class="align-center flex-wrap pb-2 gx-4 gy-3">
                                <div>
                                    <h2 class="nk-block-title fw-normal"><?php echo e($user->firstname . ' ' . $user->lastname); ?>

                                    </h2>
                                </div>
                                <div><a href="<?php echo e(route('v2.investments')); ?>" class="btn btn-white btn-light">My Plans
                                        <em class="icon ni ni-arrow-long-right ml-2"></em></a></div>
                            </div>
                            <div class="nk-block-des">
                                <p>At a glance summary of your investment account. Have fun!</p>
                            </div>
                        </div><!-- .nk-block-head-content -->
                        <?php if($runInv->isEmpty()): ?>
                        <?php else: ?>
                                            <div class="nk-block-head-content d-none d-md-block">
                                                <div class="nk-slider nk-slider-s1">
                                                    <div class="slider-init" data-slick='{"dots": true, "arrows": false, "fade": true}'>

                                                        <?php $__currentLoopData = $runInv; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $inv): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                        <?php
                                                            $totalElapse = getDays(date('Y-m-d'), $inv->end_date);
                                                            $lastWD = $inv->last_wd;
                                                            $enddate = date('Y-m-d');
                                                            $Edays = getDays($lastWD, $enddate);
                                                            $ern = $Edays * $inv->interest * $inv->capital;
                                                            $withdrawable = 0;
                                                            if ($Edays >= $inv->days_interval) {
                                                                $withdrawable = $inv->days_interval * $inv->interest * $inv->capital;
                                                            }
                                                            $totalDays = getDays($inv->date_invested, date('Y-m-d'));
                                                            $perc = ($totalDays / $inv->days_interval) * 100;
                                                            $date_invested = \Carbon\Carbon::createFromFormat('Y-m-d', $inv->date_invested);
                                                            $ended = 'No';

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    ?>
                                                                                        <div class="slider-item">
                                                                                            <div class="nk-iv-wg1">
                                                                                                <div class="nk-iv-wg1-sub sub-text">My Active Plans</div>
                                                                                                <h6 class="nk-iv-wg1-info title">
                                                                                                    <?php echo e(substr($inv->package->package_name, 0, 15)); ?>... -
                                                                                                    <?php echo e($inv->interest); ?>% daily for <?php echo e($inv->period); ?> Days
                                                                                                </h6>
                                                                                                <a href="<?php echo e(route('v2.investments.single', $inv->id)); ?>"
                                                                                                    class="nk-iv-wg1-link link link-light"><em
                                                                                                        class="icon ni ni-trend-up"></em> <span>Check
                                                                                                        Details</span></a>
                                                                                                <div class="nk-iv-wg1-progress">
                                                                                                    <div class="progress-bar bg-primary" data-progress="<?php echo e($perc); ?>"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div><!-- .slider-item -->
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                                    </div>
                                                    <div class="slider-dots"></div>
                                                </div><!-- .nk-slider -->
                                            </div><!-- .nk-block-head-content -->
                        <?php endif; ?>

                    </div><!-- .nk-block-between -->
                </div><!-- .nk-block-head -->
                <div class="nk-block">
                    <div class="nk-news card card-bordered">
                        <div class="card-inner">
                            <div class="nk-news-list">
                                <a class="nk-news-item" href="#">
                                    <div class="nk-news-icon">
                                        <em class="icon ni ni-card-view"></em>
                                    </div>
                                    <div class="nk-news-text">
                                        <p>Do you know the latest update ? <span> A overview of our is now available on
                                                YouTube</span></p>
                                        <em class="icon ni ni-external"></em>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div><!-- .card -->
                </div><!-- .nk-block -->
                <div class="nk-block">
                    <div class="nk-news card card-bordered">
                        <div class="card-inner-group">
                            <div class="card-inner">
                                <div class="line-chart " style="max-height: 300px;">
                                    <canvas id="statisticsChart2"></canvas>
                                </div>
                                <div id="myChartLegend"></div>
                            </div>
                            <div class="card-inner">
                                <ul class="nk-iv-wg3-nav">
                                    <li>
                                        <a href="<?php echo e(route('v2.user.transactions')); ?>"><em
                                                class="icon ni ni-notes-alt"></em>
                                            <span>Go to Transactions</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><em class="icon ni ni-users"></em>
                                            <span>My Referrals</span></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('v2.funds_transfer')); ?>"><em class="icon ni ni-send"></em>
                                            <span>Send Money</span></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('v2.ticket')); ?>"><em class="icon ni ni-headphone"></em>
                                            <span>Support Ticket</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- .card -->
                </div><!-- .nk-block -->
                <div class="nk-block">
                    <div class="row gy-gs">
                        <div class="col-md-6 col-lg-4">
                            <div class="nk-wg-card is-dark card card-bordered">
                                <div class="card-inner">
                                    <div class="nk-iv-wg2">
                                        <div class="nk-iv-wg2-title">
                                            <h6 class="title"><?php echo e(__('Portfolio Balance')); ?> <em
                                                    class="icon ni ni-info"></em></h6>
                                        </div>
                                        <div class="nk-iv-wg2-text">
                                            <div class="nk-iv-wg2-amount amount-reduce-size" id="amount"
                                                data-amount="<?php echo e($user->wallet); ?>">
                                                <?php echo e($settings->currency == 'USD' ? '$' : $settings->currency); ?>


                                                <?php echo e(number_format(round($user->wallet, 2), 2)); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- .card -->
                        </div><!-- .col -->
                        <div class="col-md-6 col-lg-4">
                            <div class="nk-wg-card is-s1 card card-bordered">
                                <div class="card-inner">
                                    <div class="nk-iv-wg2">
                                        <div class="nk-iv-wg2-title">
                                            <h6 class="title">Total Invested <em class="icon ni ni-info"></em></h6>
                                        </div>
                                        <div class="nk-iv-wg2-text">
                                            <div class="nk-iv-wg2-amount amount-reduce-size" id="totalInvested"
                                                data-amount="<?php echo e($totalInvested); ?>">
                                                <?php echo e($settings->currency == 'USD' ? '$' : $settings->currency); ?>


                                                <?php echo e(number_format(round($totalInvested, 2), 2)); ?> <span
                                                    class="change up"><span class="sign"></span>2.8%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- .card -->
                        </div><!-- .col -->
                        <div class="col-md-12 col-lg-4">
                            <div class="nk-wg-card is-s3 card card-bordered">
                                <div class="card-inner">
                                    <div class="nk-iv-wg2">
                                        <div class="nk-iv-wg2-title">
                                            <h6 class="title">Bitcoin Balance <em class="icon ni ni-info"></em></h6>
                                        </div>
                                        <div class="nk-iv-wg2-text">
                                            <div class="nk-iv-wg2-amount amount-reduce-size">
                                                <?php echo e(toBtc(round($user->wallet, 2))); ?><em class="icon ni ni-bitcoin"></em>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- .card -->
                        </div><!-- .col -->
                    </div><!-- .row -->
                </div><!-- .nk-block -->
                <div class="nk-block">
                    <div class="row gy-gs">
                        <div class="col-md-6 col-lg-4">
                            <div class="nk-wg-card card card-bordered h-100">
                                <div class="card-inner h-100">
                                    <div class="nk-iv-wg2">
                                        <div class="nk-iv-wg2-title">
                                            <h6 class="title">Balance in Account</h6>
                                        </div>
                                        <div class="nk-iv-wg2-text">
                                            <div class="nk-iv-wg2-amount ui-v2"><?php echo e($settings->currency); ?>

                                                <?php echo e(number_format(round($user->wallet, 2), 2)); ?>

                                            </div>
                                            <ul class="nk-iv-wg2-list">
                                                <li>
                                                    <span class="item-label">Wallet Balance</span>
                                                    <span class="item-value"><?php echo e($settings->currency); ?>

                                                        <?php echo e(number_format(round($user->wallet, 2))); ?></span>
                                                </li>
                                                <li>
                                                    <span class="item-label">Unclaimed Profit</span>
                                                    <span class="item-value"><?php echo e($settings->currency); ?>

                                                        <?php echo e(number_format(round($currentEarning, 2))); ?></span>
                                                </li>
                                                <li>
                                                    <span class="item-label">Affilate Earnings</span>
                                                    <span class="item-value"><?php echo e($settings->currency); ?>

                                                        <?php echo e(number_format(round($user->ref_bal, 2))); ?></span>
                                                </li>
                                                <li>
                                                    <span class="item-label">Deposits</span>
                                                    <span class="item-value"> <?php echo e($deposits->count()); ?></span>
                                                </li>
                                                <li>
                                                    <span class="item-label">Withdrawals</span>
                                                    <span class="item-value"><?php echo e($wd->count()); ?></span>
                                                </li>
                                                <li class="total">
                                                    <span class="item-label">Total Balance</span>
                                                    <span class="item-value"><?php echo e($settings->currency); ?>

                                                        <?php echo e(number_format(round($user->wallet, 2) + round($currentEarning, 2) + round($user->ref_bal, 2))); ?></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="nk-iv-wg2-cta">
                                            <a href="#" class="btn btn-primary btn-lg btn-block" data-toggle="modal"
                                                data-target="#modalDefault">Withdraw Funds</a>
                                            <a href="<?php echo e(route('v2.user.deposit')); ?>"
                                                class="btn btn-trans btn-block">Deposit Funds</a>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- .card -->
                        </div><!-- .col -->
                        <div class="col-md-6 col-lg-4">
                            <div class="nk-wg-card card card-bordered h-100">
                                <div class="card-inner h-100">
                                    <div class="nk-iv-wg2">
                                        <div class="nk-iv-wg2-title">
                                            <h6 class="title">Refferal Earning <em
                                                    class="icon ni ni-info text-primary"></em></h6>
                                        </div>
                                        <div class="nk-iv-wg2-text">
                                            <div class="nk-iv-wg2-amount ui-v2"><?php echo e($settings->currency); ?>

                                                <?php echo e(number_format(round($user->ref_bal, 2), 2)); ?> <span
                                                    class="change up">
                                            </div>
                                            <ul class="nk-iv-wg2-list">
                                                <?php
$refs = App\User::where('referal', $user->username)
    ->orderby('id', 'desc')
    ->get();
                                                    ?>
                                                <li>
                                                    <span class="item-label">Number of Referals</span>
                                                    <span class="item-value"><?php echo e($refs->count()); ?></span>
                                                </li>
                                                <li>
                                                    <span class="item-label">Affilate Earnings</span>
                                                    <span class="item-value"><?php echo e($settings->currency); ?>

                                                        <?php echo e(number_format(round($user->ref_bal, 2))); ?></span>
                                                </li>
                                                <li>
                                                    <span class="item-label">Investment Earnings</span>
                                                    <span class="item-value"><?php echo e($settings->currency); ?>

                                                        <?php echo e(number_format(round($currentEarning, 2))); ?></span>
                                                </li>
                                                <li>
                                                    <span class="item-label">Rewards</span>
                                                    <span class="item-value"><?php echo e($settings->currency); ?> 0</span>
                                                </li>
                                                <li class="total">
                                                    <span class="item-label">Total Profit</span>
                                                    <span class="item-value"><?php echo e($settings->currency); ?>

                                                        <?php echo e(number_format(round($user->ref_bal, 2) + round($currentEarning, 2))); ?></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="nk-iv-wg2-cta">
                                            <a href="<?php echo e(route('v2.investments.plan')); ?>"
                                                class="btn btn-primary btn-lg btn-block">Invest & Earn</a>
                                            <div class="cta-extra">Write a <a href="<?php echo e(route('v2.ticket')); ?>"
                                                    class="link link-dark">Support Ticket</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- .card -->
                        </div><!-- .col -->
                        <div class="col-md-12 col-lg-4">
                            <div class="nk-wg-card card card-bordered h-100">
                                <div class="card-inner h-100">
                                    <div class="nk-iv-wg2">
                                        <div class="nk-iv-wg2-title">
                                            <h6 class="title">My Investments</h6>
                                        </div>
                                        <div class="nk-iv-wg2-text">
                                            <div class="nk-iv-wg2-amount ui-v2"><?php echo e($runInv->count()); ?><span
                                                    class="sub"></span> Active</div>
                                            <ul class="nk-iv-wg2-list">
                                                <?php if(!$runInv->count() == 0): ?>
                                                                                            <?php $__currentLoopData = $runInv->take(7); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $inv): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                                                                        <?php

                                                                                                $totalElapse = getDays(date('Y-m-d'), $inv->end_date);
                                                                                                $lastWD = $inv->last_wd;
                                                                                                $enddate = date('Y-m-d');
                                                                                                $Edays = getDays($lastWD, $enddate);
                                                                                                $ern = $Edays * $inv->interest * $inv->capital;
                                                                                                $withdrawable = 0;
                                                                                                if ($Edays >= $inv->days_interval) {
                                                                                                    $withdrawable = $inv->days_interval * $inv->interest * $inv->capital;
                                                                                                }

                                                                                                $totalDays = getDays($inv->date_invested, date('Y-m-d'));
                                                                                                $date_invested = \Carbon\Carbon::createFromFormat('Y-m-d', $inv->date_invested);
                                                                                                $ended = 'No';

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    ?>
                                                                                                                                        <li>
                                                                                                                                            <span class="item-label"><a
                                                                                                                                                    href="<?php echo e(route('v2.investments.single', $inv->id)); ?>"><?php echo e(substr($inv->package->package_name, 0, 7)); ?>..</a>
                                                                                                                                                <small>-<?php echo e($inv->interest); ?>% daily,
                                                                                                                                                    <?php echo e($totalDays); ?> Days
                                                                                                                                                    <?php echo e(substr('Elapsed', 0, 5)); ?>.. </small></span>
                                                                                                                                            <span
                                                                                                                                                class="item-value"><?php echo e($date_invested->diffForHumans()); ?></span>
                                                                                                                                        </li>
                                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php else: ?>
                                                    <div class="nk-iv-wg2-cta" style="margin-top: 50px;">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 73.19 94.07"
                                                            width="100" height="100">
                                                            <defs>
                                                                <style>
                                                                    .cls-1 {
                                                                        opacity: 0.12;
                                                                    }

                                                                    .cls-2 {
                                                                        fill: #aaa;
                                                                    }

                                                                    .cls-3 {
                                                                        fill: #ebf0f9;
                                                                    }

                                                                    .cls-4 {
                                                                        fill: #fff;
                                                                    }

                                                                    .cls-5 {
                                                                        fill: #c2d0e1;
                                                                    }

                                                                    .cls-6 {
                                                                        fill: #606060;
                                                                    }

                                                                    .cls-7 {
                                                                        fill: #2f55d4;
                                                                    }

                                                                    .cls-8 {
                                                                        fill: #1e388f;
                                                                    }
                                                                </style>
                                                            </defs>
                                                            <title>Placeholder</title>
                                                            <g id="Layer_2" data-name="Layer 2">
                                                                <g id="Layer_1-2" data-name="Layer 1">
                                                                    <g class="cls-1">
                                                                        <path class="cls-2"
                                                                            d="M5.46,25.79c.19-5,2.3-10,6.22-13s9.74-3.7,13.92-1c1.26.81,2.34,1.87,3.61,2.67,4,2.51,9.21,2,13.72.56s8.77-3.69,13.44-4.46,10.1.38,12.66,4.35,1.15,9.84-2.52,12.93c-3.11,2.62-7.69,3.83-9.4,7.52a13,13,0,0,0-.84,5.17c-.13,5-.13,10.38,2.81,14.41A33.25,33.25,0,0,0,64,59.69c3.84,3.44,7,7.91,7.16,13.28a11.14,11.14,0,0,1-1,5c-2.71,5.82-8.89,6.5-14.36,6.09-4.2-.31-8.49-1.11-12.56,0-3,.79-5.56,2.51-8.4,3.66a23,23,0,0,1-13.31,1.19,14.32,14.32,0,0,1-7.28-3.62c-4.08-4.1-4-10.7-3-16.4s2.65-11.64.83-17.13A19.72,19.72,0,0,0,10.3,47.9,35.7,35.7,0,0,1,5.39,26.55Z" />
                                                                    </g>
                                                                    <path class="cls-3"
                                                                        d="M62.9,39V28.14H12.83V71.45a10.1,10.1,0,0,0,10.1,10.1H52.8a10.1,10.1,0,0,0,10.1-10.1V43.14" />
                                                                    <path class="cls-4"
                                                                        d="M63.22,33.38c0,9.4-.42,18.89,0,28.28-1.4-2-3.57-3.42-5-5.43-3.87-5.45-4.37-16.09-.42-21.58C59.13,32.86,61.27,33.38,63.22,33.38Z" />
                                                                    <line class="cls-5" x1="4.42" y1="62.3" x2="4.12"
                                                                        y2="64.44" />
                                                                    <path class="cls-5"
                                                                        d="M4.33,64.92a.52.52,0,0,0,.31-.41l.3-2.14a.52.52,0,1,0-1-.14l-.3,2.14a.52.52,0,0,0,.73.55Z" />
                                                                    <line class="cls-5" x1="3.2" y1="63.22" x2="5.34"
                                                                        y2="63.52" />
                                                                    <path class="cls-5"
                                                                        d="M5.54,64a.52.52,0,0,0-.14-1l-2.14-.3a.52.52,0,1,0-.14,1l2.14.3A.52.52,0,0,0,5.54,64Z" />
                                                                    <path class="cls-5"
                                                                        d="M.12,51.95A1.44,1.44,0,1,1,2,52.7,1.44,1.44,0,0,1,.12,51.95Zm.72-.31a.65.65,0,1,0,.34-.86A.66.66,0,0,0,.84,51.64Z" />
                                                                    <circle class="cls-5" cx="3.31" cy="70.82" r="0.79"
                                                                        transform="translate(-27.92 7.17) rotate(-23.46)" />
                                                                    <line class="cls-5" x1="69.58" y1="47.2" x2="68.46"
                                                                        y2="49.05" />
                                                                    <path class="cls-5"
                                                                        d="M68.46,49.57a.52.52,0,0,0,.45-.25L70,47.48a.52.52,0,1,0-.89-.54L68,48.78a.52.52,0,0,0,.45.8Z" />
                                                                    <line class="cls-5" x1="68.1" y1="47.56" x2="69.94"
                                                                        y2="48.69" />
                                                                    <path class="cls-5"
                                                                        d="M69.94,49.21a.52.52,0,0,0,.27-1l-1.84-1.12a.52.52,0,0,0-.54.89l1.84,1.12A.52.52,0,0,0,69.94,49.21Z" />
                                                                    <path class="cls-5"
                                                                        d="M66.62,38.09a1.44,1.44,0,1,1,1.44,1.44A1.44,1.44,0,0,1,66.62,38.09Zm.79,0a.65.65,0,1,0,.65-.65A.66.66,0,0,0,67.41,38.09Z" />
                                                                    <circle class="cls-5" cx="69.37" cy="58.77" r="0.79" />
                                                                    <path class="cls-5"
                                                                        d="M72.45,65.9a.6.6,0,0,0-1,0l-.33.57-.33.57a.6.6,0,0,0,.52.89h1.31A.6.6,0,0,0,73.1,67l-.33-.57Z" />
                                                                    <line class="cls-5" x1="26.78" y1="4.14" x2="28.62"
                                                                        y2="3.02" />
                                                                    <path class="cls-5"
                                                                        d="M29.15,3a.52.52,0,0,1-.25.45L27.05,4.59a.52.52,0,1,1-.54-.89l1.84-1.12a.52.52,0,0,1,.8.45Z" />
                                                                    <line class="cls-5" x1="27.14" y1="2.66" x2="28.26"
                                                                        y2="4.5" />
                                                                    <path class="cls-5"
                                                                        d="M28.79,4.5a.52.52,0,0,1-1,.27L26.69,2.93a.52.52,0,0,1,.89-.54l1.12,1.84A.52.52,0,0,1,28.79,4.5Z" />
                                                                    <path class="cls-5"
                                                                        d="M11.78,3.67a1.44,1.44,0,1,1-1.44-1.44A1.44,1.44,0,0,1,11.78,3.67Zm-2.09,0A.65.65,0,1,0,10.34,3,.66.66,0,0,0,9.68,3.67Z" />
                                                                    <circle class="cls-5" cx="44.63" cy="3.93" r="0.79" />
                                                                    <path class="cls-5"
                                                                        d="M57,1.77a.6.6,0,0,1,0-1l.57-.33.57-.33A.6.6,0,0,1,59,.6V1.91a.6.6,0,0,1-.89.52l-.57-.33Z" />
                                                                    <rect class="cls-5" x="12.95" y="29.19" width="50.26"
                                                                        height="4.19" />
                                                                    <path class="cls-5"
                                                                        d="M62.92,66l0,8.84c0,3.76-4.45,6.8-9.9,6.77l-29.15-.15c-5.45,0-9.85-3.11-9.83-6.87V71.76a56.36,56.36,0,0,0,10.76,2.48c7,.88,14.5.24,20.5-2.37,2.5-1.09,4.68-2.5,7.22-3.54A36.22,36.22,0,0,1,62.92,66Z" />
                                                                    <path class="cls-6"
                                                                        d="M22.93,82.55a11.11,11.11,0,0,1-11.1-11.1V61.13a1,1,0,0,1,2,0V71.45a9.11,9.11,0,0,0,9.1,9.1,1,1,0,0,1,0,2Z" />
                                                                    <path class="cls-6"
                                                                        d="M45.6,82.55H29.24a1,1,0,1,1,0-2H45.6a1,1,0,0,1,0,2Z" />
                                                                    <path class="cls-6"
                                                                        d="M52.8,82.55H49.42a1,1,0,1,1,0-2H52.8a9.11,9.11,0,0,0,9.1-9.1V43.14a1,1,0,0,1,2,0V71.45A11.11,11.11,0,0,1,52.8,82.55Z" />
                                                                    <path class="cls-6"
                                                                        d="M12.83,55.84a1,1,0,0,1-1-1V27.14H63.9V39a1,1,0,1,1-2,0V29.14H13.83v25.7A1,1,0,0,1,12.83,55.84Z" />
                                                                    <path class="cls-6"
                                                                        d="M48.15,21.94l-6.61-8.78a3.78,3.78,0,0,0-6.47,0l-5.39,7.16-1.6-1.2L33.47,12a5.77,5.77,0,0,1,9.66,0l6.61,8.78Z" />
                                                                    <path class="cls-6"
                                                                        d="M57.33,41.71H20.5a1,1,0,0,1,0-2H57.33a1,1,0,0,1,0,2Z" />
                                                                    <path class="cls-6"
                                                                        d="M32.9,51.13H26.78a1,1,0,1,1,0-2H32.9a1,1,0,0,1,0,2Z" />
                                                                    <path class="cls-6"
                                                                        d="M51.7,59.51H45.58a1,1,0,0,1,0-2H51.7a1,1,0,0,1,0,2Z" />
                                                                    <path class="cls-6"
                                                                        d="M51.7,51.13H36.53a1,1,0,0,1,0-2H51.7a1,1,0,0,1,0,2Z" />
                                                                    <path class="cls-6"
                                                                        d="M42,59.51H26.78a1,1,0,1,1,0-2H42a1,1,0,0,1,0,2Z" />
                                                                    <path class="cls-3"
                                                                        d="M54.5,29.19h9.86A5.25,5.25,0,0,0,69.59,24h0a5.25,5.25,0,0,0-5.24-5.24H13.47A5.25,5.25,0,0,0,8.23,24h0a5.25,5.25,0,0,0,5.24,5.24h41Z" />
                                                                    <path class="cls-5"
                                                                        d="M69.28,25.73a5.26,5.26,0,0,1-4.92,3.46H13.47A5.26,5.26,0,0,1,8.23,24,5.26,5.26,0,0,1,11.69,19a5.26,5.26,0,0,0-.31,1.78A5.26,5.26,0,0,0,16.61,26H67.5A5.25,5.25,0,0,0,69.28,25.73Z" />
                                                                    <rect class="cls-6" x="36.75" y="81.55" width="2"
                                                                        height="8.38" />
                                                                    <path class="cls-6"
                                                                        d="M50.47,30.19h-37a6.24,6.24,0,1,1,0-12.47h3.67v2H13.47a4.24,4.24,0,1,0,0,8.47h37Z" />
                                                                    <path class="cls-6"
                                                                        d="M64.36,30.19H54.5v-2h9.86a4.24,4.24,0,0,0,0-8.47h-43v-2h43a6.24,6.24,0,0,1,0,12.47Z" />
                                                                    <circle class="cls-7" cx="38.61" cy="10.86" r="3.67" />
                                                                    <path class="cls-8"
                                                                        d="M42.27,10.86a3.67,3.67,0,0,1-7.33,0,3.48,3.48,0,0,1,.16-1,3.66,3.66,0,0,0,7,0A3.48,3.48,0,0,1,42.27,10.86Z" />
                                                                    <path class="cls-6"
                                                                        d="M38.61,15.53a4.67,4.67,0,1,1,4.67-4.67A4.67,4.67,0,0,1,38.61,15.53Zm0-7.33a2.67,2.67,0,1,0,2.67,2.67A2.67,2.67,0,0,0,38.61,8.2Z" />
                                                                    <circle class="cls-5" cx="37.56" cy="90.45" r="2.62" />
                                                                    <path class="cls-6"
                                                                        d="M37.56,94.07a3.62,3.62,0,1,1,3.62-3.62A3.62,3.62,0,0,1,37.56,94.07Zm0-5.24a1.62,1.62,0,1,0,1.62,1.62A1.62,1.62,0,0,0,37.56,88.83Z" />
                                                                    <path class="cls-4"
                                                                        d="M64.26,23.91H58.79a1,1,0,0,1,0-2h5.47a1,1,0,0,1,0,2Z" />
                                                                    <path class="cls-4"
                                                                        d="M54.07,23.91H51.7a1,1,0,0,1,0-2h2.37a1,1,0,0,1,0,2Z" />
                                                                </g>
                                                            </g>
                                                        </svg>
                                                        <p>No Active Investments Yet.</p>
                                                    </div>

                                                <?php endif; ?>

                                            </ul>
                                        </div>
                                        <div class="nk-iv-wg2-cta">
                                            <a href="<?php echo e(route('v2.investments')); ?>"
                                                class="btn btn-light btn-lg btn-block">See all Investment</a>
                                            <div class="cta-extra">Send Money to <a
                                                    href="<?php echo e(route('v2.funds_transfer')); ?>"
                                                    class="link link-dark">Friend & Family</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- .card -->
                        </div><!-- .col -->
                    </div><!-- .row -->
                </div><!-- .nk-block -->
                <div class="nk-block">
                    <div class="card card-bordered">
                        <div class="nk-refwg">
                            <div class="nk-refwg-invite card-inner">
                                <div class="nk-refwg-head g-3">
                                    <div class="nk-refwg-title">
                                        <h5 class="title">Refer Us & Earn</h5>
                                        <div class="title-sub">Use the bellow link to invite your friends.</div>
                                    </div>
                                    
                                </div>
                                <div class="nk-refwg-url">
                                    <div class="form-control-wrap">
                                        <div class="form-clip clipboard-init" data-clipboard-target="#refUrl"
                                            data-success="Copied" data-text="Copy Link"><em
                                                class="clipboard-icon icon ni ni-copy"></em> <span
                                                class="clipboard-text">Copy Link</span></div>
                                        <div class="form-icon">
                                            <em class="icon ni ni-link-alt"></em>
                                        </div>
                                        <input type="text" class="form-control copy-text" id="refUrl"
                                            value="<?php echo e(request()->getHttpHost() . __('/register/') . $user->username); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="nk-refwg-stats card-inner bg-lighter">
                                <div class="nk-refwg-group g-3">
                                    <div class="nk-refwg-name">
                                        <h6 class="title">My Referral <em class="icon ni ni-info" data-toggle="tooltip"
                                                data-placement="right" title="Referral Informations"></em></h6>
                                    </div>
                                    <div class="nk-refwg-info g-3">
                                        <div class="nk-refwg-sub">
                                            <div class="title"><?php echo e($refs->count()); ?></div>
                                            <div class="sub-text">Total Joined</div>
                                        </div>
                                        <div class="nk-refwg-sub">
                                            <div class="title"><?php echo e($settings->currency); ?>

                                                <?php echo e(number_format(round($user->ref_bal, 2), 2)); ?>

                                            </div>
                                            <div class="sub-text">Referral Earn</div>
                                        </div>

                                    </div>

                                </div>
                                <div class="nk-refwg-ck">
                                    <canvas class="chart-refer-stats" id="refBarChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div><!-- .card -->
                </div><!-- .nk-block -->
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
    var inv_dates = []
    var inv_vals = []

    var inv = '<?php echo json_encode($myInv); ?>';
    var js_inv = JSON.parse(inv);

    // Sort investments by date
    js_inv.sort((a, b) => new Date(a.date_invested) - new Date(b.date_invested));

    $.each(js_inv, function (k, val) {
        var dt = moment(new Date(val['date_invested'])).format('MM/YY');
        inv_dates[k] = dt;
        inv_vals[k] = val['capital'];
    });

    var dep_vals = []
    var dep_dates = []
    var deposits = JSON.parse('<?php echo json_encode($deposits); ?>');

    // Sort deposits by date
    deposits.sort((a, b) => new Date(a.created_at) - new Date(b.created_at));

    $.each(deposits, function (k, val) {
        var dt = moment(new Date(val['created_at'])).format('MM/YY');
        dep_dates[k] = dt;
        dep_vals[k] = val['amount'];
    });

    var wd_vals = []
    var wd_dates = []
    var wd = JSON.parse('<?php echo json_encode($wd); ?>');

    // Sort withdrawals by date
    wd.sort((a, b) => new Date(a.w_date) - new Date(b.w_date));

    $.each(wd, function (k, val) {
        var dt = moment(new Date(val['w_date'])).format('MM/YY');
        wd_dates[k] = dt;
        wd_vals[k] = val['amount'];
    });

    // Combine all dates and sort them uniquely
    var all_dates = [...new Set([...inv_dates, ...dep_dates, ...wd_dates])].sort((a, b) => {
        return moment(a, 'MM/YY').valueOf() - moment(b, 'MM/YY').valueOf();
    });

    // Function to normalize data points
    function normalizeDataPoints(dates, values, all_dates) {
        let normalized = new Array(all_dates.length).fill(0);
        dates.forEach((date, index) => {
            let dateIndex = all_dates.indexOf(date);
            if (dateIndex !== -1) {
                normalized[dateIndex] = values[index];
            }
        });
        return normalized;
    }

    // Normalize all datasets to have values for all dates
    var normalized_inv_vals = normalizeDataPoints(inv_dates, inv_vals, all_dates);
    var normalized_dep_vals = normalizeDataPoints(dep_dates, dep_vals, all_dates);
    var normalized_wd_vals = normalizeDataPoints(wd_dates, wd_vals, all_dates);

    var ctx2 = document.getElementById('statisticsChart2').getContext('2d');

    var StatisticsChart2 = new Chart(ctx2, {
        type: 'line',
        data: {
            labels: all_dates,
            datasets: [{
                label: "Investment Analytics",
                borderColor: '#6576ff',
                pointBackgroundColor: '#ffffff',
                pointBorderColor: '#6576ff',
                pointRadius: 0,
                pointHoverRadius: 6,
                pointBorderWidth: 2,
                legendColor: '#6576ff',
                backgroundColor: (ctx) => {
                    const gradient = ctx2.createLinearGradient(0, 0, 0, ctx2.canvas.height);
                    gradient.addColorStop(0, 'rgba(101, 118, 255, 0.15)');
                    gradient.addColorStop(1, 'rgba(101, 118, 255, 0)');
                    return gradient;
                },
                fill: true,
                borderWidth: 2,
                cubicInterpolationMode: 'monotone',
                tension: 0.3,
                data: normalized_inv_vals,
                order: 1
            },
            {
                label: "Deposit Analytics",
                borderColor: '#1ee0ac',
                pointBackgroundColor: '#ffffff',
                pointBorderColor: '#1ee0ac',
                pointRadius: 0,
                pointHoverRadius: 6,
                pointBorderWidth: 2,
                backgroundColor: (ctx) => {
                    const gradient = ctx2.createLinearGradient(0, 0, 0, ctx2.canvas.height);
                    gradient.addColorStop(0, 'rgba(30, 224, 172, 0.15)');
                    gradient.addColorStop(1, 'rgba(30, 224, 172, 0)');
                    return gradient;
                },
                legendColor: '#1ee0ac',
                fill: true,
                borderWidth: 2,
                cubicInterpolationMode: 'monotone',
                tension: 0.3,
                data: normalized_dep_vals,
                order: 2
            },
            {
                label: "Withdrawal Analytics",
                borderColor: '#f4bd0e',
                pointBackgroundColor: '#ffffff',
                pointBorderColor: '#f4bd0e',
                pointRadius: 0,
                pointHoverRadius: 6,
                pointBorderWidth: 2,
                legendColor: '#f4bd0e',
                backgroundColor: (ctx) => {
                    const gradient = ctx2.createLinearGradient(0, 0, 0, ctx2.canvas.height);
                    gradient.addColorStop(0, 'rgba(244, 189, 14, 0.15)');
                    gradient.addColorStop(1, 'rgba(244, 189, 14, 0)');
                    return gradient;
                },
                fill: true,
                borderWidth: 2,
                cubicInterpolationMode: 'monotone',
                tension: 0.3,
                data: normalized_wd_vals,
                order: 3
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            aspectRatio: 2,
            animation: {
                duration: 1000,
                easing: 'easeInOutQuart',
                delay: function (context) {
                    return context.dataIndex * 100 + context.datasetIndex * 100;
                }
            },
            layout: {
                padding: {
                    left: 0,
                    right: 20,
                    top: 40,
                    bottom: 0
                }
            },
            plugins: {
                datalabels: {
                    backgroundColor: function (context) {
                        return context.dataset.borderColor;
                    },
                    borderRadius: 4,
                    color: 'white',
                    font: {
                        weight: 'bold',
                        size: 10
                    },
                    padding: 6,
                    formatter: function (value, context) {
                        if (!value) return '';
                        if (value >= 1000000) {
                            return '$' + (value / 1000000).toFixed(1) + 'M';
                        } else if (value >= 1000) {
                            return '$' + (value / 1000).toFixed(1) + 'K';
                        }
                        return value > 0 ? '$' + value.toFixed(0) : '';
                    },
                    display: function (context) {
                        // Only show for last point or significant changes
                        return context.dataIndex === context.dataset.data.length - 1 ||
                            (context.dataIndex > 0 &&
                                context.dataset.data[context.dataIndex] > context.dataset.data[context.dataIndex - 1] * 1.5);
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: true,
                        beginAtZero: true,
                        maxTicksLimit: 5,
                        padding: 10,
                        fontColor: "#8094ae",
                        callback: function (value) {
                            if (!value) return '0';
                            if (value >= 1000000) return '$' + (value / 1000000).toFixed(1) + 'M';
                            if (value >= 1000) return '$' + (value / 1000).toFixed(1) + 'K';
                            return '$' + value;
                        }
                    },
                    gridLines: {
                        drawBorder: false,
                        display: true,
                        color: "rgba(136, 142, 168, 0.07)",
                        zeroLineColor: "rgba(136, 142, 168, 0.07)",
                        drawTicks: false
                    }
                }],
                xAxes: [{
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        display: true,
                        padding: 10,
                        fontColor: "#8094ae",
                        fontSize: 11,
                        callback: function (value) {
                            return moment(value, 'MM/YY').format('MMM YY');
                        }
                    }
                }]
            },
            tooltips: {
                enabled: true,
                mode: 'index',
                intersect: false,
                position: 'nearest',
                callbacks: {
                    label: function (tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';
                        if (label) {
                            label += ': ';
                        }
                        var value = Number(tooltipItem.yLabel);
                        if (!value) return label + '$0';
                        if (value >= 1000000) {
                            value = (value / 1000000).toFixed(2) + 'M';
                        } else if (value >= 1000) {
                            value = (value / 1000).toFixed(2) + 'K';
                        } else {
                            value = value.toFixed(2);
                        }
                        label += '$' + value;
                        return label;
                    },
                    title: function (tooltipItems) {
                        return moment(tooltipItems[0].xLabel, 'MM/YY').format('MMMM YYYY');
                    }
                },
                backgroundColor: 'rgba(28, 43, 70, 0.95)',
                titleFontSize: 13,
                titleFontColor: '#fff',
                titleMarginBottom: 6,
                bodyFontColor: '#fff',
                bodyFontSize: 12,
                bodySpacing: 4,
                yPadding: 10,
                xPadding: 10,
                footerMarginTop: 0,
                displayColors: false
            },
            hover: {
                mode: 'index',
                intersect: false,
                animationDuration: 400
            },
            legend: {
                display: true,
                position: 'top',
                align: 'end',
                labels: {
                    usePointStyle: true,
                    padding: 20,
                    fontColor: '#8094ae',
                    fontSize: 12,
                    boxWidth: 8
                }
            },
            elements: {
                line: {
                    tension: 0.3
                },
                point: {
                    borderWidth: 2,
                    hoverRadius: 6,
                    hoverBorderWidth: 2
                }
            }
        }
    });

    // Enhanced summary stats with trends
    function addChartSummary() {
        var container = ctx2.parentElement;
        var summaryDiv = document.createElement('div');
        summaryDiv.style.display = 'flex';
        summaryDiv.style.justifyContent = 'space-between';
        summaryDiv.style.marginBottom = '20px';
        summaryDiv.style.padding = '15px';
        summaryDiv.style.background = 'rgba(255, 255, 255, 0.5)';
        summaryDiv.style.borderRadius = '8px';
        summaryDiv.style.boxShadow = '0 1px 3px rgba(0,0,0,0.1)';

        // Calculate totals and trends
        var totalInv = inv_vals.reduce((a, b) => a + b, 0);
        var totalDep = dep_vals.reduce((a, b) => a + b, 0);
        var totalWd = wd_vals.reduce((a, b) => a + b, 0);

        // Calculate month-over-month changes
        function getMonthChange(values) {
            if (values.length < 2) return 0;
            var lastMonth = values[values.length - 1] || 0;
            var prevMonth = values[values.length - 2] || 0;
            return prevMonth ? ((lastMonth - prevMonth) / prevMonth) * 100 : 0;
        }

        var summaryItems = [
            {
                label: 'Total Investment',
                value: totalInv,
                color: '#6576ff',
                change: getMonthChange(inv_vals)
            },
            {
                label: 'Total Deposits',
                value: totalDep,
                color: '#1ee0ac',
                change: getMonthChange(dep_vals)
            },
            {
                label: 'Total Withdrawals',
                value: totalWd,
                color: '#f4bd0e',
                change: getMonthChange(wd_vals)
            }
        ];

        summaryItems.forEach(item => {
            var itemDiv = document.createElement('div');
            itemDiv.style.textAlign = 'center';
            itemDiv.style.flex = '1';
            itemDiv.style.padding = '10px';

            var value = item.value >= 1000000
                ? '$' + (item.value / 1000000).toFixed(1) + 'M'
                : item.value >= 1000
                    ? '$' + (item.value / 1000).toFixed(1) + 'K'
                    : '$' + item.value.toFixed(0);

            var changeText = item.change.toFixed(1);
            var changeColor = item.change > 0 ? '#1ee0ac' : item.change < 0 ? '#e85347' : '#8094ae';
            var changeIcon = item.change > 0 ? '↑' : item.change < 0 ? '↓' : '−';

            itemDiv.innerHTML = `
                <div style="color: ${item.color}; font-size: 24px; font-weight: bold; margin-bottom: 5px;">${value}</div>
                <div style="color: #8094ae; font-size: 13px; margin-bottom: 5px;">${item.label}</div>
                <div style="color: ${changeColor}; font-size: 12px;">
                    ${changeIcon} ${Math.abs(changeText)}% from last month
                </div>
            `;
            summaryDiv.appendChild(itemDiv);
        });

        // Remove existing summary if any
        var existingSummary = container.parentElement.querySelector('.chart-summary');
        if (existingSummary) {
            existingSummary.remove();
        }

        summaryDiv.className = 'chart-summary';
        container.parentElement.insertBefore(summaryDiv, container);
    }

    // Call the summary function after chart is created
    addChartSummary();

    // Update both chart and summary on resize
    window.addEventListener('resize', function () {
        updateChartHeight();
        addChartSummary();
    });

    //Withdrawal Modal Js Code
    $('#withClass').on('change', function () {

        var url = "<?php echo e(route('v2.add.with.method')); ?>"
        console.log(url);
        var $option = $(this).find('option:selected');
        console.log($option.val());
        if ($option.val() == 'add') {
            console.log('works');
            window.location = url;
        }

    })
</script>


<script>
    var amountElement = document.getElementById("amount");
    var amountInvestedElement = document.getElementById("totalInvested");
    var amountClass = document.getElementsByClassName('amount-reduce-size');
    var amount = amountElement.getAttribute('data-amount');
    var amountInvested = amountInvestedElement.getAttribute('data-amount');

    var amountLength = amount.length;
    amountInvested = amountInvested.length;

    if (amountLength > 6 || amountInvested > 6) {

        for (var i = 0; i < amountClass.length; i++) {
            amountClass[i].style.fontSize = "24px"; // Changes the text color

        }
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('v2.layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/mazibuckler/apps/sites/hyip/resources/views/v2/index.blade.php ENDPATH**/ ?>