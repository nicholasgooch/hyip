<div class="col-lg-7 col-md-6 relative d-flex justify-content-center" style="position: relative;  "  id="">
                    <div class="ml-5 mr-5 " style="position: absolute; top: 40%; width: 50% !important;">
                        <div class="tiny-single-item w-64" style="font-weight: bolder;">
                            <div class="tiny-slide ">
                                <div class="client-testi">
                                    
                                    <p class="text-white mt-4">"Diversification is protection against ignorance. It makes
                                        very little sense for those who know what they are doing "</p>
                                    <h6 class="text-white">- Warren Buffett</h6>
                                </div>
                            </div>

                            <div class="tiny-slide w-64">
                                <div class="client-testi">
                                    
                                    <p class="text-white mt-4">" The stock market is filled with individuals who know the
                                        price of everything, but the value of nothing "</p>
                                    <h6 class="text-white">- Philip Fisher</h6>
                                </div>
                            </div>

                            <div class="tiny-slide  w-64">
                                <div class="client-testi">
                                    
                                    <p class="text-white mt-4">" Investing should be more like watching paint dry or
                                        watching grass grow. If you want excitement, take $800 and go to Las Vegas "</p>
                                    <h6 class="text-white">- Paul Samuelson</h6>
                                </div>
                            </div>

                            <div class="tiny-slide w-64">
                                <div class="client-testi">
                                    
                                    <p class="text-white mt-4">" Do not be fearful or negative too often. For those who
                                        invest, optimists have historically been the winners "</p>
                                    <h6 class="text-white">- John Templeton</h6>
                                </div>
                            </div>
                            =
                        </div>
                    </div>
                    <div class="area relative z-0">
                        <ul class="circles">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>
                </div>
<?php /**PATH /Users/mazibuckler/apps/sites/hyip/resources/views/auth/partials/side-hero-slider.blade.php ENDPATH**/ ?>