<?php
use App\deposits;
use App\packages;
use App\site_settings as settings;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\ForgotPasswordController;

Route::prefix("app")->group(function () {
    Route::get('/overview', 'v2\UserController@index')->name('v2.index');
    Route::get('/affiliates/ref', 'v2\UserController@affiliates')->name('v2.affiliates');
    Route::get('/ticket', 'v2\UserController@ticket')->name('v2.ticket');
    Route::get('/ticket/open/{id}', 'v2\UserController@ticket_open')->name('v2.ticket.open');
    Route::get('/ticket/load/chat', 'v2\UserController@load_chat')->name('v2.load_chat');
    Route::get('ticket/close/{id}/it', 'v2\UserController@close_ticket')->name('v2.close.ticket');
    Route::post('ticket/create/new', 'v2\UserController@create_ticket')->name('v2.ticket.create');
    Route::post('/comment', 'v2\UserController@ticket_comment')->name('v2.ticket.comment');

    Route::get('/send_funds/to/user', 'v2\UserController@funds_transfer')->name('v2.funds_transfer');
    Route::post('/user/send/fund', 'v2\UserController@user_send_fund')->name('v2.funds_transfer.post');

    Route::post('/user/change/pwd', 'v2\UserController@changePwd')->name('v2.profile.change_pwd.post');


    Route::post('/user/send/email-password', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('v2.profile.send_email_pwd');
    Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])
        ->name('password.reset');
    Route::post('password/reset', [ResetPasswordController::class, 'reset'])
        ->name('password.update');
    Route::group([
        'prefix' => 'v2',

    ], function () {
        Route::post('/invest/packages', 'v2\InvestmentController@invest');
        Route::post('/user/wallet/wd', 'v2\UserController@user_wallet_wd')->name('wallet.wd');
        Route::get('/plans/v2', 'v2\InvestmentController@investmentPlans')->name('v2.investments.plan');
        Route::get('/my/investments/', 'v2\InvestmentController@index')->name('v2.investments');
        Route::group([
            'prefix' => 'investment',
        ], function () {

            Route::get('/{inv_id}', 'v2\InvestmentController@show')->name('v2.investments.single');

        });

        Route::group([
            'prefix' => 'account',
        ], function () {
            Route::get('/', 'v2\UserController@profile')->name('v2.user.profile');
            Route::get('/settings', 'v2\UserController@settings')->name('v2.user.settings');
            Route::get('/notifications', 'v2\UserController@notifications')->name('v2.user.notifications');
            Route::get('/activities', 'v2\UserController@activities')->name('v2.user.activities');
            Route::get('/add/wallet/bank', 'v2\UserController@addwithmethods')->name('v2.add.with.method');
            Route::post('/user/add/wallet/obj', 'v2\UserController@addWallet')->name('v2.user.add_wallet');
            Route::post('/user/add/bank/obj', 'v2\UserController@addBank')->name('v2.user.add_bank');

        });

        Route::group([
            'prefix' => 'deposit',
        ], function () {
            Route::get('/page', 'v2\UserController@deposit_page')->name('v2.user.deposit');
            Route::get('/funds', 'v2\UserController@deposit_funds')->name('v2.user.deposit.funds');
            Route::post('/funds/xhr', 'v2\UserController@deposit_funds_xhr')->name('v2.user.deposit.funds.xhr');
        });

        Route::get('/transactions', 'v2\UserController@transactions')->name('v2.user.transactions');
        Route::get('/invest/page', 'v2\UserController@invest_page')->name('v2.user.invest.page');
        Route::get('/withdraw', 'v2\UserController@withdraw_page')->name('v2.user.withdraw.page');

        Route::group([
            'prefix' => 'kyc',
        ], function () {
            Route::get('/get/started', 'v2\KycController@kyc_page')->name('v2.kyc.page');
            Route::get('/form', 'v2\KycController@Kyc_form')->name('v2.kyc.form');
            Route::post('/page', 'v2\KycController@Kyc_form_store')->name('v2.kyc.form.post');
            Route::get('/proccessing', 'v2\KycController@kyc_proccessing_page')->name('v2.kyc.proccess');
        });

    });

    Route::group(['prefix' => '2fa'], function () {
        Route::get('/disable', 'v2\LoginSecurityController@show2faDisableForm')->name('v2.2fa_disable');
        Route::get('/', 'v2\LoginSecurityController@show2faForm')->name('v2.2fa.page');
        Route::post('/generateSecret', 'v2\LoginSecurityController@generate2faSecret')->name('generate2faSecret');
        Route::post('/enable2fa', 'v2\LoginSecurityController@enable2fa')->name('enable2fa');
        Route::post('/disable2fa', 'v2\LoginSecurityController@disable2fa')->name('disable2fa');

        // 2fa middleware
        Route::post('/2faVerify', function () {
            return redirect(URL()->previous());
        })->name('2faVerify')->middleware('authenicator2fa');
    });
    //charts
    Route::get('/deposit/chart', function () {
        $user = Auth::user();
        $mydeposits = $user->deposits;

        return response()->json($mydeposits);
    });
    Route::get('get/packages/', function () {
        $pkgs = packages::all();

        return response()->json($pkgs);
    });
    Route::get('get/settings/', function () {
        $settings = settings::first();

        return response()->json($settings);
    });
    Route::get('packages/plan/{plan_id}', function ($plan_id) {
        $pkg = packages::find($plan_id);

        return response()->json($pkg);
    });
    Route::get('get/banks/{user_id}', function ($user_id) {
        $banks = App\banks::where('user_id', $user_id)->get();
        return response()->json($banks);
    });
    Route::get('get/bank/{user_id}/{bank_id}', function ($user_id, $bank_id) {
        $bank = App\banks::where('user_id', $user_id)->where('id', $bank_id)->firstF();
        return response()->json($bank);
    });

});
