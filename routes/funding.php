<?php
    Route::prefix('loan')->group( function () {
        Route::get('/apply', 'FundingController@applicationPage')->name('funding.apply');
        Route::post('/apply', 'FundingController@applicationPost')->name('application.post');

        //api
        Route::get('/get-collateral-types', 'FundingController@getCollateralTypes');
        Route::get('/get-funding-types', 'FundingController@getFundingTypes');
        Route::get('/application/{id}', 'FundingController@getApplication');
        Route::get('/get/settings', 'FundingController@getSettings');
        Route::post('/save/withdraw/method', 'FundingController@saveWithdrawalMethod');
        Route::get('/withdrawal/methods/{type}', 'FundingController@getWithdrawalMethods');
        Route::post('/submit/withdrawal', 'FundingController@submitWithdrawal');
    });

    Route::middleware('funding_applied')->group(function(){
        Route::get('funding/onboarding','FundingController@onboardPage')->name('funding.onboarding');
        Route::get('funding/onboard', 'FundingController@onboard')->name('funding.onboard');

        Route::prefix('loan')->middleware(['funding_onboarded'])->group(function(){
            Route::get('/dashboard', 'FundingController@dashboard')->name('funding.dashboard');

            Route::get('/details', 'FundingController@applicationDetails')->name('funding.apply.details');
            Route::get('/types', 'FundingController@fundingTypesPage')->name('funding.types');
            Route::get('/applications', 'FundingController@applications')->name('funding.applications');
            Route::get('/transactions', 'FundingController@transactions_page')->name('funding.transactions');


            Route::get('/deposit', 'FundingController@deposit')->name('funding.deposit');
            Route::get('/subscription/plans', 'FundingController@subscriptionPlansPage')->name('funding.subscription.plans');

            Route::get('/withdraw', 'FundingController@withdrawPage')->name('funding.withdraw');




        });
    });

?>
