<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
|
 */

// Route::prefix('home')->group(function(){
//     Route::get('/','LandrickController@index')->name('landrick.index');
//     Route::get('/about', 'LandrickController@aboutPage')->name('landrick.about');
//     Route::get('/cloud-minning', 'LandrickController@cloudMinningPage')->name('landrick.cloud-mining');
//     Route::get('/faq','LandrickController@faqPage')->name('landrick.faq');
//     Route::get('/contact', 'LandrickController@contact')->name('landrick.contact');
//     Route::get('/buy-bitcoin', 'LandrickController@buyBitcoinPage')->name('landrick.buy-bitcoin');
//     Route::get('/how-it-works', 'LandrickController@howItWorks')->name('landrick.how-it-works');
//     Route::get('/privacy-policy', 'LandrickController@privacyPolicy')->name('landrick.privacy-policy');
//     Route::get('/terms-condition', 'LandrickController@termsCondition')->name('landrick.terms.condition');
//     Route::get('/private-equity', 'LandrickController@privateEquity')->name('landrick.private.equity');
//     Route::get('/investment-plans', 'LandrickController@InvestmentPlans')->name('landrick.investment');
//     Route::get('/dictionary', 'LandrickController@dictionary')->name('landrick.dictionary');
//     Route::get('/security', 'LandrickController@security')->name('landrick.security');
//     Route::get('/business', 'LandrickController@business')->name('landrick.business');
//     Route::get('/affiliates', 'LandrickController@affiliates')->name('landrick.affiliates');

//     Route::get('/plans/oil-gas', 'LandrickController@oilgasPage')->name('landrick.oil.gas');
//     Route::get('/plans/gold-investments', 'LandrickController@goldInv')->name('landrick.gold.inv');
//     Route::get('/plans/real-estate-investments', 'LandrickController@realEstate')->name('landrick.real.estate');
//     Route::get('/plans/agricultural-infrastructure-investments', 'LandrickController@agriInfra')->name('landrick.agri.infra');
//     Route::get('/plans/forex-investments', 'LandrickController@forex')->name('landrick.forex');
//     Route::get('/plans/crypto-investments', 'LandrickController@crypto')->name('landrick.crypto');

//     //Newsletter
//     Route::post('/newsletter-subscribe', 'NewsletterController@store')->name('newsletter.store');
//     Route::post('/contact-alert', 'NewsletterController@contactEmailAlert')->name('contact.alert');
// });

Route::middleware(['web'])->group(function () {
    Route::get('/session/set', function (Request $request) {
        Session::put('duru', 'This is a test session value.');
        return 'Session value has been set.';
    });

    Route::get('/session/get', function (Request $request) {
        $value = Session::get('duru', 'Default value if not set');
        return 'Session value: ' . $value;
    });

    Route::get('/session/clear', function (Request $request) {
        session()->forget('key');
        return 'Session value has been cleared.';
    });
});
// ///////////////////   User Routes  ///////////////////////
Auth::routes(['except' => ['verification.verify', 'verification.resend', 'logout']]);
Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify')->middleware('signed');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');

// Define a new login route
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/', 'Auth\LoginController@login');

Route::get('login/{provider}', 'Auth\LoginController@redirectToSocial')->name('social.login');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleSocialCallback')->name('social.login.callback');
Route::post('logout/', 'Auth\LoginController@logout')->name('auth.logout');

//2FA
Route::get('verify-otp', 'Auth\TwoFAController@show2faPage')->middleware(['auth', 'verified'])->name('otp.index');
Route::post('verify-otp', 'Auth\TwoFAController@store')->name('otp.store');
Route::post('verify-otp/resend', 'Auth\TwoFAController@resend')->name('otp.resend');

Route::get('preview-notification', function () {
    $markdown = new \Illuminate\Mail\Markdown(view(), config('mail.markdown'));
    $data = "Your data to be use in blade file";
    return $markdown->render("mail.regconfirm", [$data]);
});

Route::get('/user/verify/btc/payment', 'userController@btc_payment_suc');
Route::get('/reset/password/{username}/{token}', 'userController@pwd_req_verify');
Route::get('/registration/confirm/{usr}/{code}', 'Auth@verify_reg')->name('registration.confirm.link');
Route::get('/user/payment/{payAmt}/successful', 'userController@payment_suc')->middleware('auth', );
Route::get('/user/update/readmsg/{id}', 'userController@readmsg_up')->middleware('auth', );
Route::get('/user/get/states/{id}', 'userController@states');
Route::get('/user/get/countryCode/{id}', 'userController@countryCode');
Route::get('/user/remove/bankaccount/{id}', 'userController@deleteBankAccount')->middleware('auth', );
Route::post('/user/wallet/bank_deposit', 'userController@bank_deposit');

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////  admin /////////////////////////////////////////////////////////////////////

Route::prefix('back-end')->group(function () {
    Route::get('/', 'Auth\Admin\LoginController@showLoginForm')->name('admin.login');
    Route::post('/', 'Auth\Admin\LoginController@login')->name('admin.login.submit');
    Route::post('/back-end/logout', 'Auth\Admin\LoginController@logout')->name('admin.logout');

});
Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('/dashboard', 'Admin\DashboardController@index')
        ->name('dashboard');
    Route::get('/manage/users', 'Admin\DashboardController@manageUsers')
        ->name('users');
    Route::get('/view/settings', 'Admin\DashboardController@settingsPage')->name('settings');
    Route::post('/update/site/settings', 'Admin\DashboardController@adminUpdatSettings')->name('update.settings');
    Route::get('/view/userdetails/{id}', 'Admin\DashboardController@viewUser')->name('view.user');
    Route::get('/activate/kyc/{id}', 'Admin\DashboardController@activateKyc');
    Route::get('/deactivate/kyc/{id}', 'Admin\DashboardController@deactivateKyc');
    Route::post('/update/user/profile', 'Admin\DashboardController@updateUserProfile');
    Route::post('/change/user/pwd', 'Admin\DashboardController@changeUserPwd');
    Route::get('/block/user/{id}', 'Admin\DashboardController@blockUser');
    Route::get('/activate/user/{id}', 'Admin\DashboardController@activateUser');
    Route::get('/delete/user/{id}', 'Admin\DashboardController@deleteUser');

    Route::get('/manage/investments', 'Admin\DashboardController@manageInvestmentsPage')->name('investments');
    Route::get('/pause/user_inv/{id}', 'Admin\DashboardController@pauseInv');
    Route::get('/activate/user_inv/{id}', 'Admin\DashboardController@activateInv');
    Route::get('/delete/user_inv/{id}', 'Admin\DashboardController@deleteInv');
    Route::get('/edit/user_inv/{id}', 'Admin\DashboardController@editInv');
    Route::post('/update/user_inv/{id}', 'Admin\DashboardController@updateInv')->name('update.investment');
    Route::post('/search/investment', 'Admin\DashboardController@searchInv');
    Route::get('/manage/adminUsers', 'Admin\DashboardController@manageAdminUsers')->name('manageAdmin.users');
    Route::post('/addNew/admin', 'Admin\DashboardController@admAddnew');

    Route::get('/manage/deposits', 'Admin\DashboardController@manageDepositsPage')->name('manageDeposits');
    Route::get('/reject/user/payment/{id}', 'Admin\DashboardController@rejectDep')->name('deposit.decline');
    Route::get('/approve/user/payment/{id}', 'Admin\DashboardController@approveDep')->name('deposit.approve');
    Route::get('/delete/user/payment/{id}', 'Admin\DashboardController@deleteDep')->name('deposit.delete');
    Route::post('/search/xinvestment', 'Admin\DashboardController@searchXInv');
    Route::post('/search/deposit', 'Admin\DashboardController@searchDep');
    Route::get('/add/deposits', 'Admin\DashboardController@addDeposit')->name('add.deposit');
    Route::post('/save/deposit', 'Admin\DashboardController@storeDeposit')->name('save.deposit');

    Route::get('/manage/withdrawals', "Admin\DashboardController@manageWithdrawalsPage")->name('manage.withdrawals');
    Route::get('/reject/user/wd/{id}', 'Admin\DashboardController@rejectWD');
    Route::get('/approve/user/wd/{id}', 'Admin\DashboardController@approveWD');
    Route::get('/delete/user/wd/{id}', 'Admin\DashboardController@deleteWD');
    Route::post('/search/Withdrawal', 'Admin\DashboardController@searchWD');

    Route::get('/manage/packages', 'Admin\DashboardController@managePackages')->name('manage.packages');
    Route::get('/delete/pack/{id}', 'Admin\DashboardController@adminDeletePack');
    Route::get('/create/package', 'Admin\DashboardController@create_package');
    Route::post('/create/package', 'Admin\DashboardController@create_package_post');
    Route::post('/edit/packages', 'Admin\DashboardController@editPack');

    Route::get('/act_deact/pack/{id}', 'Admin\DashboardController@switch_pack');
    Route::get('/send/msg', 'Admin\DashboardController@sendMsg')->name('send.msg');
    Route::post('/send/notification', 'Admin\DashboardController@admSendMsg');

    Route::get('/change/pwd', 'Admin\DashboardController@changePwd')->name('change.pwd');
    Route::post('/change/pwd', 'Admin\DashboardController@admChangePwd');

    Route::group(
        [
            'prefix' => 'support',
            'as' => 'support.',
            'name' => 'support.',
        ],
        function () {
            Route::get('/', 'Admin\DashboardController@view_tickets')->name('index');
            Route::get('{id}', 'Admin\DashboardController@open_ticket')->name('open');
            Route::get('/close/{id}', 'Admin\DashboardController@close_ticket')->name('close');
            Route::get('/delete/{id}', 'Admin\DashboardController@delete_ticket')->name('delete');
            Route::post('/comment', 'Admin\DashboardController@ticket_comment')->name('comment');
            ///////////// ajax chat  ////////////////////////
            Route::get('/chat/{id}', 'Admin\DashboardController@ticket_chat')->name('chat');
        }
    );

    Route::get('/viewlogs', "Admin\DashboardController@viewUserLogs")->name('view.logs');

    Route::get('/profile/settings', 'Admin\DashboardController@adminViewProfileSettings');

    Route::get('/manage/xpack_investments', "Admin\DashboardController@manageXpackInvestments")->name('manage.xpack.investments');

    Route::get('/ban/admuser/{id}', 'Admin\DashboardController@admin_ban_user');
    Route::get('/activate/admuser/{id}', 'Admin\DashboardController@admin_activate_user');
    Route::get('/delete/admuser/{id}', 'Admin\DashboardController@dadmin_delete_user');

    Route::post('/search/adminUser', 'Admin\DashboardController@searchadminUser');
    Route::post('/search/user', 'Admin\DashboardController@admSearch');

    Route::get('/message/edit/{id}', 'Admin\DashboardController@editMsg');
    Route::get('/message/delete/{id}', 'Admin\DashboardController@editMsgDel');
    Route::get('/getMonthlyIvCart', 'Admin\DashboardController@getMonthlyIvCart');

    Route::post('/admin/search/stat', 'adminController@admSearchByMonth');

    Route::prefix('/exchange')->group(function () {
        Route::get('manage/currency', 'Admin\ExchangeController@ex_currencies_page')->name('ex.currency');
        Route::get('currency/fiat', 'Admin\ExchangeController@ex_currency_fiat_page')->name('ex.currency.fiat');
        Route::get('currency/crypto', 'Admin\ExchangeController@ex_currency_crypto_page')->name('ex.currency.crypto');
        Route::get('currency/create', 'Admin\ExchangeController@ex_currency_create')->name('ex.currency.create');
        Route::post('currency/create', 'Admin\ExchangeController@ex_currency_create_post')->name('ex.currency.create.post');
    });

    // Console routes
    Route::get('/console', [App\Http\Controllers\Admin\ConsoleController::class, 'index'])->name('admin.console');
    Route::post('/console/execute', [App\Http\Controllers\Admin\ConsoleController::class, 'execute'])->name('admin.console.execute');
});
