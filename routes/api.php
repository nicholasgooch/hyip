<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();

});
Route::post('/admin/edit/user/balance/{id}', 'AdminApiController@editUserBalance')->name('edit.user.balance');
Route::get('/get/btc/price/{amount}', function ($amount) {
    return response()->json(toBtc($amount));
})->name('get.btc.price');
Route::get('/get/eth/price/{amount}', function ($amount) {
    return response()->json(toEth($amount));
})->name('get.eth.price');
Route::get('/get/trx/data/{address}', function ($address) {
    return response()->json(getBtcTrxData($address));
})->name('');
Route::get('/test/get/wallet', function () {
    $xpub = 'zpub6nLqW7SuMMtc2fzWV6bkSXxQMpwcL7g1zspTYwWbe5Bs8KQ5g38GNWChFosGkDApdoBQi8EQpzP7cPQqfngxDQwHqCpVppMwFzrdiDRinBk';
    $res = getBtcWalletAddress($xpub);
    return $res;

})->name('');
