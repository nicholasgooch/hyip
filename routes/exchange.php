<?php

Route::domain('exchange.hyip.dev')->prefix('ex')->group(function(){
    Route::get('otc/dashboard','Exchange\DashboardController@index')->name('ex.dashboard');
    Route::get('/account','Exchange\DashboardController@account')->name('ex.account');
    Route::get('/buy_sell','Exchange\TradeController@trade_page')->name('ex.trade');
    Route::get('/wallets','Exchange\WalletController@index')->name('ex.wallets');
    Route::get('/trades','Exchange\TradeController@index')->name('ex.trades');
});