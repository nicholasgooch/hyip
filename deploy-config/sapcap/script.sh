#!/bin/bash

# Define color codes
GREEN='\033[0;32m'
RED='\033[0;31m'
BLUE='\033[0;34m'   
NC='\033[0m' # No Color

# Define domain variables
APP_NAME="Sapientia Capital"
DOMAIN_NAME="sapientiacapital.com"
DOMAIN="portfolio.${DOMAIN_NAME}"
PMA_SUBDOMAIN="pma.${DOMAIN_NAME}"
WWW_DOMAIN="www.${DOMAIN_NAME}"
MYSQL_ROOT_PASSWORD="Uzumaki007@"
MYSQL_DATABASE="sapcap"
MYSQL_USER="root"
SMTP_HOST="smtp-relay.brevo.com"
SMTP_PORT="587"
SMTP_USERNAME="822d84001@smtp-brevo.com"
SMTP_FROM_EMAIL="no-reply@sapientiacapital.com"
SMTP_PASSWORD="56DrJkpIcgW9YhUw"
DATABASE_DUMP_FILE="sapcap202411291055.sql"
SQLBAK_KEY=5ecd7eed-23ea-403e-963e-6a75f9b14423
BITBUCKET_REPO="hyip" #bitbucket repository name
PHP_VERSION="8.2"
BITBUCKET_APP_PASSWORD="ATBBwFVqNecqMp5Ve7MZTrJmG34j0D3B7E0F" #bitbucket app password
BITBUCKET_USERNAME="nicholasgooch" #bitbucket username
BITBUCKET_CLONE_URL="https://${BITBUCKET_USERNAME}:${BITBUCKET_APP_PASSWORD}@bitbucket.org/${BITBUCKET_USERNAME}/${BITBUCKET_REPO}.git"

# Function to handle errors
handle_error() {
    local exit_code=$?
    local command=$1
    if [ $exit_code -ne 0 ]; then
        echo -e "${RED}Warning: Command '$command' failed with exit code $exit_code${NC}"
        echo -e "${BLUE}Continuing with the next step...${NC}"
    fi
}

# Update the system
echo -e "${RED}Updating system packages${NC}"
if ! command -v apt &> /dev/null; then
    sudo apt-get update && sudo apt upgrade -y || handle_error "system update"
fi

if ! command -v nginx &> /dev/null && ! command -v curl &> /dev/null && ! command -v git &> /dev/null; then
    # Install NGINX
    echo -e "${RED}Installing Nginx${NC}"
    sudo apt-get install -y nginx curl git || handle_error "nginx installation"
fi

# Remove any existing PHP installations
echo -e "${RED}Removing existing PHP installations${NC}"
sudo apt-get purge php* -y || handle_error "PHP removal"
sudo apt-get autoremove -y || handle_error "autoremove"

if ! command -v php${PHP_VERSION} &> /dev/null; then
    # Add PHP repository 
    echo -e "${RED}Adding PHP repository${NC}"
    sudo apt -y install software-properties-common || handle_error "software-properties-common installation"
    sudo add-apt-repository ppa:ondrej/php -y || handle_error "PHP repository addition"
    sudo apt-get update || handle_error "apt update"

    echo -e "${RED}Installing PHP ${PHP_VERSION} and extensions${NC}"
    sudo apt-get install -y \
    php${PHP_VERSION} \
    php${PHP_VERSION}-fpm \
    php${PHP_VERSION}-cli \
    php${PHP_VERSION}-common \
    php${PHP_VERSION}-mysql \
    php${PHP_VERSION}-zip \
    php${PHP_VERSION}-gd \
    php${PHP_VERSION}-mbstring \
    php${PHP_VERSION}-curl \
    php${PHP_VERSION}-xml \
    php${PHP_VERSION}-bcmath || handle_error "PHP installation"

    # Ensure PHP ${PHP_VERSION} is the default version
    echo -e "${RED}Setting PHP ${PHP_VERSION} as default${NC}"
    sudo update-alternatives --set php /usr/bin/php${PHP_VERSION} || handle_error "PHP alternatives setup"
    sudo update-alternatives --set phar /usr/bin/phar${PHP_VERSION} || handle_error "PHAR alternatives setup"
    sudo update-alternatives --set phar.phar /usr/bin/phar.phar${PHP_VERSION} || handle_error "PHAR.PHAR alternatives setup"
else
    echo -e "${BLUE}PHP ${PHP_VERSION} is already installed${NC}"
fi

# Verify PHP version
echo -e "${RED}Verifying PHP version${NC}"
php -v || handle_error "PHP version check"

# Check if MySQL is installed and install if not
echo -e "${RED}Checking MySQL installation${NC}"
if ! command -v mysql &> /dev/null; then
    echo -e "${RED}Installing MySQL Server${NC}"
    echo "mysql-server mysql-server/root_password password ${MYSQL_ROOT_PASSWORD}" | sudo debconf-set-selections
    echo "mysql-server mysql-server/root_password_again password ${MYSQL_ROOT_PASSWORD}" | sudo debconf-set-selections
    sudo apt-get install -y mysql-server || handle_error "MySQL installation"
else
    echo -e "${BLUE}MySQL is already installed${NC}"
fi

# Create database and import dump if exists
{
    mysql -u ${MYSQL_USER} -p${MYSQL_ROOT_PASSWORD} -e "CREATE DATABASE IF NOT EXISTS ${MYSQL_DATABASE}"
    if [ -f ./${DATABASE_DUMP_FILE} ]; then
        echo -e "${RED}Importing database dump${NC}"
        mysql -u ${MYSQL_USER} -p${MYSQL_ROOT_PASSWORD} ${MYSQL_DATABASE} < ./${DATABASE_DUMP_FILE}
    fi
} || handle_error "Database operations"

# Install Composer if not present
if ! command -v composer &> /dev/null; then
    echo -e "${RED}Installing Composer${NC}"
    curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer || handle_error "Composer installation"
else
    echo -e "${BLUE}Composer is already installed${NC}"
fi

# Copy SSL certificates
echo -e "${RED}Copying SSL certificates${NC}"
if [ -f ./${DOMAIN}-cert.pem ] && [ -f ./${DOMAIN}-key.pem ]; then
    sudo cp ./${DOMAIN}-cert.pem /etc/ssl/certs/ || handle_error "SSL cert copy"
    sudo cp ./${DOMAIN}-key.pem /etc/ssl/private/ || handle_error "SSL key copy"
else
    echo -e "${BLUE}SSL certificate files not found!${NC}"
fi

# Install phpMyAdmin with specific version
echo -e "${RED}Installing phpMyAdmin${NC}"
if [ ! -d "/usr/share/phpmyadmin/phpMyAdmin-4.9.7-all-languages" ]; then
    cd /tmp || handle_error "changing to tmp directory"
    wget https://files.phpmyadmin.net/phpMyAdmin/4.9.7/phpMyAdmin-4.9.7-all-languages.tar.gz || handle_error "downloading phpMyAdmin"
    tar xzf phpMyAdmin-4.9.7-all-languages.tar.gz || handle_error "extracting phpMyAdmin"
    sudo mv phpMyAdmin-4.9.7-all-languages /usr/share/phpmyadmin || handle_error "moving phpMyAdmin"
    sudo mkdir -p /usr/share/phpmyadmin/tmp || handle_error "creating phpMyAdmin tmp directory"
    sudo chmod 777 /usr/share/phpmyadmin/tmp || handle_error "setting phpMyAdmin permissions"
else 
     echo -e "${BLUE}phpMyAdmin files already downloaded and extracted!${NC}"
fi

if [ ! -f "/usr/share/phpmyadmin/config.inc.php" ]; then
    # Create phpMyAdmin config
    echo -e "${RED}Creating phpMyAdmin configuration${NC}"
    sudo cp /usr/share/phpmyadmin/config.sample.inc.php /usr/share/phpmyadmin/config.inc.php || handle_error "copying phpMyAdmin config"

    # Fix the sed command for blowfish secret
    BLOWFISH_SECRET=$(openssl rand -base64 32)
    sudo sed -i "s|\\\$cfg\['blowfish_secret'\] = ''|\\\$cfg\['blowfish_secret'\] = '$BLOWFISH_SECRET'|" /usr/share/phpmyadmin/config.inc.php || handle_error "setting blowfish secret"

    # Setup Nginx for phpMyAdmin
    echo -e "${RED}Setting up Nginx for phpMyAdmin${NC}"
    if [ -L "/var/www/phpmyadmin" ]; then
        sudo rm /var/www/phpmyadmin || handle_error "removing old phpMyAdmin symlink"
    fi
    sudo ln -s /usr/share/phpmyadmin /var/www/phpmyadmin || handle_error "creating phpMyAdmin symlink"

    # Set proper permissions for phpMyAdmin
    sudo chown -R www-data:www-data /usr/share/phpmyadmin || handle_error "setting phpMyAdmin ownership"
    sudo chmod -R 755 /usr/share/phpmyadmin || handle_error "setting phpMyAdmin permissions"

    # Create NGINX configuration for phpMyAdmin
    {
        echo -e "${RED}Creating Nginx configuration for phpMyAdmin${NC}"
        sudo tee /etc/nginx/sites-available/phpmyadmin.conf > /dev/null <<EOL
        server {
            listen 80;
            listen [::]:80;
            server_name ${PMA_SUBDOMAIN};
            return 301 https://\$server_name\$request_uri;
        }

        server {
            listen 443 ssl http2;
            listen [::]:443 ssl http2;

            ssl_certificate /etc/ssl/certs/${DOMAIN}-cert.pem;
            ssl_certificate_key /etc/ssl/private/${DOMAIN}-key.pem;

            server_name ${PMA_SUBDOMAIN};

            access_log /var/log/nginx/pma.access.log;
            error_log /var/log/nginx/pma.error.log;

            root /var/www/phpmyadmin;
            index index.php;

            location / {
                try_files \$uri \$uri/ /index.php?\$query_string;
            }

            location ~ \.php$ {
                fastcgi_split_path_info ^(.+\.php)(/.+)$;
                fastcgi_pass unix:/var/run/php/php${PHP_VERSION}-fpm.sock;
                fastcgi_index index.php;
                include fastcgi_params;
                fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
                fastcgi_param PATH_INFO \$fastcgi_path_info;
                fastcgi_buffer_size 128k;
                fastcgi_buffers 4 256k;
                fastcgi_busy_buffers_size 256k;
            }

            location ~ /\.ht {
                deny all;
            }
        }
EOL
    } || handle_error "creating phpMyAdmin nginx config"
else
    echo -e "${BLUE}phpMyAdmin configuration already exists${NC}"
fi

# Update PHP-FPM configuration
{
    echo -e "${RED}Updating PHP-FPM configuration${NC}"
    sudo tee /etc/php/${PHP_VERSION}/fpm/pool.d/www.conf > /dev/null <<EOL
[www]
user = www-data
group = www-data
listen = /var/run/php/php${PHP_VERSION}-fpm.sock
listen.owner = www-data
listen.group = www-data
pm = dynamic
pm.max_children = 5
pm.start_servers = 2
pm.min_spare_servers = 1
pm.max_spare_servers = 3
pm.max_requests = 500
php_admin_value[memory_limit] = 256M
php_admin_value[upload_max_filesize] = 64M
php_admin_value[post_max_size] = 64M
php_admin_value[max_execution_time] = 600
php_admin_value[max_input_vars] = 3000
EOL
} || handle_error "updating PHP-FPM configuration"

# Restart PHP-FPM and NGINX
echo -e "${RED}Restarting services${NC}"
sudo systemctl restart php${PHP_VERSION}-fpm || handle_error "restarting PHP-FPM"
sudo systemctl restart nginx || handle_error "restarting nginx"

# Create NGINX configuration for main site
{
    echo -e "${RED}Creating Nginx configuration for main site${NC}"
    sudo tee /etc/nginx/sites-available/${DOMAIN}.conf > /dev/null <<EOL
server {
    listen 80;
    listen [::]:80;
    server_name ${DOMAIN} ${WWW_DOMAIN};
    return 301 https://\$server_name\$request_uri;
}

server {
    listen 443 ssl;
    listen [::]:443 ssl;
    server_name ${DOMAIN} ${WWW_DOMAIN};

    ssl_certificate /etc/ssl/certs/${DOMAIN}-cert.pem;
    ssl_certificate_key /etc/ssl/private/${DOMAIN}-key.pem;

    root /var/www/${DOMAIN}/public;
    index index.php index.html index.htm;

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php${PHP_VERSION}-fpm.sock;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\.ht {
        deny all;
    }
}
EOL
} || handle_error "creating main site nginx config"

# Enable the new site configurations
echo -e "${RED}Enabling site configurations${NC}"
sudo ln -sf /etc/nginx/sites-available/${DOMAIN}.conf /etc/nginx/sites-enabled/ || handle_error "enabling main site config"
sudo ln -sf /etc/nginx/sites-available/phpmyadmin.conf /etc/nginx/sites-enabled/ || handle_error "enabling phpMyAdmin config"

# Remove default nginx site if it exists
sudo rm -f /etc/nginx/sites-enabled/default || handle_error "removing default nginx site"

# Test NGINX configuration
echo -e "${RED}Testing Nginx configuration${NC}"
sudo nginx -t || handle_error "testing nginx configuration"

# Restart NGINX
echo -e "${RED}Restarting Nginx${NC}"
sudo systemctl restart nginx || handle_error "final nginx restart"

# Check and setup ${DOMAIN} directory
echo -e "${RED}Checking ${DOMAIN} directory${NC}"
if [ ! -d "/var/www/${DOMAIN}" ]; then
    echo -e "${RED}Creating /var/www/${DOMAIN} directory${NC}"
    sudo git clone ${BITBUCKET_CLONE_URL} /var/www/${DOMAIN} || handle_error "cloning repository"
    sudo chown -R www-data:www-data /var/www/${DOMAIN} || handle_error "setting directory ownership"
    sudo chmod -R 755 /var/www/${DOMAIN} || handle_error "setting directory permissions"
    sudo chmod -R 777 /var/www/${DOMAIN}/storage || handle_error "setting storage permissions"
    sudo chmod -R 777 /var/www/${DOMAIN}/bootstrap/cache || handle_error "setting cache permissions"

    sudo mkdir -p /var/www/${DOMAIN}/storage/framework/{sessions,views,cache} || handle_error "creating cache directory"
    sudo chmod -R 777 /var/www/${DOMAIN}/storage/framework || handle_error "setting cache permissions"
    echo -e "${GREEN}Directory created and permissions set${NC}"
else
    echo -e "${GREEN}Directory /var/www/${DOMAIN} already exists${NC}"
fi

# Run composer install
echo -e "${RED}Running composer install${NC}"
composer install --working-dir=/var/www/${DOMAIN} --no-dev --no-interaction || handle_error "composer install"
echo -e "${GREEN}Composer install completed${NC}"   

# Create .env file
{
    echo -e "${RED}Creating .env file${NC}"
    sudo tee /var/www/${DOMAIN}/.env > /dev/null <<EOL
APP_NAME="${APP_NAME}"
APP_ENV=local
APP_KEY=base64:mgWSdQOr0EQlbCZowvLpFyoMIG9blTlF3mf+OWCXy54=
APP_DEBUG=true
APP_URL=https://${DOMAIN}

LOG_CHANNEL=stack

HTTPS=true

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=${MYSQL_DATABASE}
DB_USERNAME=${MYSQL_USER}
DB_PASSWORD=${MYSQL_ROOT_PASSWORD}

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_CONNECTION=redis
SESSION_LIFETIME=120
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379
SESSION_CONNECTION=default

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=${SMTP_HOST}
MAIL_PORT=${SMTP_PORT}
MAIL_USERNAME=${SMTP_USERNAME}
MAIL_PASSWORD=${SMTP_PASSWORD}
MAIL_FROM_ADDRESS=${SMTP_FROM_EMAIL}
MAIL_FROM_NAME="${APP_NAME}"
MAIL_ENCRYPTION=tls

XPUB=zpub6mdidjRLqU3etTEsTyef1aoHzWXgDcf7X61zzXWZ8vpjFAovNHEV85qRmwKT3imNKFyzHAMSSPhz9Kt1a4kJD5TuvrFrL2NsLLevSwnX7q9

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="\${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="\${PUSHER_APP_CLUSTER}"

PORTFOLIO_APP_THEME=red
PRIMARY_COLOR="#CF173C"
SECONDARY_COLOR="#CF173C"
ACCENT_COLOR="#CF173C"

GOOGLE_CLIENT_ID=935540055013-349ornvc23j3gmk5gmba4scqjnnvueu2.apps.googleusercontent.com
GOOGLE_CLIENT_SECRET=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
OOGLE_REDIRECT=https://portfolio.${DOMAIN_NAME}/login/google/callback

ADMIN_EMAIL=support@${DOMAIN_NAME}
SUPPORT_EMAIL=support@${DOMAIN_NAME}

TURNSTILE_SITE_KEY=0x4AAAAAAA3HkhNDOCyXJPU8
TURNSTILE_SECRET_KEY=0x4AAAAAAA3Hkk23sc6PNVoCgkD5VEUxjW4

CLOUDINARY_API_KEY=443774618676512
CLOUDINARY_API_SECRET=PDCSL_h92VOOq3xDgka9MpK9Yvk
CLOUDINARY_CLOUD_NAME=dxkq22oyq
EOL

} || handle_error "creating .env file"

# Setup SQLBak 
if [ ${SQLBAK_KEY} ]; then
    echo -e "${RED}Setting up SQLBak${NC}"
    wget -q https://sqlbak.com/download/linux/latest/sqlbak.deb || handle_error "downloading SQLBak"
    sudo apt-get install -y ./sqlbak.deb || handle_error "installing SQLBak"
    sudo sqlbak -r -k ${SQLBAK_KEY} || handle_error "configuring SQLBak"
    echo -e "${BLUE}SQLBak setup completed${NC}"
else
    echo -e "${RED}SQLBak key not found!${NC}"
fi

# Installation Summary
echo -e "\n${GREEN}=== Installation Summary ===${NC}"
echo -e "\n${GREEN}Components Installed:${NC}"
echo -e "✓ NGINX"
echo -e "✓ PHP ${PHP_VERSION} and extensions"
echo -e "✓ MySQL Server"
echo -e "✓ Composer"
echo -e "✓ phpMyAdmin v4.9.7"

echo -e "\n${GREEN}Configurations:${NC}"
echo -e "✓ NGINX virtual hosts"
echo -e "✓ PHP-FPM settings"
echo -e "✓ SSL certificates"
echo -e "✓ Database setup"
echo -e "✓ Environment file"

echo -e "\n${GREEN}Services Status:${NC}"
echo -n "NGINX: "
if systemctl is-active --quiet nginx; then
    echo -e "${GREEN}Running${NC}"
else
    echo -e "${RED}Not Running${NC}"
fi

echo -n "PHP-FPM: "
if systemctl is-active --quiet php${PHP_VERSION}-fpm; then
    echo -e "${GREEN}Running${NC}"
else
    echo -e "${RED}Not Running${NC}"
fi

echo -n "MySQL: "
if systemctl is-active --quiet mysql; then
    echo -e "${GREEN}Running${NC}"
else
    echo -e "${RED}Not Running${NC}"
fi

echo -e "\n${GREEN}Installation Details:${NC}"
echo -e "PHP Version: $(php -v | head -n 1)"
echo -e "NGINX Version: $(nginx -v 2>&1)"
echo -e "MySQL Version: $(mysql --version)"

echo -e "\n${BLUE}Website URLs:${NC}"
echo -e "Main site: https://${DOMAIN}"
echo -e "phpMyAdmin: https://${PMA_SUBDOMAIN}"

echo -e "\n${GREEN}Installation Complete!${NC}"


