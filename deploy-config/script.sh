#!/bin/bash

# Define color codes
GREEN='\033[0;32m'
RED='\033[0;31m'
BLUE='\033[0;34m'   
NC='\033[0m' # No Color

# Define domain variables
APP_NAME="Hera Investors"
DOMAIN="hera-investors.com"
PMA_SUBDOMAIN="pma.${DOMAIN}"
WWW_DOMAIN="www.${DOMAIN}"
MYSQL_ROOT_PASSWORD="Uzumaki007@"
MYSQL_DATABASE="hera"
MYSQL_USER="root"
SMTP_HOST="smtp-relay.brevo.com"
SMTP_PORT="587"
SMTP_USERNAME="822d84001@smtp-brevo.com"
SMTP_FROM_EMAIL="noreply@hera-investors.com"
SMTP_PASSWORD="56DrJkpIcgW9YhUw"
DATABASE_DUMP_FILE="hera202412191055.sql"
SQLBAK_KEY=5ecd7eed-23ea-403e-963e-6a75f9b14423
BITBUCKET_REPO="hyip" #bitbucket repository name
PHP_VERSION="8.0"
BITBUCKET_APP_PASSWORD="ATBBwFVqNecqMp5Ve7MZTrJmG34j0D3B7E0F" #bitbucket app password
BITBUCKET_USERNAME="nicholasgooch" #bitbucket username
BITBUCKET_CLONE_URL="https://${BITBUCKET_USERNAME}:${BITBUCKET_APP_PASSWORD}@bitbucket.org/${BITBUCKET_USERNAME}/${BITBUCKET_REPO}.git"


# Replace all occurrences of hera-investors.com with ${DOMAIN}
# Replace all occurrences of pma.hera-investors.com with ${PMA_SUBDOMAIN}
# Replace all occurrences of www.hera-investors.com with ${WWW_DOMAIN}

# Examples of replacements in the script:
# ./hera-investors.com-cert.pem becomes ./${DOMAIN}-cert.pem
# pma.hera-investors.com becomes ${PMA_SUBDOMAIN}
# www.hera-investors.com becomes ${WWW_DOMAIN}

set -e


# Update the system
echo -e "${RED}Updating system packages${NC}"
if ! command -v apt &> /dev/null; then
    sudo apt-get update && sudo apt upgrade -y
fi

if ! command -v nginx &> /dev/null && ! command -v curl &> /dev/null && ! command -v git &> /dev/null; then
    # Install NGINX
    echo -e "${RED}Installing Nginx${NC}"
    sudo apt-get install -y nginx curl git
fi

# Remove any existing PHP installations
echo -e "${RED}Removing existing PHP installations${NC}"
sudo apt-get purge php* -y
sudo apt-get autoremove -y

if ! command -v php${PHP_VERSION} &> /dev/null; then
    # Add PHP repository 
    echo -e "${RED}Adding PHP repository${NC}"
    sudo apt -y install software-properties-common
    sudo add-apt-repository ppa:ondrej/php -y
    sudo apt-get update

    echo -e "${RED}Installing PHP ${PHP_VERSION} and extensions${NC}"
    sudo apt-get install -y \
    php${PHP_VERSION} \
    php${PHP_VERSION}-fpm \
    php${PHP_VERSION}-cli \
    php${PHP_VERSION}-common \
    php${PHP_VERSION}-mysql \
    php${PHP_VERSION}-zip \
    php${PHP_VERSION}-gd \
    php${PHP_VERSION}-mbstring \
    php${PHP_VERSION}-curl \
    php${PHP_VERSION}-xml \
    php${PHP_VERSION}-bcmath

    # Ensure PHP ${PHP_VERSION} is the default version
    echo -e "${RED}Setting PHP ${PHP_VERSION} as default${NC}"
    sudo update-alternatives --set php /usr/bin/php${PHP_VERSION}
    sudo update-alternatives --set phar /usr/bin/phar${PHP_VERSION}
    sudo update-alternatives --set phar.phar /usr/bin/phar.phar${PHP_VERSION}
else
    echo -e "${BLUE}PHP ${PHP_VERSION} is already installed${NC}"
fi

# Verify PHP version
echo -e "${RED}Verifying PHP version${NC}"
php -v

# Check if MySQL is installed and install if not
echo -e "${RED}Checking MySQL installation${NC}"
if ! command -v mysql &> /dev/null; then
    echo -e "${RED}Installing MySQL Server${NC}"
    echo "mysql-server mysql-server/root_password password ${MYSQL_ROOT_PASSWORD}" | sudo debconf-set-selections
    echo "mysql-server mysql-server/root_password_again password ${MYSQL_ROOT_PASSWORD}" | sudo debconf-set-selections
    sudo apt-get install -y mysql-server
else
    echo -e "${BLUE}MySQL is already installed${NC}"
fi

if ! command -v mysql -u ${MYSQL_USER} -p${MYSQL_ROOT_PASSWORD} -e "CREATE DATABASE IF NOT EXISTS ${MYSQL_DATABASE}" &> /dev/null; then
    echo -e "${RED}Creating MySQL database${NC}"
    sudo mysql -u ${MYSQL_USER} -p${MYSQL_ROOT_PASSWORD} -e "CREATE DATABASE IF NOT EXISTS ${MYSQL_DATABASE}"
    # Import database if dump file exists
    if [ -f ./${DATABASE_DUMP_FILE} ]; then
        echo -e "${RED}Importing database dump${NC}"
        sudo mysql -u ${MYSQL_USER} -p${MYSQL_ROOT_PASSWORD} ${MYSQL_DATABASE} < ./${DATABASE_DUMP_FILE}
    fi
else
    echo -e "${BLUE}MySQL database already exists${NC}"
fi


if ! command -v composer &> /dev/null; then
    echo -e "${RED}Installing Composer${NC}"
    curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
else
    echo -e "${BLUE}Composer is already installed${NC}"
fi

# Copy SSL certificates
echo -e "${RED}Copying SSL certificates${NC}"
if [ -f ./${DOMAIN}-cert.pem ] && [ -f ./${DOMAIN}-key.pem ]; then
    sudo cp ./${DOMAIN}-cert.pem /etc/ssl/certs/
    sudo cp ./${DOMAIN}-key.pem /etc/ssl/private/
else
    echo -e "${BLUE}SSL certificate files not found!${NC}"
fi


# Install phpMyAdmin with specific version
echo -e "${RED}Installing phpMyAdmin${NC}"
if [ ! -d "/usr/share/phpmyadmin/phpMyAdmin-4.9.7-all-languages" ]; then
    cd /tmp
    wget https://files.phpmyadmin.net/phpMyAdmin/4.9.7/phpMyAdmin-4.9.7-all-languages.tar.gz
    tar xzf phpMyAdmin-4.9.7-all-languages.tar.gz
    sudo mv phpMyAdmin-4.9.7-all-languages /usr/share/phpmyadmin
    sudo mkdir -p /usr/share/phpmyadmin/tmp
    sudo chmod 777 /usr/share/phpmyadmin/tmp
else 
     echo -e "${BLUE}phpMyAdmin files already downloaded and extracted!${NC}"
fi

if [ ! -f "/usr/share/phpmyadmin/config.inc.php" ]; then
    # Create phpMyAdmin config
    echo -e "${RED}Creating phpMyAdmin configuration${NC}"
    sudo cp /usr/share/phpmyadmin/config.sample.inc.php /usr/share/phpmyadmin/config.inc.php

    # Fix the sed command for blowfish secret
    BLOWFISH_SECRET=$(openssl rand -base64 32)
    sudo sed -i "s|\\\$cfg\['blowfish_secret'\] = ''|\\\$cfg\['blowfish_secret'\] = '$BLOWFISH_SECRET'|" /usr/share/phpmyadmin/config.inc.php


    # Setup Nginx for phpMyAdmin
    echo -e "${RED}Setting up Nginx for phpMyAdmin${NC}"
    if [ -L "/var/www/phpmyadmin" ]; then
        sudo rm /var/www/phpmyadmin
    fi
    sudo ln -s /usr/share/phpmyadmin /var/www/phpmyadmin

    # Set proper permissions for phpMyAdmin
    sudo chown -R www-data:www-data /usr/share/phpmyadmin
    sudo chmod -R 755 /usr/share/phpmyadmin

    # Create NGINX configuration for phpMyAdmin with updated PHP configuration
    echo -e "${RED}Creating Nginx configuration for phpMyAdmin${NC}"
    sudo tee /etc/nginx/sites-available/phpmyadmin.conf > /dev/null <<EOL
    server {
        listen 80;
        listen [::]:80;
        server_name ${PMA_SUBDOMAIN};
        return 301 https://\$server_name\$request_uri;
    }

    server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;

        ssl_certificate /etc/ssl/certs/${DOMAIN}-cert.pem;
        ssl_certificate_key /etc/ssl/private/${DOMAIN}-key.pem;

        server_name ${PMA_SUBDOMAIN};

        access_log /var/log/nginx/pma.access.log;
        error_log /var/log/nginx/pma.error.log;

        root /var/www/phpmyadmin;
        index index.php;

        location / {
            try_files \$uri \$uri/ /index.php?\$query_string;
        }

        location ~ \.php$ {
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_pass unix:/var/run/php/php${PHP_VERSION}-fpm.sock;
            fastcgi_index index.php;
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
            fastcgi_param PATH_INFO \$fastcgi_path_info;
            fastcgi_buffer_size 128k;
            fastcgi_buffers 4 256k;
            fastcgi_busy_buffers_size 256k;
        }

        location ~ /\.ht {
            deny all;
        }
    }
EOL
else
    echo -e "${BLUE}phpMyAdmin configuration already exists${NC}"
fi



# Update PHP-FPM configuration
echo -e "${RED}Updating PHP-FPM configuration${NC}"
sudo tee /etc/php/${PHP_VERSION}/fpm/pool.d/www.conf > /dev/null <<EOL
[www]
user = www-data
group = www-data
listen = /var/run/php/php${PHP_VERSION}-fpm.sock
listen.owner = www-data
listen.group = www-data
pm = dynamic
pm.max_children = 5
pm.start_servers = 2
pm.min_spare_servers = 1
pm.max_spare_servers = 3
pm.max_requests = 500
php_admin_value[memory_limit] = 256M
php_admin_value[upload_max_filesize] = 64M
php_admin_value[post_max_size] = 64M
php_admin_value[max_execution_time] = 600
php_admin_value[max_input_vars] = 3000
EOL

# Restart PHP-FPM and NGINX
echo -e "${RED}Restarting services${NC}"
sudo systemctl restart php${PHP_VERSION}-fpm
sudo systemctl restart nginx


# Create NGINX configuration for hera-investors.com
echo -e "${RED}Creating Nginx configuration for hera-investors.com${NC}"
sudo tee /etc/nginx/sites-available/${DOMAIN}.conf > /dev/null <<EOL
server {
    listen 80;
    listen [::]:80;
    server_name ${DOMAIN} ${WWW_DOMAIN};
    return 301 https://\$server_name\$request_uri;
}

server {
    listen 443 ssl;
    listen [::]:443 ssl;
    server_name ${DOMAIN} ${WWW_DOMAIN};

    ssl_certificate /etc/ssl/certs/${DOMAIN}-cert.pem;
    ssl_certificate_key /etc/ssl/private/${DOMAIN}-key.pem;

    root /var/www/${DOMAIN}/public;
    index index.php index.html index.htm;

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php${PHP_VERSION}-fpm.sock;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\.ht {
        deny all;
    }
}
EOL

# Enable the new site configurations
echo -e "${RED}Enabling site configurations${NC}"
sudo ln -sf /etc/nginx/sites-available/${DOMAIN}.conf /etc/nginx/sites-enabled/
sudo ln -sf /etc/nginx/sites-available/phpmyadmin.conf /etc/nginx/sites-enabled/

# Remove default nginx site if it exists
sudo rm -f /etc/nginx/sites-enabled/default

# Test NGINX configuration
echo -e "${RED}Testing Nginx configuration${NC}"
sudo nginx -t

# Restart NGINX
echo -e "${RED}Restarting Nginx${NC}"
sudo systemctl restart nginx

# Check and setup ${DOMAIN} directory
echo -e "${RED}Checking ${DOMAIN} directory${NC}"
if [ ! -d "/var/www/${DOMAIN}" ]; then
    echo -e "${RED}Creating /var/www/${DOMAIN} directory${NC}"
    sudo git clone ${BITBUCKET_CLONE_URL} /var/www/${DOMAIN}
    sudo chown -R www-data:www-data /var/www/${DOMAIN}
    sudo chmod -R 755 /var/www/${DOMAIN}
    sudo chmod -R 777 /var/www/${DOMAIN}/storage
    sudo chmod -R 777 /var/www/${DOMAIN}/bootstrap/cache

    echo -e "${GREEN}Directory created and permissions set${NC}"
else
    echo -e "${GREEN}Directory /var/www/${DOMAIN} already exists${NC}"
fi
    # Run composer install
    echo -e "${RED}Running composer install${NC}"
    composer install --working-dir=/var/www/${DOMAIN} --no-dev --no-interaction
    echo -e "${GREEN}Composer install completed${NC}"   

    echo -e "${RED}Create .env file${NC}"
    # Create .env file
    echo -e "${RED}Creating .env file${NC}"
    sudo tee /var/www/${DOMAIN}/.env > /dev/null <<EOL
    APP_NAME="${APP_NAME}"
    APP_ENV=local
    APP_KEY=base64:mgWSdQOr0EQlbCZowvLpFyoMIG9blTlF3mf+OWCXy54=
    APP_DEBUG=true
    APP_URL=https://${DOMAIN}

    LOG_CHANNEL=stack

    HTTPS=true

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=${MYSQL_DATABASE}
    DB_USERNAME=${MYSQL_USER}
    DB_PASSWORD=${MYSQL_ROOT_PASSWORD}

    BROADCAST_DRIVER=log
    CACHE_DRIVER=file
    SESSION_DRIVER=file
    QUEUE_CONNECTION=redis
    SESSION_LIFETIME=120
    QUEUE_DRIVER=sync

    REDIS_HOST=127.0.0.1
    REDIS_PASSWORD=null
    REDIS_PORT=6379
    SESSION_CONNECTION=default

    REDIS_HOST=127.0.0.1
    REDIS_PASSWORD=null
    REDIS_PORT=6379

    MAIL_DRIVER=smtp
    MAIL_HOST=us2.smtp.mailhostbox.com
    MAIL_PORT=587
    MAIL_USERNAME=support@${DOMAIN}
    MAIL_PASSWORD=Uzumaki007@
    MAIL_FROM_ADDRESS=support@${DOMAIN}
    MAIL_FROM_NAME="${APP_NAME}"
    MAIL_ENCRYPTION=tls

    XPUB=zpub6mdidjRLqU3etTEsTyef1aoHzWXgDcf7X61zzXWZ8vpjFAovNHEV85qRmwKT3imNKFyzHAMSSPhz9Kt1a4kJD5TuvrFrL2NsLLevSwnX7q9

    PUSHER_APP_ID=
    PUSHER_APP_KEY=
    PUSHER_APP_SECRET=
    PUSHER_APP_CLUSTER=mt1

    MIX_PUSHER_APP_KEY="\${PUSHER_APP_KEY}"
    MIX_PUSHER_APP_CLUSTER="\${PUSHER_APP_CLUSTER}"

    NOCAPTCHA_SITEKEY=6LemeVcgAAAAAMD5KuFS5S0_HLY0BcnnQ8kWDfWs
    NOCAPTCHA_SECRET=6LemeVcgAAAAAIcXJYGH3SX2rhlqmKmJzRSVXNwS

    CLOUDINARY_API_KEY=443774618676512
    CLOUDINARY_API_SECRET=PDCSL_h92VOOq3xDgka9MpK9Yvk
    CLOUDINARY_CLOUD_NAME=dxkq22oyq


    TURNSTILE_SITE_KEY=0x4AAAAAAA17508cJgBZW1oZ
    TURNSTILE_SECRET_KEY=0x4AAAAAAA1753dmoRja_jztN9zSU2CfpgY
EOL

# Setup SQLBak 
if [ ${SQLBAK_KEY} ]; then
    echo -e "${RED}Setting up SQLBak${NC}"
    wget -q https://sqlbak.com/download/linux/latest/sqlbak.deb
    sudo apt-get install -y ./sqlbak.deb
    sudo sqlbak -r -k ${SQLBAK_KEY}
    echo -e "${BLUE}SQLBak setup completed${NC}"
else
    echo -e "${RED}SQLBak key not found!${NC}"
fi

# After your last show_progress call, add this summary section:

echo -e "\n${GREEN}=== Installation Summary ===${NC}"
echo -e "\n${GREEN}Components Installed:${NC}"
echo -e "✓ NGINX"
echo -e "✓ PHP 7.4 and extensions"
echo -e "✓ MySQL Server"
echo -e "✓ Composer"
echo -e "✓ phpMyAdmin v4.9.7"

echo -e "\n${GREEN}Configurations:${NC}"
echo -e "✓ NGINX virtual hosts"
echo -e "✓ PHP-FPM settings"
echo -e "✓ SSL certificates"
echo -e "✓ Database setup"
echo -e "✓ Environment file"

echo -e "\n${GREEN}Services Status:${NC}"
echo -n "NGINX: "
if systemctl is-active --quiet nginx; then
    echo -e "${GREEN}Running${NC}"
else
    echo -e "${RED}Not Running${NC}"
fi

echo -n "PHP-FPM: "
if systemctl is-active --quiet php8.0-fpm; then
    echo -e "${GREEN}Running${NC}"
else
    echo -e "${RED}Not Running${NC}"
fi

echo -n "MySQL: "
if systemctl is-active --quiet mysql; then
    echo -e "${GREEN}Running${NC}"
else
    echo -e "${RED}Not Running${NC}"
fi

echo -e "\n${GREEN}Installation Details:${NC}"
echo -e "PHP Version: $(php -v | head -n 1)"
echo -e "NGINX Version: $(nginx -v 2>&1)"
echo -e "MySQL Version: $(mysql --version)"

echo -e "\n${BLUE}Website URLs:${NC}"
echo -e "Main site: https://${DOMAIN}"
echo -e "phpMyAdmin: https://${PMA_SUBDOMAIN}"

echo -e "\n${GREEN}Installation Complete!${NC}"


