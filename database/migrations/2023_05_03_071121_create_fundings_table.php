<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFundingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fundings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('amount');
            $table->integer('funding_type_id');
            $table->integer('collateral_type_id');
            $table->string('collateral_amount')->nullable();
            $table->string('interest_rate')->nullable();
            $table->string('funding_status_id');
            $table->string('term');
            $table->string('purpose_of_funding')->nullable();
            $table->string('repayment_frequency')->nullable();
            $table->string('available_to_withdraw')->nullable();
            $table->string('collateral_paid')->nullable();
            $table->string('business_valuation')->nullable();
            $table->string('equity_percentage')->nullable();
            $table->string('equity_type')->nullable();
            $table->string('roi')->nullable();
            $table->longText('comments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fundings');
    }
}
