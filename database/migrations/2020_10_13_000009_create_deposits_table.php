<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'deposits';

    /**
     * Run the migrations.
     * @table deposits
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('usn', 100);
            $table->double('amount');
            $table->string('currency', 50);
            $table->string('account_name')->nullable()->default('');
            $table->string('account_no')->nullable()->default('');
            $table->string('bank')->default('');
            $table->integer('status')->default('0');
            $table->integer('on_apr')->default('0');
            $table->string('pop')->default('');
            $table->string('url')->default('');
            $table->integer('ipn')->default('0');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
