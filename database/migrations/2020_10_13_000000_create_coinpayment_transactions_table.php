<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinpaymentTransactionsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'coinpayment_transactions';

    /**
     * Run the migrations.
     * @table coinpayment_transactions
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->string('txn_id', 191)->nullable()->default(null);
            $table->string('address', 191)->nullable()->default(null);
            $table->string('amount', 191)->nullable()->default(null);
            $table->string('amountf', 191)->nullable()->default(null);
            $table->string('coin', 191)->nullable()->default(null);
            $table->integer('confirms_needed')->nullable()->default(null);
            $table->string('payment_address', 191)->nullable()->default(null);
            $table->string('qrcode_url', 191)->nullable()->default(null);
            $table->string('received', 191)->nullable()->default(null);
            $table->string('receivedf', 191)->nullable()->default(null);
            $table->string('recv_confirms', 191)->nullable()->default(null);
            $table->string('status', 191)->nullable()->default(null);
            $table->string('status_text', 191)->nullable()->default(null);
            $table->string('status_url', 191)->nullable()->default(null);
            $table->string('timeout', 191)->nullable()->default(null);
            $table->string('type', 191)->nullable()->default(null);
            $table->text('payload')->nullable()->default(null);

            $table->unique(["txn_id"], 'coinpayment_transactions_txn_id_unique');
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
