<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'settings';

    /**
     * Run the migrations.
     * @table settings
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->string('site_title', 100)->nullable()->default('');
            $table->string('site_descr')->nullable()->default('');
            $table->string('site_logo')->default('');
            $table->string('header_color', 10)->default('');
            $table->string('footer_color', 200)->default('');
            $table->integer('deposit')->default('0');
            $table->integer('withdrawal')->default('0');
            $table->double('wd_fee')->default('0');
            $table->integer('investment')->default('0');
            $table->integer('user_reg')->default('0');
            $table->string('currency', 5)->default('USD');
            $table->double('currency_conversion')->default('1');
            $table->string('paypal_ID')->nullable()->default('');
            $table->string('paypal_secret')->nullable()->default('');
            $table->string('paypal_mode', 20)->nullable()->default('');
            $table->string('stripe_key')->nullable()->default('');
            $table->string('stripe_secret')->nullable()->default('');
            $table->text('btc_xpub_key', 50)->nullable()->default(null);
            $table->text('btc_wallet_address', 50)->nullable()->default(null);
            $table->integer('switch_btc')->nullable()->default('1');
            $table->string('bank_name', 50)->nullable()->default(null);
            $table->string('account_name', 50)->nullable()->default(null);
            $table->string('account_number', 50)->nullable()->default(null);
            $table->string('bank_deposit_email', 50)->nullable()->default(null);
            $table->integer('bank_deposit_switch')->nullable()->default(null);
            $table->string('min_deposit', 50)->nullable()->default(null);
            $table->string('max_deposit', 50)->nullable()->default(null);
            $table->string('ref_bonus', 50)->nullable()->default(null);
            $table->string('ref_type', 50)->nullable()->default(null);
            $table->string('ref_system', 50)->nullable()->default(null);
            $table->string('ref_level_cnt', 50)->nullable()->default(null);
            $table->string('wd_limit', 50)->nullable()->default(null);
            $table->string('min_wd', 50)->nullable()->default(null);
            $table->string('admin_email', 50)->nullable()->default(null);
            $table->string('domain_name', 50)->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
