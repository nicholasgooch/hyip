<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'users';

    /**
     * Run the migrations.
     * @table users
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('firstname', 30);
            $table->string('lastname', 30);
            $table->string('username', 20);
            $table->string('email', 50);
            $table->string('pwd');
            $table->string('phone', 20)->nullable()->default(null);
            $table->string('remember_token')->default('');
            $table->timestamp('email_verified_at')->nullable()->default(null);
            $table->string('img', 200)->default('');
            $table->string('status', 10)->default('0');
            $table->string('r_state', 15)->default('');
            $table->string('country', 50)->default('');
            $table->string('state', 50)->default('');
            $table->string('address', 100)->default('');
            $table->string('currency', 10)->default('');
            $table->double('wallet')->default('0');
            $table->double('ref_bal')->default('0');
            $table->string('referal', 200)->default('');
            $table->string('btc_wallet', 200)->default('');
            $table->string('act_code', 50)->default('');
            $table->string('reg_date', 50)->default('');

            $table->unique(["username"], 'username');

            $table->unique(["phone"], 'phone');

            $table->unique(["email"], 'email');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
