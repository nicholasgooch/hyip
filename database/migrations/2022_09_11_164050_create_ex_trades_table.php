<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ex_trades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trade_code')->nullable();
            $table->integer('trade_type')->default(0); //0 = sell crypto 1 = buy crypto
            $table->integer('wallet_id')->nullable();
            $table->integer('crypto_id')->nullable();
            $table->integer('fiat_id')->nullable();
            $table->decimal('crypto_amount', 18, 8);
            $table->decimal('fiat_amount', 18, 8);
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ex_trades');
    }
}
