<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCpCpTransactionsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'cp_cp_transactions';

    /**
     * Run the migrations.
     * @table cp_cp_transactions
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->bigIncrements('id');
            $table->string('amount1', 191);
            $table->string('amount2', 191);
            $table->string('currency1', 10);
            $table->string('currency2', 10);
            $table->string('fee', 191)->nullable()->default(null);
            $table->string('address', 191)->nullable()->default(null);
            $table->string('dest_tag', 191)->nullable()->default(null);
            $table->string('buyer_email', 191)->nullable()->default(null);
            $table->string('buyer_name', 191)->nullable()->default(null);
            $table->string('item_name', 191)->nullable()->default(null);
            $table->string('item_number', 191)->nullable()->default(null);
            $table->string('invoice', 191)->nullable()->default(null);
            $table->text('custom')->nullable()->default(null);
            $table->string('ipn_url', 191)->nullable()->default(null);
            $table->string('txn_id', 128);
            $table->unsignedTinyInteger('confirms_needed');
            $table->unsignedInteger('timeout');
            $table->string('status_url', 191);
            $table->string('qrcode_url', 191);
            $table->smallInteger('status')->nullable()->default(null);
            $table->string('status_text', 191)->nullable()->default(null);
            $table->string('received_confirms', 191)->nullable()->default(null);
            $table->string('received_amount', 191)->nullable()->default(null);

            $table->unique(["txn_id"], 'cp_transactions_txn_id_unique');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
