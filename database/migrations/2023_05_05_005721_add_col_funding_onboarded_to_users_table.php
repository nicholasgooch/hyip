<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColFundingOnboardedToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('funding_onboarded')->default(0);
            $table->string('account_type')->default('personal');
            $table->boolean('subscription_status')->default(0);
            $table->boolean('require_subscription')->default(0);
            $table->integer('subscription_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['funding_onboarded', 'account_type', 'subscription_id', 'subscription_status']);
        });
    }
}
