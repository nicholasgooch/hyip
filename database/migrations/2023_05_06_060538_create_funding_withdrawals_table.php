<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFundingWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funding_withdrawals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('bank_id');
            $table->integer('wallet_address_id');
            //submitted //processing //approved // disbursment
            $table->string('status')->default('submitted');
            //account bal /funding ba;
            $table->string('category');
            //bank //crypto
            $table->string('withdrawal_method');
            $table->string('ref')->unique();
            $table->string('amount');
            $table->string('coin_amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funding_withdrawals');
    }
}
