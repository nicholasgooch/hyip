<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeSlugToFundingTypeSlugInCollateralTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collateral_types', function (Blueprint $table) {
            $table->integer('funding_type_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collateral_types', function (Blueprint $table) {
            $table->dropColumn('funding_type_id');
        });
    }
}
