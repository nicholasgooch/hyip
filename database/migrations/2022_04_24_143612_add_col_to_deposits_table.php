<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColToDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposits', function (Blueprint $table) {
            $table->string('coin_amount')->after('bank')->nullable();
            //deposit //collateral //subscription
            $table->string('deposit_category')->after('coin_amount')->default('deposit');
            //invest //funding
            $table->string('deposit_type')->after('deposit_category')->default('invest');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deposits', function (Blueprint $table) {
            $table->dropColumn(['coin_amount', 'deposit_type', 'deposit_category']);
        });
    }
}
