<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'packages';

    /**
     * Run the migrations.
     * @table packages
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->string('package_name', 100);
            $table->string('currency', 5)->default('');
            $table->double('min');
            $table->double('max');
            $table->double('daily_interest');
            $table->integer('period');
            $table->integer('days_interval')->default('30');
            $table->double('withdrwal_fee');
            $table->double('ref_bonus')->default('0');
            $table->integer('status')->default('0');

            $table->unique(["package_name"], 'package_name');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
