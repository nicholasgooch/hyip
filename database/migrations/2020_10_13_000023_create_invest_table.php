<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'invest';

    /**
     * Run the migrations.
     * @table invest
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('usn', 50)->default('');
            $table->integer('package_id');
            $table->string('currency', 50)->default('');
            $table->string('capital', 15);
            $table->string('i_return', 15);
            $table->string('date_invested', 10);
            $table->string('end_date', 20);
            $table->string('period', 30);
            $table->string('days_interval', 15)->default('');
            $table->double('interest');
            $table->string('last_wd', 12)->default('');
            $table->string('next_w_date', 10)->default('');
            $table->string('status', 15)->default('pending');
            $table->string('img')->default('');
            $table->string('hash')->default('');
            $table->double('w_amt')->default('0');
            $table->integer('daily')->default('0');
            $table->timestamp('date')->nullable()->default(null);
            $table->string('mode')->default('');
            $table->string('s_date', 30)->default('');
            $table->integer('reinv')->default('0');
            $table->integer('state')->default('0');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
