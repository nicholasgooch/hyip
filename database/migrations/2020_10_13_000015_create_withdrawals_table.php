<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawalsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'withdrawals';

    /**
     * Run the migrations.
     * @table withdrawals
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('usn', 100);
            $table->string('package', 20);
            $table->string('invest_id', 50);
            $table->string('account', 200)->nullable();
            $table->string('amount', 12);
            $table->string('currency', 50)->default('');
            $table->double('charges')->default('0');
            $table->double('recieving');
            $table->string('status', 10)->default('Pending');
            $table->string('w_date', 10);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
