<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCryptoPaymentsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'crypto_payments';

    /**
     * Run the migrations.
     * @table crypto_payments
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('paymentID');
            $table->unsignedInteger('boxID')->default('0');
            $table->enum('boxType', ['paymentbox', 'captchabox']);
            $table->string('orderID', 50)->default('');
            $table->string('userID', 50)->default('');
            $table->string('countryID', 3)->default('');
            $table->string('coinLabel', 6)->default('');
            $table->double('amount')->default('0.00000000');
            $table->double('amountUSD')->default('0.00000000');
            $table->unsignedTinyInteger('unrecognised')->default('0');
            $table->string('addr', 34)->default('');
            $table->char('txID', 64)->default('');
            $table->dateTime('txDate')->nullable()->default(null);
            $table->unsignedTinyInteger('txConfirmed')->default('0');
            $table->dateTime('txCheckDate')->nullable()->default(null);
            $table->unsignedTinyInteger('processed')->default('0');
            $table->dateTime('processedDate')->nullable()->default(null);
            $table->dateTime('recordCreated')->nullable()->default(null);

            $table->index(["orderID"], 'orderID');

            $table->index(["boxID"], 'boxID');

            $table->index(["countryID"], 'countryID');

            $table->index(["addr"], 'addr');

            $table->index(["unrecognised"], 'unrecognised');

            $table->index(["userID"], 'userID');

            $table->index(["boxID", "orderID"], 'key1');

            $table->index(["boxType"], 'boxType');

            $table->index(["coinLabel"], 'coinLabel');

            $table->index(["txID"], 'txID');

            $table->index(["txConfirmed"], 'txConfirmed');

            $table->index(["amount"], 'amount');

            $table->index(["recordCreated"], 'recordCreated');

            $table->index(["amountUSD"], 'amountUSD');

            $table->index(["boxID", "orderID", "userID"], 'key2');

            $table->index(["txCheckDate"], 'txCheckDate');

            $table->index(["processedDate"], 'processedDate');

            $table->index(["txDate"], 'txDate');

            $table->index(["processed"], 'processed');

            $table->unique(["boxID", "orderID", "userID", "txID", "amount", "addr"], 'key3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
