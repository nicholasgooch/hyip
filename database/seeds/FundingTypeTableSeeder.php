<?php

use Illuminate\Database\Seeder;

class FundingTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('funding_types')->insert([
            'name' => 'Bank Transfer',
            'status' => 1,
        ]);
        DB::table('funding_types')->insert([
            'name' => 'Credit Card',
            'status' => 1,
        ]);
        DB::table('funding_types')->insert([
            'name' => 'Paypal',
            'status' => 1,
        ]);
    }
}
