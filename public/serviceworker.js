var staticCacheName = "pwa-v" + new Date().getTime();
var filesToCache = [
    "/offline",
    "/css/app.css",
    "/js/app.js",
    "/landrick/css/style-dark.css",
    "/landrick/css/magnific-popup.css",
    "/landrick/css/owl.carousel.min.css",
    "/landrick/css/owl.theme.default.min.css",
    "/landrick/images/pwa/android-launchericon-96-96.png",
    "/landrick/images/pwa/android-launchericon-144-144.png",
    "/landrick/images/pwa/icon-256x256.png",
    "/landrick/images/pwa/icon-192x192.png",
    "/landrick/images/pwa/icon-384x384.png",
   "/landrick/images/pwa/icon-512x512.png",
  '/landrick/images/pwa/apple-splash-640-1136.png',
    '/landrick/images/pwa/apple-splash-750-1334.png',
   '/landrick/images/pwa/apple-splash-828-1792.png',
   '/landrick/images/pwa/apple-splash-1125-2436.png',
//     '/landrick/images/pwa/apple-splash-1242-2208.png',
//    '/landrick/images/pwa/apple-splash-1536-2048.png',
//    '/landrick/images/pwa/apple-splash-1668-2224.png',
//    '/landrick/images/pwa/apple-splash-1668-2388.png',
//    '/landrick/images/pwa/apple-splash-2048-2732.png',
];

// Cache on install
self.addEventListener("install", (event) => {
    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName).then((cache) => {
            return cache.addAll(filesToCache);
        })
    );
});

// Clear cache on activate
self.addEventListener("activate", (event) => {
    event.waitUntil(
        caches.keys().then((cacheNames) => {
            return Promise.all(
                cacheNames
                    .filter((cacheName) => cacheName.startsWith("pwa-"))
                    .filter((cacheName) => cacheName !== staticCacheName)
                    .map((cacheName) => caches.delete(cacheName))
            );
        })
    );
});

// Serve from Cache
self.addEventListener("fetch", (event) => {
    event.respondWith(
        caches
            .match(event.request)
            .then((response) => {
                return response || fetch(event.request);
            })
            .catch(() => {
                return caches.match("offline");
            })
    );
});
