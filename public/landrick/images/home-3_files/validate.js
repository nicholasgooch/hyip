$(document).ready(function() {
    $('#msform').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your first name'
                    }
                }
            },
            last_name: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your last name'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your email address'
                    },
                    emailAddress: {
                        message: 'Please supply a valid email address'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your phone number'
                    },
                    phone: {
                        country: 'US',
                        message: 'Please supply a vaild phone number with area code'
                    }
                }
            },
            state_code: {
                validators: {
                    notEmpty: {
                        message: 'Please select your state'
                    }
                }
            },
            company: {
                validators: {
                    notEmpty: {
                        message: 'Please select your Company'
                    }
                }
            },
            industry: {
                validators: {
                    notEmpty: {
                        message: 'Please select your industry'
                    }
                }
            },
            url: {
                validators: {
                    notEmpty: {
                        message: 'Please select your Website URL'
                    }
                }
            },
            description: {
                validators: {
                    stringLength: {
                        min: 10,
                        max: 200,
                        message: 'Please enter at least 10 characters and no more than 200'
                    },
                    notEmpty: {
                        message: 'Please supply a description of your project'
                    }
                }
            }
        }
    }).on('success.form.bv', function(e) {
        $('#success_message').slideDown({
            opacity: "show"
        }, "slow")
        $('#msform').data('bootstrapValidator').resetForm();
        e.preventDefault();
        var $form = $(e.target);
        var bv = $form.data('bootstrapValidator');
        var rturl = 'http://mantisfunding.com/webtolead/success?first_name=' + $('#first_name').val() + '&last_name=' + $('#last_name').val() + '&email=' + $('#email').val() + '&company=' + $('#company').val() + '&phone=' + $('#phone').val() + '&state_code=' + $('#state_code').val() + '&industry=' + $('#industry').val() + '&url=' + $('#url').val() + '&description=' + $('#description').val() + '&lead_source=' + $('#lead_source').val();
        $('#rturl').val(rturl);
        $.post($form.attr('action'), $form.serialize(), function(result) {
            console.log(result);
        }, 'json');
    });
});
$(document).ready(function() {
    $('#msform input').keyup(function() {
        var rturl = 'http://mantisfunding.com/webtolead/success?first_name=' + $('#first_name').val() + '&last_name=' + $('#last_name').val() + '&email=' + $('#email').val() + '&company=' + $('#company').val() + '&phone=' + $('#phone').val() + '&state_code=' + $('#state_code').val() + '&industry=' + $('#industry').val() + '&url=' + $('#url').val() + '&description=' + $('#description').text() + '&lead_source=' + $('#lead_source').val();
        $('#rturl').val(rturl);
    });
});
$(document).ready(function() {
    $('.shownextlable').click(function() {
        $(this).addClass('selected', this.checked);
        var choice = $(this).find('input:radio').attr('checked', true);
    })
});
$(function() {
    $("#datepicker").datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $("#datepicker2").datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $("#opdob").datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $("#tid").datepicker({
        dateFormat: 'yy-mm-dd'
    });
});
$(document).ready(function() {
    jQuery('#forget-password').click(function() {
        jQuery('.login-formsigh').hide();
        jQuery('.forget-form').show();
    });
    jQuery('#back-btn').click(function() {
        jQuery('.login-formsigh').show();
        jQuery('.forget-form').hide();
    });
});
$(document).ready(function() {
    var current_fs, next_fs, previous_fs;
    var opacity;
    $(".next").click(function() {
        var form = $("#msform1");
        form.validate({
            rules: {
                business: {
                    required: true
                },
                iso_mobile: {
                    required: true
                },
                email: {
                    required: true
                },
                cnumber: {
                    required: true
                },
                phone: {
                    required: true
                },
                iso_website: {
                    required: true
                },
                company_address: {
                    required: true
                },
                comapny_city: {
                    required: true
                },
                company_state: {
                    required: true
                },
                zip_code: {
                    required: true
                },
                tax_id: {
                    required: true
                },
                yrs_business: {
                    required: true
                },
                entity: {
                    required: true
                },
                state_entity: {
                    required: true
                },
                month: {
                    required: true
                },
                deal: {
                    required: true
                },
                number: {
                    required: true
                },
                number_employee: {
                    required: true
                },
                full_legal_name: {
                    required: true
                },
                dob: {
                    required: true
                },
                home_address: {
                    required: true
                },
                year_owned: {
                    required: true
                },
                comapny_city: {
                    required: true
                },
                company_state: {
                    required: true
                },
                state_zip_code: {
                    required: true
                },
                valid_ssn: {
                    required: true
                },
                ownership: {
                    required: true
                },
                iso_title: {
                    required: true
                },
                iso_Signed: {
                    required: true
                },
                login_id: {
                    required: true
                },
                password: {
                    required: true
                },
                password_confirmation: {
                    required: true
                },
                iso_mobile: {
                    required: true
                },
                /*iso_print_name: {
                    required: true
                },
                pphone: {
                    required: true
                },
                opfname: {
                    required: true
                },
                oplname: {
                    required: true
                },
                opbusiness_owned: {
                    required: true
                },
                optitle: {
                    required: true
                },
                opsocial_security: {
                    required: true
                },
                opdob: {
                    required: true
                },
                opdl: {
                    required: true
                },
                opstreet_address: {
                    required: true
                },
                opaddress_line: {
                    required: true
                },
                opcity: {
                    required: true
                },
                opstate: {
                    required: true
                },
                opzip: {
                    required: true
                },
                oppown_rent: {
                    required: true
                },
                otheremail: {
                    required: true
                },
                otherphone: {
                    required: true
                },
                landlord_name: {
                    required: true
                },
                landlord_phone: {
                    required: true
                },
                monthly_rent: {
                    required: true
                },
                dl: {
                    required: true
                },
                street_address: {
                    required: true
                },
                address_line: {
                    required: true
                },
                Rcity: {
                    required: true
                },
                Rstate: {
                    required: true
                },
                Rzip: {
                    required: true
                },
                Rown_rent: {
                    required: true
                },
                Signed: {
                    required: true
                },
                datea: {
                    required: true
                },
                print_name: {
                    required: true
                },
                ttitle: {
                    required: true
                },
                q_answer1: {
                    required: true
                },
                q_answer2: {
                    required: true
                },
                q_answer3: {
                    required: true
                },
                q_answer4: {
                    required: true
                },
                q_answer5: {
                    required: true
                },
                q_answer6: {
                    required: true
                },
                q_answer7: {
                    required: true
                },
                q_answer8: {
                    required: true
                },
                q_answer9: {
                    required: true
                },
                q_answer10: {
                    required: true
                },
                q_answer11: {
                    required: true
                },
                q_answer12: {
                    required: true
                },
                q_answer13: {
                    required: true
                },
                q_answer14: {
                    required: true
                },
                q_answer15: {
                    required: true
                },
                q_answer16: {
                    required: true
                },
                q_answer17: {
                    required: true
                },
                q_answer18: {
                    required: true
                },
                q_answer19: {
                    required: true
                },
                q_answer20: {
                    required: true
                },
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                login_id: {
                    required: true
                },
                password: {
                    required: true
                },
                password_confirmation: {
                    required: true
                },
                company_legal_name: {
                    required: true
                },
                company_legal_reg: {
                    required: true
                },
                company_year: {
                    required: true
                },
                company_location: {
                    required: true
                },
                company_address: {
                    required: true
                },
                comapny_city: {
                    required: true
                },
                company_state: {
                    required: true
                },
                company_mobile: {
                    required: true
                },
                company_contact_point: {
                    required: true
                },
                no_of_emp: {
                    required: true
                },
                salesrepo: {
                    required: true
                },
                comapny_contact_person: {
                    required: true
                },
                comapny_contact_mobile: {
                    required: true
                },
                iso_email: {
                    required: true
                },
                identity: {
                    required: true
                },
                iso_Signed: {
                    required: true
                },
                iso_print_name: {
                    required: true
                },
                iso_mobile: {
                    required: true
                }*/
            },
        });
        if (form.valid() == true) {
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        }
        next_fs.show();
        current_fs.animate({
            opacity: 0
        }, {
            step: function(now) {
                opacity = 1 - now;
                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                next_fs.css({
                    'opacity': opacity
                });
            },
            duration: 600
        });
    });
    $(".previous").click(function() {
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
        previous_fs.show();
        current_fs.animate({
            opacity: 0
        }, {
            step: function(now) {
                opacity = 1 - now;
                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                previous_fs.css({
                    'opacity': opacity
                });
            },
            duration: 600
        });
    });
    $('.radio-group .radio').click(function() {
        $(this).parent().find('.radio').removeClass('selected');
        $(this).addClass('selected');
    });
    $(".submit").click(function() {
        return false;
    })
});
var loanYear = 20;
var stepYear = 1;
var maxLoanYear = 10;
var paymentCycle = 1;
var monthlyRepayment = 0;
var monthlyInterest = 0;
var amortData = [];
$(function() {
    $(".ul-buttons li").click(function() {
        $(".ul-buttons li").removeClass("selected");
        $(this).addClass("selected");
        paymentCycle = parseInt($(this).attr("data-value"));
        calculateLoan();
    });
    $("#txtLoan, #txtInterest").on("blur", function() {
        if (isNaN($("#txtLoan").val())) {
            $("#txtLoan").val(1000000);
        }
        if (isNaN($("#txtInterest").val())) {
            $("#txtInterest").val(8.99);
        }
        calculateLoan();
    });
});
var range = document.getElementById('yearRange');
noUiSlider.create(range, {
    range: {
        'min': 1,
        'max': maxLoanYear
    },
    step: stepYear,
    start: [loanYear],
    margin: 300,
    connect: true,
    direction: 'ltr',
    orientation: 'horizontal',
    pips: {
        mode: 'steps',
        stepped: false,
        density: 2
    }
});
range.noUiSlider.on("change", function(value) {
    loanYear = parseInt(value[0]);
    calculateLoan();
});
google.charts.load('current', {
    'packages': ['corechart']
});

function drawChart() {
    var loanData = [];
    var dt = new Date();
    var yearCounter = 1;
    var headerData = ['Year', 'Interest', 'Interest & Principal', 'Balance'];
    loanData.push(headerData);
    for (var i = dt.getFullYear(); i < dt.getFullYear() + loanYear; i++) {
        loanData.push([i.toString(), getAmortData("interest", 12 * yearCounter), monthlyRepayment * 12 * yearCounter, getAmortData("balance", 12 * yearCounter)]);
        yearCounter++;
    }
    var data = google.visualization.arrayToDataTable(loanData);
    var options = {
        title: 'Cash Chart',
        hAxis: {
            title: 'Year',
            titleTextStyle: {
                color: '#333'
            }
        },
        vAxis: {
            minValue: 0
        },
        pointsVisible: true
    };
    var chart = new google.visualization.AreaChart(document.getElementById('graph-chart'));
    chart.draw(data, options);
}

function getAmortData(dataType, terms) {
    var dataValue = 0;
    switch (dataType) {
        case "interest":
            for (var i = 0; i < terms; i++) {
                dataValue += parseFloat(amortData[i].Interest);
            }
            break;
        case "balance":
            dataValue = parseFloat(amortData[terms - 1].Balance);
            break;
    }
    return Math.round(dataValue);
}

function calculateLoan() {
    $("#year-value").html(loanYear);
    var loanBorrow = parseFloat($("#txtLoan").val());
    var interestRate = parseFloat($("#txtInterest").val()) / 1200;
    var totalTerms = 12 * loanYear;
    var schedulePayment = Math.round(loanBorrow * interestRate / (1 - (Math.pow(1 / (1 + interestRate), totalTerms))));
    monthlyRepayment = schedulePayment;
    var totalInterestPay = totalTerms * schedulePayment;
    amort(loanBorrow, parseFloat($("#txtInterest").val()) / 100, totalTerms);
    switch (paymentCycle) {
        case 2:
            var ddd = 'Fortnight';
            schedulePayment = Math.round(((schedulePayment * 12) / 52) * 2);
            break;
        case 3:
            var ddd = 'Week';
            schedulePayment = Math.round((schedulePayment * 12) / 52);
            break;
    }
    $("#repayment-value").html(schedulePayment);
    $("#ddd").html(ddd);
    $("#interest-total").html(getAmortData("interest", totalTerms));
    monthlyInterest = (totalInterestPay - loanBorrow) / totalTerms;
    google.charts.setOnLoadCallback(drawChart);
}
calculateLoan();

function amort(balance, interestRate, terms) {
    amortData = [];
    var monthlyRate = interestRate / 12;
    var payment = balance * (monthlyRate / (1 - Math.pow(1 + monthlyRate, -terms)));
    for (var count = 0; count < terms; ++count) {
        var interest = balance * monthlyRate;
        var monthlyPrincipal = payment - interest;
        var amortInfo = {
            Balance: balance.toFixed(2),
            Interest: balance * monthlyRate,
            MonthlyPrincipal: monthlyPrincipal
        }
        amortData.push(amortInfo);
        balance = balance - monthlyPrincipal;
    }
}