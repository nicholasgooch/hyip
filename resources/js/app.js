/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');


import VeeValidate from 'vee-validate';
import Vue from 'vue';
import './filters.js'; // Import your custom filters

Vue.use(VeeValidate);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('nofitications', require('./components/notifications.vue').default);
Vue.component('deposit', require('./components/deposit.vue').default);
Vue.component('invest', require('./components/invest.vue').default);
Vue.component('file-upload', require('./components/widgets/fileupload.vue').default);
Vue.component('buy-coin', require('./components/exchange/buy.vue').default);
Vue.component('apply-page', require('./components/loan/apply.vue').default);
Vue.component('funding-details', require('./components/loan/funding-details.vue').default);
Vue.component('funding-withdrawal', require('./components/loan/funding-withdrawal.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.prototype.$http= window.axios
const app = new Vue({
    el: '#app',
    data(){
        return {
            settings: {}
        }
    },
    mounted(){
        this.getSettings()
    },
    methods: {
        getSettings(){
            this.$http.get('/loan/get/settings').then(({data}) => {
              this.settings = data
            })
          }
    }
});
