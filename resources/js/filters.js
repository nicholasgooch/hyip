import Vue from 'vue';

Vue.filter('number_format', function(value) {
  if (value || value === 0) {
    let amount = parseFloat(value);
    return amount.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  } else {
    return '0.00';
  }
});

Vue.filter('formatFirstChar', function(value) {
     value = value.toString();
      value = value.replace(/-/g, " ");
      value = value.replace(/(^|\s)\S/g, (letter) => letter.toUpperCase());
      value = value.replace(/=\s*\S/g, (letter) => letter.toUpperCase());
      return value;
  });

  Vue.filter('getDollarSign', function(value) {
      const arg = value
        if (arg == null) {
            return arg;
        }
        return arg == "USD" ? "$" : arg;
 });