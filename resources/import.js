import { Validator } from 'vee-validate';

// Define a custom validation rule for checking if the withdrawal amount is not greater than the account balance
Validator.extend('balance', {
  validate(value,  balance ) {
      console.log(balance)
    return parseFloat(value) <= parseFloat(balance);
  },
  getMessage: (field,  balance ) => `The withdrawal amount cannot be greater than your account balance of $${balance}`,
});

// Define a custom validation rule for checking if the withdrawal amount is not greater than the max withdrawal limit set by the admin
Validator.extend('max', {
  validate(value, maxWithdrawalAmount ) {
    return parseFloat(value) <= parseFloat(maxWithdrawalAmount);
  },
  getMessage: (field,  maxWithdrawalAmount ) => `The withdrawal amount cannot be greater than the maximum withdrawal limit of $${maxWithdrawalAmount}`,
});