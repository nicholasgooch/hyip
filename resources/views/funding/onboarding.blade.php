@extends('v2.layouts.main') 
@section('styles')
    <link id="skin-default" rel="stylesheet" href="{{ asset('loan/css/dashboard.css?ver=3.1.3') }}">
    <link rel="stylesheet" href="{{asset('/loan/css/dashlite.css?ver=3.1.3')}}">
@endsection
@section('content')
<div class="nk-content nk-content-lg nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="kyc-app wide-sm m-auto">
                    <div class="nk-block-head nk-block-head-lg wide-xs mx-auto">
                        <div class="nk-block-head-content text-center">
                            <h2 class="nk-block-title fw-normal">Funding Onboarding</h2>
                            <div class="nk-block-des">
                                <p>Welcome to {{$settings->site_title}}, the platform that connects businesses with the funding they need to grow and succeed. Our mission is to provide a fast, easy, and transparent way for businesses to access the funding they need, when they need it.</p>
                            </div>
                        </div>
                    </div><!-- .nk-block-head -->
                    <div class="nk-block">
                        <div class="card card-bordered">
                            <div class="card-content">
                                <ul class="nav nav-tabs nav-tabs-mb-icon nav-tabs-card" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link active" data-bs-toggle="tab" href="#feature" aria-selected="true" role="tab"><em class="icon ni ni-list-index"></em><span>Funding Options</span></a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" data-bs-toggle="tab" href="#eligibility" aria-selected="false" role="tab" tabindex="-1"><em class="icon ni ni-user-check-fill"></em><span>Eligibility</span></a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" data-bs-toggle="tab" href="#eligibility" aria-selected="false" role="tab" tabindex="-1"><em class="icon ni ni-user-check-fill"></em><span>Collateral Requirements</span></a>
                                    </li>
                                </ul><!-- .nav-tabs -->
                                <div class="card-inner">
                                    <div class="tab-content">
                                        <div class="tab-pane active show" id="feature" role="tabpanel">
                                            <h6 class="title mb-2">Our service is designed to support your various needs including:</h6>
                                            <div>
                                                <ul class="list-styled">
                                                    <li class="list-item">Variety of funding options</li>
                                                    <li class="list-item">Fast and simple application process</li>
                                                    
                                                    <li class="list-item">Competitive rates </li>
                                                    <li class="list-item">Flexible repayment options</li>
                                                    <li class="list-item">Fast and simple application process</li>
                                                    <li class="list-item">Access to funding experts</li>
                                                    <li class="list-item">Quick funding disbursement</li>
                                                </ul>
                                               
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="eligibility" role="tabpanel">
                                            <h6 class="title mb-2">Our service is designed to support your various needs including:</h6>
                                            <ul class="list-styled">
                                                <li class="list-item">Businesses or individuals must provide financial statements to show their current financial status, including revenue, expenses, and cash flow.</li>
                                                <li class="list-item">Purpose of funding: Lenders may have specific restrictions on what the funds can be used for, such as business expansion or inventory purchases.</li>
                                                <li class="list-item">Collateral: Some loans require collateral, such as crypto-asset or cash, to secure the loan.</li>
                                                <li class="list-item">Domestic or foreign travel</li>
                                                <li class="list-item">Medical treatment for self/family members</li>
                                                <li class="list-item">Special Quota like superpatriot, aged &amp; disable people</li>
                                                <li class="list-item">Purchase of consumer durables</li>
                                                <li class="list-item">Education</li>
                                                <li class="list-item">Other needs</li>
                                            </ul>
                                           
                                        </div>
                                    </div>
                                    <div class="mx-auto mt-5">
                                        <a href="{{route('funding.onboard')}}" class="btn btn-lg btn-primary">Get Started</a>
                                    </div>
                                </div>
                            </div><!-- .card-content -->
                        </div><!-- .card -->
                    </div>
                    
                </div><!-- .kyc-app -->
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('loan/js/bundle.js?ver=3.1.3') }}"></script>

@endsection