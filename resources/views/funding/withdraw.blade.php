@extends('funding.layouts.app')
@section('styles')
    {{-- <link  rel="stylesheet" href="{{ asset('loan/css/dashboard.css?ver=3.1.3') }}"> --}}
@endsection
@section('content')
    <div class="nk-content ">
        <div class="container-fluid">
            <div class="nk-content-inner">
                <div class="nk-content-body">
                    <div class="nk-block-head nk-block-head-sm">
                        <div class="nk-block-between">
                            <div class="nk-block-head-content">
                                <h4 class="nk-block-title page-title">Dashboard</h4>
                            </div><!-- .nk-block-head-content -->
                        </div><!-- .nk-block-between -->
                    </div><!-- .nk-block-head -->
                    {{-- @dd($user->fundings[0]->available_to_withdraw) --}}
                   
                    <funding-withdrawal  :settings="{{$settings->toJson()}}" :account-balance="{{$user->wallet}}" :funding-balance={{isset($user->fundings)? 0 :$user->fundings[0]->available_to_withdraw}} />
                </div>
            </div>
        </div>
    </div>

@endsection

