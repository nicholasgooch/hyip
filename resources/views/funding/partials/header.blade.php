<div class="nk-header nk-header-fluid nk-header-fixed is-light">
    <div class="container-fluid">
        <div class="nk-header-wrap">
            <div class="nk-menu-trigger d-xl-none ms-n1">
                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
            </div>
            <div class="nk-header-brand d-xl-none">
                <a href="html/loan/index.html" class="logo-link">
                    <img class="logo-light logo-img" src="{{$settings->site_logo}}" srcset="{{$settings->site_logo}} 2x" alt="logo">
                    <img class="logo-dark logo-img" src="{{$settings->site_logo}}" srcset="{{$settings->site_logo}} 2x" alt="logo-dark">
                    <span class="nio-version">{{$settings->site_title}}</span>
                </a>
            </div>
            <div class="nk-header-news d-none d-xl-block">
                <div class="nk-news-list">
                    <a class="nk-news-item" href="#">
                        <div class="nk-news-icon">
                            <em class="icon ni ni-card-view"></em>
                        </div>
                        <div class="nk-news-text">
                            <p>Do you know the latest update of 2022? <span> A overview of our is now available on YouTube</span></p>
                            <em class="icon ni ni-external"></em>
                        </div>
                    </a>
                </div>
            </div>
            <div class="nk-header-tools">
                <ul class="nk-quick-nav">
                    <li class="dropdown user-dropdown">
                        <a href="#" class="dropdown-toggle" data-bs-toggle="dropdown">
                            <div class="user-toggle">
                                <div class="user-avatar sm">
                                    <em class="icon ni ni-user-alt"></em>
                                </div>
                                <div class="user-info d-none d-md-block">
                                    <div class="user-status user-status-unverified">
                                        @if ($user->kyc_status == 0)
                                                    <div class="user-status user-status-unverified">Unverified</div>
                                                @else
                                                     <div class="user-status user-status-verified">Verified</div>
                                                 @endif
                                    </div>
                                    <div class="user-name dropdown-indicator">{{$user->firstname . ' '. $user->lastname}}</div>
                                </div>
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                <div class="user-card">
                                    <div class="user-avatar">
                                        <span>{{substr(strtoupper($user->firstname),0,1)}}{{substr(strtoupper($user->lastname),0,1)}}</span>
                                    </div>
                                    <div class="user-info">
                                        <span class="lead-text">{{$user->firstname . ' '. $user->lastname}}</span>
                                        <span class="sub-text">{{$user->email}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-inner user-account-info">
                                <h6 class="overline-title-alt"> Wallet Balance</h6>
                                <div class="user-balance">{{number_format($user->wallet,2)}} <small class="currency currency-btc">{{$settings->currency}}</small></div>
                                <div class="user-balance-sub">Locked <span>0.344939 <span class="currency currency-btc">BTC</span></span></div>
                                <a href="{{route('funding.withdraw')}}" class="link"><span>Withdraw Funds</span> <em class="icon ni ni-wallet-out"></em></a>
                            </div>
                            <div class="dropdown-inner">
                                <ul class="link-list">
                                    <li><a href="html/loan/profile.html"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                    <li><a href="html/loan/security-settings.html"><em class="icon ni ni-setting-alt"></em><span>Security Setting</span></a></li>
                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                </ul>
                            </div>
                            <div class="dropdown-inner">
                                <ul class="link-list">
                                    <li><a href="#"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </li>


                </ul>
            </div>
        </div>
    </div>
</div>
