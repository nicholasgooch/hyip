<div class="nk-sidebar nk-sidebar-fixed is-light " data-content="sidebarMenu">
    <div class="nk-sidebar-element nk-sidebar-head">
        <div class="nk-sidebar-brand">
            <a href="" class="logo-link nk-sidebar-logo">
                <img class="logo-light logo-img" src="{{$settings->site_logo}}" srcset="{{$settings->site_logo}} 2x" alt="logo">
                <img class="logo-dark logo-img" src="{{$settings->site_logo}}" srcset="{{$settings->site_logo}} 2x" alt="logo-dark">
                <span class="nio-version text-danger">Funding</span>
            </a>
        </div>
        <div class="nk-menu-trigger me-n2">
            <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
        </div>
    </div><!-- .nk-sidebar-element -->
    <div class="nk-sidebar-element">
        <div class="nk-sidebar-body" data-simplebar>
            <div class="nk-sidebar-content">
                <div class="nk-sidebar-widget d-none d-xl-block">
                    <div class="user-account-info between-center">
                        <div class="user-account-main">
                            <h6 class="overline-title-alt">Wallet Balance</h6>
                            <div class="user-balance"> {{number_format($user->wallet,2)}} <small class="currency">{{$settings->currency}}</small></div>
                            <div class="user-account-label">
                               {{toBtc($user->wallet)}} BTC
                            </div>
                        </div>
                        <a href="javascript:void(0)" class="btn btn-white btn-icon btn-light"><em class="icon ni ni-line-chart"></em></a>
                    </div>
                    <ul class="user-account-data gy-1">
                        <li>
                            <div class="user-account-label">
                                <span class="overline-title-alt">Interest</span>
                            </div>
                            <div class="user-account-value">
                                <span class="sub-title text-base">4 <span class="currency">%</span></span>
                            </div>
                        </li>
                    </ul>
                    <div class="user-account-actions">
                        <ul class="g-3">
                            <li><a href="{{route('funding.deposit')}}?t=funding&c=deposit" class="btn btn-lg btn-danger"><span>Deposit</span></a></li>
                            @if ($user->fundings->isNotEmpty())
                                <li><a href="{{route('funding.apply.details')}}" class="btn btn-lg btn-outline-danger"><span>Details</span></a></li>
                            @else
                                <li><a href="{{route('funding.apply')}}" class="btn btn-lg btn-outline-danger"><span>Apply</span></a></li>
                            @endif

                        </ul>
                    </div>
                </div><!-- .nk-sidebar-widget -->

                <div class="nk-sidebar-menu">
                    <!-- Menu -->
                    <ul class="nk-menu">
                        <li class="nk-menu-heading">
                            <h6 class="overline-title">Menu</h6>
                        </li>
                        <li class="nk-menu-item">
                            <a href="{{route('funding.dashboard')}}" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-dashboard"></em></span>
                                <span class="nk-menu-text">Dashboard</span>
                            </a>
                        </li>
                        <li class="nk-menu-item">
                            <a href="{{route('funding.applications')}}" class="nk-menu-link">
                                <span class="nk-menu-icon">
                                    <em class="icon ni ni-list-index"></em></span>
                                <span class="nk-menu-text">Applications</span>
                            </a>
                        </li>
                        <li class="nk-menu-item has-sub">
                            <a href="{{route('funding.types')}}" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-swap"></em></span>
                                <span class="nk-menu-text">Funding Types</span>
                            </a>

                        </li>

                        <li class="nk-menu-item">
                            <a href="{{route('funding.withdraw')}}" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-coin-alt"></em></span>
                                <span class="nk-menu-text">Withdraw Funds</span>
                            </a>
                        </li>
                        <li class="nk-menu-item">
                            <a href="{{route('funding.transactions')}}" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-calc"></em></span>
                                <span class="nk-menu-text">Transactions</span>
                            </a>
                        </li>
                        <li class="nk-menu-heading">
                            <h6 class="overline-title">Return to</h6>
                        </li>
                        <li class="nk-menu-item">
                            <a href="{{route('v2.index')}}" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-dashlite"></em></span>
                                <span class="nk-menu-text">Investment Dashboard</span>
                            </a>
                        </li>

                    </ul><!-- .nk-menu -->
                </div><!-- .nk-sidebar-menu -->
                <div class="nk-sidebar-footer">
                    <ul class="nk-menu nk-menu-footer">
                        <li class="nk-menu-item">
                            <a href="#" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-help-alt"></em></span>
                                <span class="nk-menu-text">Support</span>
                            </a>
                        </li>
                        <li class="nk-menu-item ms-auto">
                            <div class="dropup">
                                <a href="#" class="nk-menu-link dropdown-indicator has-indicator" data-bs-toggle="dropdown" data-bs-offset="0,10">
                                    <span class="nk-menu-icon"><em class="icon ni ni-globe"></em></span>
                                    <span class="nk-menu-text">English</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-end">
                                    <ul class="language-list">
                                        <li>
                                            <a href="#" class="language-item">
                                                <img src="./images/flags/english.png" alt="" class="language-flag">
                                                <span class="language-name">English</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="language-item">
                                                <img src="./images/flags/spanish.png" alt="" class="language-flag">
                                                <span class="language-name">Español</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="language-item">
                                                <img src="./images/flags/french.png" alt="" class="language-flag">
                                                <span class="language-name">Français</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="language-item">
                                                <img src="./images/flags/turkey.png" alt="" class="language-flag">
                                                <span class="language-name">Türkçe</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul><!-- .nk-footer-menu -->
                </div><!-- .nk-sidebar-footer -->
            </div><!-- .nk-sidebar-content -->
        </div><!-- .nk-sidebar-body -->
    </div><!-- .nk-sidebar-element -->
</div>
