@extends('funding.layouts.app') 

@section('content')
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-body">
            <div class="nk-block-head">
                <div class="nk-block-head-sub"><span>Account Balance</span></div>
                <div class="nk-block-between-md g-4">
                    <div class="nk-block-head-content">
                        <h2 class="nk-block-title fw-normal">Loan Details</h2>
                        <div class="nk-block-des">
                            <p>At a glance summary of your account. Have fun!</p>
                        </div>
                    </div>
                    <div class="nk-block-head nk-block-head-sm nk-block-between">
                        <div class="btn-group">
                            <button type="button" class="btn btn-danger dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><span>Loan List</span> <em class="icon ni ni-chevron-down"></em></button>
                            <div class="dropdown-menu dropdown-menu-end" style="">
                                <ul class="link-list-opt no-bdr">
                                    <li><a href="#"><span>Home Loan (Pro)</span></a></li>
                                    <li><a href="#"><span>Personal Loan (Standard)</span></a></li>
                                    <li><a href="#"><span>SME Loan (Pro)</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- .nk-block-head -->
                </div>
            </div><!-- .nk-block-head -->
            <funding-details application_id="{{$application->id}}">
        </div>
    </div>
</div>
@endsection