@extends('v2.layouts.main')
@section('style')
@section('title', 'Deposit Fund')
<link rel="stylesheet" href="{{ asset('v2/assets/css/dashlite-crypto.css') }}" />

@endsection

@section('content')
<div class="nk-content nk-content-lg nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head">
                    <div class="nk-block-head-content">
                        <div class="nk-block-head-sub">
                            <a href="{{ route('v2.index') }}" class="back-to"><em
                                    class="icon ni ni-arrow-left"></em><span>Back to Overview</span></a>
                        </div>
                        <div class="nk-block-between-md g-4">
                            <div class="nk-block-head-content">
                                <h2 class="nk-block-title fw-normal">
                                    Deposit @if ($deposit_category == 'subscription')
                                        Subscription
                                    @elseif($deposit_category == 'collateral')
                                        Collateral
                                    @endif
                                </h2>
                                <div class="nk-block-des">
                                    <p>
                                         @if ($deposit_category == 'subscription')
                                         Welcome to our deposit subscription page! We offer multiple payment options to make it  easy and convenient for you to deposit funds into your account. Whether you
                                            prefer to use cryptocurrencies like BTC, ETH, or USDC, or prefer traditional
                                            bank transfers, we've got you covered. Choose your preferred payment method and
                                            amount to get started.
                                        @elseif($deposit_category == 'collateral')
                                        Welcome to the deposit collateral page, where you can securely deposit your collateral and apply for a loan. Our platform offers multiple payment methods, including BTC, ETH, USDC, and bank transfer, to make it easy for you to deposit your collateral. Simply choose your preferred payment method and deposit amount, and we'll take care of the rest. With our streamlined process and cutting-edge security features, you can be confident that your collateral is in good hands.
                                        @else
                                        Welcome to the deposit page for your portfolio wallet. Here, you can securely deposit funds into your wallet using a variety of payment methods, including BTC, ETH, USDC, and bank transfer. Simply choose the payment method that works best for you, select the amount you wish to deposit, and follow the instructions to complete your transaction. With a funded portfolio wallet, you'll have the flexibility and power to invest in a variety of assets, helping you achieve your financial goals faster and more efficiently. Let's get started!
                                        @endif
                                    </p>
                                </div>
                            </div>
                            @if ($deposit_type == 'invest')
                                <!-- .nk-block-head-content -->

                                <div class="nk-block-head-content">
                                    <ul class="nk-block-tools gx-3">
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#modalDefault"
                                                class="btn btn-primary"><span>Withdraw</span>
                                                <em
                                                    class="icon ni ni-arrow-long-right d-none d-sm-inline-block"></em></a>
                                        </li>

                                        <li>
                                            <a href="{{ route('v2.investments.plan') }}"
                                                class="btn btn-white btn-light"><span>Invest More</span>
                                                <em
                                                    class="icon ni ni-arrow-long-right d-none d-sm-inline-block"></em></a>
                                        </li>
                                        <li class="opt-menu-md dropdown">
                                            <a href="{{ route('v2.user.settings') }}"
                                                class="btn btn-white btn-light btn-icon" data-toggle="dropdown"><em
                                                    class="icon ni ni-setting"></em></a>

                                        </li>

                                    </ul>
                                </div>
                                <!-- .nk-block-head-content -->
                            @elseif($deposit_type == 'funding')
                                <!-- .nk-block-head-content -->
                                <div class="nk-block-head-content">
                                    <ul class="nk-block-tools gx-3">
                                        <li>
                                            <a href="{{ route('funding.dashboard') }}" data-toggle="modal"
                                                data-target="#modalDefault"
                                                class="btn btn-primary"><span>Dashboard</span>
                                                <em
                                                    class="icon ni ni-arrow-long-right d-none d-sm-inline-block"></em></a>
                                        </li>

                                    </ul>
                                </div>
                                <!-- .nk-block-head-content -->
                            @endif
                        </div>
                    </div>
                </div>
                @if ($deposit_category == 'subscription')
                    <div class="nk-block">
                        <div class="example-alert">
                            <div class="alert alert-primary alert-icon">
                                <em class="icon ni ni-alert-circle"></em> <strong></strong>. Your will be redirect for make your payment.
                            </div>
                        </div>
                    </div>
                @endif

                <div class="nk-block">
                    <div class="row">
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="card card-bordered">
                                <div class="card-inner-group">
                                    <div class="card-inner" id="app">
                                        <deposit amount="{{$amount ?? 0}}" ></deposit>
                                    </div>
                                </div>
                                <!-- .buysell-block -->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="">
                                <div class="col-md-12">
                                    <div class="card card-bordered mt-3">
                                        <div class="card-inner-group">
                                            <div class="card-inner">
                                                <div class="card-head">
                                                    <h6 class="title">Deposit Graphical Data</h6>
                                                </div>
                                            </div>
                                            <div class="nk-ck-sm">
                                                <canvas class="line-chart" id="statisticsChart2"></canvas>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-12 ">
                                <div class="card card-bordered mt-2 mb-2">

                                    <div class="card-inner p-0 border-top">
                                        <div class="nk-tb-list nk-tb-orders">
                                            <div class="nk-tb-item nk-tb-head">
                                                <div class="nk-tb-col nk-tb-orders-type"><span>Type</span></div>

                                                <div class="nk-tb-col tb-col-sm"><span>Date</span></div>
                                                <div class="nk-tb-col tb-col-xl"><span>Ref</span></div>
                                                <div class="nk-tb-col tb-col-xxl"><span></span></div>
                                                <div class="nk-tb-col tb-col-sm text-right">
                                                    <span>{{ $settings->currency }} Amount</span></div>
                                                <div class="nk-tb-col text-right"><span></span></div>
                                            </div><!-- .nk-tb-item -->
                                            @foreach ($deposits->take(5) as $dep)
                                                <div class="nk-tb-item">
                                                    <div class="nk-tb-col nk-tb-orders-type">
                                                        <ul class="icon-overlap">
                                                            @if ($dep->bank == 'ETH')
                                                                <li><em
                                                                        class="bg-eth-dim icon-circle icon ni ni-sign-eth"></em>
                                                                </li>
                                                            @elseif($dep->bank == 'BTC')
                                                                <li>
                                                                    <em
                                                                        class="bg-btc-dim icon-circle icon ni ni-sign-btc"></em>
                                                                </li>
                                                            @else
                                                                <li>
                                                                    <em
                                                                        class="bg-eth-dim icon-circle icon ni ni-building"></em>
                                                                </li>
                                                            @endif
                                                            <li><em
                                                                    class="bg-success-dim icon-circle icon ni ni-arrow-down-left"></em>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="nk-tb-col tb-col-sm">
                                                        <span
                                                            class="tb-sub">{{ $dep->created_at->diffForHumans() }}</span>
                                                    </div>
                                                    <div class="nk-tb-col tb-col-xl">
                                                        <span class="tb-sub">{{ $dep->ref }}</span>
                                                    </div>
                                                    <div class="nk-tb-col tb-col-xxl">
                                                        <span
                                                            class="tb-sub text-primary">{{ strtoupper($dep->ref) }}</span>
                                                    </div>
                                                    <div class="nk-tb-col tb-col-sm text-right">
                                                        <span
                                                            class="tb-sub tb-amount"><span>{{ $settings->currency == 'USD' ? '$' : $settings->curency }}</span>{{ number_format(round($dep->amount, 6)) }}
                                                        </span>
                                                    </div>
                                                    <div class="nk-tb-col text-right">
                                                        @if ($dep->bank == 'ETH')
                                                            <span class="tb-sub tb-amount ">
                                                                {{ toETH(round($dep->amount, 6)) }}
                                                                <span>ETH</span></span>
                                                        @elseif($dep->bank == 'BTC')
                                                            <span class="tb-sub tb-amount ">
                                                                {{ toBTC(round($dep->amount, 6)) }}
                                                                <span>BTC</span></span>
                                                        @endif

                                                    </div>
                                                </div><!-- .nk-tb-item -->
                                            @endforeach


                                        </div>
                                    </div><!-- .card-inner -->

                                </div><!-- .card -->
                            </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('v2/assets/js/bundle.js?ver=2.9.0') }}"></script>

<script src="{{ asset('v2/assets/js/scripts.js?ver=2.9.0') }}"></script>

<script src="{{ asset('v2/assets/js/charts/chart-crypto.js?ver=2.9.0') }}"></script>

<script src="{{ asset('js/app.js') }}"></script>
<script>
    var set_dates = [];
    var set_data = [];

    $.ajax({
        url: "/app/deposit/chart",
        method: "GET",
        success: function(response) {
            console.log(response);
            // response = {!! json_encode($myInv) !!}
            var js_inv = response;
            $.each(js_inv, function(k, response) {
                // $('#prnt').append(', ' +ky+": "+val['created_at']);
                var dt = moment(new Date(response["created_at"])).format(
                    "MM/YY"
                ); //new Date(val['created_at']);
                set_dates[k] = dt; // dt.getMonth() + '/'+ dt.getFullYear();
                set_data[k] = response["amount"];
            });

            var ctx = document
                .getElementById("statisticsChart2")
                .getContext("2d");

            var dp_stats = new Chart(ctx, {
                type: "line",
                scaleFontColor: "#CCC",
                data: {
                    labels: set_dates, //["Jan", "Feb", "Mar"],
                    datasets: [{
                        label: "Deposit: ",
                        legendColor: "#0361E3",
                        fill: true,
                        borderWidth: 2,
                        backgroundColor: "#7aa5eb",
                        borderWidth: 2,
                        borderColor: "transparent",
                        hoverBorderColor: "transparent",
                        borderSkipped: "bottom",
                        pointBorderColor: "#0361E3",
                        pointBackgroundColor: "#fff",
                        pointHoverBackgroundColor: "#fff",
                        pointHoverBorderColor: "#0361E3",
                        pointBorderWidth: 2,
                        pointHoverRadius: 4,
                        pointHoverBorderWidth: 2,
                        pointRadius: 4,
                        pointHitRadius: 4,

                        data: set_data, //[154, 184, 175]
                    }, ],
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: false,
                        labels: {
                            boxWidth: 30,
                            padding: 20,
                            fontColor: "#6783b8",
                        },
                    },
                    tooltips: {
                        enabled: true,
                        bodySpacing: 4,
                        xPadding: 10,
                        yPadding: 10,
                        caretPadding: 10,
                        backgroundColor: "#eff6ff",
                        titleFontSize: 13,
                        titleFontColor: "#6783b8",
                        titleMarginBottom: 6,
                        bodyFontColor: "#9eaecf",
                        bodyFontSize: 12,
                        footerMarginTop: 0,
                        displayColors: false,
                    },

                    scales: {
                        yAxes: [{
                            display: true,
                            ticks: {
                                fontStyle: "500",
                                beginAtZero: false,
                                maxTicksLimit: 5,
                                padding: 10,
                                fontSize: 12,
                                fontColor: "#9eaecf",
                            },
                            gridLines: {
                                color: "transparent",
                                tickMarkLength: 10,
                                zeroLineColor: NioApp.hexRGB(
                                    "#526484",
                                    0.2
                                ),
                                offsetGridLines: true,
                            },
                        }, ],
                        xAxes: [{
                            display: true,
                            ticks: {
                                fontSize: 12,
                                fontColor: "#9eaecf",
                                source: "auto",
                                padding: 5,
                            },
                            gridLines: {
                                color: "transparent",
                                tickMarkLength: 10,
                                zeroLineColor: NioApp.hexRGB(
                                    "#526484",
                                    0.2
                                ),
                                offsetGridLines: true,
                            },
                        }, ],
                    },
                    legendCallback: function(chart) {
                        var text = [];
                        text.push(
                            '<ul class="' +
                            chart.id +
                            '-legend html-legend">'
                        );
                        for (
                            var i = 0; i < chart.data.datasets.length; i++
                        ) {
                            text.push(
                                '<li><span style="background-color:' +
                                chart.data.datasets[i].legendColor +
                                '"></span>'
                            );
                            if (chart.data.datasets[i].label) {
                                text.push(chart.data.datasets[i].label);
                            }
                            text.push("</li>");
                        }
                        text.push("</ul>");
                        return text.join("");
                    },
                },
            });
        },
    });
</script>

@endsection
