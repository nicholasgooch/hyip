@extends('funding.layouts.app')
@section('styles')
    <link rel="stylesheet" href="{{ asset('loan/css/dashboard.css?ver=3.1.3') }}">
    <link id="skin-default" rel="stylesheet" href="{{ asset('loan/css/skins/theme-red.css?ver=3.1.3') }}">
@endsection
@section('content')
    <div class="nk-content ">
        <div class="container-fluid">
            <div class="nk-content-inner">
                <div class="nk-content-body">
                    <div class="nk-block-head nk-block-head-sm">
                        <div class="nk-block-between">
                            <div class="nk-block-head-content">
                                <h4 class="nk-block-title page-title">Dashboard</h4>
                            </div><!-- .nk-block-head-content -->
                        </div><!-- .nk-block-between -->
                    </div><!-- .nk-block-head -->
                    <div class="nk-block">
                        <div class="row g-gs">
                            <div class="col-xxl-4 col-md-6">
                                <div class="card is-dark h-100">
                                    <div class="nk-ecwg nk-ecwg1">
                                        <div class="card-inner">
                                            <div class="card-title-group">
                                                <div class="card-title">

                                                    @if ($funding->type->slug == 'private-equity')
                                                        <h6 class="title">Equity Amount <em data-bs-toggle="tooltip"
                                                                data-bs-placement="right"
                                                                class="card-hint icon ni ni-help-fill"
                                                                title="Amount of funding you are requested"></em>
                                                        </h6>
                                                    @else
                                                        <h6 class="title">Total Loan Balance <em data-bs-toggle="tooltip"
                                                                data-bs-placement="right"
                                                                class="card-hint icon ni ni-help-fill"
                                                                title="This field displays the total amount of all active loans in the system"></em>
                                                        </h6>
                                                    @endif


                                                </div>
                                                <div class="card-tools">
                                                    <a href="#" class="link">View Report</a>
                                                </div>
                                            </div>
                                            <div class="data">
                                                <div class="amount">${{ number_format($funding->amount, 2) }}</div>
                                                <div class="info"><strong>{{number_format(tobtc($funding->amount),2)}}</strong> BTC</div>
                                                <div class="info"><strong>$7,395.37</strong> next payment due</div>
                                            </div>
                                            <div class="data">
                                                @if ($funding->type->slug == 'private-equity')
                                                    <h6 class="sub-title">Security Deposit <em data-bs-toggle="tooltip"
                                                            data-bs-placement="right" class="card-hint icon ni ni-help-fill"
                                                            title="Security deposits are required for equity deals to help protect investors against potential losses"></em>
                                                    </h6>
                                                @else
                                                    <h6 class="sub-title">Total Collateral Required <em
                                                            data-bs-toggle="tooltip" data-bs-placement="right"
                                                            class="card-hint icon ni ni-help-fill"
                                                            title="Collateral required as security for loans and will be returned once the loan is repaid in full"></em>
                                                    </h6>
                                                @endif
                                                <div class="data-group">
                                                    <div class="amount">${{ number_format($funding->collateral_amount, 2) }}
                                                    </div>
                                                    <div class="info text-end"><span class="change up text-danger"><em
                                                                class="icon ni ni-arrow-long-up"></em>{{ $funding->roi }}%</span><br><span>
                                                            Return on Investment</span></div>
                                                </div>

                                            </div>
                                        </div><!-- .card-inner -->
                                        @if ($funding->type->slug == 'private-equity')
                                            <div class="nk-ecwg2-ck">
                                                <canvas class="ecommerce-bar-chart" id=""></canvas>
                                            </div>
                                        @else
                                            <div class="nk-ecwg2-ck">
                                                <canvas class="repayment-line-chart-s1" id="totalSales"></canvas>
                                            </div>
                                        @endif
                                    </div><!-- .nk-ecwg -->
                                </div><!-- .card -->
                            </div><!-- .col -->
                            <div class="col-xxl-4 col-md-6">
                                <div class="card h-100">
                                    <div class="nk-ecwg nk-ecwg2">
                                        <div class="card-inner">
                                            <div class="card-title-group mt-n1">
                                                <div class="card-title">
                                                    <h6 class="title">Available Withdrawable Balance <em
                                                            data-bs-toggle="tooltip" data-bs-placement="right"
                                                            class="card-hint icon ni ni-help-fill"
                                                            title="Available Loan to Withdraw"></em></h6>
                                                </div>
                                                <div class="card-tools me-n1">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-bs-toggle="tooltip" data-bs-placement="right"
                                                        title="Transfer to Account Balance">
                                                        Transfer
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="data">
                                                <div class="">
                                                    <div class="amount">
                                                        ${{ number_format($funding->available_to_withdraw) }}</div>
                                                    <div class="info"><strong>{{number_format(toBtc($funding->available_to_withdraw),2)}}</strong> BTC</div>

                                                </div>
                                            </div>
                                            <h6 class="sub-title">Total Platform Withdrawals </h6>
                                            <div class="nk-sale-data-group flex-md-nowrap g-4">
                                                <div class="nk-sale-data">
                                                    <span class="amount">14,299.59 <span class="change down text-danger"><em class="icon ni ni-arrow-long-down"></em>16.93%</span></span>
                                                    <span class="sub-title">This Month</span>
                                                </div>
                                                <div class="nk-sale-data">
                                                    <span class="amount">7,299.59 <span class="change up text-success"><em class="icon ni ni-arrow-long-up"></em>4.26%</span></span>
                                                    <span class="sub-title">This Week</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- .card-inner -->
                                        <div class="nk-ecwg2-ck">
                                            <canvas class="ecommerce-bar-chart-s1" id="averargeOrder"></canvas>
                                        </div>
                                    </div><!-- .nk-ecwg -->
                                </div><!-- .card -->
                            </div><!-- .col -->

                            @php
                                switch ($funding->status->slug) {
                                    case 'application-received':
                                        $progressPercentage = 25;
                                        break;
                                    case 'under-review':
                                        $progressPercentage = 50;
                                        break;
                                    case 'approved':
                                        $progressPercentage = 75;
                                        break;
                                    case 'funds-disbursed':
                                        $progressPercentage = 100;
                                        break;
                                    default:
                                        $progressPercentage = 0;
                                        break;
                                }
                            @endphp
                            <div class="col-md-6 col-xxl-4">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner pb-0">
                                        <div class="card-title-group pt-1">
                                            <div class="card-title">
                                                <h6 class="title">
                                                    {{ $funding->type->slug == 'private-equity' ? 'Equity' : 'Funding' }}
                                                    Overview</h6>
                                            </div>
                                            <div class="card-tools">
                                                <a href="html/loan/loan-details.html" class="link">See Details</a>
                                            </div>
                                        </div>
                                    </div>
                                    @if ($funding->type->slug == 'private-equity')
                                        <div class="card-inner pt-0">
                                            <div class="invest-ov gy-1">
                                                <div class="subtitle">Equity Funding Overview</div>
                                                <div class="invest-ov-details">
                                                    <div class="invest-ov-stats">
                                                        <div><span
                                                                class="amount d-flex align-items-end ">{{ $funding->equity_percentage ?? 'N/A' }}</span>
                                                        </div>
                                                        <div class="title">Equity Percentage</div>
                                                    </div>
                                                    <div class="invest-ov-info">
                                                        <div class="amount">
                                                            ${{ number_format($funding->business_valuation, 2) ?? 'N\A' }}
                                                        </div>
                                                        <div class="title">Business Valuation</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="invest-ov gy-1">
                                                <div class="subtitle">Security Deposit Status</div>
                                                <div class="invest-ov-details">
                                                    <div class="invest-ov-info">
                                                        <div><span
                                                                class="amount ">{{ number_format($funding->collateral_paid, 2) }}</span>
                                                        </div>
                                                        <div class="title">Amount Paid</div>
                                                    </div>
                                                    <div class="invest-ov-info">
                                                        <div><span class="amount ">{{ $funding->status->name }}</span>
                                                        </div>
                                                        <div class="title ">Status</div>
                                                    </div>
                                                    <div class="invest-ov-info">
                                                        <div><span class="date">Created Date</span></div>
                                                        <div class="title">{{ $funding->created_at->diffForHumans() }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="invest-ov">
                                                <div class="subtitle">Application Status</div>
                                                <div class="progress progress-lg mt-3">
                                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                                                        data-progress="{{ $progressPercentage }}"
                                                        style="width: {{ $progressPercentage }}%;">
                                                        {{ $progressPercentage }}%</div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="card-inner pt-0">
                                            <div class="invest-ov gy-1">
                                                <div class="subtitle">Activated Loan EMI</div>
                                                <div class="invest-ov-details">
                                                    <div class="invest-ov-stats">
                                                        <div><span
                                                                class="amount d-flex align-items-end text-primary">{{ $funding->repayments->count() }}<span
                                                                    class="sub-text ps-1">
                                                                    @if ($funding->repayment_frequency == 'monthly')
                                                                        Months
                                                                    @elseif ($funding->repayment_frequency == 'bi-annually')
                                                                        1/2 Years
                                                                    @elseif ($funding->repayment_frequency == 'annually')
                                                                        Years
                                                                    @endif
                                                                </span></span></div>
                                                        <div class="title">Total Repayments</div>
                                                    </div>
                                                    <div class="invest-ov-info">
                                                        <div class="amount">{{ number_format($funding->amount, 2) }} <span
                                                                class="currency currency-usd">USD</span></div>
                                                        <div class="title">Amount</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="invest-ov gy-1">
                                                <div class="subtitle">Collateral Status</div>
                                                <div class="invest-ov-details">
                                                    <div class="invest-ov-info">
                                                        <div><span
                                                                class="amount text-success">{{ number_format($funding->collateral_paid, 2) }}</span>
                                                        </div>
                                                        <div class="title">Amount Paid</div>
                                                    </div>
                                                    <div class="invest-ov-info">
                                                        <div><span class="amount ">{{ $funding->status->name }}</span>
                                                        </div>
                                                        <div class="title ">Status</div>
                                                    </div>
                                                    <div class="invest-ov-info">
                                                        <div><span class="date">Created Date</span></div>
                                                        <div class="title">{{ $funding->created_at->diffForHumans() }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="invest-ov">
                                                <div class="subtitle">Application Status</div>
                                                <div class="progress progress-lg mt-3">
                                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                                                        data-progress="{{ $progressPercentage }}"
                                                        style="width: {{ $progressPercentage }}%;">
                                                        {{ $progressPercentage }}%</div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                </div><!-- .card -->
                            </div>
                            <div class="col-md-6 col-xxl-4">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner card-inner-lg">
                                        <div class="align-center flex-wrap g-4">
                                            <div class="nk-block-image w-120px flex-shrink-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 118">
                                                    <path
                                                        d="M8.916,94.745C-.318,79.153-2.164,58.569,2.382,40.578,7.155,21.69,19.045,9.451,35.162,4.32,46.609.676,58.716.331,70.456,1.845,84.683,3.68,99.57,8.694,108.892,21.408c10.03,13.679,12.071,34.71,10.747,52.054-1.173,15.359-7.441,27.489-19.231,34.494-10.689,6.351-22.92,8.733-34.715,10.331-16.181,2.192-34.195-.336-47.6-12.281A47.243,47.243,0,0,1,8.916,94.745Z"
                                                        transform="translate(0 -1)" fill="#f6faff"></path>
                                                    <rect x="18" y="32" width="84" height="50"
                                                        rx="4" ry="4" fill="#fff"></rect>
                                                    <rect x="26" y="44" width="20" height="12"
                                                        rx="1" ry="1" fill="#e5effe"></rect>
                                                    <rect x="50" y="44" width="20" height="12"
                                                        rx="1" ry="1" fill="#e5effe"></rect>
                                                    <rect x="74" y="44" width="20" height="12"
                                                        rx="1" ry="1" fill="#e5effe"></rect>
                                                    <rect x="38" y="60" width="20" height="12"
                                                        rx="1" ry="1" fill="#e5effe"></rect>
                                                    <rect x="62" y="60" width="20" height="12"
                                                        rx="1" ry="1" fill="#e5effe"></rect>
                                                    <path
                                                        d="M98,32H22a5.006,5.006,0,0,0-5,5V79a5.006,5.006,0,0,0,5,5H52v8H45a2,2,0,0,0-2,2v4a2,2,0,0,0,2,2H73a2,2,0,0,0,2-2V94a2,2,0,0,0-2-2H66V84H98a5.006,5.006,0,0,0,5-5V37A5.006,5.006,0,0,0,98,32ZM73,94v4H45V94Zm-9-2H54V84H64Zm37-13a3,3,0,0,1-3,3H22a3,3,0,0,1-3-3V37a3,3,0,0,1,3-3H98a3,3,0,0,1,3,3Z"
                                                        transform="translate(0 -1)" fill="#e14954"></path>
                                                    <path
                                                        d="M61.444,41H40.111L33,48.143V19.7A3.632,3.632,0,0,1,36.556,16H61.444A3.632,3.632,0,0,1,65,19.7V37.3A3.632,3.632,0,0,1,61.444,41Z"
                                                        transform="translate(0 -1)" fill="#e14954"></path>
                                                    <path
                                                        d="M61.444,41H40.111L33,48.143V19.7A3.632,3.632,0,0,1,36.556,16H61.444A3.632,3.632,0,0,1,65,19.7V37.3A3.632,3.632,0,0,1,61.444,41Z"
                                                        transform="translate(0 -1)" fill="none" stroke="#e14954"
                                                        stroke-miterlimit="10" stroke-width="2"></path>
                                                    <line x1="40" y1="22" x2="57" y2="22"
                                                        fill="none" stroke="#fffffe" stroke-linecap="round"
                                                        stroke-linejoin="round" stroke-width="2"></line>
                                                    <line x1="40" y1="27" x2="57" y2="27"
                                                        fill="none" stroke="#fffffe" stroke-linecap="round"
                                                        stroke-linejoin="round" stroke-width="2"></line>
                                                    <line x1="40" y1="32" x2="50" y2="32"
                                                        fill="none" stroke="#fffffe" stroke-linecap="round"
                                                        stroke-linejoin="round" stroke-width="2"></line>
                                                    <line x1="30.5" y1="87.5" x2="30.5" y2="91.5"
                                                        fill="none" stroke="#e14954" stroke-linecap="round"
                                                        stroke-linejoin="round"></line>
                                                    <line x1="28.5" y1="89.5" x2="32.5" y2="89.5"
                                                        fill="none" stroke="#e14954" stroke-linecap="round"
                                                        stroke-linejoin="round"></line>
                                                    <line x1="79.5" y1="22.5" x2="79.5" y2="26.5"
                                                        fill="none" stroke="#e14954" stroke-linecap="round"
                                                        stroke-linejoin="round"></line>
                                                    <line x1="77.5" y1="24.5" x2="81.5" y2="24.5"
                                                        fill="none" stroke="#e14954" stroke-linecap="round"
                                                        stroke-linejoin="round"></line>
                                                    <circle cx="90.5" cy="97.5" r="3" fill="none"
                                                        stroke="#e14954" stroke-miterlimit="10"></circle>
                                                    <circle cx="24" cy="23" r="2.5" fill="none"
                                                        stroke="#e14954" stroke-miterlimit="10"></circle>
                                                </svg>
                                            </div>
                                            <div class="nk-block-content">
                                                <div class="nk-block-content-head">
                                                    <h5>We’re here to help you!</h5>
                                                    <p class="text-soft">Ask a question or file a support ticket, manage
                                                        request, report an issues. Our team support team will get back to
                                                        you by email.</p>
                                                </div>
                                            </div>
                                            <div class="nk-block-content flex-shrink-0">
                                                <a href="#" class="btn btn-lg btn-outline-primary">Get Support
                                                    Now</a>
                                            </div>
                                        </div>
                                    </div><!-- .card-inner -->
                                </div><!-- .card -->
                            </div>
                            <div class="col-md-6">
                                <div class="card card-bordered card-full">
                                    <div class="card-inner">
                                        <div class="card-title-group">
                                            <div class="card-title">
                                                <h6 class="title"><span class="me-2">Deposits</span>
                                                </h6>
                                            </div>
                                            <small class="text-right"><a href="#">See More <em
                                                class="icon ni ni-chevron-right"></em></a></small>
                                        </div>
                                    </div><!-- .card-inner -->
                                    <div class="card-inner p-0 border-top">
                                        <div class="nk-tb-list nk-tb-orders">
                                            <div class="nk-tb-item nk-tb-head">
                                                <div class="nk-tb-col nk-tb-orders-type"><span>Method</span></div>
                                                <div class="nk-tb-col tb-col-sm"><span>Date</span></div>
                                                <div class="nk-tb-col tb-col-sm"><span>Time</span></div>
                                                <div class="nk-tb-col tb-col-sm"><span>Ref</span></div>
                                                <div class="nk-tb-col tb-col-sm text-end"><span> Amount</span></div>
                                            </div><!-- .nk-tb-item -->
                                            @foreach($my_deposits as $trx)

                                            <div class="nk-tb-item">
                                                <div class="nk-tb-col nk-tb-orders-type">
                                                    <ul class="icon-overlap">
                                                        @if ($trx->bank == "BTC")
                                                    <li>
                                                        <em
                                                            class="bg-btc-dim icon-circle icon ni ni-sign-btc"></em>
                                                    </li>
                                                    @elseif($trx->bank == "ETH")
                                                    <li>
                                                        <em
                                                            class="bg-eth-dim icon-circle icon ni ni-sign-eth"></em>
                                                    </li>
                                                    @else
                                                    <em
                                                        class="bg-eth-dim icon-circle icon ni ni-building"></em>
                                                    @endif

                                                        <li><em
                                                                class="bg-success-dim icon-circle icon ni ni-arrow-down-left"></em>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <div class="nk-tb-col tb-col-sm">
                                                    <span class="tb-sub">{{ $trx->created_at->format('m/d/Y') }}</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-sm">
                                                    <span class="tb-sub">{{ $trx->created_at->format('h:i A') }}</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-sm">
                                                    <span class="tb-sub text-primary">#{{$trx->ref}}</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-sm text-end">
                                                    <span class="tb-sub tb-amount">
                                                        <span>{{ $settings->currency == 'USD' ? '$' : ' ' + $settings->currency }}</span>
                                                        {{number_format($trx->amount)}} <span>

                                                </div>

                                            </div><!-- .nk-tb-item -->
                                            @endforeach
                                        </div>
                                    </div><!-- .card-inner -->
                                    <div class="card-inner-sm border-top text-center d-sm-none">
                                        <a href="#" class="btn btn-link btn-block">See History</a>
                                    </div><!-- .card-inner -->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card card-full">
                                    <div class="card-inner">
                                        <div class="card-title-group">
                                            <div class="card-title">
                                                <h6 class="title">Withdrawals</h6>

                                            </div>
                                            <small class="text-right"><a href="#">See More <em
                                                        class="icon ni ni-chevron-right"></em></a></small>
                                        </div>
                                    </div>
                                    <div class="nk-tb-list mt-n2">
                                        <div class="nk-tb-item nk-tb-head">
                                            <div class="nk-tb-col"><span>Ref.No</span></div>
                                            <div class="nk-tb-col tb-col-md"><span>Date</span></div>
                                            <div class="nk-tb-col"><span>Amount</span></div>
                                            <div class="nk-tb-col"><span class="d-none d-sm-inline">Status</span></div>
                                        </div>
                                        @foreach ($my_withdrawals as $trx)
                                            <div class="nk-tb-item">
                                                <div class="nk-tb-col">
                                                    <span class="tb-lead"><a
                                                            href="#">#{{ $trx->ref }}</a></span>
                                                </div>

                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">{{ $trx->created_at->diffForHumans() }}</span>
                                                </div>
                                                <div class="nk-tb-col">
                                                    <span class="tb-sub tb-amount">
                                                        {{ $trx->amount }}
                                                        <span>{{ $settings->currency == 'USD' ? '$' : ' ' + $settings_currency }}
                                                        </span></span>
                                                </div>
                                                <div class="nk-tb-col">

                                                    @if ($trx->status == 'Approved')
                                                        <span
                                                            class="badge badge-sm badge-dim badge-success  d-md-inline-flex">Approved</span>
                                                    @elseif($trx->status == 'Pending')
                                                        <span
                                                            class="badge badge-sm badge-dim  badge-warning  d-md-inline-flex">Pending</span>
                                                    @elseif($trx->status == 'Rejected')
                                                        <span
                                                            class="badge badge-sm badge-dim badge-danger d-md-inline-flex">Declined</span>
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div><!-- .card -->
                            </div>


                        </div><!-- .row -->
                    </div><!-- .nk-block -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{-- <script src="{{ asset('loan/js/charts/chart-ecommerce.js?ver=3.1.3') }}"></script> --}}

    <script>
        var repayments = {!! json_encode($funding->repayments) !!};
        // Create an array of repayment labels and amounts from Laravel variable
        const labels = [];
        const data = [];
        repayments.forEach((repayment) => {
            labels.push(repayment.created_at);
            data.push(repayment.payment_amount);
        });
        console.log(repayments);

        var totalSales = {
            labels: labels,
            dataUnit: 'USD',
            lineTension: .3,
            datasets: [{
                label: "Repayment Schedule",
                color: "#9d72ff",
                background: NioApp.hexRGB('#9d72ff', .25),
                data: data
            }]
        };

        function repaymentLineS1(selector, set_data) {
            var $selector = selector ? $(selector) : $('.repayment-line-chart-s1');
            $selector.each(function() {
                var $self = $(this),
                    _self_id = $self.attr('id'),
                    _get_data = typeof set_data === 'undefined' ? eval(_self_id) : set_data;
                var selectCanvas = document.getElementById(_self_id).getContext("2d");
                var chart_data = [];
                for (var i = 0; i < _get_data.datasets.length; i++) {
                    chart_data.push({
                        label: _get_data.datasets[i].label,
                        tension: _get_data.lineTension,
                        backgroundColor: _get_data.datasets[i].background,
                        borderWidth: 2,
                        borderColor: _get_data.datasets[i].color,
                        pointBorderColor: 'transparent',
                        pointBackgroundColor: 'transparent',
                        pointHoverBackgroundColor: "#fff",
                        pointHoverBorderColor: _get_data.datasets[i].color,
                        pointBorderWidth: 2,
                        pointHoverRadius: 4,
                        pointHoverBorderWidth: 2,
                        pointRadius: 4,
                        pointHitRadius: 4,
                        data: _get_data.datasets[i].data
                    });
                }
                var chart = new Chart(selectCanvas, {
                    type: 'line',
                    data: {
                        labels: _get_data.labels,
                        datasets: chart_data
                    },
                    options: {
                        legend: {
                            display: _get_data.legend ? _get_data.legend : false,
                            rtl: NioApp.State.isRTL,
                            labels: {
                                boxWidth: 12,
                                padding: 20,
                                fontColor: '#6783b8'
                            }
                        },
                        maintainAspectRatio: false,
                        tooltips: {
                            enabled: true,
                            rtl: NioApp.State.isRTL,
                            callbacks: {
                                title: function title(tooltipItem, data) {
                                    return data['labels'][tooltipItem[0]['index']];
                                },
                                label: function label(tooltipItem, data) {
                                    return data.datasets[tooltipItem.datasetIndex]['data'][tooltipItem[
                                        'index']] + ' ' + _get_data.dataUnit;
                                }
                            },
                            backgroundColor: '#1c2b46',
                            titleFontSize: 10,
                            titleFontColor: '#fff',
                            titleMarginBottom: 4,
                            bodyFontColor: '#fff',
                            bodyFontSize: 10,
                            bodySpacing: 4,
                            yPadding: 6,
                            xPadding: 6,
                            footerMarginTop: 0,
                            displayColors: false
                        },
                        scales: {
                            yAxes: [{
                                display: false,
                                ticks: {
                                    beginAtZero: true,
                                    fontSize: 12,
                                    fontColor: '#9eaecf',
                                    padding: 0
                                },
                                gridLines: {
                                    color: NioApp.hexRGB("#526484", .2),
                                    tickMarkLength: 0,
                                    zeroLineColor: NioApp.hexRGB("#526484", .2)
                                }
                            }],
                            xAxes: [{
                                display: false,
                                ticks: {
                                    fontSize: 12,
                                    fontColor: '#9eaecf',
                                    source: 'auto',
                                    padding: 0,
                                    reverse: NioApp.State.isRTL
                                },
                                gridLines: {
                                    color: "transparent",
                                    tickMarkLength: 0,
                                    zeroLineColor: NioApp.hexRGB("#526484", .2),
                                    offsetGridLines: true
                                }
                            }]
                        }
                    }
                });
            });
        }
        NioApp.coms.docReady.push(function() {
            repaymentLineS1()
        });
    </script>

    <script>
        var chart;

        function getRandomAmount(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
        }
        // Generating last 50 days for the x-axis labels
        var last50Days = Array.from({ length: 20 }, (_, i) => {
            const d = new Date();
            d.setDate(d.getDate() - i);
            return d.toISOString().slice(0, 10);
        }).reverse();

        // Generating random withdrawals for the y-axis
        var randomWithdrawals = Array.from({ length: 50 }, () => getRandomAmount(50000, 1500000));
       // console.log(randomWithdrawals)
        var averargeOrder = {
            labels: last50Days,
            dataUnit: 'Amount',
            lineTension: .1,
            datasets: [{
                label: "Withdrawals",
                color: "#e85347",
                background: "#e85347",
                data: randomWithdrawals
            }]
        };
        function ecommerceBarS1(selector, set_data) {
            var $selector = selector ? $(selector) : $('.ecommerce-bar-chart-s1');
            $selector.each(function() {
                var $self = $(this),
                    _self_id = $self.attr('id'),
                    _get_data = typeof set_data === 'undefined' ? eval(_self_id) : set_data;
                var selectCanvas = document.getElementById(_self_id).getContext("2d");
                var chart_data = [];
                for (var i = 0; i < _get_data.datasets.length; i++) {
                    chart_data.push({
                        label: _get_data.datasets[i].label,
                        tension: _get_data.lineTension,
                        backgroundColor: _get_data.datasets[i].background,
                        borderWidth: 2,
                        borderColor: _get_data.datasets[i].color,
                        data: _get_data.datasets[i].data,
                        barPercentage: .7,
                        categoryPercentage: .7
                    });
                }
                if (chart) { // If chart is already initialized
                    chart.data.labels = _get_data.labels;
                    chart.data.datasets[0].data = _get_data.datasets[0].data;
                    chart.update(); // Update the chart
                } else {
                var chart = new Chart(selectCanvas, {
                    type: 'bar',
                    data: {
                        labels: _get_data.labels,
                        datasets: chart_data
                    },
                    options: {
                        legend: {
                            display: _get_data.legend ? _get_data.legend : false,
                            rtl: NioApp.State.isRTL,
                            labels: {
                                boxWidth: 12,
                                padding: 20,
                                fontColor: '#6783b8'
                            }
                        },
                        maintainAspectRatio: false,
                        tooltips: {
                            enabled: true,
                            rtl: NioApp.State.isRTL,
                            callbacks: {
                                title: function title(tooltipItem, data) {
                                    return false; //data['labels'][tooltipItem[0]['index']];
                                },

                                label: function label(tooltipItem, data) {
                                    return data.datasets[tooltipItem.datasetIndex]['data'][tooltipItem[
                                        'index']];
                                }
                            },
                            backgroundColor: '#1c2b46',
                            titleFontSize: 9,
                            titleFontColor: '#fff',
                            titleMarginBottom: 6,
                            bodyFontColor: '#fff',
                            bodyFontSize: 9,
                            bodySpacing: 4,
                            yPadding: 6,
                            xPadding: 6,
                            footerMarginTop: 0,
                            displayColors: false
                        },
                        scales: {
                            yAxes: [{
                                display: true,
                                position: NioApp.State.isRTL ? "right" : "left",
                                ticks: {
                                    beginAtZero: false,
                                    fontSize: 12,
                                    fontColor: '#9eaecf',
                                    padding: 0,
                                    display: false,
                                    stepSize: 100
                                },
                                gridLines: {
                                    color: "rgba(0, 0, 0,0)",
                                    tickMarkLength: 0,
                                    zeroLineColor: NioApp.hexRGB("#526484", .2)
                                }
                            }],
                            xAxes: [{
                                display: false,
                                ticks: {
                                    fontSize: 12,
                                    fontColor: '#9eaecf',
                                    source: 'auto',
                                    padding: 0,
                                    reverse: NioApp.State.isRTL
                                },
                                gridLines: {
                                    color: "rgba(0, 0, 0, 9)",
                                    tickMarkLength: 0,
                                    zeroLineColor: 'transparent',
                                    offsetGridLines: true
                                }
                            }]
                        }
                    }
                });
             }
            });
        }
        function updateChartData() {
        averargeOrder.labels = Array.from({ length: 50 }, (_, i) => {
            const d = new Date();
            d.setDate(d.getDate() - i);
            return d.toISOString().slice(0, 10);
        }).reverse();

            averargeOrder.datasets[0].data = Array.from({ length: 50 }, () => getRandomAmount(500, 1500));

            // Assuming you have a way to re-render the chart, call that function here
            // For instance, if you have a `renderChart()` function, you'd call:
            ecommerceBarS1();
        }

        // Update the chart data every 20 minutes
        setInterval(updateChartData, 20 * 60 * 1000);
        // init chart
        NioApp.coms.docReady.push(function() {
            ecommerceBarS1();
        });


        var thisMonthTotal = randomWithdrawals.reduce((a, b) => a + b, 0);
        var thisWeekTotal = randomWithdrawals.slice(-7).reduce((a, b) => a + b, 0);

        var previousMonthTotal = thisMonthTotal - thisWeekTotal; // Considering only 2 time frames
        var percentageChangeMonth = ((thisMonthTotal - previousMonthTotal) / previousMonthTotal) * 100;

        var previousWeekTotal = thisWeekTotal - randomWithdrawals.slice(-14, -7).reduce((a, b) => a + b, 0);
        var percentageChangeWeek = ((thisWeekTotal - previousWeekTotal) / previousWeekTotal) * 100;
        console.log(percentageChangeWeek);
        document.querySelector('.nk-sale-data-group .nk-sale-data:nth-child(1) .amount').innerHTML =
        thisMonthTotal.toLocaleString() +
        '<span class="change ' + (percentageChangeMonth < 0 ? 'down text-danger' : 'up text-success') + '"><em class="icon ni ni-arrow-long-' +
        (percentageChangeMonth < 0 ? 'down' : 'up') + '"></em>' +
        percentageChangeMonth.toFixed(2) + '%</span>';

    document.querySelector('.nk-sale-data-group .nk-sale-data:nth-child(2) .amount').innerHTML =
        thisWeekTotal.toLocaleString() +
        '<span class="change ' + (percentageChangeWeek < 0 ? 'down text-danger' : 'up text-success') + '"><em class="icon ni ni-arrow-long-' +
        (percentageChangeWeek < 0 ? 'down' : 'up') + '"></em>' +
        percentageChangeWeek.toFixed(2) + '%</span>';
    </script>
@endsection
