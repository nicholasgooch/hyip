@extends('funding.layouts.app')
@section('styles')
    <link  rel="stylesheet" href="{{ asset('loan/css/dashboard.css?ver=3.1.3') }}">
@endsection
@section('content')
    <div class="nk-content ">
        <div class="container-fluid">
            <div class="nk-content-inner">
                <div class="nk-content-body">
                    <div class="nk-block-head nk-block-head-sm">
                        <div class="nk-block-between">
                            <div class="nk-block-head-content">
                                <h4 class="nk-block-title page-title">Dashboard</h4>
                            </div><!-- .nk-block-head-content -->
                        </div><!-- .nk-block-between -->
                    </div><!-- .nk-block-head -->
                    <div class="nk-block">
                       <div class="row g-gs">
                           @foreach ($plans as $plan)
                           <div class="col-md-4">
                            <div class="price-plan card card-bordered text-center">
                                <div class="card-inner">
                                    <div class="price-plan-media">
                                        <img src="{{asset('v2/assets/images/icons/plan-s1.svg')}}" alt="">
                                    </div>
                                    <div class="price-plan-info">
                                        <h5 class="title">{{$plan->name}}</h5>
                                        <span>If you are a small business amn please select this plan</span>
                                    </div>
                                    <div class="price-plan-amount">
                                        <div class="amount">${{$plan->amount}} <span>/yr</span></div>
                                        <span class="bill">1 User, Billed Yearly</span>
                                    </div>
                                    <div class="price-plan-action">
                                        <a href="{{route('funding.deposit')}}?c=subscription&t=funding&plan={{$plan->name}}&amount={{$plan->amount}}" class="btn btn-danger">Select Plan</a>
                                    </div>
                                </div>
                            </div><!-- .price-item -->
                        </div>
                           @endforeach

                       </div>
                    </div><!-- .nk-block -->
                </div>
            </div>
        </div>
    </div>
@endsection

