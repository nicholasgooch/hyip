@extends('funding.layouts.app')

@section('content')
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
        <div class="nk-content-body">
            <div class="nk-block-head nk-block-head-sm">
                <div class="nk-block-between">
                    <div class="nk-block-head-content">
                        <h3 class="nk-block-title page-title">Loan Application</h3>
                        <div class="nk-block-des text-soft">
                            <p class="text-soft">Please complete the application neatly &amp; included all information, documentation, identification required</p>
                        </div>
                    </div><!-- .nk-block-head-content -->
                </div><!-- .nk-block-between -->
            </div><!-- .nk-block-head -->
            <div class="nk-block">
                <apply-page/>
            </div><!-- .nk-block -->
        </div>
    </div>
</div>

{{-- check if errors exist and display toast --}}

@endsection

@section('scripts')

@if($errors->any())
    @foreach ($errors->all() as $error)
        <script> toastr.error('{{ $error }}', '', { positionClass: 'toast-top-center' });</script>
    @endforeach
@endif

@if(session('error'))
    <script> toastr.error('{{ session('error')  }}', '', { positionClass: 'toast-top-center' });</script>
@endif
@endsection
