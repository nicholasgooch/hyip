@extends('exchange.layouts.app')

@section('content')
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-lg">
      <div class="nk-content-body">
        <div class="buysell wide-xs m-auto">
          <div class="buysell-nav text-center">
            <ul class="nk-nav nav nav-tabs nav-tabs-s2">
              <li class="nav-item">
                <a class="nav-link" href="html/crypto/buy-sell.html"
                  >Buy Coin</a
                >
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Sell Coin</a>
              </li>
            </ul>
          </div>
          <!-- .buysell-nav -->
          <div class="buysell-title text-center">
            <h2 class="title">What do you want to buy!</h2>
          </div>
          <!-- .buysell-title -->
          <div class="buysell-block" id="app">
           <buy-coin></buy-coin>
            <!-- .buysell-form -->
          </div>
          <!-- .buysell-block -->
        </div>
        <!-- .buysell -->
      </div>
    </div>
  </div>
@endsection
@section('scripts')
    <script src="{{asset('/js/app.js')}}"></script>
@endsection