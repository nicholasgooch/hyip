<div class="mt-5 mb-5">
    <div class="nk-block nk-block-lg">
        <div class="nk-block-head">
            <h5 class="nk-block-title">Sub accounts</h5>
        </div>
        <div class="row g-gs">
            <!-- .col -->
            <div class="col-lg-4 col-sm-6">
                <div class="card card-bordered h-100">
                    <div class="">

                        <div class="card-inner">
                            <div class="row gy-4">
                                <div class="col-lg">
                                    @if ($errors->any())
                                        <div class="alert alert-icon alert-danger border border-red-400 text-red-700 px-4 py-3 rounded relative"
                                            role="alert">

                                            <ul class=" mt-2">
                                                @foreach ($errors->all() as $error)
                                                    <li> <em class="icon ni ni-alert-circle"></em>
                                                        <strong> {{ $error }}</strong>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="form-group relative">
                                        @if ($this->availableShare > 0)
                                            <div class="alert alert-info">
                                                <em class="icon ni ni-alert-circle"></em> Available Share for New Users:
                                                {{ $this->availableShare }}%
                                            </div>
                                        @endif
                                        <label class="form-label block text-sm font-medium text-gray-700">User
                                            Profile</label>
                                        <div class="form-control-wrap mt-1 relative">
                                            <input type="text" wire:model.debounce.500ms="userQuery"
                                                placeholder="Search users..."
                                                class="form-control block w-full pl-3 pr-10 py-2 rounded-md border border-gray-300 shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" />

                                        </div>
                                        @if (count($searchResults) > 0)
                                            <ul
                                                class="absolute z-10 w-full bg-white shadow-lg max-h-60 overflow-y-auto rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                                                @foreach ($searchResults as $result)
                                                    <li>
                                                        <a wire:click.prevent="addUser({{ $result->id }}, '{{ addslashes($result->firstname) }}', '{{ $result->email }}')"
                                                            class="flex items-center border-b border-gray-200 last:border-0 cursor-hand pt-6 text-green-900 hover:bg-gray-100">
                                                            <!-- Example of adding an avatar or icon -->
                                                            {{-- <img src="{{ $result->avatar }}" alt="Profile"
                                                                class="h-8 w-8 rounded-full object-cover mr-3" /> --}}
                                                            <div>
                                                                <div
                                                                    class="font-medium px-5 pt-3 border-solid border-b border-gray-200 last:border-0  hover:bg-gray-100">
                                                                    {{ $result->firstname }}
                                                                </div>
                                                                <!-- You can add more user details here -->
                                                                <div class="text-sm text-gray-500 px-5">
                                                                    {{ $result->email }}
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </div>
                                    @if (count($selectedUsers) > 0)

                                        <div class="form-group">
                                            <div class="space-y-4">
                                                <label class="block text-sm font-medium text-gray-700">Percentage
                                                    Share</label>
                                                <div>
                                                    @foreach ($selectedUsers as $userId => $info)
                                                        <div class="mt-2 bg-white p-3 shadow-sm rounded-md relative">
                                                            <!-- Remove Button -->
                                                            <a wire:click="removeUser({{ $userId }})"
                                                                class="bg-success-dim">
                                                                <em class="icon ni ni-cross-c"></em>
                                                            </a>

                                                            <!-- User's Name -->
                                                            <div class="font-semibold text-gray-800">
                                                                {{ $info['name'] }}
                                                            </div>

                                                            <!-- Slider and Percentage Display -->
                                                            <div class="flex items-center space-x-2 mt-2">
                                                                <input type="range" min="0"
                                                                    class="form-range h-2  rounded-md bg-gray-200 focus:outline-none focus:ring-0 focus:shadow-none"
                                                                    data-start="{{ $info['share_percentage'] }}"
                                                                    max="{{ $remainingShare + $info['share_percentage'] }}"
                                                                    wire:model.lazy="selectedUsers.{{ $userId }}.share_percentage"
                                                                    wire:change="updateShare({{ $userId }}, $event.target.value)">
                                                                <span
                                                                    class="text-sm font-medium text-gray-700">{{ $info['share_percentage'] }}%</span>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-12">
                                    <button wire:click="saveJointInvestments" class="btn btn-primary">
                                        Save Joint Investments
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .col -->

            <div class="col-lg col-sm-6">
                <div class="card card-bordered card-stretch">
                    <div class="card-inner-group">
                        <div class="card-inner p-0">
                            <table class="nk-tb-list nk-tb-ulist">
                                <thead>

                                    <tr class="nk-tb-item nk-tb-head">

                                        <th class="nk-tb-col"><span class="sub-text">User Name</span>
                                        </th>
                                        </th>
                                        <th class="nk-tb-col tb-col-md"><span class="sub-text">Share</span></th>
                                        <th class="nk-tb-col tb-col-mb"><span class="sub-text">Amount</span></th>
                                        <th class="nk-tb-col nk-tb-col-tools text-end">
                                            <div class="dropdown">
                                                <a href="#"
                                                    class="btn btn-xs btn-trigger btn-icon dropdown-toggle me-n1"
                                                    data-bs-toggle="dropdown" data-offset="0,5"><em
                                                        class="icon ni ni-more-h"></em></a>
                                                <div class="dropdown-menu dropdown-menu-end">
                                                    <ul class="link-list-opt no-bdr">
                                                        <li><a href="#"><em
                                                                    class="icon ni ni-check-round-cut"></em><span>Mark
                                                                    As Done</span></a></li>
                                                        <li><a href="#"><em
                                                                    class="icon ni ni-archive"></em><span>Mark
                                                                    As Archive</span></a></li>
                                                        <li><a href="#"><em
                                                                    class="icon ni ni-trash"></em><span>Remove
                                                                    Projects</span></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </th>
                                    </tr><!-- .nk-tb-item -->
                                </thead>
                                <tbody>
                                    @foreach ($investmentUsers as $user)
                                        <tr class="nk-tb-item">
                                            <td class="nk-tb-col">
                                                <a href="html/apps-kanban.html" class="project-title">

                                                    <div class="project-info">
                                                        <h6 class="title">{{ $user['name'] }}</h6>
                                                    </div>
                                                </a>
                                            </td>


                                            <td class="nk-tb-col tb-col-md">
                                                <div class="project-list-progress">
                                                    <div class="progress progress-pill progress-md bg-light">
                                                        <div class="progress-bar"
                                                            data-progress="{!! $user['share_percentage'] !!}"
                                                            style="width: {!! $user['share_percentage'] !!}%;">
                                                        </div>
                                                    </div>
                                                    <div class="project-progress-percent">
                                                        {{ $user['share_percentage'] }}%
                                                    </div>
                                                </div>
                                            </td>

                                            <td class="nk-tb-col">
                                                <a href="html/apps-kanban.html" class="project-title">
                                                    <div class="project-info">
                                                        <h6 class="title">{{ number_format($user['share_amount']) }}
                                                        </h6>
                                                    </div>
                                                </a>
                                            </td>
                                        </tr><!-- .nk-tb-item -->
                                    @endforeach
                                    <!-- .nk-tb-item -->
                                </tbody>
                            </table><!-- .nk-tb-list -->
                        </div><!-- .card-inner -->
                        <div class="card-inner">
                            <div class="nk-block-between-md g-3">

                            </div><!-- .nk-block-between -->
                        </div><!-- .card-inner -->
                    </div><!-- .card-inner-group -->
                </div>
            </div>

            <!-- .col -->
        </div>
        <!-- .row -->
    </div>
</div>
