
@extends('admin.atlantis.layout')
@Section('content')
        <div class="main-panel">
            <div class="content">
                @include('admin.atlantis.exchange_admin_bar')
                <div class="page-inner mt--5">
                    <div id="prnt"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title"><i class="fa fa-money"></i> {{ __('Crypto Currencies') }} </div>
                                </div>
                                <a href="{{route('admin.ex.currency.create')}}" class="btn btn-primary mb-1 mr-3"><i class="fas fa-plus"></i> Add New</a>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-xl-4 currency--card">
                            <div class="card card-bordered">
                                <div class="card-header ">
                                  <h4><i class="fas fa-coins"></i> Bitcoin</h4>
                                </div>
                                <div class="card-body">
                                  <ul class="list-group mb-3">
                                    <li class="list-group-item d-flex justify-content-between">Currency Symbol :              <span class="font-weight-bold">₿</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between">Currency Code :              <span class="font-weight-bold">BTC</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between">Currency Type :              <span class="font-weight-bold">Crypto</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between">Rate 1 BTC =
                                      <span class="font-weight-bold">30,745.08 USD</span>
                                    </li>
                                  </ul>
                                            <a href="https://demo.royalscripts.com/crypto/admin/edit-currency/9" class="btn btn-primary btn-block"><i class="fas fa-edit"></i> Edit Currency</a>  
                                          </div>
                              </div>
                    </div>
                    </div>
                </div>
            </div>
@endSection