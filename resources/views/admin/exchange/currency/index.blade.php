<?php
   $cap = $cap2 = $dep = $dep2 = $wd_bal = $sum_cap = 0;   
?>
@extends('admin.atlantis.layout')
@Section('content')
        <div class="main-panel">
            <div class="content">
                @include('admin.atlantis.main_bar')
                <div class="page-inner mt--5">
                    <div id="prnt"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title"><i class="fa fa-money-bill"></i> {{ __('Manage Currency') }} </div>
                                </div>
                                <a href="{{route('admin.ex.currency.create')}}" class="btn btn-primary mb-1 mr-3"><i class="fas fa-plus"></i> Add New</a>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-6 col-lg-6 col-xl-4 currency--card">
                          <div class="card card-borderd">
                            <div class="card-header">
                              <h4>Fiat Currencies</h4>
                            </div>
                            <div class="card-body text-center">
                              <img class="w-50 mb-3" src="https://demo.royalscripts.com/crypto/assets/images/money.png" alt="">
                              <a href="{{route('admin.ex.currency.fiat')}}" class="btn btn-primary btn-block"><i class="fas fa-arrow-right"></i> See All</a>  
                             </div>
                          </div>
                        </div>
                      
                          <div class="col-md-6 col-lg-6 col-xl-4 currency--card">
                            <div class="card card-borderd">
                              <div class="card-header">
                                <h4>Crypto Currencies</h4>
                              </div>
                              <div class="card-body text-center">
                               <img class="w-50 mb-3" src="https://demo.royalscripts.com/crypto/assets/images/bitcoin.png" alt="">
                               <a href="{{route('admin.ex.currency.crypto')}}" class="btn btn-primary btn-block"><i class="fas fa-arrow-right"></i> See All</a>  
                              </div>
                            </div>
                          </div>
                         
                         
                      
                      </div>
                </div>
            </div>
@endSection