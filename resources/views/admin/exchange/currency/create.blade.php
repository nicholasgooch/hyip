@extends('admin.atlantis.layout')
@Section('content')
<div class="main-panel">
    <div class="content">
        @include('admin.atlantis.exchange_admin_bar')
        <div class="page-inner mt--5">
            <div id="prnt"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title"><i class="fa fa-money"></i> {{ __('Create new Currency') }} </div>
                        </div>

                    </div>
                </div>

            </div>

            <div class="row">
                <div class="offset-md-2">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="alert alert-success" style="display: none;" role="alert">
                                    <button type="button" class="close hide-close" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <p class="m-0"></p>
                                </div>

                                <div class="alert alert-danger" style="display: none;" role="alert">
                                    <button type="button" class="close hide-close" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <ul class="text-left mb-0">
                                    </ul>
                                </div>
                                <form action="{{route('admin.ex.currency.create.post')}}" method="post" enctype="multipart/form-data">
                                   @csrf
                                    <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="">Currency Icon</label>
                                                <input class="form-control" type="text" name="curr_icon" required=""
                                                value="">
                                            </div>
                                       
                                        <div class="form-group col-md-6">
                                            <label>Currency Name</label>
                                            <input class="form-control" type="text" name="curr_name" required=""
                                                value="">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Currency Code</label>
                                            <input class="form-control code" type="text" name="code" required=""
                                                value="">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Currency Symbol</label>
                                            <input class="form-control" type="text" name="symbol" required="" value="">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Currency Type</label>
                                            <select class="form-control type" name="type" required="">
                                                <option value="" selected="">--Select Type--</option>
                                                <option value="1">FIAT</option>
                                                <option value="2">CRYPTO</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>Currency Rate</label>
                                            <div class="input-group has_append">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text cur_code">1 USD = </div>
                                                </div>
                                                <input type="text" class="form-control" placeholder="0" name="rate"
                                                    value="">
                                                <div class="input-group-append">
                                                    <div class="input-group-text curr_text"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label>Set As Default </label>
                                            <select class="form-control default" name="default" required="">
                                                <option value="" selected="">--Select--</option>
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>


                                        <div class="form-group col-md-6">
                                            <label>Status </label>
                                            <select class="form-control" name="status" required="">
                                                <option value="" selected="">--Select--</option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row payments">

                                    </div>

                                    <div class="form-group text-right col-md-12">
                                        <button class="btn  btn-primary btn-lg" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @endsection

    @section('scripts')
    <script>
        'use strict';
        $('.type').on('change',function () { 
            var value = $(this).find('option:selected').val()
            if($('.code').val() == ''){
                alert('error','Please put the currency code first.')
                return false;
            }
            if(value == 2){
                $('.default').attr('disabled',true)
                $('.cur_code').text('1 '+ $('.code').val()+' =')
                $('.curr_text').text('$')

                var html = `
                           
                            <div class="input-group mb-3 col-xl-6">
                                <label class="form-control-label">Deposit Charge</label>

                                <div class="input-group has_append">
                                    <input class="form-control" type="number" name="deposit_charge" placeholder="0" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text"><span>%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group mb-3 col-xl-6">
                                <label class="form-control-label">Withdraw Charge </label>

                                <div class="input-group has_append">
                                    <input class="form-control" type="number" name="withdraw_charge" placeholder="0" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text"><span>%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group mb-3 col-xl-6">
                                <label class="form-control-label">Minimum Withdraw Limit</label>
                                <div class="input-group has_append">
                                    <input class="form-control" type="number" name="withdraw_limit_min" placeholder="0" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text"><span>${$('.code').val()}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mb-3 col-xl-6">
                                <label class="form-control-label">Maximum Withdraw Limit'</label>
                                <div class="input-group has_append">
                                    <input class="form-control" type="number" name="withdraw_limit_max" placeholder="0" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text"><span>${$('.code').val()}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                       `
                $('.payments').html(html)
            } 
            if(value == 1){
                $('.default').attr('disabled',false)
                $('.cur_code').text('1 S =')
                $('.curr_text').text($('.code').val())
                $('.payments').children().remove()
            }
        })
        $('.code').on('keyup',function () { 
            var type = $('.type').find('option:selected').val()
            var value = $(this).val()
            if(type == 1){
                $('.curr_text').text(value)
            }else{
                $('.cur_code').text('1 '+ $('.code').val()+' =')
            }
        })

   
    </script>
    @endsection