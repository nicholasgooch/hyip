
@extends('admin.atlantis.layout')
@Section('content')
        <div class="main-panel">
            <div class="content">
                @include('admin.atlantis.main_bar')
                <div class="page-inner mt--5">
                    <div id="prnt"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title"><i class="fa fa-money"></i> {{ __('Fiat Currencies') }} </div>
                                </div>
                               
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-xl-4 currency--card">
                      <div class="card card-bordered">
                        <div class="card-header default">
                          <h4><i class="fas fa-coins"></i> United State Dollar</h4>
                        </div>
                        <div class="card-body">
                          <ul class="list-group mb-3">
                            <li class="list-group-item d-flex justify-content-between">Currency Symbol :              <span class="font-weight-bold">$</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between">Currency Code :              <span class="font-weight-bold">USD</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between">Currency Type :              <span class="font-weight-bold">Fiat</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between">Rate1 USD :
                              <span class="font-weight-bold">1.000 USD</span>
                            </li>
                          </ul>
                                    <a href="https://demo.royalscripts.com/crypto/admin/edit-currency/1" class="btn btn-primary btn-block"><i class="fas fa-edit"></i> Edit Currency</a>  
                                  </div>
                      </div>
                    </div>
                    </div>
                </div>
            </div>
@endSection