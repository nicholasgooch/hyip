<?php
$cap = $cap2 = $dep = $dep2 = $wd_bal = $sum_cap = 0;
?>
@extends('admin.atlantis.layout')
@Section('content')
    <div class="main-panel">
        <div class="content">
            @include('admin.atlantis.main_bar')
            <div class="page-inner mt--5">
                <div id="prnt"></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title"><i class="fa fa-wrench"></i> {{ __('Site Settings') }} </div>
                            </div>
                            <div class="card-body pb-0 table-responsive">
                                <form id="settings_form" action="{{ route('admin.update.settings') }}" method="post"
                                    enctype="multipart/form-data">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12" align="">
                                                <h3><i class="fas fa-feather-alt"></i> {{ __('Header Color') }} </h3>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input id="input_hcolor" value="{{ $settings->header_color }}"
                                                    class="p-0 color_picker float-left with_50per" type="color"
                                                    name="hcolor" required>
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="row">
                                            <div class="col-md-12" align="">
                                                <h3><i class="fas fa-feather-alt"></i> {{ __('Footer Color') }} </h3>
                                                <input id="input_fcolor" value="{{ $settings->footer_color }}"
                                                    class="p-0 color_picker float-left with_50per" type="color"
                                                    name="fcolor" required>
                                            </div>
                                        </div>
                                        <br><br>
                                        <hr>
                                        <div class="row margin_top50">
                                            <div class="col-md-6">
                                                <h3><i class="fab fa-centercode"></i> {{ __('Site Logo') }} </h3>
                                                <input type="file" name="siteLogo" class=" btn btn-warning border_none">
                                            </div>
                                            <div class="col-md-6" align="center">
                                                <img src="{{ $settings->site_logo }}" alt="Logo" class="height_50"
                                                    align="center">
                                            </div>
                                        </div>
                                        <br><br>
                                        <hr>
                                        <div class="row margin_top50">
                                            <div class="col-md-6">
                                                <h3><i class="fab fa-centercode"></i> {{ __('Site Favicon') }} </h3>
                                                <input type="file" name="siteFavicon"
                                                    class=" btn btn-warning border_none">
                                            </div>
                                            <div class="col-md-6" align="center">
                                                <img src="{{ $settings->site_favicon }}" alt="Logo" class="height_50"
                                                    align="center">
                                            </div>
                                        </div>
                                        <br><br>
                                        <hr>
                                        <div class="row margin_top50">
                                            <div class="col-md-6">
                                                <h3><i class="fas fa-thumbtack"></i> {{ __('Site Title') }} </h3>
                                                <input type="text" name="siteTitle" value="{{ $settings->site_title }}"
                                                    class="form-control" placeholder="Site Name" required>
                                            </div>
                                        </div>
                                        <br><br>
                                        <hr>

                                        <div class="row margin_top50">
                                            <div class="col-md-12">
                                                <h3><i class="fas fa-pen"></i> {{ __('Site Description') }} </h3>
                                                <input type="text" name="siteDescr" value="{{ $settings->site_descr }}"
                                                    class="form-control" placeholder="Site Description" required>
                                            </div>
                                        </div>
                                        <br><br>
                                        <hr>
                                        <div class="row margin_top50">
                                            <div class="col-md-12">
                                                <h3><i class="fas fa-pen"></i> {{ __('Admin Email') }} </h3>
                                                <input type="text" name="admin_email"
                                                    value="{{ $settings->admin_email }}" class="form-control"
                                                    placeholder="Admin Email" required>
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="row margin_top50">
                                            <div class="col-md-12">
                                                <h3><i class="fas fa-pen"></i> {{ __('Domain Name') }} </h3>
                                                <input type="text" name="domain_name"
                                                    value="{{ $settings->domain_name }}" class="form-control"
                                                    placeholder="Domain Name" required>
                                            </div>
                                        </div>
                                        <hr>
                                        <h3><i class="fas fa-hand-holding-usd mt-5"></i> {{ __('Deposit Settings') }} </h3>
                                        <div class="row margin_top50">
                                            <div class="col-md-6">
                                                <h5> {{ __('Minimum Deposit (') . $settings->currency . __(')') }} </h5>
                                                <input type="number" name="min_deposit"
                                                    value="{{ $settings->min_deposit }}" class="form-control"
                                                    placeholder="Minimum deposit" step="0.01" required>
                                            </div>
                                            <div class="col-md-6">
                                                <h5> {{ __('Maximum Deposit (') . $settings->currency . __(')') }} </h5>
                                                <input type="number" name="max_deposit"
                                                    value="{{ $settings->max_deposit }}" class="form-control"
                                                    placeholder="Maximum deposit" step="1" required>
                                            </div>
                                            <div class="col-md-12 mt-3" align="right">
                                                <b>On/Off</b><br>
                                                <label class="switch">
                                                    <input id="wallet" type="checkbox" name="wallet"
                                                        value="{{ $settings->deposit }}"
                                                        @if ($settings->deposit == 1) {{ 'checked' }} @endif>
                                                    <span id="" class="slider round"
                                                        onclick="checkedOnOff('wallet')"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <br><br>
                                        <hr>
                                        <h3><i class="fas fa-users mt-5"></i> {{ __('Referal Bonus') }} </h3>
                                        <div class="row margin_top50">
                                            <div class="col-sm-6">
                                                <h5> {{ __('Referral Percentage (Exp. 2 means 2%)') }} </h5>
                                                <input type="number" name="ref_bonus"
                                                    value="{{ $settings->ref_bonus }}" class="form-control"
                                                    placeholder="Enter percentage value" required>
                                            </div>
                                            <div class="col-sm-6">
                                                <h5></i> {{ __('Referral Type') }} </h5>
                                                <select class="form-control" name="ref_type">
                                                    <option value="Once"
                                                        @if ($settings->ref_type == 'Once') {{ 'checked' }} @endif>Once
                                                        (1st investment only)</option>
                                                    <option value="Continous"
                                                        @if ($settings->ref_type == 'Continous') {{ 'checked' }} @endif>
                                                        Continous (for every investment)</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-sm-6">
                                                <h5></i> {{ __('Referral System') }} </h5>
                                                <select class="form-control" name="ref_system">
                                                    <option value="Single_level">Single Level (Only direct referal)
                                                    </option>
                                                    <option value="Multi_level">Multi Level (Others in the chain of
                                                        referal)</option>
                                                </select>
                                                <small class="font_11">Current selection:
                                                    {{ $settings->ref_type ?? '' }}</small>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <h5> {{ __('Referal Level') }} </h5>
                                                    <input type="number" name="ref_level_cnt"
                                                        value="{{ $settings->ref_system }}" class="form-control"
                                                        placeholder="4">
                                                </div>
                                            </div>
                                        </div>
                                        <br><br>
                                        <hr>
                                        <div class="row margin_top50">
                                            <!-- ///////////////////// Withdrawal //////////////////////// -->
                                            <div class="col-md-12 mb-3">
                                                <h2><i class="fas fa-money-bill-alt"></i> {{ __('Withdrawal Settings') }}
                                                </h2>
                                            </div>
                                            <div class="col-sm-6">
                                                <h5> {{ __('Minimum Withdrawal') }} </h5>
                                                <input type="number" name="min_wd" value="{{ $settings->min_wd }}"
                                                    class="form-control" placeholder="Enter  value">
                                            </div>
                                            <div class="col-sm-6">
                                                <h5> {{ __('Withdrawal Limit') }} </h5>
                                                <input type="number" name="wd_limit" value="{{ $settings->wd_limit }}"
                                                    class="form-control" placeholder="Enter value">
                                            </div>

                                            <div class="col-sm-12 mt-3">
                                                <h5> {{ __('Withdrawal Fee. (Exp. 2 means 2%)') }} </h5>
                                                <input type="number" name="wd_fee"
                                                    value="{{ $settings->wd_fee * 100 }}" class="form-control"
                                                    placeholder="Enter value">
                                            </div>
                                            <!-- ///////////////////// End withdrawal //////////////// -->

                                            <div class="col-md-12 mt-2" align="right">
                                                <b>On/Off</b><br>
                                                <label class="switch">
                                                    <input id="wd" type="checkbox" name="wd"
                                                        value="{{ $settings->withdrawal }}"
                                                        @if ($settings->withdrawal == 1) {{ 'checked' }} @endif>
                                                    <span id="" class="slider round"
                                                        onclick="checkedOnOff('wd')"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <br><br>
                                        <hr>


                                        <br><br>
                                        <hr>
                                        <div class="row margin_top50">
                                            <div class="col-md-12 pad_btm_40">
                                                <h3><i class="fa fa-credit-card"></i> {{ __('Payment Gateway Setup') }}
                                                </h3>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="card pad_20">
                                                    <h3 align="center"><i class="far fa-building fa-3x"></i></h3>
                                                    <h2 class="text-center">Bank Deposit Setup</h2>
                                                    <hr>
                                                    <div class="form-group">
                                                        <h5> {{ __('Bank Name') }} </h5>
                                                        <input type="text" name="bank_name"
                                                            value="{{ $settings->bank_name }}" class="form-control"
                                                            placeholder="">
                                                    </div>
                                                    <div class="form-group">
                                                        <h5> {{ __('Account Name') }} </h5>
                                                        <input type="text" name="account_name"
                                                            value="{{ $settings->account_name }}" class="form-control"
                                                            placeholder="">
                                                    </div>
                                                    <div class="form-group">
                                                        <h5> {{ __('Account Number') }} </h5>
                                                        <input type="number" name="account_number"
                                                            value="{{ $settings->account_number }}" class="form-control"
                                                            placeholder="">
                                                    </div>
                                                    <div class="form-group">
                                                        <h5> {{ __('Routing Number') }} </h5>
                                                        <input type="number" name="bank_routing_number"
                                                            value="{{ $settings->bank_routing_number }}"
                                                            class="form-control" placeholder="">
                                                    </div>
                                                    <div class="form-group">
                                                        <h5> {{ __('Bank Country') }} </h5>
                                                        <input type="text" name="bank_country"
                                                            value="{{ $settings->bank_country }}" class="form-control"
                                                            placeholder="">
                                                    </div>
                                                    <div class="form-group">
                                                        <h5> {{ __('Response Email') }} </h5>
                                                        <input type="email" name="bank_deposit_email"
                                                            value="{{ $settings->bank_deposit_email }}"
                                                            class="form-control" placeholder="">
                                                    </div>
                                                    <div class="" align="right">
                                                        <b>On/Off</b><br>
                                                        <label class="switch">
                                                            <input id="switch_bank_deposit" type="checkbox"
                                                                name="bank_deposit_switch"
                                                                value="{{ $settings->bank_deposit_switch }}"
                                                                @if ($settings->bank_deposit_switch == 1) {{ 'checked' }} @endif>
                                                            <span id="" class="slider round"
                                                                onclick="checkedOnOff('switch_bank_deposit')"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="card pad_20">
                                                    <h3 align="center"><i class="fab fa-bitcoin fa-3x"></i></h3>
                                                    <h2 class="text-center">Bitcoin Setup</h2>
                                                    <hr>
                                                    <div class="form-group">
                                                        <h5> {{ __('BTC XPub Key') }} </h5>
                                                        <textarea name="btc_xpub_key" value="" class="form-control" rows="6">{{ $settings->btc_xpub_key }}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <h5> {{ __('BTC Wallet Address') }} </h5>
                                                        <textarea name="btc_wallet_address" value="" class="form-control" rows="6">{{ $settings->btc_wallet_address }}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <h5> {{ __('Description') }} </h5>
                                                        <input type="text" name="btc_decription" value=""
                                                            class="form-control" placeholder="">
                                                    </div>


                                                    <div class="" align="right">
                                                        <b>Enable/Disable BTC Deposit</b><br>
                                                        <label class="switch">
                                                            <input id="switch_btc" type="checkbox" name="switch_btc"
                                                                value="{{ $settings->switch_btc }}"
                                                                @if ($settings->switch_btc == 1) {{ 'checked' }} @endif>
                                                            <span id="" class="slider round"
                                                                onclick="checkedOnOff('switch_btc')"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="card pad_20">
                                                    <h3 align="center"><i class="fab fa-eth fa-3x"></i></h3>
                                                    <h2 class="text-center">Ethereum Setup</h2>
                                                    <hr>
                                                    <div class="form-group">
                                                        <h5> {{ __('Wallet Address') }} </h5>
                                                        <textarea name="eth_wallet_id" value="" class="form-control">
                                                                {{ $settings->eth_wallet_id }}
                                                            </textarea>
                                                    </div>

                                                    <div class="form-group">
                                                        <h5> {{ __('Description') }} </h5>
                                                        <input type="text" name="eth_decription" value=""
                                                            class="form-control" placeholder="">
                                                    </div>


                                                    <div class="" align="right">
                                                        <b>Enable/Disable ETH Deposit</b><br>
                                                        <label class="switch">
                                                            <input id="switch_eth" type="checkbox" name="switch_eth"
                                                                value="{{ $settings->switch_eth }}"
                                                                @if ($settings->switch_eth == 1) {{ 'checked' }} @endif>
                                                            <span id="" class="slider round"
                                                                onclick="checkedOnOff('switch_eth')"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <br><br>
                                        <hr>
                                        <div class="row margin_top50">
                                            <div class="col-md-4">
                                                <h3><i class="fas fa-hand-holding-usd"></i> {{ __('Manage Investment') }}
                                                </h3>
                                            </div>
                                            <div class="col-md-8" align="right">
                                                <b>On/Off</b><br>
                                                <label class="switch">
                                                    <input id="inv" type="checkbox" name="inv"
                                                        value="{{ $settings->investment }}"
                                                        @if ($settings->investment == 1) {{ 'checked' }} @endif>
                                                    <span id="" class="slider round"
                                                        onclick="checkedOnOff('inv')"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <br><br>
                                        <hr>

                                        <div class="row margin_top50">
                                            <div class="col-md-4">
                                                <h3> <i class="fas fa-user"></i> {{ __('Enable User Registration') }}
                                                </h3>
                                            </div>
                                            <div class="col-md-8" align="right">
                                                <b>On/Off</b><br>
                                                <label class="switch">
                                                    <input id="reg" type="checkbox" name="reg"
                                                        value="{{ $settings->user_reg }}"
                                                        @if ($settings->user_reg == 1) {{ 'checked' }} @endif>
                                                    <span id="" class="slider round"
                                                        onclick="checkedOnOff('reg')"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <br><br>
                                        <hr>
                                        <div class="row margin_top50">
                                            <div class="col-md-12">
                                                <h3><i class="fa fa-coggs"></i> {{ __('Currency Settings') }} </h3>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="card pad_20">
                                                    <h5> {{ __('Currency symbol/Code') }} </h5>
                                                    <input type="text" name="cur"
                                                        value="{{ $settings->currency }}" class="form-control"
                                                        placeholder="currency symbol or code" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="card form-group pad_20">
                                                    <h5> {{ __('Currency Rate to US Dollar') }} </h5>
                                                    <input type="text" name="cur_conv"
                                                        value="{{ $settings->currency_conversion }}" class="form-control"
                                                        placeholder="Currency conversion rate to doller" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row margin_top50">
                                            <div class="col-md-12">
                                                <div class="card form-group pad_20">
                                                    <h5> {{ __('Live Chat Widget Code') }} </h5>
                                                    <textarea name="livechat_code" class="form-control" placeholder="Livechat code: Crisp Chat, Livechat etc" required
                                                        rows="10" cols="5">{{ $settings->livechat_code }}
                                                        </textarea>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                                <div class="row margin_top50">
                                    <div class="col-md-12">
                                        <button class="btn btn-warning float-right"
                                            onclick="load_post_ajax('{{ route('admin.update.settings') }}', 'settings_form', 'admin_settings_form' )">
                                            {{ __('Save Changes') }} </button>
                                    </div>
                                </div>
                                <br><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endSection
