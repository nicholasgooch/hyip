<?php
if (Session::has('val')) {
    $v = Session::get('val');
    $deps = App\deposits::where('user_id', 'like', '%' . $v . '%')
        ->orwhere('usn', 'like', '%' . $v . '%')
        ->orwhere('amount', 'like', '%' . $v . '%')
        ->orwhere('bank', 'like', '%' . $v . '%')
        ->orwhere('account_no', 'like', '%' . $v . '%')
        ->orwhere('account_name', 'like', '%' . $v . '%')
        ->orwhere('status', 'like', '%' . $v . '%')
        ->orwhere('created_at', 'like', '%' . $v . '%')
        ->orderby('id', 'desc')
        ->paginate(100);
    Session::forget('val');
} else {
    $deps = App\deposits::orderby('id', 'desc')->paginate(50);
}

?>

@extends('admin.atlantis.layout')
@Section('content')
    <div class="main-panel">
        <div class="content">
            @include('admin.atlantis.main_bar')
            <div class="page-inner mt--5">
                @include('admin.atlantis.overview')
                <div id="prnt"></div>
                @php
                    $inv = App\investment::find($id);
                @endphp
                <div class="col-md-8 offset-md-2">
                    <div class="card">
                        <div class="card-header card_header_bg_blue">
                            <div class="card-head-row card-tools-still-right">
                                <h4 class="card-title text-white">{{ __('Admin Users') }}</h4>
                            </div>
                            <p class="card-category text-white pl-2">
                                {{ __('All admin users.') }}
                            </p>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.update.investment', $id) }}" method="post">

                                <input id="token" type="hidden" class="form-control" name="_token"
                                    value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label for="">User</label>
                                    <input type="text" value="{{ $inv->usn }}" class="form-control" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="">Investment Package</label>
                                    <input id="" type="text" class="form-control" name="package"
                                        value="{{ $inv->package->package_name }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="">Interest Rate %</label>
                                    <input id="" type="text" class="form-control" name="interest"
                                        value="{{ $inv->interest }}">
                                </div>
                                <div class="form-group">
                                    <input id="" type="hidden" class="form-control" name="amount"
                                        value="{{ $inv->capital }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Created Date</label>

                                    <input type="text" name="created_at" id="created_at" value=""
                                        class="form-control datepicker">
                                </div>

                                <div class="form-group">
                                    <label for=""> End Date</label>
                                    <input type="text" name="end_date" id="end_date" value=""
                                        class="form-control datepicker">
                                </div>
                                <div class="form-group">
                                    <label for="">Last Withdrawal Date</label>

                                    <input type="text" name="last_wd" id="last_wd" value=""
                                        class="form-control datepicker">
                                </div>

                                <div class="form-group">
                                    <br>
                                    <button type="submit" class="collb btn btn-warning">{{ __('Edit') }}</button>
                                    <br>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@section('js')
    <script>
        $(function() {
            const created_at = '{{ $inv->created_at->format('Y-m-d') }}'
            const end_date = '{{ $inv->end_date }}';
            const last_wd = '{{ $inv->last_wd }}';
            $("#created_at").datepicker({
                defaultValue: created_at
            });
            $('#end_date').datepicker({
                defaultValue: end_date
            })
            $('#last_wd').datepicker({
                defaultValue: last_wd
            })
        });
    </script>
@endsection
