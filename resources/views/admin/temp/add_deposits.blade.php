<div class="">
    <div class="">
        <div class=" ">
            <form action="{{ route('admin.save.deposit') }}" method="post">

                <input id="token" type="hidden" class="form-control" name="_token" value="{{ csrf_token() }}">

                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text "><i class="fa fa-user"></i></span>
                    </div>
                    <select name="user_id" id="" class="form-control">
                        @foreach ($users as $user)
                            <option value="{{ $user->id }}">{{ $user->firsname . ' ' . $user->lastname }}</option>
                        @endforeach
                    </select>
                </div>
                <br>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text "><i class="fa fa-money"></i></span>
                    </div>
                    <input id="" type="number" class="form-control" name="amount"
                        placeholder="Enter amount to deposit" required>
                </div>
                <br>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text "><i class="fa fa-key"></i></span>
                    </div>
                    <select name="paymentMethod" id="" class="form-control">
                        <option value="BTC">Bitcoin</option>
                        <option value="ETH">ETH</option>
                    </select>
                </div>
                <br>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text "><i class="fa fa-calender"></i></span>
                    </div>
                    <input type="date" name="created_at" id="" class="form-control">
                </div>
                <br>

                <div class="form-group">
                    <br>
                    <button type="submit" class="collb btn btn-warning">{{ __('Add') }}</button>
                    <br>
                </div>
            </form>
        </div>
    </div>
</div>
