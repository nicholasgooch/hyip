@extends('admin.atlantis.layout')

@section('title', 'Command Console')

@section('styles')
<link rel="stylesheet" href="{{ asset('atlantis/css/console.css') }}">
@endsection

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div>
                            <div class="card-title">Command Console</div>
                            <div class="card-category">Execute system commands directly from the admin panel</div>
                        </div>
                        <button class="btn btn-primary btn-sm" onclick="toggleHelp()">
                            <i class="fas fa-question-circle mr-1"></i> Show Commands
                        </button>
                    </div>
                    <div class="card-body p-0">
                        <!-- Help Section -->
                        <div id="helpSection" class="p-3 border-bottom" style="display: none; background: #1a1a2e;">
                            <h5 class="text-white mb-3">Available Commands</h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="command-group">
                                        <h6 class="text-info mb-3">Artisan Commands</h6>
                                        <div class="command-list">

                                            <div class="command-item">
                                                <div class="command-name">php artisan generate:investments</div>
                                                <div class="command-args">
                                                    [<span class="arg-required">--email=</span>]
                                                    [<span class="arg-required">--amount=</span>]
                                                    [<span class="arg-optional">--count=</span>]
                                                    [<span class="arg-optional">--start=24</span>]
                                                    [<span class="arg-optional">--end=4</span>]
                                                    [<span class="arg-optional">--deposits=5</span>]
                                                    [<span class="arg-optional">--add_balance</span>]
                                                </div>
                                                <div class="command-desc">Generate investments</div>
                                            </div>
                                            <div class="command-item">
                                                <div class="command-name">php artisan settings:register</div>
                                                <div class="command-args">
                                                    [<span class="arg-required">--site-logo=</span>]
                                                    [<span class="arg-required">--site-favicon=</span>]
                                                    [<span class="arg-required">--site-title=</span>]
                                                    [<span class="arg-required">--site-descr=</span>]
                                                    [<span class="arg-required">--header-color=</span>]
                                                    [<span class="arg-required">--footer-color=</span>]
                                                </div>
                                                <div class="command-desc">Register initial settings</div>
                                            </div>
                                            <div class="command-item">
                                                <div class="command-name">php artisan seed:country-state</div>
                                                <div class="command-desc">Seed countries and states</div>
                                            </div>
                                            <div class="command-item">
                                                <div class="command-name">php artisan users:delete</div>
                                                <div class="command-args">
                                                    [<span class="arg-optional">--days=2</span>]
                                                    [<span class="arg-optional">--type=suspicious</span>]
                                                    [<span class="arg-optional">--force</span>]
                                                    [<span class="arg-optional">--filter</span>]
                                                </div>
                                                <div class="command-desc">Delete unverified users</div>
                                            </div>
                                            <div class="command-item">
                                                <div class="command-name">php artisan create:packages</div>
                                                <div class="command-args">
                                                    [<span class="arg-optional">--recreate</span>]
                                                </div>
                                                <div class="command-desc">Create default packages</div>
                                            </div>
                                            <div class="command-item">
                                                <div class="command-name">php artisan make:admin</div>
                                                <div class="command-args">
                                                    [<span class="arg-required">--email=</span>]
                                                    [<span class="arg-required">--password=</span>]
                                                    [<span class="arg-required">--name=</span>]
                                                    [<span class="arg-required">--role=</span>]
                                                </div>
                                                <div class="command-desc">Create admin user</div>
                                            </div>
                                        </div>
                                        <div class="command-list-overlay"></div>
                                        <button class="show-more-btn" onclick="toggleCommandList(this, 'artisan')">
                                            Show More <i class="fas fa-chevron-down ml-1"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="command-group">
                                        <h6 class="text-info mb-3">System Commands</h6>
                                        <div class="command-list">
                                            <div class="command-item">
                                                <div class="command-name">composer update</div>
                                                <div class="command-args">
                                                    [<span class="arg-optional">package</span>]
                                                </div>
                                                <div class="command-desc">Update dependencies based on composer.json
                                                </div>
                                            </div>
                                            <div class="command-item">
                                                <div class="command-name">git status</div>
                                                <div class="command-desc">Show working tree status</div>
                                            </div>
                                            <div class="command-item">
                                                <div class="command-name">tail</div>
                                                <div class="command-args">
                                                    [<span class="arg-optional">-f</span>]
                                                    <span class="arg-required">&lt;filename&gt;</span>
                                                </div>
                                                <div class="command-desc">Output the last part of files</div>
                                                <div class="command-example">Example: tail -f storage/logs/laravel.log
                                                </div>
                                            </div>
                                            <div class="command-item">
                                                <div class="command-name">df -h</div>
                                                <div class="command-desc">Show disk space usage in human-readable format
                                                </div>
                                            </div>
                                            <div class="command-item">
                                                <div class="command-name">ps aux</div>
                                                <div class="command-desc">List all running processes</div>
                                            </div>
                                        </div>
                                        <div class="command-list-overlay"></div>
                                        <button class="show-more-btn" onclick="toggleCommandList(this, 'system')">
                                            Show More <i class="fas fa-chevron-down ml-1"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-3">
                                <h6 class="text-info mb-3">Tips</h6>
                                <ul class="list-unstyled text-white-50">
                                    <li>• Use Up/Down arrow keys to navigate command history</li>
                                    <li>• Type 'help' to show this command list in the console</li>
                                    <li>• Arguments in <span class="arg-required">&lt;angle brackets&gt;</span> are
                                        required
                                    </li>
                                    <li>• Arguments in <span class="arg-optional">[square brackets]</span> are optional
                                    </li>
                                    <li>• Commands timeout after 60 seconds</li>
                                </ul>
                            </div>
                        </div>
                        <!-- Console Section -->
                        <div id="console" class="console-container text-light"
                            style="height: calc(100vh - 330px); min-height: 400px;">
                            <div class="console-content p-3">
                                <div class="welcome-message command-output">Welcome to Admin Console. Type 'help' for
                                    available commands.</div>
                                <div id="output"></div>
                                <div class="console-input-line">
                                    <span class="text-success prompt">$</span>
                                    <input type="text" id="commandInput" class="console-input" autocomplete="off"
                                        placeholder="Enter command...">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('js')
    <script>
        function toggleCommandList(button, type) {
            const list = button.previousElementSibling.previousElementSibling;
            const isExpanded = list.classList.contains('expanded');

            list.classList.toggle('expanded');
            button.classList.toggle('expanded');

            if (isExpanded) {
                button.innerHTML = 'Show More <i class="fas fa-chevron-down ml-1"></i>';
                // Smooth scroll back to the top of the command group
                list.closest('.command-group').scrollIntoView({ behavior: 'smooth' });
            } else {
                button.innerHTML = 'Show Less <i class="fas fa-chevron-up ml-1"></i>';
            }
        }

        function toggleHelp() {
            const helpSection = document.getElementById('helpSection');
            helpSection.style.display = helpSection.style.display === 'none' ? 'block' : 'none';
        }

        document.addEventListener('DOMContentLoaded', function () {
            const console = document.getElementById('console');
            const output = document.getElementById('output');
            const commandInput = document.getElementById('commandInput');
            let commandHistory = [];
            let historyIndex = -1;

            const helpText = `Available Commands:

Artisan Commands:
  php artisan generate:investments [--email=] [--amount=] [--count=] [--start=] [--end=] [--deposits=5] [--add_balance]
  php artisan settings:register [--site-logo=] [--site-favicon=] [--site-title=] [--site-descr=] [--header-color=] [--footer-color=]
  php artisan seed:country-state
  php artisan users:delete [--days=] [--type=] [--force] [--filter=]
  php artisan create:packages [--recreate]
  php artisan make:admin [--email=] [--password=] [--name=] [--role=]

System Commands:
  composer update [package]
    Update dependencies based on composer.json

  git status
    Show working tree status

  tail [-f] <filename>
    Output the last part of files
    Example: tail -f storage/logs/laravel.log

Tips:
• Arguments in <angle brackets> are required
• Arguments in [square brackets] are optional
• Use Up/Down arrow keys to navigate command history
• Commands timeout after 60 seconds`;

            function appendOutput(text, type = 'normal') {
                const div = document.createElement('div');
                div.className = 'command-output';

                if (type === 'error') {
                    div.classList.add('error-output');
                } else if (type === 'success') {
                    div.classList.add('success-output');
                } else if (type === 'command') {
                    div.classList.add('command-history');
                }

                div.textContent = text;
                output.appendChild(div);
                console.scrollTop = console.scrollHeight;
            }

            commandInput.addEventListener('keydown', function (e) {
                if (e.key === 'Enter') {
                    const command = this.value.trim();
                    if (!command) return;

                    this.value = '';
                    commandHistory.push(command);
                    historyIndex = commandHistory.length;

                    appendOutput('$ ' + command, 'command');

                    // Handle help command locally
                    if (command.toLowerCase() === 'help') {
                        appendOutput(helpText, 'success');
                        return;
                    }

                    fetch('/admin/console/execute', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content
                        },
                        body: JSON.stringify({ command })
                    })
                        .then(response => response.json())
                        .then(data => {
                            if (data.success) {
                                if (data.output) {
                                    appendOutput(data.output, 'success');
                                }
                            } else {
                                appendOutput(data.error || data.output, 'error');
                            }
                        })
                        .catch(error => {
                            appendOutput('Error executing command: ' + error.message, 'error');
                        });
                }
                // Command history navigation
                else if (e.key === 'ArrowUp') {
                    e.preventDefault();
                    if (historyIndex > 0) {
                        historyIndex--;
                        this.value = commandHistory[historyIndex];
                    }
                }
                else if (e.key === 'ArrowDown') {
                    e.preventDefault();
                    if (historyIndex < commandHistory.length - 1) {
                        historyIndex++;
                        this.value = commandHistory[historyIndex];
                    } else {
                        historyIndex = commandHistory.length;
                        this.value = '';
                    }
                }
            });

            // Focus input when clicking anywhere in the console
            console.addEventListener('click', function () {
                commandInput.focus();
            });

            // Initial focus
            commandInput.focus();
        });
    </script>
    @endsection