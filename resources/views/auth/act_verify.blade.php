@extends('auth.layouts.app')
<title>Verify Account - {{env('APP_NAME')}}</title>
@section('content')
<body>
    <div class="verify_form_cont">
       <div class="fixedOeverlayBG"></div>
        <div class="container" style="padding-top: 90px;">
            <div class="row pad_T90">
                <div class="col-md-4"></div>
                <div class="col-md-4">                    
                    <div class="very_form_div">                        
                        <div class="panel ">
                            <div class="card-header">
                                <div align="center">
                                     <img src="{{$settings->site_logo}}" alt="{{$settings->site_title}}" class="login_logo" style="height: 50px !important;">                 
                                    <br>
                                    <h3 class="colhd text-white"><i class="fa fa-key text-white"></i>{{ __('User Verification') }} </h3>
                                    <hr>
                                </div>
                            </div>
                            <div class="panel-body" style="">
                                @if(Session::has('msgType') && Session::get('msgType') == 'err')
                                
                                    <div class="alert alert-danger">
                                        {{Session::get('status')}}
                                    </div>
                                    {{Session::forget('status')}}
                                    {{Session::forget('msgType')}}
                                    
                                @elseif(Session::has('msgType') && Session::get('msgType') == 'suc')
                                
                                    <div class="alert alert-success">
                                        {{Session::get('status')}}
                                    </div>
                                    {{Session::forget('status')}}
                                    {{Session::forget('msgType')}}
                                @else
                                
                                    <div class="alert alert-danger">
                                       <p>
                                           {{ __('Invalid access to this page.') }}
                                       </p>
                                    </div>
                                     
                                @endif

                                <div class="form-group row mb-0">
                                    <div class="" align="center">
                                       <p>
                                           <strong><a href="/login" class="collcc btn btn-warning">{{ __('Back to Login') }}</a></strong>
                                       </p>                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection