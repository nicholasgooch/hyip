@extends('auth.layouts.app')
<title>Register Account - {{ $settings->site_title }}</title>

@section('content')
<!-- Loader -->
<div class="back-to-home rounded d-none d-sm-block d-flex justify-content-center ">
    <a href="http://{{config('app.domain')}}" class=" pt-2 btn btn-icon btn-soft-primary text-white">
        <i class="fa fa-home" style="font-size: 20px;"></i></a>
</div>

<!-- Hero Start -->
<section class="">
    <div class="container-fluid no-margin-container">

        <div class="row no-gutters" id="animatedDiv">
            @include('auth.partials.side-hero-slider')
            <div class="col-lg-5 col-md-6">
                <div class="card login_page shadow rounded border-0" style="background: url({{ 'v2/assets/images/auth-bg.jpg' }});
                background-repeat: no-repeat;
                  background-size: cover;">
                    <div class="card-body pl-5 pr-5">
                        <div class="card-title text-center">
                            <a class="" href="/">
                                <img class="img-responsive " src="{{ $settings->site_logo }}" height="50" alt="">
                            </a>
                            <h4 class="mt-4 mb-2" text-muted">{{ __('Register Your Account') }}</h4>
                            <p class="text-muted">
                                Create an account in few minutes
                            </p>
                        </div>
                        <form class="login-form mt-4" method="POST" action="{{ route('register') }}">
                            <input id="csrf" type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group position-relative">

                                        <input id="Fname" type="text"
                                            class="form-control  @error('Fname') is-invalid @enderror p-4" name="Fname"
                                            value="{{ old('Fname') }}" required autocomplete="Fname" autofocus
                                            placeholder="First Name">
                                        @error('Fname')
                                            <span class="invalid-feedback" role="alert alert-danger">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group position-relative">
                                        {{-- <label>Last Name <span class="text-danger">*</span></label> --}}
                                        <input id="Lname" type="text"
                                            class="form-control @error('Lname') is-invalid @enderror p-4 " name="Lname"
                                            value="{{ old('Lname') }}" required autocomplete="Lname" autofocus
                                            placeholder="Last Name">

                                        @error('Lname')
                                            <span class="invalid-feedback" role="alert alert-danger">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        {{-- <label>Email Address <span class="text-danger">*</span></label> --}}
                                        <input id="email" type="email"
                                            class="form-control @error('email') is-invalid @enderror p-4" name="email"
                                            value="{{ old('email') }}" required autocomplete="email"
                                            placeholder="Email">


                                        @error('email')
                                            <span class="invalid-feedback" role="alert alert-danger">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        {{-- <label for="username">Username <span class="text-danger">*</span></label>
                                        --}}
                                        <input id="username" type="text"
                                            class="form-control @error('username') is-invalid @enderror p-4"
                                            name="username" value="{{ old('username') }}" required
                                            autocomplete="username" placeholder="Username">

                                        @error('username')
                                            <span class="invalid-feedback" role="alert alert-danger">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        {{-- <label for="password">Password <span class="text-danger">*</span></label>
                                        --}}
                                        <input id="password" type="password"
                                            class="form-control p-4  @error('password') is-invalid @enderror regTxtBox"
                                            name="password" required autocomplete="new-password" placeholder="Password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert alert-danger">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        {{-- <label for="password">Confirm Password <span
                                                class="text-danger">*</span></label> --}}
                                        <input id="password-confirm" type="password" class="p-4 form-control "
                                            name="password_confirmation" required autocomplete="new-password"
                                            placeholder="Confirm password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert alert-danger">
                                                <strong>{{ $message }} </strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-12 d-flex justify-content-center">
                                    @if ($errors->has('cf-turnstile-response'))
                                        <span class="help-block mt-3 mb-3">
                                            <strong
                                                class="text-danger">{{ $errors->first('cf-turnstile-response') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-lg-12 d-flex justify-content-center mt-3">
                                    <div class="form-group position-relative">
                                        <x-turnstile />
                                    </div>
                                </div>
                                <?php
$usn = App\User::where('username', Session::get('ref'))->get();
                                    ?>

                                <div class="">

                                    <input id="ref" type="hidden" class="form-control" name="ref"
                                        value="@if (count($usn) > 0) {{ Session::get('ref') }} @endif">
                                </div>


                                <div class="col-md-12">

                                    <div class="form-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value=""
                                                id="flexCheckDefault">
                                            <label class="form-check-label" for="flexCheckDefault">I Accept <a href="#"
                                                    class="text-primary">Terms And
                                                    Condition</a></label>
                                        </div>



                                    </div>
                                    <div class="form-group">
                                        <div class="form-check">
                                            By registering you agree to {{ $settings->site_title }} <a href="#"
                                                class="text-primary"> TERMS OF USE</a></label>
                                        </div>



                                    </div>
                                </div>
                                <div class="mx-auto mb-3">
                                    <p class="mb-0 mt-3"><small class="text-dark mr-2">Already have an account
                                            ?</small> <a href="{{route('login')}}"
                                            class=" text-dark font-weight-bold">Login
                                            in</a>
                                    </p>
                                </div>
                                <div class="col-md-12">
                                    @if ($settings->user_reg == 1)
                                        <button type="submit" class="btn btn-primary btn-block">Register</button>
                                    @else
                                        <div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i>
                                            Registration disabled by admin.</div>
                                    @endif
                                </div>
                                @if (env('SOCIAL_LOGIN') == true)
                                    @include('auth.partials.social-auth')
                                @endif
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!--end col-->

    </div>
    <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->
<!-- Hero End -->
@endsection
@section('scripts')
<script src="{{ asset('landrick/js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('landrick/js/bootstrap.bundle.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.4/min/tiny-slider.js"
    integrity="sha512-j+F4W//4Pu39at5I8HC8q2l1BNz4OF3ju39HyWeqKQagW6ww3ZF9gFcu8rzUbyTDY7gEo/vqqzGte0UPpo65QQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(function () {
        $love = $('.heart');
        for (var i = 0; i < 4; i++) {
            $('.wrapper').append($love.clone());
        }
    });
</script>
<script>
    var slider = tns({
        container: '.tiny-single-item',
        items: 1,
        slideBy: 'page',
        "center": true,
        autoplay: true,
        infinite: true,
        autoplayButtonOutput: false,
        'controls': false,
        nav: true,
        "animateIn": "jello",
        "animateOut": "rollOut",
        "navContainer": false,
    });
</script>
@endsection