<div class="col-lg-12 mt-4 text-center">
    <h6>Or Login With</h6>
    <div class="row">
        {{-- <div class="col-6 mt-3">
            <div class="d-grid">
                <a href="{{ route('social.login', 'facebook') }}"
                    class="btn btn-light"><i
                        class="mdi mdi-facebook text-primary"></i>
                    Facebook</a>
            </div>
        </div> --}}
        <!--end col-->

        <div class="col-6 mt-3 mx-auto">
            <div class="d-grid">
                <a href="{{ route('social.login', 'google') }}" class="btn btn-light"><i
                        class="mdi mdi-google text-danger"></i>
                    Google</a>
            </div>
        </div>
        <!--end col-->
    </div>
</div>
