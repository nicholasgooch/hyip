@extends('auth.layouts.app')

@section('content')
<!-- Hero Start -->
<section class="cover-user bg-home bg-white">
    <div class="container-fluid px-0">
        <div class="row g-0 position-relative">
            <div class="col-lg-5 cover-my-30 order-2">
                <div class="cover-user-img d-flex align-items-center">
                    <div class="row">
                        <div class="col-12">
                            <div class="card border-0" style="z-index: 1">
                                <div class="card-body p-0">
                                    <h4 class="card-title text-center">{{ __('Verify Your Email Address') }}</h4>
                                    <form class="login-form mt-4">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                @if (session('resent'))
                                                    <div class="alert alert-success" role="alert">
                                                        {{ __('A fresh verification link has been sent to your email address.') }}
                                                    </div>
                                                @endif
                                                <p class="text-muted">
                                                    {{ __('Before proceeding, please check your email for a verification link.') }}
                                                </p>
                                                <p class="">{{ request()->user()->email }}</p>
                                                @if (Route::has('verification.resend'))
                                                    <p class="text-muted">{{ __('If you did not receive the email') }},
                                                        <a class="btn btn-primary btn-md mt-2 "
                                                            href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>
                                                    </p>
                                                @endif
                                            </div><!--end col-->
                                            <!--end col-->

                                        </div><!--end row-->
                                    </form>
                                </div>
                            </div>
                        </div><!--end col-->
                    </div><!--end row-->
                </div> <!-- end about detail -->
            </div> <!-- end col -->

            @include('auth.partials.side-hero-slider')<!-- end col -->
        </div><!--end row-->
    </div><!--end container fluid-->
</section><!--end section-->
<!-- Hero End -->
@endsection
@section('scripts')
<script src="{{ asset('landrick/js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('landrick/js/bootstrap.bundle.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.4/min/tiny-slider.js"
    integrity="sha512-j+F4W//4Pu39at5I8HC8q2l1BNz4OF3ju39HyWeqKQagW6ww3ZF9gFcu8rzUbyTDY7gEo/vqqzGte0UPpo65QQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(function () {
        $love = $('.heart');
        for (var i = 0; i < 4; i++) {
            $('.wrapper').append($love.clone());
        }
    });
</script>
<script>
    var slider = tns({
        container: '.tiny-single-item',
        items: 1,
        slideBy: 'page',
        "center": true,
        autoplay: true,
        infinite: true,
        autoplayButtonOutput: false,
        'controls': false,
        nav: true,
        "animateIn": "jello",
        "animateOut": "rollOut",
        "navContainer": false,
    });
</script>
@endsection