<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>{{ $settings->site_title }} - {{ $settings->site_descr }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="shortcut icon" href="{{ $settings->site_favicon }}">

    <!-- Bootstrap -->
    <link href="{{ asset('landrick/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Icons -->
    <link href="{{ asset('landrick/css/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v2.1.9/css/unicons.css">
    <!-- Main Css -->
    <link href="{{ asset('landrick/css/style.css') }}" rel="stylesheet" type="text/css" id="theme-opt" />
    <link href="{{ asset('landrick/colors/' . config('app.portfolio_app_theme', 'red') . '.css') }}" rel="stylesheet"
        id="color-opt">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.4/tiny-slider.css"
        integrity="sha512-eMxdaSf5XW3ZW1wZCrWItO2jZ7A9FhuZfjVdztr7ZsKNOmt6TUMTQgfpNoVRyfPE5S9BC0A4suXzsGSrAOWcoQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <style>
        body {
            height: 100vh;
            margin: 0;
            padding: 0;
            overflow: auto;
        }

        @media screen and (max-width: 600px) {
            .hide_div {
                visibility: hidden;
                clear: both;
                float: left;
                margin: 10px auto 5px 20px;
                width: 28%;
                display: none;
            }
        }

        @media only screen and (max-width: 600px) {
            .hide-div {
                visibility: hidden;
                clear: both;
                float: left;
                margin: 10px auto 5px 20px;
                width: 28%;
                display: none;
            }
        }
    </style>

    <style>
        @import url('https://fonts.googleapis.com/css?family=Exo:400,700');

        * {
            margin: 0px;
            padding: 0px;
        }

        body {
            font-family: 'Exo', sans-serif;
        }


        .context {
            width: 100%;
            position: absolute;
            top: 0;

        }

        .context h1 {
            text-align: center;
            color: #fff;
            font-size: 50px;
        }

        .area {
            background:
                {{ config('app.primary_color', '#d63152') }}
            ;
            background: -webkit-linear-gradient(to left,
                    {{ config('app.primary_color', '#d63152') }}
                    ,
                    {{ config('app.secondary_color', '#0FB4C3') }}
                );
            width: 100%;
            height: 100%;


        }

        .circles {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            overflow: hidden;
        }

        .circles li {
            position: absolute;
            display: block;
            list-style: none;
            width: 20px;
            height: 20px;
            background: rgba(255, 255, 255, 0.2);
            animation: animate 25s linear infinite;
            bottom: -150px;

        }

        .circles li:nth-child(1) {
            left: 25%;
            width: 80px;
            height: 80px;
            animation-delay: 0s;
        }


        .circles li:nth-child(2) {
            left: 10%;
            width: 20px;
            height: 20px;
            animation-delay: 2s;
            animation-duration: 12s;
        }

        .circles li:nth-child(3) {
            left: 70%;
            width: 20px;
            height: 20px;
            animation-delay: 4s;
        }

        .circles li:nth-child(4) {
            left: 40%;
            width: 60px;
            height: 60px;
            animation-delay: 0s;
            animation-duration: 18s;
        }

        .circles li:nth-child(5) {
            left: 65%;
            width: 20px;
            height: 20px;
            animation-delay: 0s;
        }

        .circles li:nth-child(6) {
            left: 75%;
            width: 110px;
            height: 110px;
            animation-delay: 3s;
        }

        .circles li:nth-child(7) {
            left: 35%;
            width: 150px;
            height: 150px;
            animation-delay: 7s;
        }

        .circles li:nth-child(8) {
            left: 50%;
            width: 25px;
            height: 25px;
            animation-delay: 15s;
            animation-duration: 45s;
        }

        .circles li:nth-child(9) {
            left: 20%;
            width: 15px;
            height: 15px;
            animation-delay: 2s;
            animation-duration: 35s;
        }

        .circles li:nth-child(10) {
            left: 85%;
            width: 150px;
            height: 150px;
            animation-delay: 0s;
            animation-duration: 11s;
        }



        @keyframes animate {

            0% {
                transform: translateY(0) rotate(0deg);
                opacity: 1;
                border-radius: 0;
            }

            100% {
                transform: translateY(-1000px) rotate(720deg);
                opacity: 0;
                border-radius: 50%;
            }

        }

        .no-margin-container {
            margin-left: 0;
            margin-right: 0;
            padding-left: 0;
            padding-right: 0;
        }

        .tns-nav button {
            background-color: #ede4e4 !important;
            border-color: #ede4e4 !important;
        }

        .tns-nav-active>button {
            background-color: #000 !important;
            border-color: #000 !important;
        }

        .tns-nav button.tns-nav-active {
            background-color:
                {{ config('app.accent_color', '#d63152') }}
                !important;
            border-color:
                {{ config('app.accent_color', '#d63152') }}
                !important;
        }
    </style>
    @yield('styles')
</head>

<body>

    <!-- <div id="preloader">
        <div id="status">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div> -->
    <div class="">
        @yield('content')
    </div>


    <!-- javascript -->
    <script src="{{ asset('landrick/js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('landrick/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('landrick/js/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('landrick/js/scrollspy.min.js') }}"></script>
    <!-- Icons -->
    <script src="{{ asset('landrick/js/feather.min.js') }}"></script>
    <script src="https://unicons.iconscout.com/release/v2.1.9/script/monochrome/bundle.js"></script>
    <!-- Main Js -->
    <script src="{{ asset('landrick/js/app.js') }}"></script>
    <script type="text/javascript">
        {!! $settings->livechat_code !!}
    </script>
    @yield('scripts')
</body>

</html>