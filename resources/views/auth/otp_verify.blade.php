<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OTP Verification</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.16/dist/tailwind.min.css" rel="stylesheet">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="{{ $settings->site_favicon }}">
</head>

<body class="h-screen flex justify-center items-center" style="background-color:{{ config('app.primary_color') }};">
    <div style="background: url({{ 'v2/assets/images/auth-bg.jpg' }});
    background-repeat: no-repeat;
      background-size: cover; ">
    </div>
    <div class="bg-white p-4 sm:p-8 rounded-lg shadow-md w-full sm:w-1/2 md:w-1/3 mx-auto">
        <div class="flex justify-center mb-5">
            <div class=" p-4 sm:p-5 rounded-full" style="background-color: {{ config('app.primary_color') }}">
                <svg class="w-8 sm:w-10 h-8 sm:h-10 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                    fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <polyline points="20 6 9 17 4 12"></polyline>
                </svg>
            </div>
        </div>
        <h2 class="text-center text-lg sm:text-xl font-medium mb-5">Enter OTP (One Time Password) Code</h2>
        <div class="mb-3 text-xs sm:text-sm">One Time Password (OTP) has been sent to your email
            <strong>{{ $user->email }}</strong>. Please enter
            the code here to login.
        </div>

        <div id="errorAlert" class="bg-red-200 text-red-700 p-3 mb-4 rounded hidden">
            Error: Invalid OTP code.
        </div>
        <div id="successAlert" class="bg-green-200 text-green-700 p-3 mb-4 rounded hidden">
            Success: OTP resent successfully to <strong>{{ $user->email }}</strong>.
        </div>

        <div class="flex justify-between mb-6 space-x-2">
            <!-- Repeat for each input, adjusted classes for better responsiveness -->
            <input type="text" maxlength="1" class="otp-input border p-2 w-full text-center" onkeyup="moveFocus(event)">
            <input type="text" maxlength="1" class="otp-input border p-2 w-full text-center" onkeyup="moveFocus(event)">
            <input type="text" maxlength="1" class="otp-input border p-2 w-full text-center" onkeyup="moveFocus(event)">
            <input type="text" maxlength="1" class="otp-input border p-2 w-full text-center" onkeyup="moveFocus(event)">
            <input type="text" maxlength="1" class="otp-input border p-2 w-full text-center" onkeyup="moveFocus(event)">
            <input type="text" maxlength="1" class="otp-input border p-2 w-full text-center" onkeyup="moveFocus(event)">
            <!-- ... -->
        </div>
        <div class="text-right mb-6">
            <a href="#" id="resendLink" onclick="resendOTP()" class=""
                style="color: {{ config('app.primary_color') }};">Resend OTP</a> <br>
            <span id="resending" class="text-xs sm:text-sm ml-2 hidden">Resending...</span>
            <span id="timer" class="text-xs sm:text-sm hidden">Resend in <span id="countdown">300</span>
                seconds</span>
        </div>
        <button id="verifyButton" onclick="verifyOTP()" class="mb-4 w-full text-white p-2 rounded relative"
            style="background-color: {{ config('app.primary_color') }}">
            <span id="loadingSpinner" class="absolute left-5 top-1/2 transform -translate-y-1/2 hidden">
                <svg class="animate-spin h-5 w-5 text-white" xmlns="http://www.w3.org/2000/svg" fill="none"
                    viewBox="0 0 24 24">
                    <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
                    <path class="opacity-75" fill="currentColor"
                        d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l1-2.647z">
                    </path>
                </svg>
            </span>
            <span id="verifyText">Verify OTP</span>
        </button>
        <button onclick="pasteFromClipboard()" class="w-full bg-gray-300 text-black p-2 rounded">Paste from
            Clipboard</button>
    </div>
    <script src="https://code.jquery.com/jquery-3.7.1.js"
        integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>

    <script>
        var isResendAvailable = true;
        var resendCooldown = 300;


        function verifyOTP() {
            $("#errorAlert").addClass("hidden");
            $("#successAlert").addClass("hidden");

            $("#loadingSpinner").removeClass("hidden");
            $("#verifyText").text("Verifying...");

            const otpInputs = document.querySelectorAll('input');
            let otpValue = '';
            otpInputs.forEach(input => {
                otpValue += input.value;
            });

            if (otpValue.length === 6) {
                $.ajax({
                    url: 'verify-otp',
                    type: 'POST',
                    data: {
                        otpValue: otpValue,
                    },
                    success: function (response) {
                        $("#loadingSpinner").addClass("hidden");
                        $("#verifyText").text("Verify OTP");


                        if (response.success === 'true') {
                            $("#errorAlert").addClass("hidden");
                            $("#successAlert").addClass("hidden");

                            window.location.href = '/app/overview';

                        } else {
                            document.getElementById('errorAlert').classList.remove('hidden');
                        }
                    }
                })
            } else {
                $("#loadingSpinner").addClass("hidden");
                $("#verifyText").text("Verify OTP");
                document.getElementById('errorAlert').classList.remove('hidden');
            }
        }

        function moveFocus(event) {
            const inputs = document.querySelectorAll('input');
            for (let i = 0; i < inputs.length; i++) {
                if (inputs[i] === event.target && i < inputs.length - 1 && event.target.value) {
                    inputs[i + 1].focus();
                    break;
                }
            }
        }

        $(document).ready(function () {
            $(".otp-input").on('paste', function (e) {
                // Get pasted data

                var pastedData = e.originalEvent.clipboardData.getData('text');

                // Check if the pasted data is a 6-digit number
                if (/^\d{6}$/.test(pastedData)) {
                    // Split the number into individual digits
                    var digits = pastedData.split('');

                    // Assign each digit to an OTP input box
                    $(".otp-input").each(function (index, input) {
                        $(input).val(digits[index]);
                    });

                    //move the focus to the submit button
                    $('#verifyButton').focus();

                } else {
                    $("#errorAlert").text("Error: Pasted content is not a 6-digit code.").removeClass(
                        "hidden");
                }
            });
        });

        // New function to handle backspace/delete
        function handleBackspaceDelete(event) {
            var key = event.which || event.keyCode;

            if (key === 8) { // Backspace key
                if (!$(event.target).val()) {
                    // If the current field is empty, move to the previous field and clear it
                    $(event.target).prev(".otp-input").val('').focus();
                } else {
                    // If the current field is not empty, just clear it
                    $(event.target).val('');
                }
            } else if (key === 46) { // Delete key
                // Clear the current input field
                $(event.target).val('');
            }
        }
        $(".otp-input").on('keydown', handleBackspaceDelete);


        async function pasteFromClipboard() {
            try {
                const text = await navigator.clipboard.readText();
                if (/^\d{6}$/.test(text)) {
                    const inputs = document.querySelectorAll('input');
                    for (let i = 0; i < text.length; i++) {
                        inputs[i].value = text[i];
                    }
                } else {
                    alert('Error: Pasted content is not a 6-digit code.');
                }
            } catch (err) {
                console.error('Failed to read clipboard contents: ', err);
            }
        }

        function resendOTP() {
            if (!isResendAvailable) return;


            // Disable the resend link and show "Resending..." message
            $("#resendLink").addClass("text-gray-500 cursor-not-allowed");
            $("#resending").removeClass("hidden");


            $("#errorAlert").addClass("hidden");
            $("#successAlert").addClass("hidden");

            $.ajax({
                url: "verify-otp/resend",
                method: "POST",
                success: function (response) {
                    if (response.success === 'true') {
                        isResendAvailable = false;

                        $("#successAlert").removeClass("hidden");
                        $("#resendLink").addClass("hidden");
                        $("#resending").addClass("hidden");
                        $("#timer").removeClass("hidden");
                        startCountdown();

                    } else {
                        $("#errorAlert").text(response.errorMsg).removeClass("hidden");
                    }
                },
                error: function () {

                    // Re-enable the resend link and hide "Resending..." message
                    $("#resendLink").removeClass("text-gray-500 cursor-not-allowed");
                    $("#resending").addClass("hidden");

                    $("#errorAlert").text("Server error!").removeClass("hidden");
                }
            });
        }

        function startCountdown() {
            var counter = resendCooldown;
            var interval = setInterval(function () {
                counter--;
                $("#countdown").text(counter);

                if (counter === 0) {
                    clearInterval(interval);
                    isResendAvailable = true;
                    $("#resendLink").removeClass("hidden");
                    $("#timer").addClass("hidden");
                }
            }, 1000);
        }
    </script>
</body>

</html>