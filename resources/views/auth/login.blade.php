@extends('auth.layouts.app')
<title>
    Login Account - {{ $settings->site_title }}</title>

@section('content')
<!-- Loader -->
<div class="back-to-home rounded d-none d-sm-block d-flex justify-content-center ">
    <a href="{{ env('PORTFOLIO_URL') }}" class=" pt-2 btn btn-icon btn-soft-primary text-white">
        <i class="fa fa-home" style="font-size: 20px;"></i></a>
</div>

<!-- Hero Start -->
<section class="h-screen">
    <div class="container-fluid no-margin-container">
        <div class="row no-gutters" id="animatedDiv">
            @include('auth.partials.side-hero-slider')
            <div class="col-lg-5 col-md-6">


                <div class="card login_page shadow rounded border-0" style="background: url({{ 'v2/assets/images/auth-bg.jpg' }});
                background-repeat: no-repeat;
                  background-size: cover;">
                    <div class="card-body pl-5 pr-5">
                        <div class="card-title text-center">
                            <a class="mt-5 pt-5" href="/">
                                <img class="img-responsive " src="{{ $settings->site_logo }}" height="50" alt="">
                            </a>
                            <h4 class="mt-5 mb-5 text-muted">{{ __('Login In') }}</h4>
                        </div>
                        <form class="login-form mt-5" method="POST" action="{{ route('login') }}">
                            <div class="row">
                                <input id="csrf" type="hidden" name="_token" value="{{ csrf_token() }}">

                                @if (Session::has('err_msg'))
                                    <div class="alert alert-danger">
                                        {{ Session::get('err_msg') }}
                                    </div>
                                    {{ Session::forget('err_msg') }}
                                @endif

                                @if (Session::has('regMsg'))
                                    <div class="alert alert-success">
                                        {{ Session::get('regMsg') }}
                                    </div>
                                    {{ Session::forget('regMsg') }}
                                @endif
                                <div class="col-lg-12">
                                    <div class="form-group position-relative">
                                        <label>Your Email <span class="text-danger">*</span></label>
                                        <div class="form-icon position-relative">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-user fea icon-sm icons">
                                                <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                                <circle cx="12" cy="7" r="4"></circle>
                                            </svg>
                                            <input id="email" type="email"
                                                class="p-4 @error('email') is-invalid @enderror form-control pl-5"
                                                name="email" value="{{ old('email') }}" required autocomplete="email"
                                                autofocus placeholder="E-Mail Address">
                                            @error('email')
                                                <span class="invalid-feedback" role="alert alert-danger">
                                                    {{ $message }}
                                                </span>
                                            @enderror
                                        </div>

                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group position-relative">
                                        <label for="password">{{ __('Password') }} <span
                                                class="text-danger">*</span></label>
                                        <div class="form-icon position-relative">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-key fea icon-sm icons">
                                                <path
                                                    d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4">
                                                </path>
                                            </svg>
                                            <input id="password" type="password"
                                                class="p-4 @error('password') is-invalid @enderror form-control pl-5"
                                                name="password" required autocomplete="current-password"
                                                placeholder="Password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert alert-danger">
                                                    {{ $message }}
                                                </span>
                                            @enderror
                                        </div>


                                    </div>
                                </div>
                                <div class="col-lg-12 d-flex justify-content-center">
                                    <div class="form-group position-relative">
                                        @if ($errors->has('cf-turnstile-response'))
                                            <span class="help-block">
                                                <strong
                                                    class="text-danger">{{ $errors->first('cf-turnstile-response') }}</strong>
                                            </span>
                                        @endif
                                        <x-turnstile />
                                    </div>
                                </div>

                                <div class="col-lg-12">

                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value=""
                                                    id="remember {{ old('flexCheckDefault') ? 'checked' : '' }}">
                                                <label class="form-check-label" for="flexCheckDefault">Remember me
                                                </label>
                                            </div>
                                        </div>
                                        @if (Route::has('password.request'))
                                            <p class="forgot-pass mt-4 mb-0"><a href="{{ route('password.request') }}"
                                                    class="text-dark font-weight-bold">Forgot Password ?</a></p>
                                        @endif
                                    </div>

                                </div>
                                <div class="col-12 text-center mb-3">
                                    <p class="mb-0 mt-3"><small class="text-dark mr-2">Don't have an account ?</small>
                                        <a href="/register" class="text-dark font-weight-bold">Create An Account</a>
                                    </p>
                                </div>
                                <div class="col-lg-12 mb-0">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                                @if (env('SOCIAL_LOGIN') == true)
                                    @include('auth.partials.social-auth')
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--end col-->

        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->
<!-- Hero End -->
@endsection
@section('scripts')
<script src="{{ asset('landrick/js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('landrick/js/bootstrap.bundle.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.4/min/tiny-slider.js"
    integrity="sha512-j+F4W//4Pu39at5I8HC8q2l1BNz4OF3ju39HyWeqKQagW6ww3ZF9gFcu8rzUbyTDY7gEo/vqqzGte0UPpo65QQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(function () {
        $love = $('.heart');
        for (var i = 0; i < 4; i++) {
            $('.wrapper').append($love.clone());
        }
    });
</script>
<script>
    var slider = tns({
        container: '.tiny-single-item',
        items: 1,
        slideBy: 'page',
        "center": true,
        autoplay: true,
        infinite: true,
        autoplayButtonOutput: false,
        'controls': false,
        nav: true,
        "animateIn": "jello",
        "animateOut": "rollOut",
        "navContainer": false,
    });
</script>
@endsection