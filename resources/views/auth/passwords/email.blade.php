@extends('auth.layouts.app')
<title>Password Recovery - {{env('APP_NAME')}}</title>
@section('content')
<div class="back-to-home rounded d-none d-sm-block d-flex justify-content-center ">
    <a href="/" class=" pt-2 btn btn-icon btn-soft-primary text-white">
        <i class="fa fa-home" style="font-size: 20px;"></i></a>
</div>


<!-- Hero Start -->
<section class="bg-home d-flex align-items-center">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7 col-md-6">
                <div class="mr-lg-5">
                    <img src="{{asset('landrick/images/user/recovery-red.svg')}}" class="img-fluid d-block mx-auto"
                        alt="">
                </div>
            </div>
            <div class="col-lg-5 col-md-6">
                <div class="card login_page shadow rounded border-0">
                    <div class="card-body">
                        <div class="card-title text-center">
                            <a class="" href="/">
                                <img class="img-responsive " src="{{$settings->site_logo}}" height="50" alt="">
                            </a>
                            <h4 class=" r">{{ __('Recover Account') }}</h4>
                        </div>
                        @if(Session::has('status'))
                            <div class="alert alert-success">
                                {{Session::get('status')}}
                            </div>
                        @endif
                        <form method="POST" action="{{route('password.email')}}">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <p class="text-muted">Please enter your email address. You will receive a link to
                                        create a new password via email.</p>
                                    <div class="form-group position-relative">
                                        <label for="email">Email Address <span class="text-danger">*</span></label>
                                        <input type="email"
                                            class="form-control p-4 @error('email') is-invalid @enderror" name="email"
                                            value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <button type="submit"
                                        class="btn btn-primary btn-block">{{ __('Reset Password') }}</button>
                                </div>
                                <div class="mx-auto">
                                    <p class="mb-0 mt-3"><small class="text-dark mr-2">Remember Your Password ?</small>
                                        <a href="/login" class="text-dark font-weight-bold">Sign in</a>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div> <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<!-- Hero End -->
@endsection