@extends('auth.layouts.app')
<title>Password Reset - </title>
@section('content')

<body>
    <div class="bg-home d-flex align-items-center">
        <div class="container" style="padding-top: 50px; ">
            <div class="row align-items-center">
                <div class="col-lg-7 col-md-6">
                    <div class="mr-lg-5">
                        <img src="{{asset('landrick/images/user/recovery-red.svg')}}" class="img-fluid d-block mx-auto"
                            alt="">
                    </div>
                </div>
                <div class="col-lg-5 col-md-6">
                    <div class="card login_page shadow rounded border-0">
                        <div class="card-body">
                            <div class="card-title text-center">
                                <a class="" href="/">
                                    <img class="img-responsive " src="{{$settings->site_logo}}" height="50" alt="">
                                </a>
                                <h4 class="mt-3">{{ __('Recover Account') }}</h4>
                            </div>
                            <hr>
                        </div>


                        <div class="card-body">
                            @if(Session::has('status'))
                                <div class="alert alert-success">
                                    {{Session::get('status')}}
                                </div>
                            @endif
                            <form method="POST" action="{{route('password.update')}}">
                                @csrf

                                <div class="form-group position-relative">
                                    <label for="email">Email Address <span class="text-danger">*</span></label>
                                    <input id="email" type="email"
                                        class="form-control p-4 @error('email') is-invalid @enderror" name="email"
                                        value="{{ $email ?? old('email') }}" required autofocus autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group position-relative">
                                    <label for="password">New Password <span class="text-danger">*</span></label>
                                    <input id="password" type="password"
                                        class="form-control p-4 @error('password') is-invalid @enderror" name="password"
                                        required>

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm">Confirm New Password <span
                                            class="text-danger">*</span></label>
                                    <input id="password-confirm" type="password"
                                        class="form-control p-4 @error('password_confirmation') is-invalid @enderror"
                                        name="password_confirmation" required>
                                </div>

                                <button type="submit" class="btn btn-primary btn-block">
                                    Reset Password
                                </button>
                            </form>


                            <div class="form-group row mb-0">
                                <div class="col-md-12" align="center">
                                    <a href="/login">
                                        <i class="fa fa-arrow-left"></i> {{ __('Back to Login') }}
                                    </a>
                                </div>
                                <br><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    @endsection