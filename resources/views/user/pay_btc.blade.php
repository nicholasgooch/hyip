@include('user.inc.fetch')
@extends('layouts.atlantis.layout')
@Section('content')
    <div class="main-panel">
        <div class="content">
            <?php ($breadcome = 'Bitcoin Payment'); ?>
            @include('user.atlantis.main_bar')
            <div class="page-inner mt--5">
                
                <div id="prnt"></div>
                <div class="row">      
                    <div class="col-sm-3"></div>              
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-head-row">
                                    <div class="card-title text-center" align="center">{{ __('Bitcoin Payment') }}</div>                                       
                                </div>
                            </div>
                            <div class="card-body table-responsive"> 
                                <blockquote align="center">Send the equal amount of Bitcoin to your auto-generated wallet, displayed below. <span>The confirmation is automatic.</span></blockquote>
                                <h4 class="text-center"><b>{{toBTC($trans->amount)}} BTC</b> / <b>${{number_format($trans->amount)}}</b></h4> 
                                <h2 class="small text-center text-bold">TO</h2>
                                <h3 class="text-center">{{$settings->btc_wallet_id}}</h3>
                                <p class="text-center"> 
                                      
                                    <br>                                   
                                    <img src="{{$trans->qr_code}}" alt="" style=" margin:20px ;" class="image-thumbnail" height="200px" width="200px">                                
                                    <br>
                                    <span >     
                                         <a href="{{$trans->status_url}}" target="_blank" class="btn btn-warning">Proceed here</a>                     
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
@endSection
            