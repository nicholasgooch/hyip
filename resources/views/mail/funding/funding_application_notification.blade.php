@component('mail::message')
# {{$subject}}

<br>
Hello {{ $fundingApplication->user->fullname }},
<br>
<br>
@if ($status == 'application-received')
This is to confirm that we have received your funding application. We appreciate your interest in our platform and
we are reviewing your application. We will keep you informed of the status of your application as it progresses.
<br>
<br>
@elseif($status == 'under-review')
We are currently reviewing your funding application and will keep you informed of the status of your application as
it progresses. We appreciate your patience and look forward to working with you.
<br>
<br>
@elseif($status === 'approved')
We are pleased to inform you that your funding application has been approved. We will be in touch with you shortly
to finalize the disbursement of funds.
<br>
<br>
@elseif($status === 'funds-disbursed')
We are pleased to inform you that the funds for your approved funding application have been disbursed. You should
see the funds in your account shortly.
<br>
<br>
@elseif($status === 'declined')
We're sorry, but your application has been declined. If you have any questions, please contact support
<br>
<br>
@endif

Thanks, <br>
{{ config('app.name') }}
@endcomponent
