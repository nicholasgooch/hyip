@component('mail::message')
# Withdrawal Notification
<br>
<br>
Hello {{ $user->fullname }},
<br>
<br>
Your withdrawal request for {{ $amount }} {{ $currency }} has been received and is currently being processed.
<br>
<br>
Status: {{ $status }}
<br>
<br>
Thank you for using our platform.
<br>
<br>
Best regards,<br>
{{ config('app.name') }}
@endcomponent