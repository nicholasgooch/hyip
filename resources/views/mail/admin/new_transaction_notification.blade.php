<?php
    $st = App\site_settings::find(1);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Withdrawal Approval</title>
	<link rel="stylesheet" href="https://wallet.diamondhubplus.com/css/bootstrap.min.css">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >
</head>
<body>
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4" style="border:1px solid #CCC; padding:4%; box-shadow:2px 2px 4px 4px #CCC;">
            <div align="">
        		<img src="{{$st->site_logo}}" style="height:100px; width:100px;" align="center">
        	</div>
			<p style="margin-bottom: 10px;">Hi Admin,</p>
			<p style="margin-bottom: 10px;">A {{$transaction->type}}  Hi, a new transaction has just been created: <b>Ref: {{$transaction->ref}}
        	<p>
        	  </b> <br> 
        	   <b>User: {{$transaction->usn}} </b><br>
        	   <b>Amount: {{$settings->currency}} {{$transaction->amount}}</b>
			   <b>Date: {{$transaction->created_at->diffForHumans()}}</b>
        	</p>
        	<p>
        		<i class="fa fa-certificate">{{$st->site_title}} Investment.
        	</p>
        </div>
    </div>
	
</body>
</html>