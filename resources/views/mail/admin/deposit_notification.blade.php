@component('mail::message')
# Deposit Notification

Hello Admin,

<br>
<br>

A deposit request has been submitted by {{ $user->fullname }}. The details of the transaction are as follows:
<br>
<br>

- Amount: {{ $deposit->amount }}
- Payment method: {{ $deposit->bank }}
- Status: {{ $deposit->deposit_status }}

<br>
<br>
Please click the link below to approve or decline the transaction:
<br>
<br>

To approve this deposit, click the button below:

@component('mail::button', ['url' => $approveUrl])
Approve
@endcomponent
<br>
<br>
To decline this deposit, click the button below:

@component('mail::button', ['url' => $declineUrl])
Decline
@endcomponent

Thank you for using our platform!

@endcomponent