@component('mail::message')
# New Funding Withdrawal Request
<br>
<br>
A new withdrawal request has been made by {{ $user->fullname }}. Here are the details:
<br>
<br>

- Amount: {{ $withdrawal->amount }}
- Withdraw From: {{ $withdrawal->category }}
- Withdrawal Method: {{ $withdrawal->withdrawal_method }}
- Ref: {{ $withdrawal->ref }}

<br>
<br>

Please take action on the request by clicking one of the following links:
<br>
<br>
@component('mail::button', ['url' => $approve_url])
Approve Withdrawal
@endcomponent
<br>
<br>
@component('mail::button', ['url' => $decline_url])
Decline Withdrawal
@endcomponent
<br>
<br>
Thanks,<br>
{{ config('app.name') }}
@endcomponent