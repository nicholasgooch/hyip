@component('mail::message')
# New Funding Application

<br>
Hello Admin,
<br>
<br>
A new funding application has been submitted to the platform.
<br>
<br>

- Applicant Name: {{ $fundingApplication->user->fullname }}
- Applicant Email: {{ $fundingApplication->user->email }}
- Application Type: {{ !isset($fundingApplication->type->name) ? 0 :'1ee' }}
- Requested Amount: {{ $fundingApplication->amount}}
- Status: {{ !isset($fundingApplication->status->name) ? 0 : 'okjsljcs'  }}

<br>
<br>
@component('mail::button', ['url' => ''])
View Application
@endcomponent


Thanks,<br>
{{ config('app.name') }}
@endcomponent