@php
use BaconQrCode\Renderer\Image\Png;
@endphp
@php

if ($deposit->bank === 'ETH') {
$wallet_address = $settings->eth_wallet_id;
} else {
$wallet_address = $settings->btc_wallet_address;
}
@endphp
@component('mail::message')
# Deposit Notification

{{-- When the deposit just gets submitted --}}
@if ($status == 'initiated')
<br><br>
Hello {{ $user->fullname }},
<br><br>
We have received your deposit request for {{ $settings->currency == 'USD' ? '$' : ''}} {{ number_format($deposit->amount,2 )}}. We will process your transaction as soon as
possible. Please go ahead to send {{ $deposit->bank === 'BTC' ? toBtc($deposit->amount) : toEth($deposit->amount) }}
{{ $deposit->bank }} to {{ $wallet_address }} You will receive another email when your deposit has been confirmed.
<br><br>
Thank you for using our platform!
<br>
<br>
@elseif ($status == 'approved')
<br><br>
Hello {{ $user->fullname }},
<br><br>
Your deposit request for {{ $settings->currency == 'USD' ? '$' : ''}} {{ number_format($deposit->amount,2 )}} has been approved. Your account has been credited with the deposit
amount.
<br><br>
Thank you for using our platform!
<br><br>
@elseif ($status == 'declined')
<br><br>
Hello {{ $user->fullname }},
<br><br>
Your deposit request for {{ $settings->currency == 'USD' ? '$' : ''}} {{ number_format($deposit->amount,2 )}} has been declined. Please contact our customer support team for more
information.
<br><br>
Thank you for using our platform!
<br><br>
@elseif ($status == 'pending')
<br><br>
Dear {{ $user->fullname }},
<br>
<br>
We noticed that you recently made a deposit request for {{ number_format($deposit->amount, 2) }}
{{ $settings->currency }} / {{ $deposit->bank === 'BTC' ? toBtc($deposit->amount) : toEth($deposit->amount) }}
{{ $deposit->bank }}, but the transaction has not yet been confirmed.
<br>
<br>
To prevent any errors or delays, we kindly ask that you complete the previous deposit request before submitting a
new one. Please
use the following wallet address to continue the transaction:
<br><br>

{{ $wallet_address }}

<br><br>
Thank you for using our platform!
@endif


<br>
<br>
{{ config('app.name') }}
@endcomponent
