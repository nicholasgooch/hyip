<div class="cf-turnstile" data-sitekey="{{ config('turnstile.site_key') }}"
    data-theme="{{ config('turnstile.theme') }}"></div>
<script src="https://challenges.cloudflare.com/turnstile/v0/api.js" async defer></script>