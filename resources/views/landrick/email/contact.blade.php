
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>{{$settings->site_title}} Email Notification</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
       
        <!-- favicon -->
        <link rel="shortcut icon" href="images/favicon.ico">
        <!-- Bootstrap -->
        <link href="{{asset('landrick/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700&display=swap" rel="stylesheet">
        <!-- Main Css -->
        <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" />
    </head>

    <body style="font-family: Nunito, sans-serif; font-size: 15px; font-weight: 400;">
        <!-- Loader -->
        <!-- <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div> -->
        <!-- Loader -->

        <!-- Hero Start -->
        <section style="align-items: center;">
            <div class="container">
                <div class="row" style="justify-content: center;">
                    <div class="col-lg-6 col-md-8"> 
                        <table style="box-sizing: border-box; width: 100%; border-radius: 6px; overflow: hidden; background-color: #fff; box-shadow: 0 0 3px rgba(60, 72, 88, 0.15);">
                            <thead>
                                <tr style="  text-align: center; ">
                                    <th scope="col">  <img src="{{$settings->site_logo}}" height="50" alt=""></th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td style="padding: 24px 24px;">
                                        <div style="padding: 8px; color: #e43f52; background-color: rgba(228, 63, 82, 0.2); border: 1px solid rgba(228, 63, 82, 0.2); border-radius: 6px; ">
                                           You Received a Contact Message
                                        </div>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td style="padding: 0 24px 15px; color: #8492a6;">
                                        <div>
                                            Full Name: <span style="padding: 4px 8px; border-radius: 6px;">{{$name}}</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 24px 15px; color: #8492a6;">
                                        <div>
                                            Email: <span style=" padding: 4px 8px; border-radius: 6px;">{{$email}}</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 24px 15px; color: #8492a6;">
                                        <div>
                                            Subject: <span style=" padding: 4px 8px; border-radius: 6px;">{{$subject}}</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 24px 15px; color: #8492a6;">
                                        <div>
                                            Message: <span style=" padding: 4px 8px; border-radius: 6px;">{{$comments}}</span>
                                        </div>
                                    </td>
                                </tr>



                                <tr>
                                    <td style="padding: 16px 8px; color: #8492a6; background-color: #f8f9fc; text-align: center;">
                                        © <?php echo date("Y"); ?> {{$settings->site_title}}.
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div><!--end col-->
                </div><!--end row-->
            </div> <!--end container-->
        </section><!--end section-->
        <!-- Hero End -->

        <!-- javascript -->
        <script src="js/jquery-3.5.1.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <!-- Icons -->
        <script src="js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="js/app.js"></script>
    </body>
</html>