
        <!-- Navbar STart -->
        <header id="topnav" class="defaultscroll " >
            <div class="container">
                <!-- Logo container-->
                <div>
                    <a class="logo" href="/">
                        <img src="{{$settings->site_logo}}" height="50" alt="">
				
                    </a>
                </div>                 
                @guest
                <div class="buy-button float-right">
                    <a href="/register" target="" class="btn btn-primary text">Register</a>
                    <a href="/login" target="" class="btn btn-secondary">Login</a>
                </div><!--end login button-->
            @endguest
            @auth
            <div class="buy-button float-right">
                <a href="{{route('v2.index')}}" target="" class="btn btn-primary ">
                    <i class="uil uil-user-circle" style="font-size: 20px;"></i>
                    Dashboard</a>
            </div>
             @endauth
                <!-- End Logo container-->
                <div class="menu-extras">
                    <div class="menu-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </div>
                </div>
        
                <div id="navigation">
                    <!-- Navigation Menu-->   
                    <ul class="navigation-menu nav-left nav-dark" >
                        <li><a href="/" class="text-muted">  Home</a></li>
                        <li class="">
                            <a href="{{route('landrick.about')}}" class="text-muted">About</a><span class="menu-arrow"></span>
                        </li>
                        {{-- <li class="">
                            <a href="{{route('ex.dashboard')}}">Exchange</a><span class="menu-arrow"></span>
                        </li> --}}
                        <li class="">
                            <a href="{{route('landrick.investment')}}" class="text-muted">Plans</a><span class="menu-arrow"></span>
                          
                        
                        <li class="">
                            <a href="{{route('landrick.business')}}" class="text-muted">Business</a>
                          
                        </li>
                        {{-- <li class="has-submenu">
                            <a href="javascript:void(0)" class="text-white">Plans</a><span class="menu-arrow" class="text-white"></span>
                            <ul class="submenu ">
                            <li><a href="{{route('landrick.cloud-mining')}}" >Cloud Minning</a></li>
                            <li><a href="{{route('landrick.private.equity')}}">Private Equity</a></li>
                            </ul>
                        </li> --}}
                        <li class="has-submenu">
                            <a href="javascript:void(0)" class="text-muted">Resources</a><span class="menu-arrow" class="text-white"></span>
                            <ul class="submenu">
                            </li><li class="">
                                <a href="{{route('landrick.affiliates')}}" class="text-muted">Affiliates</a>
                            </li>
                                <li><a href="{{route('landrick.faq')}}">FAQ</a></li>
                                <li><a href="{{route('landrick.how-it-works')}}">How It Works</a></li>
                                <li><a href="{{route('landrick.dictionary')}}">Dictionary</a></li>
                                <li><a href="{{route('landrick.security')}}">Security</a></li>
                                <li><a href="{{route('landrick.privacy-policy')}}">Privacy Policy</a></li>
                                <li><a href="{{route('landrick.terms.condition')}}">Terms And Conditions</a></li>
                                
                            </ul>
                        </li>
                        
                        <li class="">
                            <a href="{{route('landrick.contact')}}" class="text-muted">Contact</a>
                        </li>
                        
                    </ul><!--end navigation menu-->
                    @guest
                    <div class="buy-menu-btn d-none">
                        <a href="/login" target="" class="btn btn-secondary">Login</a>
                        </div><!--end login button-->
                    @endguest
                    @auth
                            <div class="buy-menu-btn d-none">
                                <a href="{{route('v2.index')}}" target="" class="btn btn-primary ">
                                <i class="uil uil-user-circle" style="font-size: 15px;"></i>
                                Dashboard</a>
                            </div>
                     @endauth
                </div><!--end navigation-->
            </div><!--end container-->
        </header><!--end header-->