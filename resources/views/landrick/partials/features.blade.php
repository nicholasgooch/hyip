<section class="section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4 ">Invest with confidence</h4>
                    <p class=" para-desc mb-0 mx-auto"><span class="text-primary font-weight-bold">{{$settings->site_title}}</span> helps new and experienced investors securely grow their digital assets portfolio  </p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row">
            <div class="col-lg-3 col-md-4 mt-4 pt-2">
                <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                    <span class="h1 icon2 text-primary">
                        <i class="uil uil-bolt"></i>
                    </span>
                    <div class="card-body p-0 content">
                        <h5>Fast-track verification</h5>
                        <p class="para text-muted mb-0">Complete your account set up and start investing in minutes</p>
                    </div>
                    <span class="big-icon text-center">
                        <i class="uil uil-bolt"></i>
                    </span>
                </div>
            </div><!--end col-->
            
            <div class="col-lg-3 col-md-4 mt-4 pt-2">
                <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                    <span class="h1 icon2 text-primary">
                        <i class="uil uil-headphones"></i>
                    </span>
                    <div class="card-body p-0 content">
                        <h5>24/7 Support</h5>
                        <p class="para text-muted mb-0">Our team of Happiness Heroes stands by to help out with anything - any time.</p>
                    </div>
                    <span class="big-icon text-center">
                        <i class="uil uil-headphones"></i>
                    </span>
                </div>
            </div><!--end col-->
            
            <div class="col-lg-3 col-md-4 mt-4 pt-2">
                <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                    <span class="h1 icon2 text-primary">
                        <i class="uil uil-atom"></i>
                    </span>
                    <div class="card-body p-0 content">
                        <h5>Experience</h5>
                        <p class="para text-muted mb-0">We have built a modern and easy to use platform for everyone</p>
                    </div>
                    <span class="big-icon text-center">
                        <i class="uil uil-atom"></i>
                    </span>
                </div>
            </div><!--end col-->
            
            <div class="col-lg-3 col-md-4 mt-4 pt-2">
                <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                    <span class="h1 icon2 text-primary">
                        <i class="uil uil-shield-check"></i>
                    </span>
                    <div class="card-body p-0 content">
                        <h5>Security</h5>
                        <p class="para text-muted mb-0">Our platform is highly secured, built with cloud infrastructure.</p>
                    </div>
                    <span class="big-icon text-center">
                        <i class="uil uil-shield-check"></i>
                    </span>
                </div>
            </div><!--end col-->
            
            <div class="col-lg-3 col-md-4 mt-4 pt-2">
                <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                    <span class="h1 icon2 text-primary">
                        <i class="uil uil-lock"></i>
                    </span>
                    <div class="card-body p-0 content">
                        <h5>Secured Payment</h5>
                        <p class="para text-muted mb-0">Secured payment using cryptocurrency as a primary payment.</p>
                    </div>
                    <span class="big-icon text-center">
                        <i class="uil uil-lock"></i>
                    </span>
                </div>
            </div><!--end col-->
            
            <div class="col-lg-3 col-md-4 mt-4 pt-2">
                <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                    <span class="h1 icon2 text-primary">
                        <i class="uil uil-money-withdrawal"></i>
                    </span>
                    <div class="card-body p-0 content">
                        <h5>Instant Withdrawals</h5>
                        <p class="para text-muted mb-0">All withdrawals are processed instantly without latency.</p>
                    </div>
                    <span class="big-icon text-center">
                        <i class="uil uil-money-withdrawal"></i>
                    </span>
                </div>
            </div><!--end col-->
            
            <div class="col-lg-3 col-md-4 mt-4 pt-2">
                <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                    <span class="h1 icon2 text-primary">
                        <i class="uil uil-file-alt"></i>
                    </span>
                    <div class="card-body p-0 content">
                        <h5>Certified Company</h5>
                        <p class="para text-muted mb-0">We are certified to operate within investment businesses..</p>
                    </div>
                    <span class="big-icon text-center">
                        <i class="uil uil-file-alt"></i>
                    </span>
                </div>
            </div><!--end col-->
            
           
            <div class="col-lg-3 col-md-4 mt-4 pt-2">
                <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                    <span class="h1 icon2 text-primary">
                        <i class="uil uil-chart-growth"></i>
                    </span>
                    <div class="card-body p-0 content">
                        <h5>Guarantee</h5>
                        <p class="para text-muted mb-0">Upon initial investment you earn until your package elapses.</p>
                    </div>
                    <span class="big-icon text-center">
                        <i class="uil uil-chart-growth"></i>
                    </span>
                </div>
            </div><!--end col-->

            
        </div><!--end row-->
    </div><!--end container-->

  
</section>