<section class="bg-half-170 d-table w-100" style="background: url('landrick/hero-3.jpeg') center center;" id="home">
    <div class="container">
        <div class="row position-relative align-items-center pt-4">
            <div class="col-lg-7 offset-lg-5">
                <div class="card title-heading studio-home rounded shadow mt-5" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">
                    <h1 class="heading mb-3" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">WEALTH AND ASSET MANAGEMENT</h1>
                    <p class="para-desc text-muted" data-aos="fade-up" data-aos-duration="1600" data-aos-anchor-placement="top-bottom">Enjoy easy access to innovative investment options.
                        Invest any amount in Gold, Oil & Gas Sector, Real Estate, Agriculture & Infrastructure, Forex & Cryptocurrency.</p>
                    <div class="mt-4">
                        <a href="{{route('register')}}" data-aos="fade-right" data-aos-duration="1600" data-aos-anchor-placement="center-bottom" class="btn btn-primary mt-2 mr-2 me-2"><i class="uil uil-sign-in-alt"></i> Get Started</a>
                        <a href="{{route('landrick.about')}}"data-aos="fade-left" data-aos-duration="1600" data-aos-anchor-placement="center-bottom" class="btn btn-outline-primary mt-2"><i class="uil uil-book-alt"></i> Learn More</a>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container--> 
</section><!--end section-->