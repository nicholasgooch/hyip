<section class="section bg-light" >
   
    <div class="container ">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="section-title text-center mb-4 pb-2">
                    <h6 class="">Investment Process</h6>
                    <h4 class="title mb-4">How it Works ?</h4>
                    <p class="text-muted para-desc mx-auto mb-0">You don't need a deep wallet to start investing. Pick a suitable investment option you can afford, and build your wealth over time</p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row">
            <div class="col-md-4 mt-4 pt-2">
                <div class="card features work-process bg-transparent process-arrow border-0 text-center">
                    <div class="icons rounded h1 text-center text-primary px-3">
                        <i class="uil uil-presentation-edit"></i>
                    </div>

                    <div class="card-body">
                        <h4 class="title ">Sign-up</h4>
                        <p class="text-muted mb-0">Create and Verify your account, once verified you will be given $5 sign-up bouns</p>
                    </div>
                </div>
            </div><!--end col-->
            
            <div class="col-md-4 mt-md-5 pt-md-3 mt-4 pt-2">
                <div class="card features work-process bg-transparent process-arrow border-0 text-center">
                    <div class="icons rounded h1 text-center text-primary px-3">
                        <i class="uil uil-airplay"></i>
                    </div>

                    <div class="card-body">
                        <h4 class="title ">Investment</h4>
                        <p class="text-muted mb-0">Choose the plan that fits your investment goal and start an investment timeline for your account.</p>
                    </div>
                </div>
            </div><!--end col-->
            
            <div class="col-md-4 mt-md-5 pt-md-5 mt-4 pt-2">
                <div class="card features work-process bg-transparent d-none-arrow border-0 text-center">
                    <div class="icons rounded h1 text-center text-primary px-3">
                        <i class="uil uil-image-check"></i>
                    </div>

                    <div class="card-body">
                        <h4 class="title ">Earn Profits</h4>
                        <p class="text-muted mb-0">Watch as your investment grows over the course of the investment term</p>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->

</section><!--end section-->
