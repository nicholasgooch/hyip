
@foreach ($packages as $item)
<div class="col-lg-3 col-md-6 col-12 mt-4 pt-2" >
    <div class="card pricing-rates business-rate shadow border-0 rounded">
        <div class="card-body">
            <h2 class="title text-uppercase mb-4 text-muted"> {{strtoupper($item->package_name)}}</h2>
            <div class=" mb-4">
                <span class="h5 mb-0 mt-2 text-muted">{{$settings->currency == 'USD' ? '$' : $settings->currency}}</span>
                <span class="price h5 mb-0 text-muted">{{number_format($item->min)}} - {{number_format($item->max)}}</span>
                <br>
                <span class="h6 align-self-end mb-1 text-muted">{{$item->period}} Days</span>
            </div>
            
            <ul class="list-unstyled mb-0 pl-0">
                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uim uim-check-circle"></i></span>{{$item->daily_interest * $item->period}}% Daily Interest</li>
            </ul>
            <a href="{{route('v2.user.invest.page')}}?plan='{{$item->id}}" class="btn btn-primary mt-4 text-dark">Get Started</a>
        </div>
    </div>
</div><!--end col-->  
@endforeach
