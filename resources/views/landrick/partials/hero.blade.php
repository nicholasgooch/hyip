 <!-- Hero Start -->
 <section class="bg-half-170 border-bottom d-table w-100" id="home">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-7">
                <div class="title-heading mt-4">
                    <div class="alert alert-light alert-pills shadow" role="alert">
                        {{-- <span class="badge badge-pill badge-danger mr-1">v2.5</span> --}}
                        <span class="content"> WELCOME <span class="">TO</span> {{strtoupper($settings->site_title)}}</span>
                    </div>
                    <h1 class="heading mb-3 ">We put the power in your hands to <span class="element text-primary" ></span> Cryptocurrency</h1>
                    <p class="para-desc text-muted">We are {{$settings->site_title}}, consistently rated the best and most secure cryptocurrency investment paltform.</p>
                    <p class="para-desc text-muted">Jump start your crypto portfolio with $5 free after you sign up. Terms and Conditions apply.
                    </p>
                    <div class="mt-4">
                        <a href="/register" class="btn btn-outline-primary rounded mr-2"><i class="mdi mdi-account-circle text-white"></i> <span class="">Get Started</span></a>
                        <pwa-install class="install-button"  installbuttontext="Install App" descriptionheader="Investment Firm"></pwa-install>  
                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-6 col-md-5 mt-4 pt-2 mt-sm-0 pt-sm-0">
                <div class="position-relative">
                    <img src="{{asset('/landrick/main.png')}}" class="rounded img-fluid mx-auto d-block" alt="">
                    <div class="play-icon">
                        <a href="https://www.youtube.com/watch?v=8muvXtLIWpI" class="play-btn video-play-icon">
                            <i class="mdi mdi-play text-primary rounded-circle bg-white shadow"></i>
                        </a>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container--> 
</section><!--end section-->
<!-- Hero End -->