

                        <div class="faq-content mt-4 pt-3">
                            <div class="accordion" id="accordionExamplefour">
                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsefifthenn" class="faq position-relative" aria-expanded="true" aria-controls="collapsefifthenn">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfiftheen">
                                            <h6 class="title mb-0"> WHAT WITHDRAWAL METHODS DO YOU OFFER?</h6>
                                        </div>
                                    </a>
                                    <div id="collapsefifthenn" class="collapse show" aria-labelledby="headingfiftheen" data-parent="#accordionExamplefour">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">{{$settings->site_title}} Investments offers a variety of cryptocurrency withdrawal methods including Bitcoin, Ethereum and USD Tether, And also direct wire transfer to banks

                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsesixteen" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsesixteen">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingsixteen">
                                            <h6 class="title mb-0"> What is the main process open account ? </h6>
                                        </div>
                                    </a>
                                    <div id="collapsesixteen" class="collapse" aria-labelledby="headingsixteen" data-parent="#accordionExamplefour">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapseseventeen" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapseseventeen">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingseventeen">
                                            <h6 class="title mb-0">HOW LONG DOES IT TAKE TO RECEIVE THE WITHDRAWAL? </h6>
                                        </div>
                                    </a>
                                    <div id="collapseseventeen" class="collapse" aria-labelledby="headingseventeen" data-parent="#accordionExamplefour">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Withdrawal requests are processed instantly. Please note, at the company’s sole discretion, in some cases we can require additional information on the event of the withdrawal, in which case the request might be processed later. After the withdrawal request is processed, it depends on the withdrawal method how long it takes for the client to receive the funds.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapseeigheteen" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapseeigheteen">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingeighteen">
                                            <h6 class="title mb-0"> WHAT ARE THE MINIMUM AND THE MAXIMUM AMOUNTS I AM ALLOWED TO WITHDRAW?
                                            </h6>
                                        </div>
                                    </a>
                                    <div id="collapseeigheteen" class="collapse" aria-labelledby="headingeighteen" data-parent="#accordionExamplefour">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">You can withdraw any remaining balance in your account that is not currently being used to secure open investments, however, please note, if the requested amount is lower than the processing fee, we won’t be able to proceed with the request.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded">
                                    <a data-toggle="collapse" href="#collapsetwenty" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsetwenty">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingtwenty">
                                            <h6 class="title mb-0"> CAN I WITHDRAW WITHOUT VERIFYING MY ACCOUNT?</h6>
                                        </div>
                                    </a>
                                    <div id="collapsetwenty" class="collapse" aria-labelledby="headingtwenty" data-parent="#accordionExamplefour">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">No, only verified accounts are eligible for withdrawals.</p>
                                        </div>
                                    </div>
                                </div>
                              
                                
                            </div>
                        </div>