
                        <div class="faq-content mt-4 pt-2">
                            <div class="accordion" id="accordionExampleone">
                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapseone" class="faq position-relative" aria-expanded="true" aria-controls="collapseone">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfifone">
                                            <h6 class="title mb-0"> ABOUT <span class="text-primary">{{$settings->site_title}}</span>   </h6>
                                        </div>
                                    </a>
                                    <div id="collapseone" class="collapse show" aria-labelledby="headingfifone" data-parent="#accordionExampleone">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans"> @php
                                               echo $_SERVER['SERVER_NAME']
                                            @endphp ("Website") is a website owned and operated by {{$settings->site_title}} Inc ("We" or "Us"). We are an international financial company engaged in investment activities, which are related to wealth and asset management, commodity and economic trading, fund management, ETFs, trading on financial markets and cryptocurrency performed by qualified investments and financial professional.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsetwo" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsetwo">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingtwo">
                                            <h6 class="title mb-0"> IS {{strtoupper($settings->site_title)}} INVESTMENTS A REGULATED AND LICENSED COMPANY ? </h6>
                                        </div>
                                    </a>
                                    <div id="collapsetwo" class="collapse" aria-labelledby="headingtwo" data-parent="#accordionExampleone">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">{{$settings->site_title}} Investments Inc is a CANADIAN registered company and with a branch that is being regulated by the Office of the Superintendent of Financial Institutions and Department of Finance.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsethree" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsethree">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingthree">
                                            <h6 class="title mb-0">HOW DO I JOIN THE PLATFORM? </h6>
                                        </div>
                                    </a>
                                    <div id="collapsethree" class="collapse" aria-labelledby="headingthree" data-parent="#accordionExampleone">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">All you need do is go to the sign up page, provide your full name, a valid email address and enter a secure password on the from provided and your account will be set up for investment.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsefour" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsefour">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfour">
                                            <h6 class="title mb-0"> ARE MY PERSONAL DETAILS SECURE WITH YOU? </h6>
                                        </div>
                                    </a>
                                    <div id="collapsefour" class="collapse" aria-labelledby="headingfour" data-parent="#accordionExampleone">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">We take a number of precautionary measures to ensure that the personal details of our investors are held in absolute confidence and are securely stored so as not to be accessible by unauthorized persons.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded">
                                    <a data-toggle="collapse" href="#collapsefive2" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsefive2">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfive2">
                                            <h6 class="title mb-0"> WHEN CAN I START INVESTING WITH MY ACCOUNT?</h6>
                                        </div>
                                    </a>
                                    <div id="collapsefive2" class="collapse" aria-labelledby="headingfive2" data-parent="#accordionExampleone">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Investing commences on your account as soon as your account has been verified and funded.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card border-0 rounded">
                                    <a data-toggle="collapse" href="#collapsesix" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsesix">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfive2">
                                            <h6 class="title mb-0"> IS <span class="text-primary">{{strtoupper($settings->site_title)}}</span> A SECURED PLATFORM?</h6>
                                        </div>
                                    </a>
                                    <div id="collapsesix" class="collapse" aria-labelledby="headingfive2" data-parent="#accordionExampleone">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Investing commences on your account as soon as your account has been verified and funded.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>