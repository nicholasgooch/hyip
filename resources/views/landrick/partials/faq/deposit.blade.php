
                        <div class="faq-content mt-4 pt-3">
                            <div class="accordion" id="accordionExamplethree">
                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapseten" class="faq position-relative" aria-expanded="true" aria-controls="collapseten">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingten">
                                            <h6 class="title mb-0"> HOW LONG DOES IT TAKE FOR MY FUNDS TO BE CREDITED INTO MY ACCOUNT?</h6>
                                        </div>
                                    </a>
                                    <div id="collapseten" class="collapse show" aria-labelledby="headingten" data-parent="#accordionExamplethree">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Depending on the cryptocurrency used, fund are credited instantly when recieved, but maybe affected by network congestions. Please contact support@ {{$_SERVER['SERVER_NAME']}} , in case you have sent funds but funds are not showing in your investing account.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapseeleven" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapseeleven">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingeleven">
                                            <h6 class="title mb-0"> WHAT DEPOSIT METHODS DO YOU OFFER? </h6>
                                        </div>
                                    </a>
                                    <div id="collapseeleven" class="collapse" aria-labelledby="headingeleven" data-parent="#accordionExamplethree">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">{{$settings->site_title}} Investments offers a variety of cryptocurrency payment methods including Bitcoin, Ethereum and USD Tether.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsetwelve" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsetwelve">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingtwelve">
                                            <h6 class="title mb-0"> IS THERE A FIXED CURRENCY RATE? HOW DO YOU ACCEPT CRYPTOCURRENCY BEING USD BASED?</h6>
                                        </div>
                                    </a>
                                    <div id="collapsetwelve" class="collapse" aria-labelledby="headingtwelve" data-parent="#accordionExamplethree">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">{{$settings->site_title}} Investments uses USD in crypto equivalents because of volatility in cryptocurrency prices. USD equivalents of cryptocurrency is used for deposits and withdrawals in real time.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsethirteen" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsethirteen">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingthirteen">
                                            <h6 class="title mb-0"> DO YOU RETURN THE INITIAL INVESTED CAPITAL WHEN INVESTMENTS DURATION ENDS? </h6>
                                        </div>
                                    </a>
                                    <div id="collapsethirteen" class="collapse" aria-labelledby="headingthirteen" data-parent="#accordionExamplethree">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Yes, your initial invested capital will be returned to your account balance at the end of investment duration.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded">
                                    <a data-toggle="collapse" href="#collapsefourteen" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsefourteen">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfourteen">
                                            <h6 class="title mb-0"> DO YOU PAY INTEREST ON CALENDAR OR BUSINESS DAYS? </h6>
                                        </div>
                                    </a>
                                    <div id="collapsefourteen" class="collapse" aria-labelledby="headingfourteen" data-parent="#accordionExamplethree">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Profit accrues in accordance with the selected investment plan every calendar day, from Sunday till Saturday ( including weekends and public holidays).</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card border-0 rounded">
                                    <a data-toggle="collapse" href="#collapsefifteen" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsefifteen">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfifteen">
                                            <h6 class="title mb-0">WHAT ARE THE MINIMUM AND THE MAXIMUM AMOUNTS I AM ALLOWED TO DEPOSIT?</h6>
                                        </div>
                                    </a>
                                    <div id="collapsefifteen" class="collapse" aria-labelledby="headingfifteen" data-parent="#accordionExamplethree">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">The minimum amount to deposit is $200. There is no maximum amount. We accept unlimited range of deposit.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card border-0 rounded">
                                    <a data-toggle="collapse" href="#collapsesixteen" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsesixteen">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingsixteen">
                                            <h6 class="title mb-0">HOW OFTEN CAN I MAKE DEPOSITS INTO MY ACCOUNT? </h6>
                                        </div>
                                    </a>
                                    <div id="collapsesixteen" class="collapse" aria-labelledby="headingsixteen" data-parent="#accordionExamplethree">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">You can deposit at any time and as many times as you need, choosing different payment methods or the same ones.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card border-0 rounded">
                                    <a data-toggle="collapse" href="#collapseseventeen" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapseseventeen">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingseventeen">
                                            <h6 class="title mb-0"> CAN I REINVEST PROFITS FROM MY ACCOUNT BALANCE? </h6>
                                        </div>
                                    </a>
                                    <div id="collapseseventeen" class="collapse" aria-labelledby="headingseventeen" data-parent="#accordionExamplethree">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Yes, you can always invest with funds in your account balance at all times.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
