
                        <div class="faq-content mt-4 pt-3">
                            <div class="accordion" id="accordionExampletwo">
                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsefive" class="faq position-relative" aria-expanded="true" aria-controls="collapsefive">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfive">
                                            <h6 class="title mb-0"> WHAT IS MY EXPECTED RETURN ON INVESTMENT?</h6>
                                        </div>
                                    </a>
                                    <div id="collapsefive" class="collapse show" aria-labelledby="headingfive" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">We put in extra efforts to guarantee a steady profit return for all our clients depending on the investment option they invest into.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsesix3" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsesix3">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingsix3">
                                            <h6 class="title mb-0"> WHEN ARE INVESTMENTS PROFITS PAID TO ACCOUNT?</h6>
                                        </div>
                                    </a>
                                    <div id="collapsesix3" class="collapse" aria-labelledby="headingsix3" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">All investments profits are accurred till end of investments and are made available in account balance at end of investment duration.

                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapseseven" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapseseven">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingseven">
                                            <h6 class="title mb-0"> DO YOU SUPPORT INVESTING AS AN ENTITY, SUCH AS A COMPANY?</h6>
                                        </div>
                                    </a>
                                    <div id="collapseseven" class="collapse" aria-labelledby="headingseven" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Yes. We are able to facilitate trading with all entity structures, such as companies, trusts, and superannuation funds.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapseeight" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapseeight">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingeight">
                                            <h6 class="title mb-0"> CAN I HAVE MULTIPLE INVESTMENTS AT ONCE ON MY ACCOUNT?</h6>
                                        </div>
                                    </a>
                                    <div id="collapseeight" class="collapse" aria-labelledby="headingeight" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Yes. Every account is eligible to run as many investments as possible, there are no limits to number of active investments.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded">
                                    <a data-toggle="collapse" href="#collapsenine" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsenine">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingnine">
                                            <h6 class="title mb-0"> CAN I INVEST IN SAME INVESTMENT PLAN SIMULTANEOUSLY? </h6>
                                        </div>
                                    </a>
                                    <div id="collapsenine" class="collapse" aria-labelledby="headingnine" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Yes. There are no limits to number of active investments on a plans or multiple plan.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>