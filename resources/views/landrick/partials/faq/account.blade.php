
                        <div class="faq-content mt-4 pt-3">
                            <div class="accordion" id="accordionExampletwo">
                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsefive56" class="faq position-relative" aria-expanded="true" aria-controls="collapsefive56">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfive56">
                                            <h6 class="title mb-0"> CAN I HAVE MULTIPLE ACCOUNTS ON {{strtoupper($settings->site_title)}}?</h6>
                                        </div>
                                    </a>
                                    <div id="collapsefive56" class="collapse show" aria-labelledby="headingfive56" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Clients are allowed to have just one account on {{$_SERVER['SERVER_NAME']}}. Our technical control service checks every sign up and if several accounts are found that contain the same user data, all these accounts are blocked. The funds in these accounts are also getting blocked. Contact support for resolve if use case applies to you.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapseseven34" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapseseven34">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingseven34">
                                            <h6 class="title mb-0"> I HAVE FORGOTTEN MY ACCOUNT PASSWORD?
                                            </h6>
                                        </div>
                                    </a>
                                    <div id="collapseseven34" class="collapse" aria-labelledby="headingsix" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">First, you need to use <a href="{{route('password.request')}}">Forgot Password</a>  to request a password reset link (you can find this link at the sign in page). This will help send a reset link to your email to set a new password to access your account. Otherwise, you can contact our support team to get the instructions for resolve.

                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsetwenty35" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsetwenty35">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingtwenty35">
                                            <h6 class="title mb-0"> HOW CAN I CHANGE MY ACCOUNT PASSWORD?</h6>
                                        </div>
                                    </a>
                                    <div id="collapsetwenty35" class="collapse" aria-labelledby="headingtwenty35" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">You can change your account password by clicking the link <a href="{{route('password.request')}}">Password Setting</a>  on sidebar in your account dashboard.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapseeight45" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapseeight45">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingeight45">
                                            <h6 class="title mb-0">I RECEIVED AN EMAIL WITH A REQUEST TO SPECIFY MY EMAIL AND PASSWORD. WHAT TO DO?</h6>
                                        </div>
                                    </a>
                                    <div id="collapseeight45" class="collapse" aria-labelledby="headingeight45" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Do not respond to such an email! Under no circumstances you should give out your personal/account information to third parties or personnels claiming to be from {{$_SERVER['SERVER_NAME']}}. Please note that phising emails may present themselves as email from {{$_SERVER['SERVER_NAME']}}. We will never ask you for your password. Our company emails are "contact@ {{$_SERVER['SERVER_NAME']}}" "support@ {{$_SERVER['SERVER_NAME']}}".

                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded">
                                    <a data-toggle="collapse" href="#collapsenine5" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsenine5">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingnine5">
                                            <h6 class="title mb-0">IF I HAVE QUESTIONS THAT ARE NOT MENTIONED IN THIS FAQ, WHO SHOULD I CONTACT? </h6>
                                        </div>
                                    </a>
                                    <div id="collapsenine5" class="collapse" aria-labelledby="headingnine5" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Use support services like email, whatsapp, phone calls and text messages to contact us for any questions you may have. Do not duplicate your question on our support services, please use only one way for reaching us.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>