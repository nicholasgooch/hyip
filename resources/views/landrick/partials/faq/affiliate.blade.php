
                        <div class="faq-content mt-4 pt-3">
                            <div class="accordion" id="accordionExampletwo">
                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsefive3" class="faq position-relative" aria-expanded="true" aria-controls="collapsefive3">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfive3">
                                            <h6 class="title mb-0"> DOES {{strtoupper($settings->site_title)}} INVESTMENTS HAVE AN AFFILIATE PROGRAM?</h6>
                                        </div>
                                    </a>
                                    <div id="collapsefive3" class="collapse show" aria-labelledby="headingfive3" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">{{$settings->site_title}} Investments has a 5-level affiliate program. On the first level our members are paid a 0.01%, on the second level - 0.1%, on the third level - 0.5%, on the fourth level - 1% and on the fifth level - 3% affiliate commissions on referrals deposits.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapseseven3" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapseseven3">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingseven3">
                                            <h6 class="title mb-0"> CAN I RECEIVE AFFILIATE COMMISSIONS WITHOUT BEING AN ACTIVE INVESTOR?
                                            </h6>
                                        </div>
                                    </a>
                                    <div id="collapseseven3" class="collapse" aria-labelledby="headingsix" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Yes. Our affiliate program is an alternative way to earn on {{$settings->site_title}} Investments.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapsetwenty3" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsetwenty3">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingtwenty3">
                                            <h6 class="title mb-0"> DO YOU SUPPORT INVESTING AS AN ENTITY, SUCH AS A COMPANY?</h6>
                                        </div>
                                    </a>
                                    <div id="collapsetwenty3" class="collapse" aria-labelledby="headingtwenty3" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Yes. We are able to facilitate trading with all entity structures, such as companies, trusts, and superannuation funds.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapseeight4" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapseeight4">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingeight4">
                                            <h6 class="title mb-0">WHAT ARE THE WORKING HOURS OF THE {{strtoupper($settings->site_title)}} INVESTMENTS SUPPORT SERVICES?</h6>
                                        </div>
                                    </a>
                                    <div id="collapseeight4" class="collapse" aria-labelledby="headingeight4" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">If you use email to contact us, we work around the clock. If you decide to contact us via online chat - support will be available 24/7, with breaks of 1-2 hours a day.

                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card border-0 rounded">
                                    <a data-toggle="collapse" href="#collapsenine5" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsenine5">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingnine5">
                                            <h6 class="title mb-0">IF I HAVE QUESTIONS THAT ARE NOT MENTIONED IN THIS FAQ, WHO SHOULD I CONTACT? </h6>
                                        </div>
                                    </a>
                                    <div id="collapsenine5" class="collapse" aria-labelledby="headingnine5" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Use support services like email, whatsapp, phone calls and text messages to contact us for any questions you may have. Do not duplicate your question on our support services, please use only one way for reaching us.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>