<footer class="footer ">
    
  
   
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-12 mb-0 mb-md-4 pb-0 pb-md-2">
                <a href="#" class="logo-footer">
                    <img src="{{$settings->site_logo}}" height="50" alt="">
                </a>
                <p class="mt-4 text-muted">Start working with {{$settings->site_title}} to start investing and making real world profits.</p>
                <ul class="list-unstyled social-icon social mb-0 mt-4">
                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="facebook" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="instagram" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i data-feather="twitter" class="fea icon-sm fea-social"></i></a></li>
                    <li class="list-inline-item"><a href="https://www.linkedin.com/company/{{$settings->site_title}}-llc/about/ c " class="rounded"><i data-feather="linkedin" class="fea icon-sm fea-social"></i></a></li>
                </ul><!--end icon-->
            </div><!--end col-->
            
            <div class="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h4 class="text-dark footer-head">Company</h4>
                <ul class="list-unstyled footer-list mt-4">
                    <li><a href="{{route('landrick.about')}}" class="text-muted"><i class="mdi mdi-chevron-right mr-1"></i> About us</a></li>
                    <li><a href="{{route('landrick.contact')}}" class="text-muted"><i class="mdi mdi-chevron-right mr-1"></i> Contact us</a></li>
                    <li><a href="{{route('landrick.faq')}}" class="text-muted"><i class="mdi mdi-chevron-right mr-1"></i> FAQ</a></li>
                    <li><a href="{{route('landrick.privacy-policy')}}" class="text-muted"><i class="mdi mdi-chevron-right mr-1"></i> Privacy Policy</a></li>
                    <li><a href="{{route('landrick.terms.condition')}}" class="text-muted"><i class="mdi mdi-chevron-right mr-1"></i> T & C</a></li>
                    <li><a href="{{route('register')}}" class="text-muted"><i class="mdi mdi-chevron-right mr-1"></i> Register</a></li>
                    <li><a href="{{route('login')}}" class="text-muted"><i class="mdi mdi-chevron-right mr-1"></i> Login</a></li>
                    </ul>
            </div><!--end col-->
            
            <div class="col-lg-3 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h4 class="text-dark footer-head">Useful Links</h4>
                <ul class="list-unstyled footer-list mt-4">
                    <li><a href="{{route('landrick.how-it-works')}}" class="text-muted"><i class="mdi mdi-chevron-right mr-1"></i> How it Works</a></li>
                    <li><a href="{{route('landrick.buy-bitcoin')}}" class="text-muted"><i class="mdi mdi-chevron-right mr-1"></i> How To Buy Bitcoin</a></li>
                    <li><a href="{{route('landrick.cloud-mining')}}" class="text-muted"><i class="mdi mdi-chevron-right mr-1"></i> Cloud Minning</a></li>
                    <li><a href="{{route('landrick.private.equity')}}" class="text-muted"><i class="mdi mdi-chevron-right mr-1"></i> Private Equity</a></li>
                    <li><a href="{{route('landrick.investment')}}" class="text-muted"><i class="mdi mdi-chevron-right mr-1"></i> Investment Plans</a></li>
                    <li><a href="{{route('landrick.security')}}" class="text-muted"><i class="mdi mdi-chevron-right mr-1"></i> Security</a></li>
                </ul>
            </div><!--end col-->

            <div class="col-lg-3 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h4 class="text-dark footer-head">Newsletter</h4>
                <p class="mt-4 text-muted">Sign up and receive the latest tips via email.</p>
                <form method="POST" action="{{route('newsletter.store')}}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="foot-subscribe foot-white form-group position-relative">
                                <label class="text-muted">Write your email <span class="text-danger">*</span></label>
                                <i data-feather="mail" class="fea icon-sm icons"></i>
                                <input type="email" name="email" id="emailsubscribe" class="form-control bg-light border pl-5 rounded" placeholder="Your email : " required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <input type="submit" id="submitsubscribe" name="send" class="btn btn-primary btn-block text-dark" value="Subscribe">
                        </div>
                    </div>
                </form>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</footer><!--end footer-->
<footer class="footer footer-bar">
    <div class="container text-center">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <div class="text-sm-left">
                    <p class="mb-0">© <?php echo date("Y"); ?> {{$settings->site_title}}</p>       </div>
            </div><!--end col-->

            <div class="col-sm-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <ul class="list-unstyled text-sm-right mb-0">
                    <li class="list-inline-item"><a href="javascript:void(0)"><img src="{{asset('landrick/images/payments/american-ex.png')}}" class="avatar avatar-ex-sm" title="American Express" alt=""></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)"><img src="{{asset('landrick/images/payments/discover.png')}}" class="avatar avatar-ex-sm" title="Discover" alt=""></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)"><img src="{{asset('landrick/images/payments/master-card.png')}}" class="avatar avatar-ex-sm" title="Master Card" alt=""></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)"><img src="{{asset('landrick/images/payments/paypal.png')}}" class="avatar avatar-ex-sm" title="Paypal" alt=""></a></li>
                    <li class="list-inline-item"><a href="javascript:void(0)"><img src="{{asset('landrick/images/payments/visa.png')}}" class="avatar avatar-ex-sm" title="Visa" alt=""></a></li>
                </ul>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</footer><!--end footer-->
<!-- Footer End -->

<!-- Back to top -->
<a href="#" class="btn btn-icon btn-soft-primary back-to-top"><i data-feather="arrow-up" class="icons"></i></a>
<!-- Back to top -->

<!-- javascript -->
<script src="{{asset('landrick/js/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('landrick/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('landrick/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('landrick/js/scrollspy.min.js')}}"></script>
<!-- Magnific Popup -->
<script src="{{asset('landrick/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('landrick/js/magnific.init.js')}}"></script>
<!-- SLIDER -->
<script src="{{asset('landrick/js/owl.carousel.min.js')}} "></script>
<script src="{{asset('landrick/js/owl.init.js')}} "></script>
<!-- Counter -->
<script src="{{asset('landrick/js/counter.init.js ')}}"></script>
<!-- Icons -->
<script src="{{asset('landrick/js/aos.js')}}"></script>
<script src="{{asset('landrick/js/feather.min.js')}}"></script>
<script src="https://unicons.iconscout.com/release/v2.1.9/script/monochrome/bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script>
<!-- Main Js -->
<script src="{{asset('landrick/js/particles.js')}}"></script>

<script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.11"></script>
<script src="{{asset('landrick/js/app.js')}}"></script>


{{-- <script>
    
    var typed = new Typed('.element', {
    strings: ["Invest", "Earn", "HODL"],
    typeSpeed: 30
    });
</script> --}}


<script>
    particlesJS.load('particles', 'landrick/particles.json', function() {
    console.log('callback - particles.js config loaded');
  });

</script>
@if ((session()->has('jsAlert')))
    <script>
        swal("Success", "You have been added to our subscibers list", "success")
    </script>
    @endif
    <script>
       
    var slider = tns({
        container: '.tiny-single-item',
        controls: false,
        mouseDrag: true,
        loop: true,
        rewind: true,
        slideBy: "page",
        autoplay: true,
        autoplayButtonOutput: false,
        autoplayTimeout: 3000,
        navPosition: "bottom",
        controlsContainer: '#controls',
        prevButton: '.previous',
        nextButton: '.next',
        autoplayButton: '.auto',
                speed: 400,
        gutter: 12,
        responsive: {
            992: {
                items: 3
            },

            767: {
                items: 2
            },

            320: {
                items: 1
            },
        },
    });

      </script>
      <script>
          AOS.init()
      </script>
<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="0400c2ad-ceef-4a1b-b2bd-a49f1dbce1e1";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>

</body>
</html>