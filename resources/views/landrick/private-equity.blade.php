@extends('landrick.layouts.app')

@section('content')
<section class="bg-half d-table w-100"  style="background: url('/landrick/images/crypto/crypto.png') top; center top / auto scroll; z-index: 0;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h3 class="title text-white"> Private Equity </h3>
                    <ul class="list-unstyled mt-4">
                        {{-- <li class="list-inline-item h6 date text-muted"> <span class="text-dark">Last Revised :</span> 23th Sep, 2019</li> --}}
                    </ul>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
  <!-- Shape Start -->
  <div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#ffba00"></path>
        </svg>
    </div>
</div>
<!--Shape End-->
<section  class="section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-10">
                <div class="section-title">
                    <div class="text-center">
                        <h3 class="title mb-4 ">What Is Private Equity? </h3>
                     
                    </div>
                    <p class="text-muted mb-0 mt-4">
                        Private equity (PE) typically refers to investment funds, generally organized as limited partnerships, that buy and restructure companies that are not publicly traded.
                    </p>
                    <p class="text-muted mb-0 mt-4">
                        Private equity is a type of equity and one of the asset classes consisting of equity securities and debt in operating companies that are not publicly traded on a stock exchange.
                    </p>
                
                    <h3 class="my-4 ">Leveraged buyout</h3>
                    <p class="text-muted">
                        Leveraged buyout, LBO, or Buyout refers to a strategy of making equity investments as part of a transaction in which a company, business unit, or business assets is acquired from the current shareholders typically with the use of financial leverage. The companies involved in these transactions are typically mature and generate operating cash flows.

                        Private-equity firms view target companies as either Platform companies which have sufficient scale and a successful business model to act as a stand-alone entity, or as add-on / tuck-in / bolt-on acquisitions, which would include companies with insufficient scale or other deficits.

                        Leveraged buyouts involve a financial sponsor agreeing to an acquisition without itself committing all the capital required for the acquisition. To do this, the financial sponsor will raise acquisition debt which ultimately looks to the cash flows of the acquisition target to make interest and principal payments. Acquisition debt in an LBO is often non-recourse to the financial sponsor and has no claim on other investments managed by the financial sponsor. Therefore, an LBO transaction's financial structure is particularly attractive to a fund's limited partners, allowing them the benefits of leverage but greatly limiting the degree of recourse of that leverage. This kind of financing structure leverage benefits an LBO's financial sponsor in two ways: (1) the investor itself only needs to provide a fraction of the capital for the acquisition, and (2) the returns to the investor will be enhanced (as long as the return on assets exceeds the cost of the debt).

                        As a percentage of the purchase price for a leverage buyout target, the amount of debt used to finance a transaction varies according to the financial condition and history of the acquisition target, market conditions, the willingness of lenders to extend credit (both to the LBO's financial sponsors and the company to be acquired) as well as the interest costs and the ability of the company to cover those costs. Historically the debt portion of a LBO will range from 60%–90% of the purchase price. Between 2000–2005 debt averaged between 59.4% and 67.9% of total purchase price for LBOs in the United States.
                    </p>      
                    <p class="text-muted mb-0">
                        It can therefore be summarised that the benefits of remote hosted mining are tight control over the mining process, maintenance support and subsequent ownership of the hardware. The big drawbacks are risks associated with the procurement of expensive hardware and the very high cost of entry, both in terms of investment and technical experience.
                    </p>
                    <h4 class="my-4  ">Growth capital</h4>
                    <p class="text-muted">
                        Growth Capital refers to equity investments, most often minority investments, in relatively mature companies that are looking for capital to expand or restructure operations, enter new markets or finance a major acquisition without a change of control of the business.

                        Companies that seek growth capital will often do so in order to finance a transformational event in their life cycle. These companies are likely to be more mature than venture capital-funded companies, able to generate revenue and operating profits but unable to generate sufficient cash to fund major expansions, acquisitions or other investments. Because of this lack of scale, these companies generally can find few alternative conduits to secure capital for growth, so access to growth equity can be critical to pursue necessary facility expansion, sales and marketing initiatives, equipment purchases, and new product development.
                        
                        The primary owner of the company may not be willing to take the financial risk alone. By selling part of the company to private equity, the owner can take out some value and share the risk of growth with partners.[19] Capital can also be used to effect a restructuring of a company's balance sheet, particularly to reduce the amount of leverage (or debt) the company has on its balance sheet.
                        
                        A Private investment in public equity, or PIPEs, refer to a form of growth capital investment made into a publicly traded company. PIPE investments are typically made in the form of a convertible or preferred security that is unregistered for a certain period of time.
                        
                        The Registered Direct, or RD, is another common financing vehicle used for growth capital. A registered direct is similar to a PIPE but is instead sold as a registered security.
                    </p>
                    <h4 class="my-4 text-white">Venture capital</h4>
                    <p class="text-muted">
                        Venture capital or VC is a broad subcategory of private equity that refers to equity investments made, typically in less mature companies, for the launch of a seed or startup company, early-stage development, or expansion of a business. Venture investment is most often found in the application of new technology, new marketing concepts and new products that do not have a proven track record or stable revenue streams.

                        Venture capital is often sub-divided by the stage of development of the company ranging from early-stage capital used for the launch of startup companies to late stage and growth capital that is often used to fund expansion of existing business that are generating revenue but may not yet be profitable or generating cash flow to fund future growth.

                        Entrepreneurs often develop products and ideas that require substantial capital during the formative stages of their companies' life cycles. Many entrepreneurs do not have sufficient funds to finance projects themselves, and they must, therefore, seek outside financing. The venture capitalist's need to deliver high returns to compensate for the risk of these investments makes venture funding an expensive capital source for companies. Being able to secure financing is critical to any business, whether it is a startup seeking venture capital or a mid-sized firm that needs more cash to grow Venture capital is most suitable for businesses with large up-front capital requirements which cannot be financed by cheaper alternatives such as debt. Although venture capital is often most closely associated with fast-growing technology, healthcare and biotechnology fields, venture funding has been used for other more traditional businesses.

                        Investors generally commit to venture capital funds as part of a wider diversified private-equity portfolio, but also to pursue the larger returns the strategy has the potential to offer. However, venture capital funds have produced lower returns for investors over recent years compared to other private-equity fund types, particularly buyout.
                    </p>
                    <p class="text-muted mb-0">
                        {{$settings->site_title}} is happy to offer our private equity strategy range, to our Diamond and Platinum Packages
                    </p>
                    
                    
                    
                   
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
@endsection