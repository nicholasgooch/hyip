@extends('landrick.layouts.app')

@section('content')
@include('landrick.partials.hero-2')

<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden ">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#2f55d4"></path>
        </svg>
    </div>
</div>
<!--Shape End-->
<!-- Partners start -->
<section class="py-4 border-bottom">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-2 col-md-2 col-6 text-center py-4">
                <img src="{{asset('landrick/images/client/amazon.svg')}}" class="avatar avatar-ex-sm" alt="">
            </div><!--end col-->

            <div class="col-lg-2 col-md-2 col-6 text-center py-4">
                <img src="{{asset('landrick/images/client/google.svg')}}" class="avatar avatar-ex-sm" alt="">
            </div><!--end col-->

            <div class="col-lg-2 col-md-2 col-6 text-center py-4">
                <img src="{{asset('landrick/images/client/lenovo.svg')}}" class="avatar avatar-ex-sm" alt="">
            </div><!--end col-->

            <div class="col-lg-2 col-md-2 col-6 text-center py-4">
                <img src="{{asset('landrick/images/client/paypal.svg')}}" class="avatar avatar-ex-sm" alt="">
            </div><!--end col-->

            <div class="col-lg-2 col-md-2 col-6 text-center py-4">
                <img src="{{asset('landrick/images/client/shopify.svg')}}" class="avatar avatar-ex-sm" alt="">
            </div><!--end col-->

            <div class="col-lg-2 col-md-2 col-6 text-center py-4">
                <img src="{{asset('landrick/images/client/spotify.svg')}}" class="avatar avatar-ex-sm" alt="">
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
<!-- Partners End -->

<section class="section  " style="background: url('landrick/images/about-bg.png') center center; ">
    <div class="container ">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h2 class="title mb-4 " data-aos="fade-up" data-aos-duration="1000"
                        data-aos-anchor-placement="center-bottom">About Us</h2>
                    <p class=" para-desc mx-auto mb-0 " data-aos="fade-up" data-aos-duration="1000"
                        data-aos-anchor-placement="center-bottom">Start investing with <span
                            class="text-primary fw-bold ">{{$settings->site_title}}</span> that can provide everything
                        you need to generate passive income, drive investments and assets.</p>
                </div>
            </div><!--end col-->
        </div>
        <div class="row align-items-center mt-100 mt-60">
            <div class="col-lg-5 col-md-6">
                <img data-aos="flip-left" data-aos-duration="1000" data-aos-anchor-placement="center-bottom"
                    src="landrick/images/hosting/sora-5668859.jpeg" class="img-fluid" alt="">
            </div><!--end col-->

            <div class="col-lg-7 col-md-6  mt-sm-0   pt-sm-0">
                <div class="section-title " style="font-size: 1.5rem; line-height: 2.5rem;">
                    <p class=" ml-lg-3" data-aos="fade-up" data-aos-duration="1000"
                        data-aos-anchor-placement="center-bottom">
                        We are a wealth and asset management firm that makes accumulating wealth possible for everyone.
                        We have been working very efficiently in wealth and asset management for 7 years. Our investment
                        model has returned +200.98% on average over the past years.
                    </p>
                    <p class=" ml-lg-3" data-aos="fade-up" data-aos-duration="1400"
                        data-aos-anchor-placement="center-bottom">
                        {{$settings->site_title}} gives you the power to hit the ground running, no matter your
                        experience level.
                    </p>
                    {{-- <ul class="list-unstyled mb-0 ">
                        <li class="mb-0"><span class="text-primary h5 mr-2"><i
                                    class="uim uim-check-circle"></i></span>Digital Marketing Solutions for Tomorrow
                        </li>
                        <li class="mb-0"><span class="text-primary h5 mr-2"><i
                                    class="uim uim-check-circle"></i></span>Our Talented & Experienced Marketing Agency
                        </li>
                        <li class="mb-0"><span class="text-primary h5 mr-2"><i
                                    class="uim uim-check-circle"></i></span>Create your own skin to match your brand
                        </li>
                    </ul> --}}
                    <a href="{{route('register')}}" data-aos="fade-up" data-aos-duration="2000"
                        data-aos-anchor-placement="center-bottom" class="btn btn-primary mt-3">Learn More </i></a>
                </div>
            </div><!--end col-->
</section>
<section class="section bg-light">
    <div class="col-12 text-center">
        <div class="section-title">
            <p class=" para-desc mx-auto mb-0 " style="font-size: 1.5rem; line-height: 2.5rem;" data-aos="fade-up"
                data-aos-duration="1000" data-aos-anchor-placement="center-bottom">
                "We like to think of ourselves as superheroes here at {{$settings->site_title}}. We aren’t in the
                business of doing things just because they are routine, we are in the business of creating lifetime
                wealth for our investors. We have created a fair and inclusive investment model available via our
                platform by combining the skills of our financial analyst & experts, quants, investment experts from
                wall street and from around the world. "
            </p>

        </div>
    </div>
    </div><!--end row-->
    </div><!--end container-->
</section>

<section class="section bg-primary  " style="background: url('/landrick/images/cta-earn.png') center center;">

    <div class="container mt-100">
        <div class="row align-items-center">
            <div class="col-lg-5 col-md-5">
                <img src="{{asset('landrick/images/hosting/ebrand-1367269.jpeg')}}" class="img-fluid" alt=""
                    data-aos="flip-left" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">
            </div><!--end col-->

            <div class="col-lg-7 col-md-7 mt-4 pt-2 mt-sm-0 pt-sm-0">
                <div class="section-title ml-lg-3  ">
                    <h4 class="title mb-4 " data-aos="fade-up" data-aos-duration="1000"
                        data-aos-anchor-placement="center-bottom">Earn more from your Investments</h4>
                    <p class="" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">
                        With a {{$settings->site_title}} Interest Account, your Investment Portfolio can earn up to
                        15.6% APY. Interest accrues daily and is paid monthly.</p>
                    <a href="{{route('landrick.how-it-works')}} text-white-5" class="mt-3 h6" data-aos="fade-up"
                        data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Find Out More <i
                            class="mdi mdi-chevron-right"></i></a>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
    <!-- Feature End -->

</section>

<!-- Feature Start -->
<section class="section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center ">
                <div class="section-title">
                    <p class=" para-desc mx-auto mb-0 " style="font-size: 1.5rem; line-height: 2.5rem;"
                        data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">
                        Enjoy easy access to innovative investment options.
                        Invest any amount in Gold, Oil & Gas Sector, Real Estate, Agriculture & Infrastructure, Forex &
                        Cryptocurrency.
                    </p>

                </div>
            </div>
        </div>
    </div>
    <div class="container mt-100 mt-60">
        <div class="row align-items-center">
            <div class="col-md-6 order-1 order-md-2">
                <div class="ms-lg-5">
                    <img src="{{asset('landrick/images/home-3.jpeg')}}" class="img-fluid" alt="" data-aos="flip-right"
                        data-aos-duration="1000" data-aos-anchor-placement="center-bottom">
                </div>
            </div>
            <!--end col-->

            <div class="col-md-6 order-2 order-md-1 mt-4 pt-2 mt-sm-0 pt-sm-0">
                <div class="section-title">
                    <h4 class="title mb-4" data-aos="fade-up" data-aos-duration="1000"
                        data-aos-anchor-placement="center-bottom">
                        Who We Are
                    </h4>
                    <p style="font-size: 1.5rem; line-height: 2.5rem;" data-aos="fade-up" data-aos-duration="1400"
                        data-aos-anchor-placement="center-bottom"> {{$settings->site_title}} Investments built its
                        reputation on successfully working with a wide range of investors. From one-stop multi asset
                        solutions to funds that focus on specific areas of specific markets, we’ve worked with investors
                        to create investment solutions that help them achieve their goals.
                    </p>
                    <div class="mt-4">
                        <a href="{{route('landrick.about')}}" data-aos="fade-up" data-aos-duration="1600"
                            data-aos-anchor-placement="center-bottom" class="btn btn-pills btn-soft-primary">Learn
                            more</a>
                    </div>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
</section>
<section class="section pt-0">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-12">
                <div class="video-solution-cta position-relative" style="z-index: 1;">
                    <div class="position-relative py-5 rounded"
                        style="background: url('landrick/images/it-cta.jpeg') top;">
                        <div class="bg-overlay rounded bg-primary bg-gradient" style="opacity: 0.8;"></div>

                        <div class="row" id="counter">
                            <div class="col-md-3 col-6 mt-4 pt-2">
                                <div class="counter-box text-center">
                                    <img src="{{asset('landrick/images/illustrator/Asset190.svg')}}"
                                        class="avatar avatar-small" alt="">
                                    <h2 class="mb-0 mt-4 ">$<span class="counter-value"
                                            data-count="120.98">100.98</span>B</h2>
                                    <h6 class="counter-head ">Market Cap</h6>
                                </div><!--end counter box-->
                            </div>

                            <div class="col-md-3 col-6 mt-4 pt-2">
                                <div class="counter-box text-center">
                                    <img src="{{asset('landrick/images/illustrator/Asset187.svg')}}"
                                        class="avatar avatar-small" alt="">
                                    <h2 class="mb-0 mt-4  "><span class="counter-value" data-count="300">100</span>K
                                    </h2>
                                    <h6 class="counter-head  ">Daily Transactions</h6>
                                </div><!--end counter box-->
                            </div>

                            <div class="col-md-3 col-6 mt-4 pt-2">
                                <div class="counter-box text-center">
                                    <img src="{{asset('landrick/images/illustrator/Asset186.svg')}}"
                                        class="avatar avatar-small" alt="">
                                    <h2 class="mb-0 mt-4 "><span class="counter-value" data-count="100">2467</span></h2>
                                    <h6 class="counter-head ">Active Accounts</h6>
                                </div><!--end counter box-->
                            </div>

                            <div class="col-md-3 col-6 mt-4 pt-2">
                                <div class="counter-box text-center">
                                    <img src="{{asset('landrick/images/illustrator/Asset189.svg')}}"
                                        class="avatar avatar-small" alt="">
                                    <h2 class="mb-0 mt-4 "><span class="counter-value" data-count="20">10</span>+</h2>
                                    <h6 class="counter-head ">Partners</h6>
                                </div><!--end counter box-->
                            </div>
                        </div><!--end row-->
                    </div>

                </div>
            </div><!--end col-->
        </div><!--end row -->
        <div class="feature-posts-placeholder bg-light"></div>
    </div><!--end container-->
</section>

<section class="section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 col-md-5">
                <div class="position-relative">
                    <img src="{{asset('landrick/images/hosting/3194519.jpeg')}}"
                        class="rounded img-fluid mx-auto d-block" alt="" data-aos="flip-left" data-aos-duration="1000"
                        data-aos-anchor-placement="center-bottom">
                    <div class="play-icon">
                        <a href="http://vimeo.com/287684225" class="play-btn video-play-icon">
                            <i class="mdi mdi-play text-primary rounded-circle bg-white shadow"></i>
                        </a>
                    </div>
                </div>
            </div><!--end col-->


            <div class="col-lg-7 col-md-7 mt-4 pt-2 mt-sm-0 pt-sm-0">
                <div class="section-title ml-lg-4">
                    <h4 class="title mb-4 " data-aos="fade-up" data-aos-duration="1000"
                        data-aos-anchor-placement="center-bottom">Opening an account is simple and quick</h4>
                    {{-- <p class="">Start working with <span class="text-primary font-weight-bold">Landrick</span> that
                        can provide everything you need to generate awareness, drive traffic, connect.</p> --}}
                    <ul class="list-unstyled ">
                        <li class="mb-0" data-aos="fade-up" data-aos-duration="1300"
                            data-aos-anchor-placement="center-bottom"><span class="text-primary h5 mr-2"><i
                                    class="uim uim-check-circle"></i></span>Sign up for an account</li>
                        <li class="mb-0" data-aos="fade-up" data-aos-duration="1500"
                            data-aos-anchor-placement="center-bottom"><span class="text-primary h5 mr-2"><i
                                    class="uim uim-check-circle"></i></span>Fund your account with crypto</li>
                        <li class="mb-0" data-aos="fade-up" data-aos-duration="1700"
                            data-aos-anchor-placement="center-bottom"><span class="text-primary h5 mr-2"><i
                                    class="uim uim-check-circle"></i></span>Start earning interest.</li>
                    </ul>
                    <a href="/register" data-aos="fade-up" data-aos-duration="2000"
                        data-aos-anchor-placement="center-bottom" class="btn btn-primary mt-3">Join
                        {{$settings->site_title}} </i></a>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
<!-- About End -->
<section class="section bg-light" style="background: url('landrick/images/blockchain.gif') center center;">
    <div class="bg-overlay rounded bg-primary bg-gradient" style="opacity: 0.8;"></div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div class="section-title ml-lg-5">
                    <h4 class="title mb-4" data-aos="fade-up" data-aos-duration="1000"
                        data-aos-anchor-placement="center-bottom">BLOCKCHAIN POWERED PLATFORM </h4>
                    <p class="" style="font-size: 1rem; line-height: 1.5rem;" data-aos="fade-up"
                        data-aos-duration="1300" data-aos-anchor-placement="center-bottom">Our blockchain powered
                        investment platform that makes capital investing for wealth creation easy. Invest with as little
                        as $200, withdraw anytime no lock-up, invest from anywhere with other investors from over 150
                        countries and control your assets.</p>
                    <a href="/register" data-aos="fade-up" data-aos-duration="1600"
                        data-aos-anchor-placement="center-bottom" class="btn btn-primary mt-3">Get Started <i
                            class="mdi mdi-chevron-right"></i></a>
                </div>
            </div><!--end col-->
            <div class="col-lg-5 col-md-6">
                <img src="{{asset('landrick/images/hosting/g8b27cf6af_640.jpeg')}}" class="img-fluid rounded"
                    data-aos="flip-left" data-aos-duration="1000" data-aos-anchor-placement="center-bottom" alt="">
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->

</section>
@include('landrick.partials.how-it-works')
<section id="particles"></section>

<!-- Price Startinvest -->
{{-- <section class="section">
    <div class="bg-overlay rounded bg-primary bg-gradient" style="opacity: 0.5;"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4 ">Our Investment Plans</h4>
                    <p class=" para-desc mb-0 mx-auto ">Start working with <span
                            class="text-primary font-weight-bold">{{$settings->site_title}}</span> that can provide
                        everything you need to generate awareness, drive traffic, connect.</p>
                </div>
            </div><!--end col-->
        </div><!--end row-->
        <div class="row mb-3">
            @include('landrick.partials.investment-plans' , ['packages' => $packages])
        </div>

    </div><!--end container-->
    <!-- Price End -->
</section> --}}

<section class="section" id="courses">
    <div class="bg-overlay rounded bg-primary bg-gradient" style="opacity: 0.5;"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4 " data-aos="fade-up" data-aos-duration="1000"
                        data-aos-anchor-placement="center-bottom">Our Investment Plans</h4>
                    {{-- <p class=" para-desc mx-auto mb-0">{{$settings->site_title}}</span> that can provide everything
                        you need to generate awareness, drive traffic, connect</p> --}}
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row  justify-content-center">



            <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                    <div class="position-relative d-block overflow-hidden">
                        <img src="{{asset('landrick/images/gold-inv.jpeg')}}" class="img-fluid rounded-top mx-auto"
                            alt="">
                        <div class="overlay-work bg-dark"></div>
                        <a href="{{route('landrick.gold.inv')}}" class="text-white h6 preview">Explore Now <i
                                class="uil uil-angle-right-b align-middle"></i></a>
                    </div>

                    <div class="card-body">
                        <h5><a href="{{route('landrick.gold.inv')}}" class="title text-dark">Gold Investments</a></h5>
                        <div class="rating">
                            <p class="">Gold is the ultimate currency, Of all the precious metals,
                                gold is the most popular as an investment</p>
                        </div>
                        <div class="fees d-flex justify-content-between">
                            <a href="{{route('landrick.gold.inv')}}" class="btn btn-block btn-primary mt-4">Explore</a>
                        </div>
                    </div>
                </div>
            </div>
             <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                    <div class="position-relative d-block overflow-hidden">
                        <img src="{{asset('landrick/images/real-estate.jpeg')}}" class="img-fluid rounded-top mx-auto"
                            alt="">
                        <div class="overlay-work bg-dark"></div>
                        <a href="{{route('landrick.real.estate')}}" class="text-white h6 preview">Explore Now <i
                                class="uil uil-angle-right-b align-middle"></i></a>
                    </div>

                    <div class="card-body">
                        <h5><a href="{{route('landrick.real.estate')}}" class="title text-dark">Real Estate
                                Investments</a></h5>
                        <div class="rating">
                            <p class="">Exceptional properties. Exceptional clients. You have plenty of options when it
                                comes to investing in real estate.</p>
                        </div>
                        <div class="fees d-flex justify-content-between">
                            <a href="{{route('landrick.real.estate')}}"
                                class="btn btn-block btn-primary mt-4">Explore</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                    <div class="position-relative d-block overflow-hidden">
                        <img src="{{asset('landrick/images/oil-and-gas.jpeg')}}" class="img-fluid rounded-top mx-auto"
                            alt="">
                        <div class="overlay-work bg-dark"></div>
                        <a href="{{route('landrick.oil.gas')}}" class="text-white h6 preview">Explore Now <i
                                class="uil uil-angle-right-b align-middle"></i></a>
                    </div>

                    <div class="card-body">
                        <h5><a href="{{route('landrick.oil.gas')}}" class="title text-dark">Oil & Gas Investments</a>
                        </h5>
                        <div class="rating">
                            <p class="">The oil and gas industry is one of the largest sectors in the world in terms of
                                dollar value,</p>
                        </div>
                        <div class="fees d-flex justify-content-between">
                            <a href="{{route('landrick.oil.gas')}}" class="btn btn-block btn-primary mt-4">Explore</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                    <div class="position-relative d-block overflow-hidden">
                        <img src="{{asset('landrick/images/agri-infra.jpeg')}}" class="img-fluid rounded-top mx-auto"
                            alt="">
                        <div class="overlay-work bg-dark"></div>
                        <a href="{{route('landrick.agri.infra')}}" class="text-white h6 preview">Explore Now <i
                                class="uil uil-angle-right-b align-middle"></i></a>
                    </div>

                    <div class="card-body">
                        <h5><a href="{{route('landrick.agri.infra')}}" class="title text-dark">Agriculture &
                                Infrastructure Investments</a></h5>
                        <div class="rating">
                            <p class="">Cultivating and constructing ideas for growth. Investment in agriculture. </p>
                        </div>
                        <div class="fees d-flex justify-content-between">
                            <a href="{{route('landrick.agri.infra')}}"
                                class="btn btn-block btn-primary mt-4">Explore</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                    <div class="position-relative d-block overflow-hidden">
                        <img src="{{asset('landrick/images/forex.jpeg')}}" class="img-fluid rounded-top mx-auto" alt="">
                        <div class="overlay-work bg-dark"></div>
                        <a href="{{route('landrick.forex')}}" class="text-white h6 preview">Explore Now <i
                                class="uil uil-angle-right-b align-middle"></i></a>
                    </div>

                    <div class="card-body">
                        <h5><a href="{{route('landrick.forex')}}" class="title text-dark">Forex Investments</a></h5>
                        <div class="rating">
                            <p class="">A profound name in the market. Foreign exchange is the marketplace for trading
                                all the world's currencies </p>
                        </div>
                        <div class="fees d-flex justify-content-between">
                            <a href="{{route('landrick.forex')}}" class="btn btn-block btn-primary mt-4">Explore</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                    <div class="position-relative d-block overflow-hidden">
                        <img src="{{asset('landrick/images/crypto.jpeg')}}" class="img-fluid rounded-top mx-auto"
                            alt="">
                        <div class="overlay-work bg-dark"></div>
                        <a href="{{route('landrick.crypto')}}" class="text-white h6 preview">Explore Now <i
                                class="uil uil-angle-right-b align-middle"></i></a>
                    </div>

                    <div class="card-body">
                        <h5><a href="{{route('landrick.crypto')}}" class="title text-dark">Cryptocurrency
                                Investments</a></h5>
                        <div class="rating">
                            <p class="">The currency of the future. Blockchain is the most promising technology since
                                the internet. </p>
                        </div>
                        <div class="fees d-flex justify-content-between">
                            <a href="{{route('landrick.crypto')}}" class="btn btn-block btn-primary mt-4">Explore</a>
                        </div>
                    </div>
                </div>
            </div>

        </div><!--end row-->

    </div><!--end container-->

</section>
<section class="section ">
    <div class="container">
        <div class="rounded bg-primary bg-gradient p-lg-5 p-4"
            style="background: url('landrick/images/bg3.png') center center;">
            <div class="bg-overlay "></div>
            <div class="row align-items-end">
                <div class="col-md-8">
                    <div class="section-title text-md-start text-center">
                        <h4 class="title mb-3 text-white title-dark" data-aos="fade-up" data-aos-duration="1000"
                            data-aos-anchor-placement="center-bottom">Affiliate Program</h4>
                        <p class="text-white-5 mb-0" data-aos="fade-up" data-aos-duration="1300"
                            data-aos-anchor-placement="center-bottom">We are offering a certain level of affiliate
                            income through our affiliate program via commission awarding to investors for the
                            sales/referral of a company’s service or product in the affiliate structure. You can
                            increase your income by simply referring other to join the platform.</p>
                    </div>
                </div><!--end col-->

                <div class="col-md-4 mt-4 mt-sm-0">
                    <div class="text-md-end text-center">
                        <a href="javascript:void(0)" data-aos="fade-left" data-aos-duration="1500"
                            data-aos-anchor-placement="center-bottom" class="btn btn-primary">Learn More</a>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div>
    </div>
</section>

@include('landrick.partials.features')

<section class="section">
    <div class="row my-md-5 pt-md-3 my-4 pt-2 pb-lg-4 justify-content-center">
        <div class="col-12 text-center">
            <div class="section-title">
                <h4 class="title mb-4" data-aos="fade-up" data-aos-duration="1000"
                    data-aos-anchor-placement="center-bottom">Have Question ? Get in touch!</h4>
                <p class="text-muted para-desc mx-auto" data-aos="fade-up" data-aos-duration="1300"
                    data-aos-anchor-placement="center-bottom">Start working with <span
                        class="text-primary fw-bold">{{$settings->site_title}}</span> that can provide everything you
                    need.</p>
                <a href="{{route('landrick.contact')}}" data-aos="fade-up" data-aos-duration="1600"
                    data-aos-anchor-placement="center-bottom" class="btn btn-primary mt-4"><i class="uil uil-phone"></i>
                    Contact us</a>
            </div>
        </div><!--end col-->
    </div><!--end row-->
</section>
<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden ">
        <svg viewBox="0 0 120 28">
            <defs>
                <filter id="goo">
                    <feGaussianBlur in="SourceGraphic" stdDeviation="1" result="blur" />
                    <feColorMatrix in="blur" mode="matrix" values="
                  1 0 0 0 0  
                  0 1 0 0 0  
                  0 0 1 0 0  
                  0 0 0 13 -9" result="goo" />
                    <xfeBlend in="SourceGraphic" in2="goo" />
                </filter>
                <path id="wave"
                    d="M 0,10 C 30,10 30,15 60,15 90,15 90,10 120,10 150,10 150,15 180,15 210,15 210,10 240,10 v 28 h -240 z" />
            </defs>

            <use id="wave3" class="wave" xlink:href="#wave" x="0" y="-2"></use>
            <use id="wave2" class="wave" xlink:href="#wave" x="0" y="0"></use>


            <g class="gooeff" filter="url(#goo)">
                <circle class="drop drop1" cx="20" cy="2" r="8.8" />
                <circle class="drop drop2" cx="25" cy="2.5" r="7.5" />
                <circle class="drop drop3" cx="16" cy="2.8" r="9.2" />
                <circle class="drop drop4" cx="18" cy="2" r="8.8" />
                <circle class="drop drop5" cx="22" cy="2.5" r="7.5" />
                <circle class="drop drop6" cx="26" cy="2.8" r="9.2" />
                <circle class="drop drop1" cx="5" cy="4.4" r="8.8" />
                <circle class="drop drop2" cx="5" cy="4.1" r="7.5" />
                <circle class="drop drop3" cx="8" cy="3.8" r="9.2" />
                <circle class="drop drop4" cx="3" cy="4.4" r="8.8" />
                <circle class="drop drop5" cx="7" cy="4.1" r="7.5" />
                <circle class="drop drop6" cx="10" cy="4.3" r="9.2" />

                <circle class="drop drop1" cx="1.2" cy="5.4" r="8.8" />
                <circle class="drop drop2" cx="5.2" cy="5.1" r="7.5" />
                <circle class="drop drop3" cx="10.2" cy="5.3" r="9.2" />
                <circle class="drop drop4" cx="3.2" cy="5.4" r="8.8" />
                <circle class="drop drop5" cx="14.2" cy="5.1" r="7.5" />
                <circle class="drop drop6" cx="17.2" cy="4.8" r="9.2" />
                <use id="wave1" class="wave" xlink:href="#wave" x="0" y="1" />
            </g>
            <!-- g mask="url(#xxx)">
           <path   id="wave1"  class="wave" d="M 0,10 C 30,10 30,15 60,15 90,15 90,10 120,10 150,10 150,15 180,15 210,15 210,10 240,10 v 28 h -240 z" />
           </g>
         </g -->

        </svg>
    </div>
</div>
<style>
    .wave {
        animation: wave 3s linear;
        animation-iteration-count: infinite;
        fill: #2f55d4;
    }

    .drop {
        fill: var(--col-deepblue);
        xfill: #99000055;
        animation: drop 3.2s linear infinite normal;
        stroke: var(--col-deepblue);
        stroke-width: 0.5;
        transform: translateY(25px);
        transform-box: fill-box;
        transform-origin: 50% 100%;
    }

    .drop1 {}

    .drop2 {
        animation-delay: 3s;
        animation-duration: 3s;
    }

    .drop3 {
        animation-delay: -2s;
        animation-duration: 3.4s;
    }

    .drop4 {
        animation-delay: 1.7s;
    }

    .drop5 {
        animation-delay: 2.7s;
        animation-duration: 3.1s;
    }

    .drop6 {
        animation-delay: -2.1s;
        animation-duration: 3.2s;
    }

    .gooeff {
        filter: url(#goo);
    }

    #wave2 {
        animation-duration: 5s;
        animation-direction: reverse;
        opacity: .6
    }

    #wave3 {
        animation-duration: 7s;
        opacity: .3;
    }

    @keyframes drop {
        0% {
            transform: translateY(25px);
        }

        30% {
            transform: translateY(-10px) scale(.1);
        }

        30.001% {
            transform: translateY(25px) scale(1);
        }

        70% {
            transform: translateY(25px);
        }

        100% {
            transform: translateY(-10px) scale(.1);
        }
    }

    @keyframes wave {
        to {
            transform: translateX(-100%);
        }
    }
</style>

@endsection