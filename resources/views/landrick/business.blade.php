@extends('landrick.layouts.app')

@section('content')
<section class="bg-half-170 bg-primary d-table w-100" style="background: url('landrick/images/about-5.jpeg') center center;" id="home">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row mt-5 justify-content-center">
            <div class="col-lg-12">
                <div class="title-heading text-center">
                    <h1 class="heading title-dark text-white mb-3">A PROCESS BUILT
                        ON PARTNERSHIP</h1>
                    <p class="para-desc para-dark mx-auto text-muted">Our fund managers undertake several visits to evaluate,
                        first-hand, investment decisions.</p>
                    <div class="mt-4 pt-2">
                        <a href="{{route('register')}}" class="btn btn-primary">Get Started</a>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container--> 
</section>
<div class="position-relative">
    <div class="shape overflow-hidden text-light">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#2f55d4"></path>
        </svg>
    </div>
</div>
<section class="section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-6">
                <img src="{{('landrick/images/business-2.jpeg')}}" class="img-fluid shadow rounded" alt="">
            </div><!--end col-->

            <div class="col-lg-6 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div class="section-title ms-lg-5">
                    <h4 class="title mb-4">ACTIVE COMMITMENT</h4>
                    <p class="text-muted">We provide transparency on our investment decisions and will always be accountable for them. We are committed to trying harder and better to actively manage our clients’ investments over the long-term.
                    </p>
                    
                    
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div>
    <div class="container mt-100 mt-60">
        <div class="row align-items-center">
            
            <div class="col-lg-6 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div class="section-title ms-lg-5">
                    <h4 class="title mb-4">OUR PLATFORM</h4>
                    <p class="text-muted">Seamlessly invest in and manage holdings of our investment products using our blockchain-powered investor platform developed by our dedicated tech team made up of blockchain engineers, data scientists, developers and designers.</p>
                 
                </div>
            </div><!--end col-->
            <div class="col-lg-6 col-md-6">
                <img src="{{('landrick/images/business-1.jpeg')}}" class="img-fluid shadow rounded" alt="">
            </div><!--end col-->

        </div><!--end row-->
    </div>
</section>

<section class="section">
    
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-12 mt-4 pt-2">
                <div class="d-flex">
                    
                    <div class="flex-1">
                        <h5 class="mt-0">GOLD INVESTMENT</h5>
                        <p class="answer text-muted mb-0">Gold benefits from diverse sources of demand: as an investment, a reserve asset, jewellery, and a technology component. It is highly liquid, no one’s liability, carries no credit risk, and is scarce, historically preserving its value over time.</p>
                        <a href="{{route('landrick.gold.inv')}}" class="btn btn-primary mt-4">Explore</a>
                    </div>
                </div>
            </div><!--end col-->
            
            <div class="col-md-6 col-12 mt-4 pt-2">
                <div class="d-flex">
                    
                    <div class="flex-1">
                        <h5 class="mt-0">OIL & GAS INVESTMENT</h5>
                        <p class="answer text-muted mb-0">The oil and gas industry is one of the largest sectors in the world in terms of dollar value, generating an estimated $3.3 trillion in revenue annually. Oil is crucial to the global economic framework, Investors looking to enter the oil and gas industry can quickly be overwhelmed by the complex jargon and unique metrics used throughout the sector.</p>

                        <a href="{{route('landrick.real.estate')}}" class="btn btn-primary mt-4">Explore</a>
                    </div>
                </div>
            </div><!--end col-->
            
            <div class="col-md-6 col-12 mt-4 pt-2">
                <div class="d-flex">
                    
                    <div class="flex-1">
                        <h5 class="mt-0"> REAL ESTATE INVESTMENT</h5>
                        <p class="answer text-muted mb-0">Real estate is the land along with any permanent improvements attached to the land, whether natural or man-made—including water, trees, minerals, buildings, homes, fences, and bridges. Real estate is a form of real property. It differs from personal property, which are things not permanently attached to the land, such as vehicles, boats, jewelry, furniture, and farm equipment.</p>
                        <a href="{{route('landrick.oil.gas')}}" class="btn btn-primary mt-4">Explore</a>
                    </div>
                </div>
            </div><!--end col-->
            
            <div class="col-md-6 col-12 mt-4 pt-2">
                <div class="d-flex">
                    
                    <div class="flex-1">
                        <h5 class="mt-0"> AGRICULTURE & INFRASTRUCTURE INVESTMENT</h5>
                        <p class="answer text-muted mb-0">The role of infrastructure is crucial for agriculture development and for taking the production dynamics to the next level. It is only through the development of infrastructure, especially at the post harvest stage that the produce can be optimally utilized with opportunity for value addition and fair deal for the farmers. Development of such infrastructure shall also address the vagaries of nature, the regional disparities, development of human resource and realization of full potential of our limited land resource.</p>
                        <a href="{{route('landrick.agri.infra')}}" class="btn btn-primary mt-4">Explore</a>
                    </div>
                </div>
                
            </div><!--end col-->
            <div class="col-md-6 col-12 mt-4 pt-2">
                <div class="d-flex">
                    
                    <div class="flex-1">
                        <h5 class="mt-0">FOREX & CRYPTOCURRENCY INVESTMENT</h5>
                        <p class="answer text-muted mb-0"> The foreign exchange market is a global decentralized or over-the-counter market for the trading of currencies. This market determines foreign exchange rates for every currency. It includes all aspects of buying, selling and exchanging currencies at current or determined prices. Cryptocurrency trading involves speculating on price movements via a CFD trading account, buying and selling the underlying coins via an exchange.</p>
                        <a href="{{route('landrick.crypto')}}" class="btn btn-primary mt-4">Explore</a>
                    </div>
                </div>
                
            </div><!--end col-->
        </div><!--end row-->

       
    </div>
</section>
<section class="section bg-light">
    <div class="row my-md-5 pt-md-3 my-4 pt-2 pb-lg-4 justify-content-center">
        <div class="col-12 text-center">
            <div class="section-title">
                <h4 class="title mb-4">Have Question ? Get in touch!</h4>
                <p class="text-muted para-desc mx-auto">Start working with <span class="text-primary fw-bold">{{$settings->site_title}}</span> that can provide everything you need.</p>
                <a href="{{route('landrick.contact')}}" class="btn btn-primary mt-4"><i class="uil uil-phone"></i> Contact us</a>
            </div>
        </div><!--end col-->
    </div><!--end row-->
</section>
<section class="section">
   
    <div class="container mt-100 mt-60">
        <div class="rounded bg-primary bg-gradient p-lg-5 p-4">
            <div class="row align-items-end">
                <div class="col-md-12">
                    <div class="section-title text-md-start text-center">
                        <h4 class="title mb-3 text-white title-dark">ENTREPRENEURIAL SPIRIT</h4>
                        <p class="text-white-50 mb-0">A team of independent thinkers. The courage to translate it into strong convictions and implement them.</p>
                    </div>
                </div><!--end col-->
                
            </div><!--end row-->
        </div>
    </div>
</section>
@endsection