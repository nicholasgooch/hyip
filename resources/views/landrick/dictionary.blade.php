@extends('landrick.layouts.app')

@section('content')
<section class="bg-half">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-10">
                <div class="section-title">
                   
                    <div class="col-xs-12 col-sm-10 col-sm-push-1">
                        <h4 class="title--beta">BEAR</h4>
                        <p>
                        This refers to the people who are expecting the price of a crypto asset to fall.
                        </p>
                        <h4 class="title--beta">BEARISH</h4>
                        <p>
                        This refers to the sentiments of a trader who thinks a crypto asset will depreciate in price.
                        </p>
                        <h4 class="title--beta">BITCOIN CORE</h4>
                        <p>
                        This is the main software that allows users to interact with the Bitcoin blockchain network.
                        </p>
                        <h4 class="title--beta">BITCOIN HALVING</h4>
                        <p>
                        This is when the miner's bitcoin reward is cut in half.
                        </p>
                        <h4 class="title--beta">BLOCK HEIGHT</h4>
                        <p>
                        Block height is a measurement of the number of blocks that came before a particular block in a blockchain network.
                        </p>
                        <h4 class="title--beta">BLOCK REWARD</h4>
                        <p>
                        A block reward is a payment awarded to a blockchain network miner upon successfully validating a new block.
                        </p>
                        <h4 class="title--beta">BLOCK SIZE</h4>
                        <p>
                        In blockchain technology, block size refers to the amount of data about transactions a single block in the chain can carry.
                        </p>
                        <h4 class="title--beta">BLOCK TIME</h4>
                        <p>
                        Block time refers to the amount of time it takes for a new block to be added to a blockchain.
                        </p>
                        <h4 class="title--beta">BLOCK WALLET</h4>
                        <p>
                        A brain wallet refers to a crypto private key or seed phrase that has been memorized by its owner.
                        </p>
                        <h4 class="title--beta">BRUTE FORCE ATTACK</h4>
                        <p>
                        This refers to an attack in which the attacker tries to bypass an accounts security.
                        </p>
                        <h4 class="title--beta">BUBBLE</h4>
                        <p>
                        A bubble occurs when the prices of an asset get really inflated and then crashes.
                        </p>
                        <h4 class="title--beta">BUILDER</h4>
                        <p>
                        This refers to members of the blockchain and crypto community who are helping to improve adoption by contributing to the construction of blockchain infrastructure over time.
                        </p>
                        <h4 class="title--beta">BULL MARKET</h4>
                        <p>
                        A bull market is a market that is on the rise and where the economy is sound.
                        </p>
                        <h4 class="title--beta">BULL RUN</h4>
                        <p>
                        A bull run is a specific time period in a financial market cycle during which asset prices can experience a significant upward trend.
                        </p>
                        <h4 class="title--beta">BULL TRAP</h4>
                        <p>
                        A bull trap is a market signal that signifies an initial recovery in the price of a declining asset, followed by a further decline.
                        </p>
                        <h4 class="title--beta">BULLISH</h4>
                        <p>
                        A person who is bullish on bitcoin may also be referred to as a bitcoin "bull".
                        </p>
                        <h4 class="title--beta">BURN/BURNING</h4>
                        <p>
                        In this process, the miners and developers intentionally remove the coins from circulation.
                        </p>
                        <h4 class="title--beta">BUY THE DIP / BUYING THE DIP</h4>
                        <p>
                        In this sense, a dip is regarded as a Crypto flash sale that should be taken advantage of to acquire more of the said crypto = asset.
                        </p>
                        <h4 class="title--beta">CARDANO</h4>
                        <p>
                        Cardano is a public open-source and decentralized blockchain platform. it is designed to be a more efficient alternative to proof of work systems.
                        </p>
                        <h4 class="title--beta">CENTRAL BANK</h4>
                        <p>
                        A central bank controls the monetary policy and currency of a nation-state.
                        </p>
                        <h4 class="title--beta">CENTRAL BANK DIGITAL CURRENCY</h4>
                        <p>
                        A central bank digital currency (CBDC) is a digital version of a country's fiat currency.
                        </p>
                        <h4 class="title--beta">CENTRALIZATION</h4>
                        <p>
                        This refers to the level of privilege and distribution of nodes verifying and managing a blockchain network.
                        </p>
                        <h4 class="title--beta">CHAINLINK (LINK)</h4>
                        <p>
                        Chainlink is the "oracle network" that serves real-time data to smart contracts on the blockchain.
                        </p>
                        <h4 class="title--beta">CIPHERTEXT</h4>
                        <p>
                        Ciphertext refers to encrypted text that is unreadable without authorized access.
                        </p>
                        <h4 class="title--beta">CIRCULATING SUPPLY</h4>
                        <p>
                        This refers to the total number of tokens of any cryptocurrencies that are available on the market at a given time.
                        </p>
                        <h4 class="title--beta">COIN SWAP ( OR TOKEN SWAP )</h4>
                        <p>
                        This happens when a platform replaces an existing token with a significantly updated one.
                        </p>
                        <h4 class="title--beta">COLD STORAGE</h4>
                        <p>
                        This is the offline storage of cryptocurrencies.
                        </p>
                        <h4 class="title--beta">COLD WALLET</h4>
                        <p>
                        A cold wallet is a cryptocurrency wallet that is not connected to the internet.
                        </p>
                        <h4 class="title--beta">CONFIRMATION</h4>
                        <p>
                        This is the measurement of how many blocks have been finalized since a transaction was sent from one address to another.
                        </p>
                        <h4 class="title--beta">CONFIRMATION TIME</h4>
                        <p>
                        The is the time frame it takes for transactions sent from one address to another to be finalized.
                        </p>
                        <h4 class="title--beta">CONSENSUS MECHANISM</h4>
                        <p>
                        This is an algorithm that participants in a blockchain network use to reach an agreement on the state of the blockchain ledger.
                        </p>
                        <h4 class="title--beta">CORRECTION</h4>
                        <p>
                        This is an occurrence that signifies that the market or a specific asset has just had a large drop in price from its recent higher price.
                        </p>
                        <h4 class="title--beta">COST BASIS</h4>
                        <p>
                        This the pegged starting value of a cryptocurrency that you own.
                        </p>
                        <h4 class="title--beta">COUNTERPARTY</h4>
                        <p>
                        This protocol allows the issuance of tokens on the Bitcoin blockchain.
                        </p>
                        <h4 class="title--beta">CRYPTO RANSOMWARE</h4>
                        <p>
                        In this case, a user's access to their data is blocked by an attacker using malware.
                        </p>
                        <h4 class="title--beta">CRYPTO TRADING PAIRS</h4>
                        <p>
                        This means that you can view the value of one cryptocurrency asset against another.
                        </p>
                        <h4 class="title--beta">CRYPTO-BACKED LOAN</h4>
                        <p>
                        This lives on the blockchain and requires borrowers to provide cryptocurrency as collateral.
                        </p>
                        <h4 class="title--beta">CRYPTO-BACKED LOAN</h4>
                        <p>
                        This lives on the blockchain and requires borrowers to provide cryptocurrency as collateral.
                        </p>
                        <h4 class="title--beta">CRYPTO-BACKED STABLECOIN</h4>
                        <p>
                        They are pegged to the value of an underlying cryptocurrency asset.
                        </p>
                        <h4 class="title--beta">CRYPTO-COLLATERIZED LOAN</h4>
                        <p>
                        For this loan, the lender takes a cryptocurrency deposit as collateral to issue a loan in another cryptocurrency or fiat currency.
                        </p>
                        <h4 class="title--beta">CRYPTO JACKING</h4>
                        <p>
                        In this instance, the victim's computer, or other hardware, is made a crypto mine without their knowledge.
                        </p>
                        <h4 class="title--beta">CRYPTO ART</h4>
                        <p>
                        Is native to a blockchain and has its own particular aesthetic.
                        </p>
                        <h4 class="title--beta">CRYPTOCURRENCY</h4>
                        <p>
                        Cryptocurrency is a digital asset that circulates on the internet as a medium of exchange.
                        </p>
                        <h4 class="title--beta">CRYPTOCURRENCY EXCHANGE</h4>
                        <p>
                        This is an exchange where digital assets can be sold traded and bought, sold, for fiat currency or other cryptocurrencies.
                        </p>
                        <h4 class="title--beta">CRYPTOCURRENCY PAIR ( TRADING PAIR )</h4>
                        <p>
                        This is a function available on exchanges that allow users to view the value of one cryptocurrency asset against another.
                        </p>
                        <h4 class="title--beta">CRYPTOCURRENCY WALLET</h4>
                        <p>
                        Has a public address that people can use to send you digital assets, and a private key to confirm the transfer of digital assets to others.
                        </p>
                        <h4 class="title--beta">CRYPTOGRAPHIC PROOF</h4>
                        <p>
                        This is used to prove and verify` data without revealing any other details about the data itself.
                        </p>
                        <h4 class="title--beta">CRYPTOMINNING</h4>
                        <p>
                        This process allows network nodes on a Proof-of-Work consensus mechanism to verify transactions and add new blocks to the blockchain.
                        </p>
                        <h4 class="title--beta">CRYPTOSIS</h4>
                        <p>
                        This refers to the endless desire to consume every bit of available information on cryptocurrencies.
                        </p>
                        <h4 class="title--beta">CUSTODIAL WALLET</h4>
                        <p>
                        Here a third party holds a user's private keys and cryptocurrency funds.
                        </p>
                        <h4 class="title--beta">CYBER ATTACK</h4>
                        <p>
                        This is an intrusion on online data carried out by criminals against a computer network, or related software/hardware device.
                        </p>
                        <h4 class="title--beta">CYBERSECURITY</h4>
                        <p>
                        This is also known as computer security or information technology (IT) security.
                        </p>
                        <h4 class="title--beta">CYPHERPUNK</h4>
                        <p>
                        This includes individuals (cypherpunks) who advocate the widespread use of cryptography, blockchain, as a means for engendering social and political change.
                        </p>
                        <h4 class="title--beta">DAO ( DECENTRALIZED AUTONOMOUS ORGANIZATION )</h4>
                        <p>
                        This is a blockchain-based organization that is democratically managed by members through self-executing open-source code and formalized by smart contracts.
                        </p>
                        <h4 class="title--beta">DAPPS</h4>
                        <p>
                        This stands for Decentralized Applications which refers to applications designed by developers and deployed on a blockchain to carry out actions without intermediaries.
                        </p>
                        <h4 class="title--beta">DAY TRADING</h4>
                        <p>
                        This involves taking a position in the market and exiting that same day or within a 24-hour timeframe.
                        </p>
                        <h4 class="title--beta">DCA ( DOLLAR COST AVERAGING )</h4>
                        <p>
                        This is the short form of "Dollar Cost Averaging" which is an investing strategy where an investor invests a total sum of money in small increments over a period of time, in a crypto asset.
                        </p>
                        <h4 class="title--beta">DEAD COIN</h4>
                        <p>
                        These cryptocurrencies have been abandoned by non-functioning projects.
                        </p>
                        <h4 class="title--beta">DECENTRALIZATION</h4>
                        <p>
                        This simply refers to the distribution of power away from a central point.
                        </p>
                        <h4 class="title--beta">DECRYPTION</h4>
                        <p>
                        Often relates to methods of unencrypting data manually, through a unique identifier code, or using specialized cryptographic keys.
                        </p>
                        <h4 class="title--beta">DEFI</h4>
                        <p>
                        This is the short form of Decentralized Finance which refers to financial activities that are conducted without the involvement of an intermediary like a bank, the government or other financial institution.
                        </p>
                        <h4 class="title--beta">DELEGATED PROOF OF STAKE ( DPoS )</h4>
                        <p>
                        This encourages users to confirm network data and ensure system security by staking collateral.
                        </p>
                        <h4 class="title--beta">DELISTING</h4>
                        <p>
                        This is the removal of a crypto asset from an exchange.
                        </p>
                        <h4 class="title--beta">DESKTOP WALLET</h4>
                        <p>
                        This kind of software wallet is downloaded directly onto a computing device. They are almost always non-custodial in nature.
                        </p>
                        <h4 class="title--beta">DIGITAL ASSET</h4>
                        <p>
                        All cryptocurrencies are digital assets, while not all digital assets are cryptocurrencies.
                        </p>
                        <h4 class="title--beta">DIGITAL DOLLAR</h4>
                        <p>
                        This is the tokenized version of the United States Dollar.
                        </p>
                        <h4 class="title--beta">DIGITAL SIGNATURE</h4>
                        <p>
                        In cryptocurrency, this process involves using a private key to digitally sign a transaction.
                        </p>
                        <h4 class="title--beta">DISTRIBUTED LEDGER</h4>
                        <p>
                        This refers to a system of recording information that is distributed or spread across several devices.
                        </p>
                        <h4 class="title--beta">DISTRIBUTED LEDGER TECHNOLOGY ( DLT )</h4>
                        <p>
                        This is a shared database upon which transactions and associated details are recorded by different parties all at once.
                        </p>
                        <h4 class="title--beta">DO YOUR OWN RESEARCH</h4>
                        <p>
                        This term is used to encourage potential investors to study, analyze, and research thoroughly before investing.
                        </p>
                        <h4 class="title--beta">DOUBLE-SPEND PROBLEM</h4>
                        <p>
                        This refers to a critical risk with digital currencies, in which the same funds can be copied and spent more than once.
                        </p>
                        <h4 class="title--beta">DUE DILIGENCE</h4>
                        <p>
                        This refers to a thorough investigation, audit, or review performed to confirm the facts of a matter under consideration.
                        </p>
                        <h4 class="title--beta">DUMP</h4>
                        <p>
                        This refers to the crash in the price of a crypto asset which is triggered by massive sell-offs after an associated spread of fear, uncertainty and doubt.
                        </p>
                        <h4 class="title--beta">ENHANCED DUE DILIGENCE</h4>
                        <p>
                        This is (KYC) process provides a better background knowledge of potential business partnerships and highlights risks that cannot be detected via Due Diligence (DD) alone.
                        </p>
                        <h4 class="title--beta">ESCROW</h4>
                        <p>
                        This refers to a third party that holds the financial resources presented for a transaction on behalf of other parties when both parties involved in the transaction may not trust each other.
                        </p>
                        <h4 class="title--beta">ETHER ( ETH )</h4>
                        <p>
                        Ether (ETH) is the native cryptocurrency of the Ethereum blockchain and plays an integral role in the Ethereum ecosystem.
                        </p>
                        <h4 class="title--beta">ETHEREUM</h4>
                        <p>
                        Launched in 2015 as a decentralized, blockchain-based global supercomputer to serve as the foundation for an ecosystem of interoperable, decentralized applications (dApps) powered by token economies and automated smart contracts.
                        </p>
                        <h4 class="title--beta">ETHEREUM 2.0</h4>
                        <p>
                        This is a significant set of updates to the Ethereum blockchain intended to vastly improve its scalability and broader utility.
                        </p>
                        <h4 class="title--beta">EXCHANGE</h4>
                        <p>
                        A digital marketplace that facilities the buying and selling of cryptocurrencies.
                        </p>
                        <h4 class="title--beta">FAIR LAUNCH</h4>
                        <p>
                        This is the equitable and transparent initial distribution of coins in a blockchain project.
                        </p>
                        <h4 class="title--beta">FEAR OF MISSING OUT ( FOMO )</h4>
                        <p>
                        Is an acronym that refers to the feeling that follows missing out on a specific investment opportunity after the price of a cryptocurrency or another asset substantially rise.
                        </p>
                        <h4 class="title--beta">FIAT CURRENCIES</h4>
                        <p>
                        These are traditional currencies or paper money, minted by the Central Banks or Federal Reserves of Countries.
                        </p>
                        <h4 class="title--beta">FRACTAL</h4>
                        <p>
                        This is a pattern of price movement which has occurred earlier and is likely to occur again.
                        </p>
                        <h4 class="title--beta">FUD</h4>
                        <p>
                        This stands for Fear, Uncertainty and Doubt.
                        </p>
                        <h4 class="title--beta">GAS</h4>
                        <p>
                        This refers to a fee that is being paid to use a service on a blockchain network.
                        </p>
                        <h4 class="title--beta">GENESIS BLOCK</h4>
                        <p>
                        This refers to the first block of a cryptocurrency ever mined.
                        </p>
                        <h4 class="title--beta">HALVING</h4>
                        <p>
                        This refers to when the amount of new Bitcoin entering circulation gets halved.
                        </p>
                        <h4 class="title--beta">HASH</h4>
                        <p>
                        This is a unique string of numbers and letters that identify blocks or transactions done on a blockchain.
                        </p>
                        <h4 class="title--beta">HODL</h4>
                        <p>
                        This stands for “Hold on For Dear Life”
                        </p>
                        <h4 class="title--beta">HOT WALLET</h4>
                        <p>
                        This is a software-based cryptocurrency wallet that is connected to the internet.
                        </p>
                        <h4 class="title--beta">ICO</h4>
                        <p>
                        This stands for "Initial Coin Offering"
                        </p>
                        <h4 class="title--beta">KYC</h4>
                        <p>
                        This stands for “Know Your Customer”
                        </p>
                        <h4 class="title--beta">LEVERAGE</h4>
                        <p>
                        This is the extra amount of asset bought or sold which is over your capital.
                        </p>
                        <h4 class="title--beta">LIMIT ORDER</h4>
                        <p>
                        A limit order is an order to buy or sell a crypto asset at a specific price or desired price.
                        </p>
                        <h4 class="title--beta">LIQUIDATION</h4>
                        <p>
                        This happens when the trade or position goes the opposite direction.
                        </p>
                        <h4 class="title--beta">LIQUIDITY</h4>
                        <p>
                        This refers to the measure of how actively a crypto asset is being traded in the market.
                        </p>
                        <h4 class="title--beta">LONG POSITION</h4>
                        <p>
                        This is simply a buy position with leverage. You enter a long position when you expect the price of a crypto asset to rise.
                        </p>
                        <h4 class="title--beta">VOLATILITY</h4>
                        <p>
                        This is the degree of uncertainty about the future price of a crypto asset.
                        </p>
                        <h4 class="title--beta">WHITE PAPER</h4>
                        <p>
                        This refers to documents that are created by the developers of a certain digital asset. These documents offer comprehensive information on the digital asset as well as its underlying technology.
                        </p>
                        </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section>
@endsection