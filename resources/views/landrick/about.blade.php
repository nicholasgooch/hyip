@extends('landrick.layouts.app')

@section('content')
<!-- Hero Start -->
<section class="bg-half d-table w-100" style="background: url('{{asset('landrick/images/about-4.jpeg')}}');">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level title-heading">
                    <h1 class="text-white-5 "> About Us </h1>
                    <p class="text-white-50 para-desc mb-0 mx-auto">World's leading wealth and asset management firm We believe that investing is very much a people’s business. By bringing together a like-minded, talented team we can deliver the best possible results.</p>
                    
                </div>
            </div><!--end col-->
        </div><!--end row--> 
    </div> <!--end container-->
</section><!--end section-->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="#2f55d4" xmlns="http://www.w3.org/2000/svg')}}">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z"></path>
        </svg>
    </div>
</div>
<!-- Hero End -->

<section class="section">
    <div class="container">
        <div class="row align-items-center" id="counter">
            <div class="col-md-6">
                <img src="{{asset('landrick/images/our-story.jpeg')}}" class="img-fluid" alt="">
            </div><!--end col-->

            <div class="col-md-6 mt-4 pt-2 mt-sm-0 pt-sm-0">
                <div class="ml-lg-4">
                    <div class="d-flex mb-4">
                        <span class="text-primary h1 mb-0"><span class="counter-value display-1 font-weight-bold" data-count="15">0</span>+</span>
                        <span class="h6 align-self-end ml-2">Years <br> Experience</span>
                    </div>
                    <div class="section-title">
                        <span class="badge bg-soft-primary rounded">OVERVIEW</span>
                        <h4 class="title mb-4">Who we are ?</h4>
                        <p class="text-muted">{{$settings->site_title}} Investments was established in 2015 by a group of highly experienced individuals dedicated to providing investors with superior investment returns and excellent finacial service. {{$settings->site_title}} is an active wealth and asset management company registered in Canada. We invest in gold, major oil and gas corporations, agriculture and infrastructure, cryptocurrency, forex, stocks and real estate sectors respectively. We provide 100% money back with guaranteed profits from our various investments options. Investment will always get better with us.</p>
                        <a href="javascript:void(0)" class="btn btn-primary mt-3">CONTACT US</a>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
    <div class="container mt-100 mt-60">
        <div class="row align-items-center" id="counter">
           
            <div class="col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div class="section-title">
                    <span class="badge bg-soft-primary rounded">OUR GOAL</span>
                    <h5 class="title mb-3 mt-2">WE ARE DEDICTED TO PROVIDING THE BEST INVESTMENT PRODUCTS</h5>
                    <p class="text-muted">Our goal is to provide our investors with a reliable source of high income, while eliminating any possible risks and offering a high-quality service, allowing us to automate and simplify the relations between the investors and the trustees. </p>

                    <p class="text-muted mb-0">We work towards increasing your profit margin by profitable investment options. We look forward to you being part of our community.</p>

                    <a href="{{route('register')}}" class="btn btn-primary mt-3">JOIN US</a>
                </div>
            </div>
            <div class="col-md-6">
                <img src="{{asset('landrick/images/our-story-2.jpeg')}}" class="img-fluid" alt="">
            </div><!--end col-->

        </div><!--end row-->
    </div><!--end container-->
    <div class="container mt-100 mt-60">
        <div class="row align-items-center" id="counter">
            <div class="col-md-6">
                <img src="{{asset('landrick/images/our-story-3.jpeg')}}" class="img-fluid" alt="">
            </div><!--end col-->
            <div class="col-md-6 mt-4 pt-2 mt-sm-0 pt-sm-0">
                <div class="ml-lg-4">
                   
                    <div class="section-title">
                        <span class="badge bg-soft-primary rounded">OUR APPROACH</span>
                        <h4 class="title mb-4">WHY INVEST WITH {{strtoupper($settings->site_title)}}?</h4>
                        <p class="text-muted">It’s not just our track record, we give you access to the investment opportunities that the 1% have been taking advantage of for years. Drive and dedication has ensured {{$settings->site_title}}  have delivered true value to investors since 2015, and it’s how we’ll be a true partner in driving of your success.</p>
                        <a href="{{route('register')}}" class="btn btn-primary mt-3">JOIN US</a>
                    </div>
                </div>
            </div><!--end col-->
            

        </div><!--end row-->
    </div><!--end container-->

    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                <img src="{{asset('landrick/images/client/amazon.svg')}}" class="avatar avatar-ex-sm" alt="">
            </div><!--end col-->

            <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                <img src="{{asset('landrick/images/client/google.svg')}}" class="avatar avatar-ex-sm" alt="">
            </div><!--end col-->
            
            <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                <img src="{{asset('landrick/images/client/lenovo.svg')}}" class="avatar avatar-ex-sm" alt="">
            </div><!--end col-->
            
            <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                <img src="{{asset('landrick/images/client/paypal.svg')}}" class="avatar avatar-ex-sm" alt="">
            </div><!--end col-->
            
            <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                <img src="{{asset('landrick/images/client/shopify.svg')}}" class="avatar avatar-ex-sm" alt="">
            </div><!--end col-->
            
            <div class="col-lg-2 col-md-2 col-6 text-center pt-4">
                <img src="{{asset('landrick/images/client/spotify.svg')}}" class="avatar avatar-ex-sm" alt="">
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
    <div class="container mt-100 mt-60">
        <div class="row align-items-end mb-4 pb-4">
            <div class="col-md-8">
                <div class="section-title text-center text-md-left">
                    <h4 class="title mb-4">What we do ?</h4>
                    <p class="text-muted mb-0 para-desc">Start working with <span class="text-primary font-weight-bold">{{$settings->site_title}}</span>  Investment Service</p>
                </div>
            </div><!--end col-->

            <div class="col-md-4 mt-4 mt-sm-0">
                <div class="text-center text-md-right">
                    <a href="javascript:void(0)" class="text-primary h6">See More <i data-feather="arrow-right" class="fea icon-sm"></i></a>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row">
            <div class="col-md-4 mt-4 pt-2">
                <ul class="nav nav-pills nav-justified flex-column bg-white rounded shadow p-3 mb-0 sticky-bar" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link rounded active" id="webdeveloping" data-toggle="pill" href="#developing" role="tab" aria-controls="developing" aria-selected="false">
                            <div class="text-center pt-1 pb-1">
                                <h6 class="title font-weight-normal mb-0">QUANTITATIVE TRADING</h6>
                            </div>
                        </a><!--end nav link-->
                    </li><!--end nav item-->
                    
                    <li class="nav-item mt-2">
                        <a class="nav-link rounded" id="database" data-toggle="pill" href="#data-analise" role="tab" aria-controls="data-analise" aria-selected="false">
                            <div class="text-center pt-1 pb-1">
                                <h6 class="title font-weight-normal mb-0">Invest Cryptocurrecy</h6>
                            </div>
                        </a><!--end nav link-->
                    </li><!--end nav item-->
                    
                    <li class="nav-item mt-2">
                        <a class="nav-link rounded" id="server" data-toggle="pill" href="#security" role="tab" aria-controls="security" aria-selected="false">
                            <div class="text-center pt-1 pb-1">
                                <h6 class="title font-weight-normal mb-0">Earn Cryptocurrecy</h6>
                            </div>
                        </a><!--end nav link-->
                    </li><!--end nav item-->
                    
                    <li class="nav-item mt-2">
                        <a class="nav-link rounded" id="ira" data-toggle="pill" href="#data-analise" role="tab" aria-controls="data-analise" aria-selected="false">
                            <div class="text-center pt-1 pb-1">
                                <h6 class="title font-weight-normal mb-0">Crypto IRA</h6>
                            </div>
                        </a><!--end nav link-->
                    </li><!--end nav item-->
                </ul><!--end nav pills-->
            </div><!--end col-->

            <div class="col-md-8 col-12 mt-4 pt-2">
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade bg-white show active p-4 rounded shadow" id="developing" role="tabpanel" aria-labelledby="webdeveloping">
                        <img src="{{asset('landrick/images/work/7.jpeg')}}"class="img-fluid rounded shadow" alt="">
                        <div class="mt-4">
                            <p class="text-muted">We optimize investment strategies within the nascent world of cryptocurrency and decentralized finance, helping to both build out innovative investment products, and optimize the performance of current offerings. Our team of investment professionals has a wealth of expertise in both traditional and alternative investments. We hire the most talented data scientists, engineers, and financial analysts.</p>
                        <a href="{{route('register')}}" class="text-primary">Get Started <i data-feather="arrow-right" class="fea icon-sm"></i></a>
                        </div>
                    </div><!--end teb pane-->
                    
                    <div class="tab-pane fade bg-white p-4 rounded shadow" id="data-analise" role="tabpanel" aria-labelledby="database">
                        <img src="{{asset('landrick/images/work/8.jpg')}}"class="img-fluid rounded shadow" alt="">
                        <div class="mt-4">
                            <p class="text-muted">The {{$settings->site_title}} Investment Account (CIA) lets you put your crypto to work and earn monthly interest payments in the asset-type that you deposit with Coinpurt.</p>
                            <a href="javascript:void(0)" class="text-primary">See More <i data-feather="arrow-right" class="fea icon-sm"></i></a>
                        </div>
                    </div><!--end teb pane-->

                    <div class="tab-pane fade bg-white p-4 rounded shadow" id="security" role="tabpanel" aria-labelledby="server">
                        <img src="{{asset('landrick/images/work/9.png')}}"class="img-fluid rounded shadow" alt="">
                        <div class="mt-4">
                            <p class="text-muted">Risk-free. Earn high interest on your crypto deposit.Turn your cold assets into hot profit and earn up to 12% APR + compounding interest by depositing crypto in {{$settings->site_title}}  Accounts. Earn interest in Bitcoin (BTC), ETH , and all major stablecoins. Crypto interest earnings are deposited directly into your wallet every week.</p>
                         <a href="{{route('register')}}" class="text-primary">Get Started <i data-feather="arrow-right" class="fea icon-sm"></i></a>
                        </div>
                    </div><!--end teb pane-->
                    <div class="tab-pane fade bg-white p-4 rounded shadow" id="ira" role="tabpanel" aria-labelledby="server">
                        <img src="{{asset('landrick/images/work/9.png')}}"class="img-fluid rounded shadow" alt="">
                        <div class="mt-4">
                            <p class="text-muted">Risk-free. Earn high interest on your crypto deposit.Turn your cold assets into hot profit and earn up to 12% APR + compounding interest by depositing crypto in {{$settings->site_title}}  Accounts. Earn interest in Bitcoin (BTC), ETH , and all major stablecoins. Crypto interest earnings are deposited directly into your wallet every week.</p>
                         <a href="{{route('register')}}" class="text-primary">Get Started <i data-feather="arrow-right" class="fea icon-sm"></i></a>
                        </div>
                    </div><!--end teb pane-->
                    
                </div><!--end tab content-->
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
@include('landrick.partials.how-it-works')
<section class="section bg-cta" style="background: url('landrick/images/approach-get.jpeg') center center;" id="cta">
    <div class="bg-overlay"></div>
    <div class="container"> 
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title">
                    <h4 class="title title-dark text-white mb-4">OUR APPROACH</h4>
                    <p class="text-white-50 para-dark para-desc mx-auto">In our everyday business, we focus on these 6 things: investors focus, integrity, reliability innovation, trendsetting and market pioneering..</p>
                    <a href="#!" data-type="youtube" data-id="yba7hPeTSjk" class="play-btn  mt-4 lightbox">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-play fea icon-ex-md text-white title-dark"><polygon points="5 3 19 12 5 21 5 3"></polygon></svg>
                    </a>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
    
</section>
<section class="section">
    @include('landrick.partials.features')
    
</section>
{{-- <section class="section bg-light">
    <div class="container mt-100 mt-60">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="section-title text-center mb-4 pb-2">
                    <h4 class="title mb-4">Meet The Team</h4>
              
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row">
            <div class="col-lg-3 col-md-6 mt-4 pt-2">
                <div class="card team team-primary text-center border-0">
                    <div class="position-relative">
                        <img src="{{('landrick/images/client/01.jpg')}}" class="img-fluid avatar avatar-ex-large rounded-circle shadow" alt="">
                        <ul class="list-unstyled mb-0 team-icon">
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook icons"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram icons"><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line></svg></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter icons"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></svg></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-linkedin icons"><path d="M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z"></path><rect x="2" y="9" width="4" height="12"></rect><circle cx="4" cy="4" r="2"></circle></svg></a></li>
                        </ul><!--end icon-->
                    </div>
                    <div class="card-body py-3 px-0 content">
                        <h5 class="mb-0"><a href="javascript:void(0)" class="name text-dark">Ronny Jofra</a></h5>
                        <small class="designation text-muted">C.E.O</small>
                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-3 col-md-6 mt-4 pt-2">
                <div class="card team team-primary text-center border-0">
                    <div class="position-relative">
                        <img src="{{asset('landrick/images/client/04.jpg')}}" class="img-fluid avatar avatar-ex-large rounded-circle shadow" alt="">
                        <ul class="list-unstyled mb-0 team-icon">
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook icons"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram icons"><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line></svg></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter icons"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></svg></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-linkedin icons"><path d="M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z"></path><rect x="2" y="9" width="4" height="12"></rect><circle cx="4" cy="4" r="2"></circle></svg></a></li>
                        </ul><!--end icon-->
                    </div>
                    <div class="card-body py-3 px-0 content">
                        <h5 class="mb-0"><a href="javascript:void(0)" class="name text-dark">Micheal Carlo</a></h5>
                        <small class="designation text-muted">Director</small>
                    </div>
                </div>
            </div><!--end col-->
            
            <div class="col-lg-3 col-md-6 mt-4 pt-2">
                <div class="card team team-primary text-center border-0">
                    <div class="position-relative">
                        <img src="{{asset('landrick/images/client/02.jpg')}}" class="img-fluid avatar avatar-ex-large rounded-circle shadow" alt="">
                        <ul class="list-unstyled mb-0 team-icon">
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook icons"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram icons"><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line></svg></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter icons"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></svg></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-linkedin icons"><path d="M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z"></path><rect x="2" y="9" width="4" height="12"></rect><circle cx="4" cy="4" r="2"></circle></svg></a></li>
                        </ul><!--end icon-->
                    </div>
                    <div class="card-body py-3 px-0 content">
                        <h5 class="mb-0"><a href="javascript:void(0)" class="name text-dark">Aliana Rosy</a></h5>
                        <small class="designation text-muted">Manager</small>
                    </div>
                </div>
            </div><!--end col-->
            
            <div class="col-lg-3 col-md-6 mt-4 pt-2">
                <div class="card team team-primary text-center border-0">
                    <div class="position-relative">
                        <img src="{{asset('landrick/images/client/03.jpg')}}" class="img-fluid avatar avatar-ex-large rounded-circle shadow" alt="">
                        <ul class="list-unstyled mb-0 team-icon">
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook icons"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></svg></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram icons"><rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect><path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path><line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line></svg></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter icons"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></svg></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-linkedin icons"><path d="M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z"></path><rect x="2" y="9" width="4" height="12"></rect><circle cx="4" cy="4" r="2"></circle></svg></a></li>
                        </ul><!--end icon-->
                    </div>
                    <div class="card-body py-3 px-0 content">
                        <h5 class="mb-0"><a href="javascript:void(0)" class="name text-dark">Sofia Razaq</a></h5>
                        <small class="designation text-muted">Developer</small>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div>
</section> --}}

    
@endsection