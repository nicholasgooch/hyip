@extends('landrick.layouts.app')

@section('content')
<section class="bg-half  d-table w-100"  style="background: url('/landrick/images/about-5.jpeg') top; center top / auto scroll; z-index: 0;" >
    <div class="container ">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h3 class="title"> How It Works </h3>
                    <ul class="list-unstyled mt-4">
                        {{-- <li class="list-inline-item h6 date text-muted"> <span class="text-dark">Last Revised :</span> 23th Sep, 2019</li> --}}
                    </ul>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
  <!-- Shape Start -->
  <div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#2f55d4"></path>
        </svg>
    </div>
</div>
<!--Shape End-->
@include('landrick.partials.how-it-works')
<section class="section ">
    <div class="row my-md-5 pt-md-3 my-4 pt-2 pb-lg-4 justify-content-center">
        <div class="col-12 text-center">
            <div class="section-title">
                <h4 class="title mb-4">Have Question ? Get in touch!</h4>
                <p class="text-muted para-desc mx-auto">Start working with <span class="text-primary fw-bold">{{$settings->site_title}}</span> that can provide everything you need.</p>
                <a href="{{route('landrick.contact')}}" class="btn btn-primary mt-4"><i class="uil uil-phone"></i> Contact us</a>
            </div>
        </div><!--end col-->
    </div><!--end row-->
</section>
@include('landrick.partials.features')
<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden ">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#2f55d4"></path>
        </svg>
    </div>
</div>
<!--Shape End-->
@endsection