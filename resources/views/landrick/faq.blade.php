@extends('landrick.layouts.app')

@section('content')
        
        <!-- Hero Start -->
        <section class="bg-half  d-table w-100"  style="background: url('/landrick/images/about-4.jpeg') top; center top / auto scroll; z-index: 0;">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12 text-center">
                        <div class="page-next-level ">
                            <h4 class="title text-white-5"> Frequently Asked Questions </h4>

                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div> <!--end container-->
        </section><!--end section-->
        <!-- Hero End -->
  <!-- Shape Start -->
  <div class="position-relative">
    <div class="shape overflow-hidden ">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="##ffba00"></path>
        </svg>
    </div>
</div>
<!--Shape End-->
        <!-- Start Section -->
        <section class="section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-5 col-12 d-none d-md-block">
                        <div class="rounded shadow p-4 sticky-bar">
                            <ul class="list-unstyled mb-0">
                                <li><a href="#{{$settings->site_title}}" class="mouse-down h6 text-danger ">{{$settings->site_title}} Questions</a></li>
                                <li class="mt-4"><a href="#digital" class="mouse-down h6 text-danger ">Investment Questions</a></li>
                                <li class="mt-4"><a href="#account" class="mouse-down h6 text-danger ">Account Questions</a></li>
                                <li class="mt-4"><a href="#verification" class="mouse-down h6 text-danger ">Verification Questions</a></li>
                                <li class="mt-4"><a href="#deposit" class="mouse-down h6 text-danger ">Deposit Questions</a></li>
                                <li class="mt-4"><a href="#withdrawal" class="mouse-down text-danger h6 ">Withdrawal Questions</a></li>
                                <li class="mt-4"><a href="#affiliate" class="mouse-down h6 text-danger ">Affiliate & Other Questions</a></li>
                            </ul>
                        </div>
                    </div><!--end col-->

                    <div class="col-lg-8 col-md-7 col-12">
                        <div class="section-title" id="{{$settings->site_title}}">
                            <h4 class="">{{$settings->site_title}} Questions</h4>
                        </div>

                        @include('landrick.partials.faq.about')
                        <div class="section-title mt-5" id="account">
                            <h4 class="">Account Questions</h4>
                        </div>
                        @include('landrick.partials.faq.account')

                        <div class="section-title mt-5" id="verification">
                            <h4 class="">Verification Questions</h4>
                        </div>

                        <div class="faq-content mt-4 pt-3">
                            <div class="accordion" id="accordionExampletwo">
                                <div class="card border-0 rounded mb-2">
                                    <a data-toggle="collapse" href="#collapse30" class="faq position-relative" aria-expanded="true" aria-controls="collapse30">
                                        <div class="card-header border-0 bg-light p-3 pr-5" id="heading30">
                                            <h6 class="title mb-0"> HOW DO I BECOME A VERIFIED CLIENT?</h6>
                                        </div>
                                    </a>
                                    <div id="collapse30" class="collapse show" aria-labelledby="heading30" data-parent="#accordionExampletwo">
                                        <div class="card-body px-2 py-4">
                                            <p class="text-muted mb-0 faq-ans">Once you become a verified client, you will have access to all of {{$settings->site_title}} products and services. Verify your profile by uploading clear colour copies (mobile photo or a scan) of the following documents:</p>
                                            <p>
                                                <ul>
                                                    <li>Proof of identity – passport, national identity card or driving license (if your identification document also states your correct residential address, then an additional proof of address document may not be required.)</li>
                                                    <li>
                                                        Proof of address – bank/card statement or utility bill. Examples of documents which can be provided are: Water/gas/electric/internet/telephone bill. <br>
                                                        Residency certificate or tenancy contract.
                                                    </li>
                                                </ul>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @include('landrick.partials.faq.affiliate')
                        <div class="section-title mt-5" id="digital">
                            <h4 class="">Investment Questions</h4>
                        </div>

                        @include('landrick.partials.faq.investment')

                        <div class="section-title mt-5" id="deposit">
                            <h4 class="">Deposit Questions</h4>
                        </div>

                        @include('landrick.partials.faq.deposit')

                        <div class="section-title mt-5" id="withdrawal">
                            <h4 class="">Withdrawal Questions</h4>
                        </div>

                        @include('landrick.partials.faq.withdrawal')
                        <div class="section-title mt-5" id="affiliate">
                            <h4 class="">Afilliate Questions</h4>
                        </div>

                        @include('landrick.partials.faq.affiliate')

                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->

            <div class="container mt-100 mt-60">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="media align-items-center shadow rounded p-4 features">
                            <div class="icons m-0 rounded h2 text-primary text-center px-3">
                                <i class="uil uil-envelope-check"></i>
                            </div>
                            <div class="content ml-4">
                                <h5 class="mb-1"><a href="{{route('landrick.contact')}}" class="text-white-5">Get in Touch !</a></h5>
                                <p class="text-muted mb-0">Need More information? Contact us Now</p>
                                <div class="mt-2">
                                    <a href="{{route('landrick.contact')}}" class="btn btn-soft-primary">Submit a Request</a>
                                </div>
                            </div>
                        </div>
                    </div><!--end col-->
                    
                    
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- End Section -->

@endsection