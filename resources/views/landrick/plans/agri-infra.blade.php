@extends('landrick.layouts.app')

@section('content')
     
 <!-- Hero Start -->
 <section class="bg-half-170 d-table w-100" style="background: url('/landrick/images/company/agri-infra.jpeg');">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row mt-5 justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="pages-heading title-heading">
                    <h2 class="text-white title-dark" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom"> AGRICULTURE & INFRASTRUCTURE WITH A NEW SKILL </h2>
                    <p class="text-white-50 para-desc mb-0 mx-auto" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">Cultivating and constructing ideas for growth</p>
                </div>
            </div><!--end col-->
        </div><!--end row--> 
      
    </div> <!--end container-->
</section><!--end section-->
<div class="position-relative">
    <div class="shape overflow-hidden text-color-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#2f55d4"></path>
        </svg>
    </div>
</div>
<!-- Hero End -->

<section class="section">
   
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="section-title text-center mb-4 pb-2">
                    <i class="uil uil-chart-pie-alt text-primary h2"></i>
                    <h4 class="title mt-3 mb-4" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">AGRICULTURE & INFRASTRUCTURE INVESTMENT OPTION</h4>
                    <p class="text-muted mx-auto para-desc"><ul class="list-group list-group-flush">
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">Minimum Capital Margin: $200</li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Maximum Capital Margin: Unlimited</li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1600" data-aos-anchor-placement="center-bottom">Investment Capital Return: True</li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1800" data-aos-anchor-placement="center-bottom">
                            Return On Investments: 0.03% - Daily
                        </li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="2000" data-aos-anchor-placement="center-bottom">
                            Elapse Duration: 180 Days
                        </li>
                        </ul>
                        <a href="{{'register'}}" data-aos="fade-up" data-aos-duration="2200" data-aos-anchor-placement="center-bottom" class="btn btn-primary btn-lg mt-5">Join Now</a>
                    </p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row justify-content-center">
            <div class="col-lg-6 mt-2">
                <div class="px-md-4 pt-4">
                    <h4 class="mb-3" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">WHY AGRICULTURE & INFRASTRUCTURE?</h4>
                    <div class="position-relative mt-5" data-aos="flip-right" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">
                        <div class="bg-overlay bg-primary bg-gredient rounded-md" style="opacity: 0.6;"></div>
                        <img src="{{asset('/landrick/images/company/agri-infra2.jpeg')}}" class="rounded-md shadow img-fluid" alt="">
                    </div>

                    <p class="text-muted mb-0 mt-4 text-lg" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Investment in agriculture is relatively a low-risk portfolio diversification; offering profitable returns financially and also ensuring food security in the economy. Presently, crude agricultural practices has been discouraged with the advent of precision agriculture.</p>
                    <p class="text-muted mb-0 mt-4" data-aos="fade-up" data-aos-duration="1600" data-aos-anchor-placement="center-bottom">
                         Responsible investment
                        makes a significant contribution to enhancing sustainable livelihoods, in particular
                        for smallholders, and members of marginalized and vulnerable groups, creating
                        decent work for all agricultural and food workers, eradicating poverty, fostering social
                        and gender equality, eliminating the worst forms of child labour, promoting social
                        participation and inclusiveness, increasing economic growth, and therefore achieving
                        sustainable development. 
                    </p>

                    
                    <div class="px-md-4 pt-4 text-start">
                     

                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-6 mt-2">
                <div class="px-md-4 pt-4">
                    <h4 class="mb-3" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">AGRICULTURE IS MORE SUSTAINABLE</h4>
                    <div class="position-relative mt-5" data-aos="flip-left" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">
                        <div class="bg-overlay bg-primary bg-gredient rounded-md" style="opacity: 0.6;"></div>
                        <img src="/landrick/images/company/agri-infra3.jpeg" class="rounded-md shadow img-fluid" alt="">
                    </div>

                    <p class="text-muted mb-0 mt-4" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">When you invest in agriculture, you are investing in a physical plot of land. This land’s value only appreciates, unlike stocks in businesses. Investing in agriculture is typically a long term venture. Returns vary depending not only on commodity prices, but on how much and how fast the land appreciates.
                    </p>
                    <p class="text-muted mb-0 mt-4" data-aos="fade-up" data-aos-duration="1600" data-aos-anchor-placement="center-bottom"> Depending on the farm’s location, current commodity prices and other factors, returns on investment range from 3-5% for commodity based land, or 10-15% for specialty crops. Because agriculture investments are long term, they can be set up to be kept in the investor’s family and passed down to future generations. Agricultural investments can appreciate indefinitely.</p>

                   
                    <div class="px-md-4 pt-4 text-start">
                       
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section>
        <!-- CTA Start -->
        <section class="section bg-cta" data-jarallax='{"speed": 0.5}' style="background: url('/landrick/images/company/agri-infra4.jpeg') center center;" id="cta">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <h4 class="title title-dark text-white mb-4" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">OUR WORD</h4>
                            <p class="text-white-50  para-desc mx-auto" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">Investing in agriculture and farmers keeps people fed and clothed. We all need to eat. Trends show that the amount of food, especially protein, people take in per day is steadily increasing. At the same time, some experts fear that at the rate of the population’s consumption, soil could become a scarce commodity, making the demand for fertile agricultural land even larger.</p>
                            <a href="{{route('register')}}" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom" class="btn  btn-primary mt-4 lightbox">
                                Join Now
                            </a>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- CTA End -->
<section  class="section">
    <div class="container">
        <h4 class=" display-5 fw-bold mt-10"data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom"> BENEFITS OF AGRICULTURE & INFRASTRUCTURE INVESTMENT</h4>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">
                                        BUILT-IN SCARCITY</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">When you invest in gold, you’re investing in a finite resource. The amount of farmland in the United States is decreasing every year, and yet the country’s farms make up 10 percent of the world’s farmland. Coupled with a growing population and a demand for food that’s not decreasing anytime soon, the need for farmland is only going to increase over time—even if there’s less of it than ever.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">MULTIPLE REVENUE SOURCES</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Farmland is a unique investment insofar as it has multiple income sources. The value of the land itself is perhaps the largest source of potential income for investors, but it’s far from the only one. When you invest in farmland, you’re also opening up your portfolio to gains through other revenue streams. For example, you’re entitled to a share of the profit when goods go to market, and enjoy a stake in the farm upon which the land sits. When either of these two generate income or revenue, a portion of that goes to you as a partial owner.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">UNIQUE INVESTMENT OPTION</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Farmland is a unique investment insofar as it has multiple income sources. The value of the land itself is perhaps the largest source of potential income for investors, but it’s far from the only one. When you invest in farmland, you’re also opening up your portfolio to gains through other revenue streams. For example, you’re entitled to a share of the profit when goods go to market, and enjoy a stake in the farm upon which the land sits. When either of these two generate income or revenue, a portion of that goes to you as a partial owner.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">INFLATION HEDGING</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Finding good cover from inflation can be challenging enough for the average investor. Add market volatility and near-zero interest rates into the mix, and this task gets significantly harder. Savvy investors usually seek shelter by way of inflation-hedged investments. Essentially, these investments can provide a position that is either less affected or positively affected by inflation than your usual market pick.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">DIVERSIFICATION</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Historically, oil and gas companies have provided useful diversification in an investor’s portfolio. Because oil and gas are essential parts of the economy and especially transportation, rising gas prices often weaken other sectors of the economy. Exposure to oil can provide a hedge against falling prices in other companies’ shares.</p>
                                </div>
                            </div>
                        </div>
                    </div>
    </div>
   
</section>
@include('landrick.partials.features')
  

@endsection

@section('scripts')
<script src="{{asset('landrick/js/parallax.js ')}}"></script>   
@endsection