@extends('landrick.layouts.app')

@section('content')
     
 <!-- Hero Start -->
 <section class="bg-half-170 d-table w-100" style="background: url('/landrick/images/company/oil-gas.jpeg');">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row mt-5 justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="pages-heading title-heading">
                    <h2 class="text-white title-dark" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom"> OIL & GAS INVESTMENTS </h2>
                    <p class="text-white-50 para-desc mb-0 mx-auto" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">The oil and gas industry is one of the largest sectors in the world in terms of dollar value.</p>
                </div>
            </div><!--end col-->
        </div><!--end row--> 
      
    </div> <!--end container-->
</section><!--end section-->
<div class="position-relative">
    <div class="shape overflow-hidden text-color-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#2f55d4"></path>
        </svg>
    </div>
</div>
<!-- Hero End -->

<section class="section">
   
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="section-title text-center mb-4 pb-2">
                    <i class="uil uil-chart-pie-alt text-primary h2"></i>
                    <h4 class="title mt-3 mb-4" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">Oil & Gas Investments</h4>
                    <p class="text-muted mx-auto para-desc"><ul class="list-group list-group-flush">
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">Minimum Capital Margin: $200</li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Maximum Capital Margin: Unlimited</li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1600" data-aos-anchor-placement="center-bottom">Investment Capital Return: True</li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1800" data-aos-anchor-placement="center-bottom">
                            Return On Investments: 0.0028% - Daily
                        </li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="2000" data-aos-anchor-placement="center-bottom">
                            Elapse Duration: 180 Days
                        </li>
                        </ul>
                        <a href="{{'register'}}" data-aos="fade-up" data-aos-duration="2200" data-aos-anchor-placement="center-bottom" class="btn btn-primary btn-lg mb-0 mt-4">Join Now</a>
                    </p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row justify-content-center">
            <div class="col-lg-6 mt-2">
                <div class="px-md-4 pt-4 text-center">
                    <h4 class="mb-3" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">WHY OIL & GAS?</h4>
                    <div class="position-relative mt-5" data-aos="flip-right" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">
                        <div class="bg-overlay bg-primary bg-gredient rounded-md" style="opacity: 0.6;"></div>
                        <img src="{{asset('/landrick/images/company/oil-gas2.jpg')}}" class="rounded-md shadow img-fluid" alt="">
                    </div>

                    <p class="text-muted mb-0 mt-4 text-lg" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">The oil and gas industry is one of the largest sectors in the world in terms of dollar value, generating an estimated $3.3 trillion in revenue annually. Oil is crucial to the global economic framework, Investors looking to enter the oil and gas industry can quickly be overwhelmed by the complex jargon and unique metrics used throughout the sector.</p>

                    
                    <div class="px-md-4 pt-4 text-start">
                     

                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-6 mt-2">
                <div class="px-md-4 pt-4 text-center">
                    <h4 class="mb-3" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">OUR WORD</h4>
                    <div class="position-relative mt-5" data-aos="flip-left" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">
                        <div class="bg-overlay bg-primary bg-gredient rounded-md" style="opacity: 0.6;"></div>
                        <img src="{{asset('/landrick/images/company/oil-gas3.jpg')}}" class="rounded-md shadow img-fluid" alt="">
                    </div>

                    <p class="text-muted mb-0 mt-4" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Oil and gas are two of the most important commodities in the modern world. Investing in them has the potential to generate significant profits for you, but commodities can also be volatile and risky.</p>

                   
                    <div class="px-md-4 pt-4 text-start">
                       
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section>
        <!-- CTA Start -->
        <section class="section bg-cta" data-jarallax='{"speed": 0.5}' style="background: url('/landrick/images/company/oil-gas-4.jpg') center center;" id="cta">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <h4 class="title title-dark text-white mb-4" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">Invest With Us</h4>
                            <p class="text-white-50  para-desc mx-auto" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">If you're ready to diversify and protect your wealth in these uncertain times, we encourage you to join us today. Take advantage of the safe harbor that only gold can provide.</p>
                            <a href="{{route('register')}}" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom" class="btn  btn-primary mt-4 lightbox">
                                Join Now
                            </a>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- CTA End -->
<section  class="section">
    <div class="container">
        <h4 class=" display-5 fw-bold mt-10" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom"> BENEFITS OF OIL & GAS INVESTMENTS</h4>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">HIGH ROI</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Every business owner aims to earn a high return on investment when they put a lot of money at stake. However, when it comes to investing in oil and gas, this is the strongest reason for everyone to buy an interest in this industry. For your information, oil and gas have a huge potential for profit, which is why it is a compelling choice. However, it is slightly riskier than the other industries because of the varying prices of oil and gas.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">STRONG CASH FLOW</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">A profitable investment in any part of this industry is equivalent to owning your annuity that provides you with a consistent cash flow. Bear in mind, with most of the financial annuities, you will have to pay a premium amount for several years before you can receive a payment. Put this on the other side and compare it with a profitable oil or gas project. This means you will only have to wait for a few months for the first payment to arrive. Most people are skeptical about investing in the oil and gas industry because they doubt the time of receiving their first payment</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">STABLE AND MARKET-PROOF</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Unlike the stock market, oil and gas investments are relatively stable and generally free from the influence of destabilizing market forces. As such, a portfolio rich in oil and gas investments can provide insulation from outside market forces and inflation.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">LOADED WITH TAX INCENTIVES</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">As detailed here, the benefits to oil and gas investors when it comes to taxation are numerous and unique. Intangible Drilling Cost deductions provide up to 60-80 percent of well expenses off taxes in the first year. And 15 percent of the property’s gross is tax free. You’d be hard pressed to find a better incentivized investment opportunity.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">DIVERSIFICATION</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Historically, oil and gas companies have provided useful diversification in an investor’s portfolio. Because oil and gas are essential parts of the economy and especially transportation, rising gas prices often weaken other sectors of the economy. Exposure to oil can provide a hedge against falling prices in other companies’ shares.</p>
                                </div>
                            </div>
                        </div>
                    </div>
    </div>
    <div class="container">
        <h4 class=" display-5 fw-bold  mt-100 mb-3" > HOW WE INVEST IN OIL & GAS</h4>
        <div class="col-lg-12 col-md-12">
            <div class="accordion" id="accordionExampleone">
                <div class="card border-0 rounded mb-2">
                    <a data-toggle="collapse" href="#collapseone" class="faq position-relative" aria-expanded="true" aria-controls="collapseone">
                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfifone">
                            <h6 class="title mb-0"> OIL AND NATURAL GAS FUTURES</span></h6>
                        </div>
                    </a>
                    <div id="collapseone" class="collapse show" aria-labelledby="headingfifone" data-parent="#accordionExampleone">
                        <div class="card-body px-2 py-4">
                            <p class="text-muted mb-0 faq-ans">Short of buying barrels of oil and storing them in your garage, futures are one of the most direct ways to invest in oil and natural gas (or any commodity). Oil futures are popular because they are highly liquid and offer the potential for significant returns. Each futures contract involves 1,000 barrels of oil, so investors can get a large stake in oil at a low cost. However, using leverage also increases risk, so people considering investing in futures should make sure they know what they’re doing and can accept that risk.</p>
                        </div>
                    </div>
                </div>

                <div class="card border-0 rounded mb-2">
                    <a data-toggle="collapse" href="#collapsetwo" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsetwo">
                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingtwo">
                            <h6 class="title mb-0"> DIRECT PARTICIPATION IN OIL WELLS </h6>
                        </div>
                    </a>
                    <div id="collapsetwo" class="collapse" aria-labelledby="headingtwo" data-parent="#accordionExampleone">
                        <div class="card-body px-2 py-4">
                            <p class="text-muted mb-0 faq-ans">Another way to get direct exposure to oil is through a direct participation program (DPP). DPPs pool investor money, creating a limited partnership, and use those funds to finance oil-related projects like drilling and extracting oil from a well. In effect, you become a partial owner of an oil company. DPPs are typically passive investments but have long timelines, often five to 10 years or more. During the DPP’s life, investors receive the partnership’s cash flows. You can also receive tax advantages based on your ownership.</p>
                        </div>
                    </div>
                </div>

                <div class="card border-0 rounded mb-2">
                    <a data-toggle="collapse" href="#collapsethree" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsethree">
                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingthree">
                            <h6 class="title mb-0"> BUYING MINERAL RIGHTS </h6>
                        </div>
                    </a>
                    <div id="collapsethree" class="collapse" aria-labelledby="headingthree" data-parent="#accordionExampleone">
                        <div class="card-body px-2 py-4">
                            <p class="text-muted mb-0 faq-ans">Leasing the mineral rights of an oilfield to an oil or gas company can be a great way to produce cash flow. A common arrangement is for the landowner to receive royalties based on the amount of oil or gas extracted. If you have resource-rich land, you can earn a significant profit. Of course, there are risks. One is that mineral rights can be expensive, especially if the land is known to have a lot of valuable resources in it. You may overpay and not receive sufficient returns to cover your costs. You’ll also face risks based on changing commodity prices.</p>
                        </div>
                    </div>
                </div>

                <div class="card border-0 rounded mb-2">
                    <a data-toggle="collapse" href="#collapsefour" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsefour">
                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfour">
                            <h6 class="title mb-0"> BUYING SHARES OF OIL AND GAS COMPANIES </h6>
                        </div>
                    </a>
                    <div id="collapsefour" class="collapse" aria-labelledby="headingfour" data-parent="#accordionExampleone">
                        <div class="card-body px-2 py-4">
                            <p class="text-muted mb-0 faq-ans">A less direct method of investing in oil and natural gas is to invest in the companies involved in those industries. This is one of the simplest ways to get exposure to oil and gas in your portfolio. These companies operate in many different sectors of the oil and natural gas industry and you can get started for the cost of a single share, or less if your brokerage lets you buy partial shares. Stocks are a highly liquid investment and are often less volatile than commodity prices, which can reduce your risk somewhat. Many oil and gas companies also pay dividends, which let you earn a cash flow from your investment.</p>
                        </div>
                    </div>
                </div>

                <div class="card border-0 rounded">
                    <a data-toggle="collapse" href="#collapsefive2" class="faq position-relative collapsed" aria-expanded="false" aria-controls="collapsefive2">
                        <div class="card-header border-0 bg-light p-3 pr-5" id="headingfive2">
                            <h6 class="title mb-0">BUYING OIL AND GAS MUTUAL FUNDS OR ETFS </h6>
                        </div>
                    </a>
                    <div id="collapsefive2" class="collapse" aria-labelledby="headingfive2" data-parent="#accordionExampleone">
                        <div class="card-body px-2 py-4">
                            <p class="text-muted mb-0 faq-ans">One of the primary risks of investing in individual stocks is a lack of diversification. If you buy shares in a single company and it loses value, you don’t own other shares that can make up the difference. Investing in mutual funds or exchange-traded funds (ETFs) is a good way to build a diversified portfolio easily. You can buy shares in a single fund to get exposure to dozens or hundreds of different stocks.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--end col-->

    </div>
</section>

@endsection

@section('scripts')
<script src="{{asset('landrick/js/parallax.js ')}}"></script>   
@endsection