@extends('landrick.layouts.app')

@section('content')
     
 <!-- Hero Start -->
 <section class="bg-half-170 d-table w-100" style="background: url('/landrick/images/company/gold-inv.jpeg');">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row mt-5 justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="pages-heading title-heading">
                    <h2 class="text-white title-dark" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom"> GOLD IS THE
                        ULTIMATE CURRENCY </h2>
                    <p class="text-white-50 para-desc mb-0 mx-auto" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Gold is the ultimate currency, Of all the precious metals, gold is the most popular as an investment</p>
                </div>
            </div><!--end col-->
        </div><!--end row--> 
      
    </div> <!--end container-->
</section><!--end section-->
<div class="position-relative">
    <div class="shape overflow-hidden text-color-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#2f55d4"></path>
        </svg>
    </div>
</div>
<!-- Hero End -->

<section class="section">
   
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="section-title text-center mb-4 pb-2">
                    <i class="uil uil-chart-pie-alt text-primary h2"></i>
                    <h4 class="title mt-3 mb-4" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">GOLD INVESTMENT OPTION</h4>
                    <p class="text-muted mx-auto para-desc text-bold"><ul class="list-group list-group-flush">
                        <li class="list-group-item list-group-item-dark " data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">Minimum Capital Margin: $200</li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Maximum Capital Margin: Unlimited</li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1600" data-aos-anchor-placement="center-bottom">Investment Capital Return: True</li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1800" data-aos-anchor-placement="center-bottom">
                            Return On Investments: 0.03% - Daily
                        </li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="2000" data-aos-anchor-placement="center-bottom">
                            Elapse Duration: 30 Days
                        </li>
                        </ul>
                        <a href="{{'register'}}" class="btn btn-primary btn-lg mt-5" data-aos="fade-up" data-aos-duration="2200" data-aos-anchor-placement="center-bottom">Join Now</a>
                    </p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row justify-content-center">
            <div class="col-lg-6 mt-2">
                <div class="px-md-4 pt-4 ">
                    <h4 class="mb-3" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">WHY GOLD?</h4>
                    <div class="position-relative mt-5">
                        <div class="bg-overlay bg-primary bg-gredient rounded-md" style="opacity: 0.6;"></div>
                        <img src="{{asset('/landrick/images/company/gold-inv3.jpeg')}}"  class="rounded-md shadow img-fluid" alt="">
                    </div>

                    <p class="text-muted mb-0 mt-4 " data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Investors generally buy gold as a way of diversifying risk, especially through the use of futures contracts and derivatives. The gold market is subject to speculation and volatility as are other markets. Compared to other precious metals used for investment, gold has been the most effective safe haven across a number of countries..</p>
                    <p class="text-muted mb-0 mt-4" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">Gold is one of the best-known items of value in the world. It goes across cultural boundaries, and throughout history, gold has played numerous roles from currency, to essential materials in electronics, to jewellery with artisanal value. To many investors today, gold maintains its allure as a one-of-a-kind asset, which offers an alternative to conventional stock and bond markets.</p>
                    
                    <div class="px-md-4 pt-4 text-start">
                     

                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-6 mt-2">
                <div class="px-md-4 pt-4 ">
                    <h4 class="mb-3" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">GOLD IRAS</h4>
                    <div class="position-relative mt-5">
                        <div class="bg-overlay bg-primary bg-gredient rounded-md" style="opacity: 0.6;"></div>
                        <img src="/landrick/images/company/gold-inv2.jpeg" class="rounded-md shadow img-fluid" alt="">
                    </div>

                    <p class="text-muted mb-0 mt-4" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Gold-based IRAs are becoming increasingly popular these days. Backing your IRA with gold offers excellent protection for your retirement funds when market changes can implode your overall investment portfolio. The gold IRA is a retirement account that is approved by the government and backed by physical gold. Traditional IRAs are often loaded with equities from a time when the economy and government was more stable.</p>
                    <p class="text-muted mb-0 mt-4" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom"> The global economy is showing signs of weakness. Therefore, paper assets are too much of a risk for many investors. Especially given the events of the Great Recession, the uncertainties of investing in paper assets led to the reduction of the retirement accounts of several people. A Gold based IRA can offer substantial portfolio diversification that hedges against risks in the overall market. It can also protect your hard-earned money from the forces of inflation and unsound fiscal policy.</p>

                   
                    <div class="px-md-4 pt-4 text-start">
                       
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section>
        <!-- CTA Start -->
        <section class="section bg-cta" data-jarallax='{"speed": 0.5}' style="background: url('/landrick/images/company/oil-gas-4.jpg') center center;" id="cta">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <h4 class="title title-dark text-white mb-4" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">OUR WORD</h4>
                            <p class="text-white-50  para-desc mx-auto" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">If you're ready to diversify and protect your wealth in these uncertain times, we encourage you to join us today. Take advantage of the safe harbor that only gold can provide.</p>
                            <a href="#!" class="btn  btn-primary mt-4 lightbox">
                                Join Now
                            </a>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- CTA End -->
        @include('landrick.partials.features')
  
@endsection

@section('scripts')
<script src="{{asset('landrick/js/parallax.js ')}}"></script>   
@endsection