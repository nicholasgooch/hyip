@extends('landrick.layouts.app')

@section('content')

<!-- Hero Start -->
<section class="bg-half-170 d-table w-100" style="background: url('/landrick/images/company/real-estate.jpeg');">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row mt-5 justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="pages-heading title-heading">
                    <h2 class="text-white title-dark" data-aos="fade-up" data-aos-duration="1000"
                        data-aos-anchor-placement="center-bottom">HOW REAL ESTATE
                        GETS REAL </h2>
                    <p class="text-white-50 para-desc mb-0 mx-auto" data-aos="fade-up" data-aos-duration="1200"
                        data-aos-anchor-placement="center-bottom">Exceptional properties. Exceptional clients.</p>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->

    </div>
    <!--end container-->
</section>
<!--end section-->
<div class="position-relative">
    <div class="shape overflow-hidden text-color-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#2f55d4"></path>
        </svg>
    </div>
</div>
<!-- Hero End -->

<section class="section">

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="section-title text-center mb-4 pb-2">
                    <i class="uil uil-chart-pie-alt text-primary h2"></i>
                    <h4 class="title mt-3 mb-4" data-aos="fade-up" data-aos-duration="1000"
                        data-aos-anchor-placement="center-bottom">REAL ESTATE INVESTMENT OPTION</h4>
                    <p class="text-muted mx-auto para-desc">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1200"
                                data-aos-anchor-placement="center-bottom">Minimum Capital Margin: $200</li>
                            <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1400"
                                data-aos-anchor-placement="center-bottom">Maximum Capital Margin: Unlimited</li>
                            <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1600"
                                data-aos-anchor-placement="center-bottom">Investment Capital Return: True</li>
                            <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1800"
                                data-aos-anchor-placement="center-bottom">
                                Return On Investments: 0.0017% - Daily
                            </li>
                            <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="2000"
                                data-aos-anchor-placement="center-bottom">
                                Elapse Duration: 120 Days
                            </li>
                        </ul>
                        <a href="{{'register'}}" class="btn btn-primary btn-lg mb-0 mt-4" data-aos="fade-up"
                            data-aos-duration="2200" data-aos-anchor-placement="center-bottom">Join Now</a>
                    </p>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->

        <div class="row justify-content-center">
            <div class="col-lg-6 mt-2">
                <div class="px-md-4 pt-4 ">
                    <h4 class="mb-3" data-aos="fade-up" data-aos-duration="1000"
                        data-aos-anchor-placement="center-bottom">WHY REAL ESTATE?</h4>
                    <br>
                    <div class="position-relative mt-5">
                        <div class="bg-overlay bg-primary bg-gredient rounded-md" style="opacity: 0.6;"></div>
                        <img src="{{asset('/landrick/images/company/real-estate2.jpeg')}}"
                            class="rounded-md shadow img-fluid" alt="">
                    </div>

                    <p class="text-muted mb-0 mt-4 text-lg" data-aos="fade-up" data-aos-duration="1400"
                        data-aos-anchor-placement="center-bottom">You have plenty of options when it comes to investing
                        in real estate. You can purchase a single-family home, rent it out and collect monthly rent
                        checks while waiting for its value to rise high enough to generate a big profit when you sell.
                        Or you can purchase a small strip mall and collect monthly rents from hair salons, pizza
                        restaurants, mattress stores and other businesses. You can go bigger and invest in an apartment
                        building with dozens of units, collecting a steady stream of rent checks from your tenants each
                        month</p>


                    <div class="px-md-4 pt-4 text-start">


                    </div>
                </div>
            </div>
            <!--end col-->

            <div class="col-lg-6 mt-2">
                <div class="px-md-4 pt-4 ">
                    <h4 class="mb-3" data-aos="fade-up" data-aos-duration="1000"
                        data-aos-anchor-placement="center-bottom">REITS (REAL ESTATE INVESTMENT TRUSTS)</h4>
                    <div class="position-relative mt-5">
                        <div class="bg-overlay bg-primary bg-gredient rounded-md" style="opacity: 0.6;"></div>
                        <img src="{{asset('/landrick/images/company/real-estate3.jpeg')}}"
                            class="rounded-md shadow img-fluid" alt="">
                    </div>

                    <p class="text-muted mb-0 mt-4" data-aos="fade-up" data-aos-duration="1400"
                        data-aos-anchor-placement="center-bottom">Buying into REITs , short for real estate investment
                        trusts, is one of the easiest ways to invest in real estate. Why? With a REIT, you invest in
                        real estate without having to worry about maintaining or managing any physical buildings. REITs
                        are companies that own real estate, anything from retail properties to apartment buildings,
                        hotels, offices or warehouses. When you buy into a REIT, you purchase a share of these
                        properties. </p>
                    <p class="text-muted mb-0 mt-4" data-aos="fade-up" data-aos-duration="1600"
                        data-aos-anchor-placement="center-bottom">
                        It's a bit like investing in a mutual fund, only instead of stocks, a REIT deals with real
                        estate. You can earn money from a REIT in two ways: First, REITs make regular dividend payments
                        to investors. Secondly, if the value of the REIT increases, you can sell your investment for a
                        profit. You can invest in a REIT just as you would invest in a stock: REITs are listed on the
                        major stock exchanges.</p>


                    <div class="px-md-4 pt-4 text-start">

                    </div>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!-- CTA Start -->
<section class="section bg-cta" data-jarallax='{"speed": 0.5}'
    style="background: url('/landrick/images/company/real-estate4.jpeg') center center;" id="cta">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title">
                    <h4 class="title title-dark text-white mb-4" data-aos="fade-up" data-aos-duration="1000"
                        data-aos-anchor-placement="center-bottom">OUR WORD</h4>
                    <p class="text-white-50  para-desc mx-auto" data-aos="fade-up" data-aos-duration="1200"
                        data-aos-anchor-placement="center-bottom">Real estate is less volatile than stocks, whose value
                        can rise or fall more quickly. But real estate is less liquid than stocks: It’s easier to sell
                        your stocks and gain access to your money than it is your real estate investments.</p>
                    <a href="{{route('register')}}" data-aos="fade-up" data-aos-duration="1400"
                        data-aos-anchor-placement="center-bottom" class="btn  btn-primary mt-4 lightbox">
                        Join Now
                    </a>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->
<!-- CTA End -->
<section class="section">
    <div class="container">
        <h4 class=" display-5 fw-bold mt-10" data-aos="fade-up" data-aos-duration="1000"
            data-aos-anchor-placement="center-bottom"> BENEFITS OF REAL ESTATE INVESTMENTS</h4>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                <div class="d-flex features feature-primary pt-4 pb-4">

                    <div class="flex-1">
                        <h4 class="title" data-aos="fade-up" data-aos-duration="1200"
                            data-aos-anchor-placement="center-bottom">LONG-TERM SECURITY</h4>
                        <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400"
                            data-aos-anchor-placement="center-bottom">Real estate is a long-term investment, meaning you
                            can hold it for several years as you wait for it to appreciate. At the same time, if you
                            rent out your real estate you can earn monthly income while you wait for your property’s
                            value to rise.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                <div class="d-flex features feature-primary pt-4 pb-4">

                    <div class="flex-1">
                        <h4 class="title" data-aos="fade-up" data-aos-duration="1200"
                            data-aos-anchor-placement="center-bottom">PROTECTION AGAINST INFLATION</h4>
                        <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400"
                            data-aos-anchor-placement="center-bottom">Real estate investments are considered protection
                            against inflation. When the prices of goods and services are rising, home values and rents
                            typically increase, too. Investment properties, then, can provide you with rising monthly
                            income and appreciation to help protect you financially when the costs of everything else is
                            going up, too.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                <div class="d-flex features feature-primary pt-4 pb-4">

                    <div class="flex-1">
                        <h4 class="title" data-aos="fade-up" data-aos-duration="1200"
                            data-aos-anchor-placement="center-bottom">CHANCE TO BUILD CAPITAL</h4>
                        <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400"
                            data-aos-anchor-placement="center-bottom">The big goal of real estate investing is to
                            increase your cash, otherwise known as building capital. When you sell a property that has
                            risen in value, you’ll boost your capital. The key, of course, is to invest in the right
                            properties that will rise in value.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                <div class="d-flex features feature-primary pt-4 pb-4">

                    <div class="flex-1">
                        <h4 class="title" data-aos="fade-up" data-aos-duration="1200"
                            data-aos-anchor-placement="center-bottom">ABILITY TO LEVERAGE FUNDS</h4>
                        <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400"
                            data-aos-anchor-placement="center-bottom">When investing in real estate you probably can’t
                            afford to buy properties in full. After all, that single-family home you plan to rent might
                            cost $200,000 or more. That’s where leverage comes in. Leverage in real estate means you’re
                            using other people’s money to purchase properties. In this case, you’ll take out loans from
                            banks, mortgage lenders or credit unions and pay them back over time. This allows you to add
                            to your real estate holdings without spending the full amount of money you’d need to buy
                            them on your own.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                <div class="d-flex features feature-primary pt-4 pb-4">

                    <div class="flex-1">
                        <h4 class="title" data-aos="fade-up" data-aos-duration="1200"
                            data-aos-anchor-placement="center-bottom">DIVERSIFICATION</h4>
                        <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400"
                            data-aos-anchor-placement="center-bottom">Adding real estate to your investments boosts your
                            diversification, which can protect you in times of economic turmoil. Say certain stocks are
                            suffering because of an economic downturn. The investment properties in your portfolio might
                            still be increasing in value, protecting you from the losses your other investments are
                            taking.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

@endsection

@section('scripts')
<script src="{{asset('landrick/js/parallax.js ')}}"></script>
@endsection
