@extends('landrick.layouts.app')

@section('content')
     
 <!-- Hero Start -->
 <section class="bg-half-170 d-table w-100" style="background: url('/landrick/images/company/forex.jpeg');">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row mt-5 justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="pages-heading title-heading">
                    <h2 class="text-white title-dark" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom"> THE FUTURE OF TRADING IS HERE </h2>
                    <p class="text-white-50 para-desc mb-0 mx-auto" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">A profound name in the market</p>
                </div>
            </div><!--end col-->
        </div><!--end row--> 
      
    </div> <!--end container-->
</section><!--end section-->
<div class="position-relative">
    <div class="shape overflow-hidden text-color-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#2f55d4"></path>
        </svg>
    </div>
</div>
<!-- Hero End -->

<section class="section">
   
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="section-title text-center mb-4 pb-2">
                    <i class="uil uil-chart-pie-alt text-primary h2"></i>
                    <h4 class="title mt-3 mb-4" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">FOREX INVESTMENT OPTION</h4>
                    <p class="text-muted mx-auto para-desc"><ul class="list-group list-group-flush">
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">Minimum Capital Margin: $200</li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Maximum Capital Margin: Unlimited</li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1600" data-aos-anchor-placement="center-bottom">Investment Capital Return: True</li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1800" data-aos-anchor-placement="center-bottom">
                            Return On Investments: 0.03% - Daily
                        </li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="2000" data-aos-anchor-placement="center-bottom">
                            Elapse Duration: 30 Days
                        </li>
                        </ul>
                        <a href="{{'register'}}" class="btn btn-primary btn-lg mb-0 mt-4" data-aos="fade-up" data-aos-duration="2200" data-aos-anchor-placement="center-bottom">Join Now</a>
                    </p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row justify-content-center">
            <div class="col-lg-6 mt-2">
                <div class="px-md-4 pt-4">
                    <h4 class="mb-3" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">WHY FOREX?</h4>
                    <div class="position-relative mt-5" data-aos="flip-right" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">
                        <div class="bg-overlay bg-primary bg-gredient rounded-md" style="opacity: 0.6;"></div>
                        <img src="{{asset('/landrick/images/company/forex2.jpeg')}}" class="rounded-md shadow img-fluid" alt="">
                    </div>

                    <p class="text-muted mb-0 mt-4 text-lg" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Foreign exchange (forex, or FX for short) is the marketplace for trading all the world's currencies and is the largest financial market in the world. There are many benefits of trading forex, which include convenient market hours, high liquidity and the ability to trade on margin.</p>

                    
                    <div class="px-md-4 pt-4 text-start">
                     

                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-6 mt-2">
                <div class="px-md-4 pt-4 text-center">
                    <h4 class="mb-3" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">FOREX TRADING</h4>
                    <div class="position-relative mt-5" data-aos="flip-left" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">
                        <div class="bg-overlay bg-primary bg-gredient rounded-md" style="opacity: 0.6;"></div>
                        <img src="{{asset('/landrick/images/company/forex3.jpeg')}}" class="rounded-md shadow img-fluid" alt="">
                    </div>

                    <p class="text-muted mb-0 mt-4" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Forex trading was very difficult for individual investors prior to the Internet. Most currency traders were large multinational corporations, hedge funds, or high-net-worth individuals (HNWIs) because forex trading required a lot of capital. With help from the Internet, a retail market aimed at individual traders has emerged, providing easy access to the foreign exchange markets through either the banks themselves or brokers making a secondary market.
                    </p>
                    <p class="text-muted mb-0 mt-4" data-aos="fade-up" data-aos-duration="1600" data-aos-anchor-placement="center-bottom">
                    Most online brokers or dealers offer very high leverage to individual traders who can control a large trade with a small account balance.

                    </p>

                   
                    <div class="px-md-4 pt-4 text-start">
                       
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section>
        <!-- CTA Start -->
        <section class="section bg-cta" data-jarallax='{"speed": 0.5}' style="background: url('/landrick/images/company/forex4.jpeg') center center;" id="cta">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <h4 class="title title-dark text-white mb-4" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">OUR WORD</h4>
                            <p class="text-white-50  para-desc mx-auto" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">The foreign exchange market has emerged as a lucrative opportunity for people with a financial background. With low entry requirements and markets open 24/7, anyone with a laptop or smartphone can potentially score large profits in the forex markets. However, those opportunities also come with high leverage and high risk. Anyone seeking their fortune in forex will need strict discipline and skill in order to succeed.</p>
                            <a href="{{route('register')}}" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom" class="btn  btn-primary mt-4 lightbox">
                                Join Now
                            </a>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- CTA End -->
<section  class="section">
    <div class="container">
        <h4 class=" display-5 fw-bold mt-10" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom"> BENEFITS OF FOREX INVESTMENTS</h4>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">TRANSACTION COSTS</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">The cost of a transaction is typically built into the price in the forex market in the form of the spread. Forex brokers pocket the spread as their payment for facilitating the trade. Spreads are measured in pips. For most currencies, a pip is the fourth place after the decimal point, or 1/100 of a percent. (For trades involving the Japanese yen, a pip is the second place after the decimal point, or 1 percent.)</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">LEVERAGE</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Forex brokers often allow traders to buy and sell in the market using significant amounts of leverage, which gives them the ability to trade with higher amounts of money than what is actually in their accounts. If you were to trade at 50:1 leverage, for instance, you could trade $50 for every $1 that was in your account. That means you could control a trade of $50,000 using only $1,000 of capital.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">24-HOUR MARKET FOR FIVE DAYS</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">The forex market is worldwide, so trading is pretty much continuous as long as there's a market open somewhere in the world. Trading hours start in the U.S. when the first major market opens, in Sydney, Australia, at 5 p.m. Eastern time on Sunday. Trading ends for the week when the last major market, in New York, closes on Friday at 5 p.m.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">HIGH LIQUIDITY</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Liquidity is the ability of an asset to be quickly converted into cash. In the world of forex, the high liquidity means large amounts of money can be moved into and out of currencies with generally small spreads—the differences between the bid prices for potential buyers and the ask prices for potential sellers.</p>
                                </div>
                            </div>
                        </div>
                    
                    </div>
    </div>
  
</section>

@endsection

@section('scripts')
<script src="{{asset('landrick/js/parallax.js ')}}"></script>   
@endsection