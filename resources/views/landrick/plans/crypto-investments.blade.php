@extends('landrick.layouts.app')

@section('content')
     
 <!-- Hero Start -->
 <section class="bg-half-170 d-table w-100" style="background: url('/landrick/images/company/oil-gas.jpeg');">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row mt-5 justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="pages-heading title-heading">
                    <h2 class="text-white title-dark"> THE CURRENCY OF THE
                        FUTURE </h2>
                    <p class="text-white-50 para-desc mb-0 mx-auto">Blockchain is the most promising technology since the internet.</p>
                </div>
            </div><!--end col-->
        </div><!--end row--> 
      
    </div> <!--end container-->
</section><!--end section-->
<div class="position-relative">
    <div class="shape overflow-hidden text-color-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#2f55d4"></path>
        </svg>
    </div>
</div>
<!-- Hero End -->

<section class="section">
   
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="section-title text-center mb-4 pb-2">
                    <i class="uil uil-chart-pie-alt text-primary h2"></i>
                    <h4 class="title mt-3 mb-4" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">CRYPTOCURRENCY INVESTMENT OPTION</h4>
                    <p class="text-muted mx-auto para-desc"><ul class="list-group list-group-flush">
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">Minimum Capital Margin: $200</li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Maximum Capital Margin: Unlimited</li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1600" data-aos-anchor-placement="center-bottom">Investment Capital Return: True</li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="1800" data-aos-anchor-placement="center-bottom">
                            Return On Investments: 0.02% - Daily
                        </li>
                        <li class="list-group-item list-group-item-dark" data-aos="fade-up" data-aos-duration="2000" data-aos-anchor-placement="center-bottom">
                            Elapse Duration: 30 Days
                        </li>
                        </ul>
                        <a href="{{'register'}}" data-aos="fade-up" data-aos-duration="2200" data-aos-anchor-placement="center-bottom" class="btn btn-primary btn-lg mb-0 mt-4">Join Now</a>
                    </p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row justify-content-center">
            <div class="col-lg-6 mt-2">
                <div class="px-md-4 pt-4 ">
                    <h4 class="mb-3" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">WHY CRYPTOCURRENCY?</h4>
                    <div class="position-relative mt-5" data-aos="flip-right" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">
                        <div class="bg-overlay bg-primary bg-gredient rounded-md" style="opacity: 0.6;"></div>
                        <img src="{{asset('/landrick/images/company/crypto2.jpeg')}}" class="rounded-md shadow img-fluid" alt="">
                    </div>

                    <p class="text-muted mb-0 mt-4 text-lg" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Cryptocurrency provides many incentives for entrepreneurs across the globe. It has made it easier for entrepreneurs to reach international markets rather than strictly sticking to the national markets. This has allowed sellers to create relationships and foster trusts with markets never before available and has been fantastic for developing nations. During the last three months of 2021, each day saw an average of 587 thousand confirmed Bitcoin transactions worldwide.

                    </p>

                    
                    <div class="px-md-4 pt-4 text-start">
                     

                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-6 mt-2">
                <div class="px-md-4 pt-4 text-center">
                    <h4 class="mb-3" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">CRYPTOCURRENCY ETF</h4>
                    <div class="position-relative mt-5" data-aos="flip-left" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">
                        <div class="bg-overlay bg-primary bg-gredient rounded-md" style="opacity: 0.6;"></div>
                        <img src="/landrick/images/company/crypto3.jpeg" class="rounded-md shadow img-fluid" alt="">
                    </div>

                    <p class="text-muted mb-0 mt-4" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">A cryptocurrency exchange traded fund (ETF) is a fund consisting of cryptocurrencies. While most ETFs track an index or a basket of assets, a cryptocurrency ETF tracks the price of one or more digital tokens. Based on investor sales or purchases, the share price of cryptocurrency ETFs fluctuates on a daily basis. Just like common stocks, they are also traded on a daily basis. Cryptocurrency ETFs provide several benefits to investors, such as significantly lower cryptocurrency ownership costs and outsourcing of the steep learning curve required to trade cryptocurrencies.</p>

                   
                    <div class="px-md-4 pt-4 text-start">
                       
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section>
        <!-- CTA Start -->
        <section class="section bg-cta" data-jarallax='{"speed": 0.5}' style="background: url('/landrick/images/company/crypto4.jpeg') center center;" id="cta">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <h4 class="title title-dark text-white mb-4" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom">OUR WORD</h4>
                            <p class="text-white-50  para-desc mx-auto" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">Cryptocurrency ETFs are a nascent asset class, and given the regulatory uncertainty, their market is still being defined. But they might be one of the best instruments through which to own cryptocurrencies.</p>
                            <a href="{{route('register')}}" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom" class="btn  btn-primary mt-4 lightbox">
                                Join Now
                            </a>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- CTA End -->
<section  class="section">
    <div class="container">
        <h4 class=" display-5 fw-bold mt-10" data-aos="fade-up" data-aos-duration="1000" data-aos-anchor-placement="center-bottom"> BENEFITS OF CRYPTOCURRENCY INVESTMENTS</h4>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">IMPROVED LIQUIDITY</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">
                                        Liquidity is the measure of how quickly and easily a cryptocurrency can be converted into cash, without impacting the market price. Liquidity is important because it brings about better pricing, faster transaction times and increased accuracy for technical analysis.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">CRYPTOCURRENCY MARKET HOURS</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">The cryptocurrency market is usually available to trade 24 hours a day, seven days a week because there is no centralised governance of the market. Cryptocurrency transactions take place directly between individuals, on cryptocurrency exchanges all over the world. However, there may be periods of downtime when the market is adjusting to infrastructural updates, or ‘forks’.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">SECURITY</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Blockchain technology, which translates as a chain of blocks, ensures security behind the cryptocurrencies and makes them transparent. In addition, the blockchain guarantees users that digital money will function optimally. The complete system of the blockchain is a network and works decentrally. This means that the data is stored on many different end devices, computers and nodes in the network and not in one central location, as is the case with a bank.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 mt-4 pt-2">
                            <div class="d-flex features feature-primary pt-4 pb-4">
                               
                                <div class="flex-1">
                                    <h4 class="title" data-aos="fade-up" data-aos-duration="1200" data-aos-anchor-placement="center-bottom">CRYPTOCURRENCY VOLATILITY</h4>
                                    <p class="text-muted para mb-0" data-aos="fade-up" data-aos-duration="1400" data-aos-anchor-placement="center-bottom">Although the cryptocurrency market is relatively new, it has experienced significant volatility due to huge amounts of short-term speculative interest. The volatility of cryptocurrencies is part of what makes this market so exciting. Rapid intraday price movements can provide a range of opportunities to traders to go long and short but also come with increased risk. So, if you decide to explore the cryptocurrency market, make sure that you have done your research and developed a risk management strategy.</p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
    </div>
  
</section>

@endsection

@section('scripts')
<script src="{{asset('landrick/js/parallax.js ')}}"></script>   
@endsection