@extends('landrick.layouts.app')

@section('content')
<section class="bg-half d-table w-100"  style="background: url('/landrick/images/crypto/crypto.png') top; center top / auto scroll; z-index: 0;" >
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h3 class="title text-white"> Cloud Minning </h3>
                    <ul class="list-unstyled mt-4">
                        {{-- <li class="list-inline-item h6 date text-muted"> <span class="text-dark">Last Revised :</span> 23th Sep, 2019</li> --}}
                    </ul>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<div class="position-relative">
    <div class="shape overflow-hidden ">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg')}}">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#ffba00"></path>
        </svg>
    </div>
</div>
<!-- Hero End -->
<section  class="section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-10">
                <div class="section-title">
                    <div class="text-center">
                        <h3 class="title mb-4 ">WHAT IS CLOUD MINING? </h3>
                     
                    </div>
                    <p class="text-muted mb-0 mt-4">
                        Cloud (or remote) mining – is the process of using hardware power to mine cryptocurrency (such as Bitcoin or Litecoin) remotely. This mining model came to existence due to the fact that the increasing difficulty of mining has made it
                         unprofitable for mining enthusiasts to mine cryptocurrency at home.
                    </p>
                    <p class="text-muted mb-0 mt-4">
                        Cloud mining gives people a unique opportunity to begin mining cryptocurrency without the need for a large initial investment in hardware or technical knowledge. Despite the simplicity of the cloud mining model, it is worth elaborating on a few details, 
                        specifically it’s important to highlight that remote mining comes in two forms: hosted or cloud based mining.
                    </p>
                
                    <h3 class="my-4">REMOTE HOSTED MINING</h3>
                    <p class="text-muted">
                        The first form of remote mining is remote hosting. 
                        This model suits users with a high level mining experience 
                        and know-how and who require a high degree of control over 
                        their mining hardware. Under this model, the mining hardware 
                        is hosted in a remote datacentre and the user assumes full c
                        ontrol over the setup and configuration of the mining hardware. 
                        Under this model, the miner pays a fee to the hosting company that 
                        would cover maintenance and electricity costs. This helps the miner 
                        handle the risks associated with maintenance of the kit as well as any 
                        risks with the shipment of the hardware. On the other hand, it presents 
                        the miner with risk on the initial hardware investment and 
                        requires much more time and technical knowledge to implement successfully.
                    </p>      
                    <p class="text-muted mb-0">
                        It can therefore be summarised that the benefits of remote hosted mining are tight control over the mining process, maintenance support and subsequent ownership of the hardware. The big drawbacks are risks associated with the procurement of expensive hardware and the very high cost of entry, both in terms of investment and technical experience.
                    </p>
                    <h4 class="my-4">CLOUD MINING</h4>
                    <p class="text-muted">
                        This option provides a range of benefits: instant connection (meaning no hardware shipment wait times and delivery risks), fixed maintenance and electricity fees and no nuisances associated with mining at home such as noise, heat or space. Another key point is that this model of cloud mining requires no technical experience. Obviously, it’s very important that miners understand the mining process, however this model doesn’t require hardware expertise or significant configuration / implementation cost. Since customers can purchase any amount of mining power they wish, this means that the level of investment will depend only on the miners’ ambition. This means that the cost of entry and subsequent risk is far lower than in comparison with the remotely hosted model.
                    </p>
                    <p class="text-muted mb-0">
                        {{$settings->site_title}} is happy to be offering its new cloud mining services range. We guarantee an instant connection, around-the-clock access and monitoring, an easy-to-use management interface, 24/7 uptime as well as daily payouts.
                    </p>
                    <p class="text-muted mb-0">
                        Cloud mining is greatly suited for novice miners who would like to try out mining and earning cryptocurrency as well as seasoned miners who don’t want the hassle or risks of hosted or home-based mining
                    </p>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
@endsection