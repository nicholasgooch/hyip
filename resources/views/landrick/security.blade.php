@extends('landrick.layouts.app')

@section('content')
 
        <!-- Hero Start -->
        <section class="bg-half d-table w-100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12 text-center">
                        <div class="page-next-level">
                            <h4 class="title"> Security </h4>
                            <ul class="list-unstyled mt-4">
                                <li class="list-inline-item h6 date text-muted"> <span class="text-dark">Last Revised :</span> 23th Sep, 2019</li>
                            </ul>
                        </div>
                    </div>  <!--end col-->
                </div><!--end row-->
            </div> <!--end container-->
        </section><!--end section-->
        <!-- Hero End -->   

        <!-- Shape Start -->
        <div class="position-relative">
            <div class="shape overflow-hidden text-white">
                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#2f55d4"></path>
                </svg>
            </div>
        </div>
        <!--Shape End-->
        
        <!-- Start Privacy -->
        <section class="section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-9">
                        <div class="card shadow rounded border-0">
                            <div class="card-body">
                                <p class="text-muted">
                                    Thank you for visiting the website or mobile application (together, the “Site”) owned and operated by of {{$settings->site_title}} LLC (“{{$settings->site_title}}” or “we”). We respect your privacy and want to protect your personal information. To learn more, please read this Privacy Policy.
                                    This Privacy Policy explains how we collect, use and, under certain conditions, disclose your personal information. This Privacy Policy also explains the steps we have taken to secure your personal information. Finally, this Privacy Policy explains your options regarding the collection, use and disclosure of your personal information. By visiting the Site, you accept the practices described in this Privacy Policy for the Site.
                                    This Privacy Policy applies only to the Site. This Privacy Policy does not necessarily apply to any offline collection of your personal information. Please see below for details.


                                </p>
                                <h5 class="card-title">1. HOW YOU CAN PROTECT YOURSELF: </h5>
                                <p class="text-muted">Beware Fraudulent E-Mails and Web Sites “Phishing” is a rampant Internet scam that relies on “spoofed” e-mails, purportedly from well known firms, to lure individuals to fraudulent Web sites that look and feel like the well known firm’s Web site. At such Web sites, victims are asked to provide personal information about themselves, such as their name, address and credit card number. These fraudulent e-mails and Web sites may also try to install malicious software on your computer that monitors your activities and sends sensitive personal information (your passwords, for example) to a remote location. With that information, criminals can commit identity theft, credit card fraud and other crimes. You can protect yourself by following these best practices when using the Internet:
                                    <ul class="list-unstyled text-muted">
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>provide you with the services we offer.</li>
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>Be aware that e-mail is insecure and easy to forge. E-mail that appears to be from a friend or company you do business with may be fraudulent and designed to trick you into providing personal information about yourself or installing dangerous software.</li>
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>Do not respond to e-mails or pop-up messages that solicit your personal information: name, address, Social Security number, etc.</li>
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>Only access trusted Web sites that you found other than by clicking on a Web site address in an e-mail and then added to your browser’s bookmarks. Otherwise, manually type the address into your browser and then bookmark it. When you receive an e-mail, rather than clicking on a Web site address in the e-mail, which can bring you to a fraudulent site, use the bookmark to access that site.</li>
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>If you receive an e-mail from {{$settings->site_title}} investments partners you are uncertain about, or which you believe to be fraudulent, please forward it to support@{{$settings->site_title}}.com. {{$settings->site_title}} partners will investigate the e-mail and respond back to you. If you are a client of the firm, please notify your sales representative or investment professional, as well. Please be advised that from time to time external parties may pose as {{$settings->site_title}}partners through fraudulent communications via email and phone calls in scams to market fake prospectus documents and to solicit monetary payments. It is important to know that any communication you receive from {{$settings->site_title}} partners would come from an @ {{$settings->site_title}}.com e-mail address (not from a free email account such as Yahoo, Gmail or any other domain outside of “@{{$settings->site_title}}.com ”) and/or be found on the {{$settings->site_title}} website. If you receive a communication that you believe may not be from {{$settings->site_title}} partners, please contact us. Personal Computer Security Tips No security practice is foolproof. You can, however, help protect yourself by following these best practices to secure your personal computer:</li>
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>  Make sure your computer is up to date with the most recent software patches. Patches are software updates that often address software vulnerabilities that phishing scams and viruses exploit.</li>
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i> Install a firewall between your computer and the Internet. A firewall is software or hardware that acts as a buffer between your computer and the Internet that limits access to your computer and blocks communications from 
                                            <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i> </li>Kindly contact the manufacturer of your computer for additional information and recommendations.</li>
                                    </ul>
                                </p>
                            </div>
                            <div class="col-xs-12 col-sm-10 col-sm-push-1 text-muted">
                                <h4 class="">How You Can Protect Yourself</h4>
                                <p>
                                Beware Fraudulent E-Mails and Web Sites “Phishing” is a rampant Internet scam that relies on “spoofed” e-mails,
                                purportedly from well known firms, to lure individuals to fraudulent Web sites that look and feel like the well known firm’s Web site.
                                At such Web sites, victims are asked to provide personal information about themselves, such as their name, address and credit card number.
                                These fraudulent e-mails and Web sites may also try to install malicious software on your computer that monitors your activities and sends
                                sensitive personal information (your passwords, for example) to a remote location. With that information, criminals can commit identity theft,
                                credit card fraud and other crimes. You can protect yourself by following these best practices when using the Internet:
                                <br>
                                <br>
                                - Be aware that e-mail is insecure and easy to forge. E-mail that appears to be from a friend or company you do business with may be
                                fraudulent and designed to trick you into providing personal information about yourself or installing dangerous software.<br>
                                - Do not respond to e-mails or pop-up messages that solicit your personal information: name, address, Social Security number, etc.<br>
                                - Only access trusted Web sites that you found other than by clicking on a Web site address in an e-mail and then added to your browser’s bookmarks.
                                Otherwise, manually type the address into your browser and then bookmark it.
                                When you receive an e-mail, rather than clicking on a Web site address in the e-mail,
                                which can bring you to a fraudulent site, use the bookmark to access that site.<br>
                                - If you receive an e-mail from {{$settings->site_title}} partners you are uncertain about, or which you believe to be fraudulent, please forward it to support@{{$settings->site_title}}.
                                {{$settings->site_title}} partners will investigate the e-mail and respond back to you. If you are a client of the firm, please notify your sales representative or investment professional, as well.
                                Please be advised that from time to time external parties may pose as {{$settings->site_title}} partners through fraudulent communications via email and phone calls in scams to market fake prospectus documents and to solicit monetary payments.
                                It is important to know that any communication you receive from {{$settings->site_title}} partners would come from an @{{$settings->site_title}}.com e-mail address (not from a free email account such as Yahoo,
                                Gmail or any other domain outside of “@{{$settings->site_title}}.com ”) and/or be found on the worthfininvestments.com website. If you receive a communication that you believe may not be from {{$settings->site_title}} partners, please contact us.
                                Personal Computer Security Tips No security practice is foolproof. You can, however, help protect yourself by following these best practices to secure your personal computer:
                                <br>
                                <br>
                                * Install antivirus and anti-spyware software on your computer and make sure it is up to date with the most recent virus/spyware signatures.<br>
                                * Make sure your computer is up to date with the most recent software patches. Patches are software updates that often address software vulnerabilities that phishing scams and viruses exploit.<br>
                                * Install a firewall between your computer and the Internet. A firewall is software or hardware that acts as a buffer between your computer and the Internet that limits access to your computer and blocks communications from unauthorized sources.<br>
                                * Kindly contact the manufacturer of your computer for additional information and recommendations.
                                </p>
                                <h2 class="title--beta">Glossary of Terms</h2>
                                <h3>FIREWALL</h3>
                                <p>
                                A system designed to prevent unauthorised access to or from a private network.
                                Firewalls can be implemented in both hardware and software, or a combination of both.
                                Firewalls are frequently used to prevent unauthorised Internet users from accessing private networks connected to the Internet
                                </p>
                                <h3>PATCH</h3>
                                <p>
                                Also called a service patch, a fix to a program bug.
                                A patch is an actual piece of object code that is inserted into (patched into) an executable program.
                                Patches typically are available as downloads over the Internet.
                                </p>
                                <h3>ANTIVIRUS SOFTWARE</h3>
                                <p>
                                A utility that searches a hard disk for viruses and removes any that are found.
                                Most antivirus programs include an auto-update feature that enables the program to download profiles of new viruses so that it can check for the new viruses as soon as they are discovered.
                                </p>
                                <h3>URL</h3>
                                <p>
                                Abbreviation of Uniform Resource Locator, the global address of documents and other resources on the World Wide Web.
                                </p>
                                <h3>SPOOF</h3>
                                <p>
                                To fool. In networking, the term is used to describe a variety of ways in which hardware and software can be fooled.
                                </p>
                                <h3>PHISING</h3>
                                <p>
                                Phishing attacks use ”spoofed” e-mails and fraudulent Web sites designed to fool recipients into divulging personal financial data such as credit card numbers,
                                account usernames and passwords, Social Security numbers, etc.
                                By hijacking the trusted brands of well-known banks, online retailers and credit card companies, phishers are able to convince up to 5% of recipients to respond to them.
                                </p>
                                <h3>COMPUTER VIRUS</h3>
                                <p>
                                A program or piece of code that is loaded onto your computer without your knowledge and runs against your wishes.
                                Viruses can also replicate themselves.
                                All computer viruses are manmade.
                                An even more dangerous type of virus is one capable of transmitting itself across networks and bypassing security systems.
                                </p>
                                </div>
                        </div>
                    </div><!--end col-->
                    
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- End Privacy -->
@endsection