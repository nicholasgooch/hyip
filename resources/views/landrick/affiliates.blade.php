@extends('landrick.layouts.app')

@section('content')

<section class="bg-half-170 bg-primary d-table w-100" style="background: url('{{asset('landrick/images/about-4.jpeg')}}') center center;">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title-heading text-center mt-4">
                    <h1 class="display-4 title-dark fw-bold text-white mb-3">TODAY'S LEADS <br> TOMORROWS PROFITS</h1>
                    <p class="para-desc mx-auto text-white-50">There's no better way to spread the word about our platform than straight from you.
                        Earn up-to 3% commission on every deposit your referrals makes.</p>
                   
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container--> 
</section>
<div class="position-relative">
    <div class="shape overflow-hidden text-color-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#243447"></path>
        </svg>
    </div>
</div>
    
@endsection