
<!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        
        <title>{{$settings->site_title}} Investments - {{$settings->site_descr}}</title>
        {{-- @laravelPWA --}}
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Strategic Cryptocurrency Investment, Buy Bitcoin, Invest Crptocurrency,Earn Cryptocurrency, HODL Cryptocurrency, Hold Cryptocurrency,Cloud Minnig Platform, Bitcoin Credit Card, Earn Sign up Bonus " />
        <meta name="keywords" content="cloud minning platfrom, buy bitcoin with wire trasfer, invest crypto, earn crytpo, hodl cryto, {{$settings->site_title}} investment," />
        <!-- favicon -->
         <link rel="shortcut icon" href="{{asset('images/favicon.png')}}">
        <!-- Bootstrap -->
        <link href="{{asset('landrick/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Icons -->
        <link href="{{asset('landrick/css/materialdesignicons.min.css')}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v2.1.9/css/unicons.css">
        <!-- Magnific -->
        <link href="{{asset('landrick/css/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('landrick/css/aos.css')}}" rel="stylesheet" type="text/css">
        <!-- Slider -->               
        <link rel="stylesheet" href="{{asset('landrick/css/owl.carousel.min.css')}}"/> 
        <link rel="stylesheet" href="{{asset('landrick/css/owl.theme.default.min.css')}}"/> 
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- Main Css -->
        <link href="{{asset('landrick/css/style-dark.css')}}" rel="stylesheet" type="text/css" id="theme-opt" />
        <link href="{{asset('landrick/colors/blue.css')}}" rel="stylesheet" id="color-opt" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.4/tiny-slider.css">
        <script
        type="module"
        src="https://cdn.jsdelivr.net/npm/@pwabuilder/pwainstall"
        ></script>
   
        <style>
            .install-button{
                --install-button-color : #2f55d4;
            }
            pwa-install::part(openButton) {
            background: #2f55d4;
            background-color: #2f55d4 !important;
            border: 1px solid #2f55d4 !important;
            cursor: pointer;
            padding: .375rem 2rem;
            font-size: 1rem;
            border-radius: 6px !important;
            box-shadow: 0 3px 5px 0 rgb(47 85 212 / 30%);
            }
            @media (max-width: 991px) {
                .login-form{
                    display: none;
                }
                .footer.desktop{
                    display: none;
                }
            }
        </style>
       <style>
            canvas {
            display: block;
            vertical-align: bottom;
            }

            /* ---- particles.js container ---- */

            #particles {
            position: absolute;
            width: 100%;
            height: 100%;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: 50% 50%;

            }
        </style>
    </head>

    <body >
        {{-- <!-- Loader -->
         <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- Loader --> --}}
        
        <!-- Navbar STart -->
@include('landrick.partials.nav')
        <!-- Navbar End -->
@yield('content')

<!-- Footer Start -->
@include('landrick.partials.footer')
@yield('scripts')