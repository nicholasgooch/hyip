@extends('landrick.layouts.app')

@section('content')
 
        <!-- Hero Start -->
        <section class="bg-half bg-light d-table w-100"  style="background: url('/landrick/images/crypto/crypto.png') top; center top / auto scroll; z-index: 0;">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12 text-center">
                        <div class="page-next-level">
                            <h3 class="title"> How to buy Bitcoin? (BTC)</h3>
                            <h5 class="text-muted text-center">Bitcoin is an open-source software that since 2009 has enabled the exchange of an entirely new form of money (BTC) over the internet.</h5>
                            
                        </div>
                    </div>  <!--end col-->
                </div><!--end row-->
            </div> <!--end container-->
        </section><!--end section-->
        <!-- Hero End -->   

        <!-- Shape Start -->
        <div class="position-relative">
            <div class="shape overflow-hidden text-white">
                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#2f55d4 "></path>
                </svg>
            </div>
        </div>
        <!--Shape End-->
        
        <!-- Start Privacy -->
        <section class="section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-9">
                        <div class="card  shadow rounded border-0">
                            <div class="card-body">
                                <h5 class="card-title">Overview :</h5>
                                <p class="text-muted">Created by an unknown individual or group operating under the pseudonym Satoshi Nakamoto, Bitcoin sought to offer a “peer-to-peer digital cash system” that removed middlemen from online commerce through the clever use of public key cryptography and peer-to-peer networking.

                                    The result was that, for the first time, money could be introduced to an economy on a schedule immune to the influence of any central bank or operator. As set out in the rules of the software, only 21 million bitcoins, divisible into many more smaller units, can ever be created.
                                    
                                    By creating true digital scarcity, Bitcoin would go on to entice a new generation of traders who now see BTC as a viable alternative to gold and traditional monies</p>
                                <p class="text-muted">In the 1960s, the text suddenly became known beyond the professional circle of typesetters and layout designers when it was used for Letraset sheets (adhesive letters on transparent film, popular until the 1980s) Versions of the text were subsequently included in DTP programmes such as PageMaker etc.</p>
                                <p class="text-muted">There is now an abundance of readable dummy texts. These are usually used when a text is required purely to fill a space. These alternatives to the classic Lorem Ipsum texts are often amusing and tell short, funny or nonsensical stories.</p>
                            
                                <h5 class="card-title">Why buy Bitcoin ?</h5>
                               <p class="text-muted">Today, Bitcoin advocates see the software as an alternative money system offering users the ability to exit their government economy and claim sovereignty over their financial assets.

                                Investors in BTC, likewise, view it as an alternative to hard assets with a finite supply like gold, silver and other commodities.</p>
    
                                <h5 class="card-title">Where can I buy BTC ?</h5>
                                <ul class="list-group bg-dark list-group-flush text-muted">
                                    <li class="list-group-item " ><i data-feather="arrow-right" class="fea icon-sm mr-2"></i><strong>Direct Purchase</strong> <br> You can purchase it directly from another individual in person or over the web.</li>
                                    <li class="list-group-item"><i data-feather="arrow-right" class="fea icon-sm mr-2"></i><strong> Crypto ATM</strong> <br> You can try to locate a crypto ATM near you that offers Bitcoin.</li>
                                    <li class="list-group-item"><i data-feather="arrow-right" class="fea icon-sm mr-2"></i><strong>Exchange </strong><br> You can buy from register and approved crypto exchanges such as:
                                        <ul class=" ">
                                            <li ><a href="crypto.com">Crypto.com</a></li>
                                            <li ><a href="kraken.com">Kraken.com</a></li>
                                            <li ><a href="coinbase.com">Coinbase.com</a></li>
                                        </ul>
                                    </li>
                                    
                                </ul>
                              
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- End Privacy -->

@endsection