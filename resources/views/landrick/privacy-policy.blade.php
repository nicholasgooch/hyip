@extends('landrick.layouts.app')

@section('content')
 
        <!-- Hero Start -->
        <section class="bg-half d-table w-100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12 text-center">
                        <div class="page-next-level">
                            <h4 class="title"> Privacy Policy </h4>
                            <ul class="list-unstyled mt-4">
                                <li class="list-inline-item h6 date text-muted"> <span class="text-dark">Last Revised :</span> 23th Sep, 2019</li>
                            </ul>
                        </div>
                    </div>  <!--end col-->
                </div><!--end row-->
            </div> <!--end container-->
        </section><!--end section-->
        <!-- Hero End -->   

        <!-- Shape Start -->
        <div class="position-relative">
            <div class="shape overflow-hidden text-white">
                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#2f55d4"></path>
                </svg>
            </div>
        </div>
        <!--Shape End-->
        
        <!-- Start Privacy -->
        <section class="section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-9">
                        <div class="card shadow rounded border-0">
                            <div class="card-body">
                                <p class="text-muted">
                                    Thank you for visiting the website or mobile application (together, the “Site”) owned and operated by of {{$settings->site_title}} LLC (“{{$settings->site_title}}” or “we”). We respect your privacy and want to protect your personal information. To learn more, please read this Privacy Policy.
                                    This Privacy Policy explains how we collect, use and, under certain conditions, disclose your personal information. This Privacy Policy also explains the steps we have taken to secure your personal information. Finally, this Privacy Policy explains your options regarding the collection, use and disclosure of your personal information. By visiting the Site, you accept the practices described in this Privacy Policy for the Site.
                                    This Privacy Policy applies only to the Site. This Privacy Policy does not necessarily apply to any offline collection of your personal information. Please see below for details.


                                </p>
                                <h5 class="card-title">1. INFORMATION COLLECTION AND USE :</h5>
                                <p class="text-muted">We collect information from you in several different ways on the Site. One goal in collecting personal information from you is to provide an efficient, meaningful, and customized experience. For example, we can use your personal information to:
                                    <ul class="list-unstyled text-muted">
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>provide you with the services we offer.</li>
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>help make the Site easier for you to use by not having to enter information more than once.</li>
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>help you quickly find information, products, and services.</li>
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>help us create content on the Site that is most relevant to you.</li>
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>alert you to new information, products, and services that we offer.</li>
                                    </ul>
                                </p>
                                <p class="text-muted">(a) Registration and Investing. Before using certain parts of the Site or using our services, you may have to register. During registration, you will be prompted to provide to us certain personal information, which may include, but not be limited to, your name, mobile phone number, physical address, email address, social security number, date of birth and government-issued identification. These kinds of personal information are used to provide services to you in accordance with existing regulations, to assist third-party service providers to provide services to you, to communicate with you about our services and our Site, and for marketing purposes. If we encounter a problem when providing services, we use the personal information to contact you.</p>
                                <p class="text-muted">(b) Email Addresses. Several locations of the Site may permit you to enter or for us to capture your email address for purposes including but not limited to: to register for promotions, referral programs or services; or to request us to notify you regarding particular promotions, referral programs or services. Your participation in a promotion or referral program is completely voluntary, so you have a choice whether to participate and disclose information to us. We use this information to notify promotion winners, pay referral fees and to award prizes.</p>
                                <p class="text-muted">
                                    (c) Cookies and Other Technology. Like many websites, the Site employs cookies, location-based services and web beacons (also known as clear GIF technology or “action tags”) to speed your navigation of the Site, recognize you and your access privileges, and track your usage.
                                    <ul class="list-unstyled text-muted">
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>(i) The Site captures some information about you automatically utilizing background local storage and session storage technologies (“Cookies”) in order to improve the user experience. Cookies are small files or other pieces of data which are downloaded or stored on your computer or other device, that can be tied to information about your use of the Site (including certain third-party services and features offered as part of our website). Examples of information of this type are your IP address, the browser you are using, the operating system you are using, the pages on the website that you visit and details of your transaction activity, such as the amount, date and time for each transaction. When we use Cookies, we may use “session” Cookies that last until you close your browser or “persistent” cookies that last until you or your browser delete them. You may change your browser setting to decline the use of Cookies. THE SITE’S COOKIES DO NOT AND CANNOT INFILTRATE A USER’S HARD DRIVE TO GATHER A USER’S CONFIDENTIAL INFORMATION. Our Cookies are not “spyware.” </li>
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>(ii) Web beacons assist in delivering cookies and help us determine whether a web page on the Site has been viewed and, if so, how many times. For example, any electronic image on the Site, such as an ad banner, can function as a web beacon. We may also use web beacons to collect information about whether our electronic communications to our users have been delivered, viewed and opened. This data may impact the quantity and content of future communications from us to our users.</li>
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>(iii) We may use third-party companies to help us tailor Site content to users or to serve ads or messages on our behalf. These companies may employ cookies and web beacons to measure advertising or messaging effectiveness (such as which web pages are visited, which messages are responded to, or what products are purchased and in what amount). Any information that these third parties collect via cookies and web beacons is not linked (by them) to any personal information collected by us.</li>
                                        <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>(iii) Age Requirement. We do not knowingly collect information from users of the Site under the age of 18 and/or their Internet usage, and such persons are not authorized to use the Site. If we obtain actual knowledge that a user is under the age of 18, then we will take steps to remove that user’s personal information from our databases. By using the Site, you represent that you are at least 18 years old. You also represent, by accessing or using the Site, that you are of legal age to enter into legal agreements, or if you are not, that you have obtained your parent’s or legal guardian’s consent to accept the terms in this Privacy Policy.
                                        </li>
                                        
                                    </ul>
                                </p>
                            
                                <h5 class="card-title">2.INFORMATION USE AND DISCLOSURE.</h5>
                                <p class="text-muted">
                                    (a) Internal Use. We use your personal information to provide you with our services. We may internally use your personal information to improve the Site’s content and layout, to improve our outreach and for our own marketing efforts (including marketing our services to you), and to determine general marketplace information about visitors’ usage behavior to the Site.

                                    (b) Communications with You. We will use your personal information to communicate with you about the Site, your use of our services, and new information available on the Site. Also, we may send you a confirmation email when you register with us or when you have received a gift from another person. We may send you a service-related announcement on the rare occasions when it is necessary (for example, if we must temporarily suspend our service for maintenance.) Also, we will send you a notification email or text message when you request us to notify you when particular services, promotions or referral programs become available or change. We always permit you to unsubscribe from promotional notifications. Because we have to communicate with you about our services that you choose to use, you cannot opt out of receiving emails related to those services or your account with us.

                                   
                                </p>
                                <h5 class="card-title">3. YOUR RIGHTS, CHOICES AND DISCLOSURES</h5>
                                <p class="text-muted">(a) General.</p>
                                <ul class="list-unstyled text-muted">
                                    <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>D(i) We provide herein certain specific disclosures on privacy rights set forth below. We are committed to facilitate the exercise of your rights granted by the laws of your jurisdiction, which may include the right to request the correction, modification or deletion of your personal information and the right to opt out of the sale of your personal information (as applicable). We will do our best to honor your requests subject to any legal and contractual obligations. Although we may do so in our discretion, we do not delete your information upon request, unless required to do so under applicable law.
                                    </li>
                                    <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>(ii) You may opt out of receiving electronic marketing communications from us by clicking next to the “unsubscribe” link on the communication or contacting us at support@{{$settings->site_title}}.com. Some non-marketing communications may not be subject to a general opt-out, such as fulfilling our obligations when services are requested and disclosures to comply with legal requirements, software updates and other support-related information</li>
                                    <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>(iii) Subject to local law, you may have additional rights under the laws of your jurisdiction regarding your personal data, such as the right to complain to your local data protection authority.</li>
                                    <li><i data-feather="arrow-right" class="fea icon-sm mr-2"></i>(iv) You may be able to disable Cookies through your browser settings, but if you delete or disable Cookies, you may experience interruptions or limited functionality in certain areas of the Services.</li>

                                    
                                </ul>
    
                                <h5 class="card-title">4. STORAGE OF DATA; DATA SECURITY :</h5>
                                <p class="text-muted">We retain the personal information we collect for so long as reasonably necessary to fulfill the purposes for which the data was collected and to perform our contractual and legal obligations. Notwithstanding the generality of the foregoing, we store email addresses and phone numbers until the user requests to be unsubscribed or removes themselves through any self-service tools offered to the user.

                                The Site incorporates physical, electronic, and administrative procedures to safeguard the confidentiality of your personal information, including Secure Sockets Layer (“SSL”) for all transactions through the Site. We use SSL encryption to protect your personal information online, and we also take several steps to protect your personal information in our facilities. Access to your personal information is restricted. Only employees who need access to your personal information to perform a specific job are granted access to your personal information. Finally, we rely on third-party service providers for the physical security of some of our computer hardware. We require those third-party service providers to comply with commercially reasonable security practices and measures. For example, when you visit our Site, you access servers that are kept in a secure physical environment, behind a locked cage and an electronic firewall. While we use industry-standard precautions to safeguard your personal information, we cannot and do not guarantee complete security. 100% complete security does not presently exist anywhere online or offline. As such, you assume the risk of security breaches and all consequences resulting from them. To that end, please safeguard your credentials.</p>

                                <h5 class="card-title">5. JURISDICTION</h5>
                                <p class="text-muted">The Site is controlled, operated and hosted in the United States. We do not currently accept users outside of the United States. Please be advised that through your continued use of the Site, which is governed by Canadian law, this Privacy Policy, and our Terms of Use, your personal information will be held in the United States and you expressly consent thereto and consent to be governed by Canadian law for these purposes.</p>

                                <h5 class="card-title">6. LINKS TO OTHER SITES</h5>
                                <p class="text-muted">The Site may contain links to other websites for your convenience or information. These websites may be operated by companies unaffiliated with the Company, and we are not responsible for the content or privacy practices of those websites. Linked websites may have their own terms of use and privacy policies, and we encourage you to review those policies whenever you visit the websites.

                                
                                <h5 class="card-title">7. PRIVACY POLICY CHANGES</h5>
                                <p  class="text-muted">
                                    We may update this Privacy Policy from time to time and without prior notice to you to reflect changes in our information practices, and any such amendments shall apply to information already collected and to be collected. Your continued use of the Site after any changes to this Privacy Policy indicates your agreement with the terms of the revised Privacy Policy. Please review this Privacy Policy periodically and especially before you provide personal data to us. If we make material changes to this Privacy Policy, we will notify you here, by email or by means of a notice on the home page the Site. The date of the last update of the Privacy Policy is indicated at the top of this Privacy Policy.


                                </p>
                                </p>
                                <a href="javascript:void(0)" class="btn btn-primary">Print</a>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- End Privacy -->
@endsection