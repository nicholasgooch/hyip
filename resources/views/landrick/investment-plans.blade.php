@extends('landrick.layouts.app')

@section('content')
<section class="bg-half  d-table w-100" style="background: url('{{asset('landrick/images/about-4.jpeg')}}');">
    <div class="bg-overlay" style="opacity: 0.5;"></div>
  
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <div class="page-next-level">
                    <h3 class="title text-white-5"> INVESTMENT PLANS</h3>
                    <ul class="list-unstyled mt-4">
                        {{-- <li class="list-inline-item h6 date text-muted"> <span class="text-dark">Last Revised :</span> 23th Sep, 2019</li> --}}
                    </ul>
                </div>
            </div>  <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->


</section><!--end section-->
<div class="position-relative">
    <div class="shape overflow-hidden ">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg')}}">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="#2f55d4"></path>
        </svg>
    </div>
</div>
<!-- Hero End -->
<section class="section bg-light">
    {{-- <div class="bg-overlay rounded bg-primary bg-gradient" style="opacity: 0.5;"></div> --}}
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4 text-muted">Our Investment Plans</h4>
                    {{-- <p class="text-muted para-desc mx-auto mb-0">{{$settings->site_title}}</span> that can provide everything you need to generate awareness, drive traffic, connect</p> --}}
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row justify-content-center ">
           
           
           
            <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                    <div class="position-relative d-block overflow-hidden">
                        <img src="{{asset('landrick/images/gold-inv.jpeg')}}" class="img-fluid rounded-top mx-auto" alt="">
                        <div class="overlay-work bg-dark"></div>
                        <a href="{{route('landrick.gold.inv')}}" class="text-white h6 preview">Explore Now <i class="uil uil-angle-right-b align-middle"></i></a>
                    </div>

                    <div class="card-body">
                        <h5><a href="{{route('landrick.gold.inv')}}" class="title text-dark">Gold Investments</a></h5>
                        <div class="rating">
                            <p class="">Gold is the ultimate currency, Of all the precious metals,
                                gold is the most popular as an investment</p>
                        </div>
                        <div class="fees d-flex justify-content-between">
                            <a href="{{route('landrick.gold.inv')}}" class="btn btn-block btn-primary mt-4">Explore</a>
                        </div>
                    </div>
                </div>
            </div>
             <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                    <div class="position-relative d-block overflow-hidden">
                        <img src="{{asset('landrick/images/real-estate.jpeg')}}" class="img-fluid rounded-top mx-auto" alt="">
                        <div class="overlay-work bg-dark"></div>
                        <a href="{{route('landrick.real.estate')}}" class="text-white h6 preview">Explore Now <i class="uil uil-angle-right-b align-middle"></i></a>
                    </div>

                    <div class="card-body">
                        <h5><a href="{{route('landrick.real.estate')}}" class="title text-dark">Real Estate Investments</a></h5>
                        <div class="rating">
                            <p class="">Exceptional properties. Exceptional clients. You have plenty of options when it comes to investing in real estate.</p>
                        </div>
                        <div class="fees d-flex justify-content-between">
                            <a href="{{route('landrick.real.estate')}}" class="btn btn-block btn-primary mt-4">Explore</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                    <div class="position-relative d-block overflow-hidden">
                        <img src="{{asset('landrick/images/oil-and-gas.jpeg')}}" class="img-fluid rounded-top mx-auto" alt="">
                        <div class="overlay-work bg-dark"></div>
                        <a href="{{route('landrick.oil.gas')}}" class="text-white h6 preview">Explore Now <i class="uil uil-angle-right-b align-middle"></i></a>
                    </div>

                    <div class="card-body">
                        <h5><a href="{{route('landrick.oil.gas')}}" class="title text-dark">Oil & Gas Investments</a></h5>
                        <div class="rating">
                            <p class="">The oil and gas industry is one of the largest sectors in the world in terms of dollar value,</p>
                        </div>
                        <div class="fees d-flex justify-content-between">
                            <a href="{{route('landrick.oil.gas')}}" class="btn btn-block btn-primary mt-4">Explore</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                    <div class="position-relative d-block overflow-hidden">
                        <img src="{{asset('landrick/images/agri-infra.jpeg')}}" class="img-fluid rounded-top mx-auto" alt="">
                        <div class="overlay-work bg-dark"></div>
                        <a href="{{route('landrick.agri.infra')}}" class="text-white h6 preview">Explore Now <i class="uil uil-angle-right-b align-middle"></i></a>
                    </div>

                    <div class="card-body">
                        <h5><a href="{{route('landrick.agri.infra')}}" class="title text-dark">Agriculture & Infrastructure Investments</a></h5>
                        <div class="rating">
                            <p class="">Cultivating and constructing ideas for growth. Investment in agriculture. </p>
                        </div>
                        <div class="fees d-flex justify-content-between">
                            <a href="{{route('landrick.agri.infra')}}" class="btn btn-block btn-primary mt-4">Explore</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                    <div class="position-relative d-block overflow-hidden">
                        <img src="{{asset('landrick/images/forex.jpeg')}}" class="img-fluid rounded-top mx-auto" alt="">
                        <div class="overlay-work bg-dark"></div>
                        <a href="{{route('landrick.forex')}}" class="text-white h6 preview">Explore Now <i class="uil uil-angle-right-b align-middle"></i></a>
                    </div>

                    <div class="card-body">
                        <h5><a href="{{route('landrick.forex')}}" class="title text-dark">Forex Investments</a></h5>
                        <div class="rating">
                            <p class="">A profound name in the market. Foreign exchange is the marketplace for trading all the world's currencies </p>
                        </div>
                        <div class="fees d-flex justify-content-between">
                            <a href="{{route('landrick.forex')}}" class="btn btn-block btn-primary mt-4">Explore</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                <div class="card courses-desc overflow-hidden rounded shadow border-0">
                    <div class="position-relative d-block overflow-hidden">
                        <img src="{{asset('landrick/images/crypto.jpeg')}}" class="img-fluid rounded-top mx-auto" alt="">
                        <div class="overlay-work bg-dark"></div>
                        <a href="{{route('landrick.crypto')}}" class="text-white h6 preview">Explore Now <i class="uil uil-angle-right-b align-middle"></i></a>
                    </div>

                    <div class="card-body">
                        <h5><a href="{{route('landrick.crypto')}}" class="title text-dark">Cryptocurrency Investments</a></h5>
                        <div class="rating">
                            <p class="">The currency of the future. Blockchain is the most promising technology since the internet. </p>
                        </div>
                        <div class="fees d-flex justify-content-between">
                            <a href="{{route('landrick.crypto')}}" class="btn btn-block btn-primary mt-4">Explore</a>
                        </div>
                    </div>
                </div>
            </div>
            
        </div><!--end row-->
    </div><!--end container-->

</section>

<section class="section">
    @include('landrick.partials.features')
    
</section>
@stop