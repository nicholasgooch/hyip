@extends('v2.layouts.main')
@section('title', $inv->package->package_name . ' Plan')
@section('content')
    <div class="nk-content nk-content-lg nk-content-fluid">
        <div class="container-xl wide-lg">
            <div class="nk-content-inner">
                <div class="nk-content-body">
                    <div class="nk-block-head">
                        <div class="nk-block-head-content">
                            <div class="nk-block-head-sub">
                                <a href="{{ route('v2.investments') }}" class="text-soft back-to"><em
                                        class="icon ni ni-arrow-left"> </em><span>My Investment</span></a>
                            </div>
                            <div class="nk-block-between-md g-4">
                                <div class="nk-block-head-content">
                                    <h2 class="nk-block-title fw-normal">
                                        {{ $inv->package->package_name }} - Daily {{ $inv->interest }}% for
                                        {{ $inv->period }} Days
                                    </h2>
                                    <div class="nk-block-des">
                                        <p>
                                            INV-498238
                                            @if ($ended == 'Yes')
                                                <span class="badge badge-outline badge-secondary">ENDED</span>
                                            @else
                                                <span class="badge badge-outline badge-primary">RUNNING</span>
                                            @endif

                                        </p>
                                    </div>
                                </div>
                                <div class="nk-block-head-content">
                                    <ul class="nk-block-tools gx-3">
                                        @if ($ended == 'Yes')
                                            <li class="order-md-last">
                                                <button {{ $withdrawable == 0 ? 'disabled' : '' }}
                                                    onclick="wd_earnings('{{ $inv->id }}', '{{ $ern }}', '{{ $withdrawable }}', '{{ $Edays }}', '{{ $ended }}')"
                                                    class="btn btn-primary"><em class="icon ni ni-cross"></em>
                                                    <span>WIthdraw Earnings</span>
                                                </button>
                                            </li>
                                        @else
                                            <li class="order-md-last">
                                                <a href="#" class="btn btn-danger"><em class="icon ni ni-cross"></em>
                                                    <span>Cancel Plan</span>
                                                </a>
                                            </li>
                                        @endif
                                        <li>
                                            <a href="#" class="btn btn-icon btn-light"><em
                                                    class="icon ni ni-reload"></em></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .nk-block-head -->
                    <div class="nk-block">
                        <div class="card card-bordered">
                            <div class="card-inner">
                                <div class="row gy-gs">
                                    <div class="col-md-6">
                                        <div class="nk-iv-wg3">
                                            <div class="nk-iv-wg3-group flex-lg-nowrap gx-4">
                                                <div class="nk-iv-wg3-sub">
                                                    <div class="nk-iv-wg3-amount">
                                                        <div class="number">
                                                            {{ $settings->currency == 'USD' ? '$' : $settings->currency }}{{ number_format($inv->capital, 2) }}
                                                        </div>
                                                    </div>
                                                    <div class="nk-iv-wg3-subtitle">
                                                        Invested Amount
                                                    </div>
                                                </div>
                                                @if ($ended == 'Yes')
                                                @else
                                                    <div class="nk-iv-wg3-sub">
                                                        <span class="nk-iv-wg3-plus text-soft"><em
                                                                class="icon ni ni-plus"></em></span>
                                                        <div class="nk-iv-wg3-amount">
                                                            <div class="number">
                                                                {{ $settings->currency == 'USD' ? '$' : $settings->currency }}{{ number_format($currentEarning, 2) }}
                                                                <span class="number-up">{{ $inv->interest }}% </span>
                                                            </div>
                                                        </div>
                                                        <div class="nk-iv-wg3-subtitle">
                                                            Profit Earned
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .col -->
                                    @if ($ended == 'Yes')
                                    @else
                                        <div class="col-md-6 col-lg-4 offset-lg-2">
                                            <div class="nk-iv-wg3 pl-md-3">
                                                <div class="nk-iv-wg3-group flex-lg-nowrap gx-4">

                                                    <div class="nk-iv-wg3-sub">
                                                        <div class="nk-iv-wg3-amount">
                                                            <div class="number">
                                                                {{ $settings->currency == 'USD' ? '$' : $settings->currency }}{{ $totalElapse == 0 ? '0.00' : number_format($dailyErn, 2) }}
                                                                <span style="color: #1ee0ac; font-size: 50%">
                                                                    <span style="margin: 0px 0px;"> <em
                                                                            class="icon ni ni-plus"></em></span>
                                                                    {{ $settings->currency == 'USD' ? '$' : $settings->currency }}{{ number_format($dailyErn, 2) }}

                                                                    <em class="icon ni ni-info-fill" style="color: #526484;"
                                                                        data-toggle="tooltip" data-placement="right"
                                                                        title=""
                                                                        data-original-title="total returns"></em></span>
                                                            </div>
                                                        </div>
                                                        <div class="nk-iv-wg3-subtitle">Today's Return</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <!-- .col -->
                                </div>
                                <!-- .row -->
                            </div>
                            <div id="schemeDetails" class="nk-iv-scheme-details">
                                <ul class="nk-iv-wg3-list">
                                    <li>
                                        <div class="sub-text">Term</div>
                                        <div class="lead-text">{{ $inv->period }} Days</div>
                                    </li>
                                    <li>
                                        <div class="sub-text">Term start at</div>
                                        <div class="lead-text">{{ $date_invested->format('M D Y ') }}</div>
                                    </li>
                                    <li>
                                        <div class="sub-text">Term end at</div>
                                        <div class="lead-text">{{ $enddate->format('M D Y ') }}</div>
                                    </li>
                                    <li>
                                        <div class="sub-text">Daily interest</div>
                                        <div class="lead-text">{{ $inv->interest }}%</div>
                                    </li>
                                </ul>
                                <!-- .nk-iv-wg3-list -->
                                <ul class="nk-iv-wg3-list">
                                    <li>
                                        <div class="sub-text">Days Elapsed</div>
                                        <div class="lead-text">
                                            {{ $totalElapse == 0 ? 'Plan Ended' : $totalDays . ' Days' }}</div>
                                    </li>
                                    <li>
                                        <div class="sub-text">Last Withdrawal</div>
                                        <div class="lead-text">{{ $inv->last_wd }}</div>
                                    </li>
                                    <li>
                                        <div class="sub-text">Payment method</div>
                                        <div class="lead-text">NioWallet</div>
                                    </li>
                                    @if ($ended == 'Yes')
                                        @if ($withdrawable == 0)
                                            <li>
                                                <div class="sub-text">
                                                    Withdrawable Amount</small>
                                                </div>
                                                <div class="lead-text">
                                                    <div class="badge badge-info">Earnings Clamied</div>
                                                </div>
                                            </li>
                                        @elseif ($withdrawable > 0)
                                            <li>
                                                <div class="sub-text">
                                                    Withdrawable Amount</small>
                                                </div>
                                                <div class="lead-text">
                                                    {{ $settings->currency == 'USD' ? '$' : $settings->currency }}{{ number_format($withdrawable, 2) }}
                                                </div>
                                            </li>
                                        @endif
                                    @else
                                        <li>
                                            <div class="sub-text">
                                                Withdrawable Amount</small>
                                            </div>
                                            <div class="lead-text">
                                                {{ $settings->currency == 'USD' ? '$' : $settings->currency }}
                                                {{ number_format($withdrawable, 2) }}
                                            </div>
                                        </li>
                                    @endif

                                </ul>
                                <!-- .nk-iv-wg3-list -->
                                <ul class="nk-iv-wg3-list">
                                    <li>
                                        <div class="sub-text">Captial invested</div>
                                        <div class="lead-text">
                                            <span class="currency currency-usd">USD</span>
                                            {{ $settings->currency == 'USD' ? '$' : $settings->currency }}
                                            {{ number_format($inv->capital, 2) }}
                                        </div>
                                    </li>
                                    <li>
                                        <div class="sub-text">Daily profit</div>
                                        <div class="lead-text">
                                            {{ $settings->currency == 'USD' ? '$' : $settings->currency }}
                                            {{ number_format($dailyErn, 2) }}
                                        </div>
                                    </li>
                                    <li>
                                        <div class="sub-text">Accumlated profit</div>
                                        <div class="lead-text">
                                            {{ $settings->currency == 'USD' ? '$' : $settings->currency }}
                                            {{ number_format(round($totalDays * $dailyErn, 2)) }}

                                        </div>
                                    </li>
                                    <li>
                                        <div class="sub-text">Total return</div>
                                        <div class="lead-text">
                                            {{ $settings->currency == 'USD' ? '$' : $settings->currency }}{{ number_format($inv->i_return, 2) }}
                                        </div>
                                    </li>
                                </ul>
                                <!-- .nk-iv-wg3-list -->
                            </div>
                            <!-- .nk-iv-scheme-details -->
                        </div>
                    </div>
                    <!-- .nk-block -->
                    @livewire('joint-investment-manager', ['investment' => $inv])
                    <!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <h5 class="nk-block-title">Transactions</h5>
                        </div>
                        <div class="card card-bordered">
                            <table class="table table-iv-tnx">
                                <thead class="thead-light">
                                    <tr>
                                        <th class="tb-col-type">
                                            <span class="overline-title">Type</span>
                                        </th>
                                        <th class="tb-col-date">
                                            <span class="overline-title">Date</span>
                                        </th>
                                        <th class="tb-col-time tb-col-end">
                                            <span class="overline-title">Amount</span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="tb-col-type">
                                            <span class="sub-text">Investment</span>
                                        </td>
                                        <td class="tb-col-date">
                                            <span class="sub-text">04 Nov, 2018</span>
                                        </td>
                                        <td class="tb-col-time tb-col-end">
                                            <span class="lead-text text-danger">- 2,500.00</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tb-col-type">
                                            <span class="sub-text">Profit - 4.76%</span>
                                        </td>
                                        <td class="tb-col-date">
                                            <span class="sub-text">05 Nov, 2018</span>
                                        </td>
                                        <td class="tb-col-time tb-col-end">
                                            <span class="lead-text">+ 119.10</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tb-col-type">
                                            <span class="sub-text">Profit - 4.76%</span>
                                        </td>
                                        <td class="tb-col-date">
                                            <span class="sub-text">06 Nov, 2018</span>
                                        </td>
                                        <td class="tb-col-time tb-col-end">
                                            <span class="lead-text">+ 119.10</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- .card -->
                    </div>
                    <!-- .nk-block -->
                </div>
            </div>
        </div>
    </div>
    @include('v2.partials.modals.withdraw-earning')
@endsection

@section('script')
    <script type="text/javascript">
        $("#wallet_wd_close").click(function() {
            $("#wallet_wd").hide();
        });

        function handleSelect(elm) {
            if (elm.value == 'add')
                window.location = "{{ route('v2.user.profile') }}";
        }
    </script>
    <script>
        function wd_earnings(id, earn, w_able, days, ended) {
            $('#earned').text(earn);
            $('#days').text(days);
            $('#inv_id').val(id);
            $('#ended').val(ended);
            $('#withdrawable_amt').val(w_able);
            $('#wd_earning_modal').modal('show');

        }
    </script>
@endsection
