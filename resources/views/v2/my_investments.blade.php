@extends('v2.layouts.main')
@section('title', 'My Investment')
@section('content')
    <div class="nk-content nk-content-lg nk-content-fluid">
        <div class="container-xl wide-lg">
            <div class="nk-content-inner">
                <div class="nk-content-body">
                    <div class="nk-block-head">
                        <div class="nk-block-head-content">
                            <div class="nk-block-head-sub"><span>My Plan</span></div>
                            <div class="nk-block-between-md g-4">
                                <div class="nk-block-head-content">
                                    <h2 class="nk-block-title fw-normal">
                                        Invested Schemes
                                    </h2>
                                    <div class="nk-block-des">
                                        <p>
                                            Here is your current balance and your active
                                            investement plans.
                                        </p>
                                    </div>
                                </div>
                                <!-- .nk-block-head-content -->
                                <div class="nk-block-head-content">
                                    <ul class="nk-block-tools gx-3">
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#modalDefault"
                                                class="btn btn-primary"><span>Withdraw</span>
                                                <em class="icon ni ni-arrow-long-right d-none d-sm-inline-block"></em></a>
                                        </li>
                                        <li>
                                            <a href="{{ route('v2.investments.plan') }}"
                                                class="btn btn-white btn-light"><span>Invest More</span>
                                                <em class="icon ni ni-arrow-long-right d-none d-sm-inline-block"></em></a>
                                        </li>
                                        <li class="opt-menu-md dropdown">
                                            <a href="{{ route('v2.user.settings') }}"
                                                class="btn btn-white btn-light btn-icon"><em
                                                    class="icon ni ni-setting"></em></a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- .nk-block-head-content -->
                            </div>
                        </div>
                    </div>
                    <!-- .nk-block-head -->
                    <div class="nk-block">
                        <div class="card card-bordered">
                            <div class="card-inner-group">
                                <div class="card-inner">
                                    <div class="row gy-gs">
                                        <div class="col-lg-5">
                                            <div class="nk-iv-wg3">
                                                <div class="nk-iv-wg3-title">Total Balance</div>
                                                <div class="nk-iv-wg3-group flex-lg-nowrap gx-4">
                                                    <div class="nk-iv-wg3-sub mb-5">
                                                        <div class="nk-iv-wg3-amount">
                                                            <div class="number">
                                                                <small class="currency currency-usd">
                                                                    {{ $settings->currency == 'USD' ? '$' : $settings->currencyy }}</small>{{ number_format($user->wallet) }}

                                                            </div>
                                                        </div>
                                                        <div class="nk-iv-wg3-subtitle">
                                                            Available Balance
                                                        </div>
                                                    </div>
                                                    <div class="nk-iv-wg3-sub">
                                                        <span class="nk-iv-wg3-plus text-soft"><em
                                                                class="icon ni ni-plus"></em></span>
                                                        <div class="nk-iv-wg3-amount">
                                                            <div class="number-sm">
                                                                {{ $settings->currency == 'USD' ? '$' : $settings->currency }}{{ number_format($currentEarning, 2) }}
                                                            </div>
                                                        </div>
                                                        <div class="nk-iv-wg3-subtitle">
                                                            Investment Profits
                                                            <em class="icon ni ni-info-fill" data-toggle="tooltip"
                                                                data-placement="right"
                                                                title="Profits earned from investments"></em>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- .col -->
                                        <div class="col-lg-7">
                                            <div class="nk-iv-wg3">
                                                <div class="nk-iv-wg3-title mb-5">
                                                    Active Plans
                                                    <em class="icon ni ni-info-fill" data-toggle="tooltip"
                                                        data-placement="right" title="Current Month Profit"></em>

                                                </div>
                                                <div class="nk-iv-wg3-group flex-md-nowrap g-4 ">
                                                    <div class="nk-iv-wg3-sub-group gx-4">
                                                        <div class="nk-iv-wg3-sub mt-5">
                                                            <div class="nk-iv-wg3-amount">
                                                                <div class="number">
                                                                    {{ $settings->currency == 'USD' ? '$' : $settings->currency }}{{ number_format($total_run_profit, 2) }}
                                                                </div>
                                                            </div>
                                                            <div class="nk-iv-wg3-subtitle">
                                                                Total Profit
                                                            </div>
                                                        </div>
                                                        <div class="nk-iv-wg3-sub">
                                                            <span class="nk-iv-wg3-plus text-soft"><em
                                                                    class="icon ni ni-plus"></em></span>
                                                            <div class="nk-iv-wg3-amount">
                                                                <div class="number-sm">
                                                                    {{ $settings->currency == 'USD' ? '$' : $settings->currency }}{{ number_format($total_daily_ern, 2) }}
                                                                </div>
                                                            </div>
                                                            <div class="nk-iv-wg3-subtitle">
                                                                Today's Profit
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-iv-wg3-sub flex-grow-1 ml-md-3">
                                                        <div class="nk-iv-wg3-ck">
                                                            <canvas class="chart-profit" id="profitCM"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- .col -->
                                    </div>
                                    <!-- .row -->
                                </div>
                                <!-- .card-inner -->
                                <div class="card-inner">
                                    <ul class="nk-iv-wg3-nav">
                                        <li>
                                            <a href="{{ route('v2.user.transactions') }}"><em
                                                    class="icon ni ni-notes-alt"></em>
                                                <span>Go to Transactions</span></a>
                                        </li>
                                        <li>
                                            <a href="#"><em class="icon ni ni-growth"></em>
                                                <span>Analytic Reports</span></a>
                                        </li>
                                        {{-- <li>
                      <a href="#"
                        ><em class="icon ni ni-report-profit"></em>
                        <span>Monthly Statement</span></a
                      >
                    </li>
                    <li>
                      <a href="#"
                        ><em class="icon ni ni-help"></em>
                        <span>Investment Tips</span></a
                      >
                    </li> --}}
                                    </ul>
                                </div>
                                <!-- .card-inner -->
                            </div>
                            <!-- .card-inner-group -->
                        </div>
                        <!-- .card -->
                    </div>
                    <!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head-sm">
                            <div class="nk-block-head-content">
                                <h5 class="nk-block-title">
                                    Investment History <span class="count text-base">({{ $runInv->count() }})</span>
                                </h5>
                            </div>
                        </div>
                        @if (!$runInv->isEmpty())
                            <div class="nk-iv-scheme-list">
                                @foreach ($runInv as $inv)
                                    <?php
                                    $totalElapse = getDays(date('Y-m-d'), $inv->end_date);
                                    $perc = 0;
                                    $dailyErn = 0;
                                    $currentEarnings = 0;
                                    
                                    if ($totalElapse == 0) {
                                        $lastWD = $inv->last_wd;
                                        $enddate = $inv->end_date;
                                        $Edays = getDays($lastWD, $enddate);
                                        $ern = $Edays * $inv->interest * $inv->capital;
                                        $withdrawable = $ern;
                                        $totalDays = getDays($inv->date_invested, $inv->end_date);
                                        $date_invested = \Carbon\Carbon::createFromFormat('Y-m-d', $inv->date_invested);
                                        $enddate = \Carbon\Carbon::createFromFormat('Y-m-d', $inv->end_date);
                                        $ended = 'Yes';
                                        $perc = 100;
                                        $currentEarnings = $ern;
                                        $dailyErn = 0;
                                    } else {
                                        $lastWD = $inv->last_wd;
                                        $enddate = date('Y-m-d');
                                        $Edays = getDays($lastWD, $enddate);
                                        $ern = $Edays * $inv->interest * $inv->capital;
                                        $withdrawable = 0;
                                        if ($Edays >= $inv->days_interval) {
                                            $withdrawable = $inv->days_interval * $inv->interest * $inv->capital;
                                        }
                                    
                                        $totalDays = getDays($inv->date_invested, date('Y-m-d'));
                                        $date_invested = \Carbon\Carbon::createFromFormat('Y-m-d', $inv->date_invested);
                                        $enddate = \Carbon\Carbon::createFromFormat('Y-m-d', $inv->end_date);
                                        $ended = 'No';
                                        $perc = ($totalDays / $inv->days_interval) * 100;
                                        $period = $inv->package->period;
                                        $dailyErn = round($inv->i_return / $inv->days_interval, 6);
                                        $currentEarnings = $dailyErn * $totalDays;
                                    }
                                    
                                    ?>


                                    <div class="nk-iv-scheme-item">
                                        <div class="nk-iv-scheme-icon is-running">
                                            <em class="icon ni ni-update"></em>
                                        </div>
                                        <div class="nk-iv-scheme-info">
                                            <div class="nk-iv-scheme-name">
                                                {{ $inv->package->package_name }} - Daily {{ $inv->interest }}% for
                                                {{ $inv->period }} Days
                                            </div>
                                            <div class="nk-iv-scheme-desc">
                                                Invested Amount - <span class="amount">
                                                    {{ $settings->currency == 'USD' ? '$' : '' }}
                                                    {{ number_format($inv->capital, 2) }}</span>
                                            </div>
                                        </div>
                                        <div class="nk-iv-scheme-term">
                                            <div class="nk-iv-scheme-start nk-iv-scheme-order">
                                                <span class="nk-iv-scheme-label text-soft">Start Date</span>
                                                <span
                                                    class="nk-iv-scheme-value date">{{ $date_invested->format('M D Y') }}</span>
                                            </div>
                                            <div class="nk-iv-scheme-end nk-iv-scheme-order">
                                                <span class="nk-iv-scheme-label text-soft">End Date</span>
                                                <span
                                                    class="nk-iv-scheme-value date">{{ $enddate->format('M D Y') }}</span>
                                            </div>
                                        </div>
                                        <div class="nk-iv-scheme-amount">
                                            <div class="nk-iv-scheme-amount-a nk-iv-scheme-order">
                                                <span class="nk-iv-scheme-label text-soft">Total Return</span>
                                                <span class="nk-iv-scheme-value amount">
                                                    {{ $settings->currency == 'USD' ? '$' : $settings->currency }}
                                                    {{ number_format($inv->i_return, 2) }}</span>
                                            </div>
                                            <div class="nk-iv-scheme-amount-b nk-iv-scheme-order">
                                                <span class="nk-iv-scheme-label text-soft">Net Profit Earn</span>
                                                <span
                                                    class="nk-iv-scheme-value amount">{{ $settings->currency == 'USD' ? '$' : $settings->currency }}
                                                    {{ number_format($currentEarnings, 2) }}
                                                    <span class="amount-ex"><em class="icon ni ni-plus"></em>
                                                        {{ $settings->currency == 'USD' ? '$' : $settings->currency }}{{ number_format($dailyErn, 2) }}
                                                        <span class="change up"><span class="sign"></span>daily</span>
                                                    </span></span>
                                            </div>
                                        </div>
                                        <div class="nk-iv-scheme-more">
                                            <a class="btn btn-icon btn-lg btn-round btn-trans"
                                                href="{{ route('v2.investments.single', $inv->id) }}"><em
                                                    class="icon ni ni-forward-ios"></em></a>
                                        </div>
                                        <div class="nk-iv-scheme-progress">
                                            <div class="progress-bar" data-progress="{{ round($perc, 2) }}"></div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="text-center mt-5 mb-5">
                                    <em class="icon ni ni-map-pin" style="font-size: 70px;"></em><br><br>
                                    <p>You do not have any active investments yet, <a
                                            href="{{ route('v2.user.invest.page') }}">Click Here</a> to get started</p>
                                </div>
                        @endif



                        <!-- .nk-iv-scheme-item -->
                    </div>
                    <!-- .nk-iv-scheme-list -->
                </div>
                @if (!$endInv->isEmpty())
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head-sm">
                            <div class="nk-block-between">
                                <div class="nk-block-head-content">
                                    <h5 class="nk-block-title">
                                        Recently Ended <span class="count text-base">({{ $endInv->count() }})</span>
                                    </h5>
                                </div>

                            </div>
                        </div>


                    </div>
                    <div class="nk-iv-scheme-list">
                        @foreach ($endInv as $inv)
                            <?php
                            $totalElapse = getDays(date('Y-m-d'), $inv->end_date);
                            $perc = 0;
                            $dailyErn = 0;
                            $currentEarnings = 0;
                            
                            if ($totalElapse == 0) {
                                $lastWD = $inv->last_wd;
                                $enddate = $inv->end_date;
                                $Edays = getDays($lastWD, $enddate);
                                $ern = $Edays * $inv->interest * $inv->capital;
                                $withdrawable = $ern;
                                $totalDays = getDays($inv->date_invested, $inv->end_date);
                                $date_invested = \Carbon\Carbon::createFromFormat('Y-m-d', $inv->date_invested);
                                $enddate = \Carbon\Carbon::createFromFormat('Y-m-d', $inv->end_date);
                                $ended = 'Yes';
                                $perc = 100;
                                $currentEarnings = $ern;
                                $dailyErn = 0;
                            } else {
                                $lastWD = $inv->last_wd;
                                $enddate = date('Y-m-d');
                                $Edays = getDays($lastWD, $enddate);
                                $ern = $Edays * $inv->interest * $inv->capital;
                                $withdrawable = 0;
                                if ($Edays >= $inv->days_interval) {
                                    $withdrawable = $inv->days_interval * $inv->interest * $inv->capital;
                                }
                            
                                $totalDays = getDays($inv->date_invested, date('Y-m-d'));
                                $date_invested = \Carbon\Carbon::createFromFormat('Y-m-d', $inv->date_invested);
                                $enddate = \Carbon\Carbon::createFromFormat('Y-m-d', $inv->end_date);
                                $ended = 'No';
                                $perc = ($totalDays / $inv->days_interval) * 100;
                                $period = $inv->package->period;
                                $dailyErn = round($inv->i_return / $inv->days_interval, 6);
                                $currentEarnings = $dailyErn * $totalDays;
                            }
                            
                            ?>


                            <div class="nk-iv-scheme-item">
                                <div class="nk-iv-scheme-icon is-done">
                                    <em class="icon ni ni-offer"></em>
                                </div>
                                <div class="nk-iv-scheme-info">
                                    <div class="nk-iv-scheme-name">
                                        {{ $inv->package->package_name }} - Daily {{ $inv->interest }}% for
                                        {{ $inv->period }} Days
                                    </div>
                                    <div class="nk-iv-scheme-desc">
                                        Invested Amount - <span class="amount">
                                            {{ $settings->currency == 'USD' ? '$' : '' }}
                                            {{ number_format($inv->capital, 2) }}</span>
                                    </div>
                                </div>
                                <div class="nk-iv-scheme-term">
                                    <div class="nk-iv-scheme-start nk-iv-scheme-order">
                                        <span class="nk-iv-scheme-label text-soft">Start Date</span>
                                        <span class="nk-iv-scheme-value date">{{ $date_invested->format('M D Y') }}</span>
                                    </div>
                                    <div class="nk-iv-scheme-end nk-iv-scheme-order">
                                        <span class="nk-iv-scheme-label text-soft">End Date</span>
                                        <span class="nk-iv-scheme-value date">{{ $enddate->format('M D Y') }}</span>
                                    </div>
                                </div>
                                <div class="nk-iv-scheme-amount">
                                    <div class="nk-iv-scheme-amount-a nk-iv-scheme-order">
                                        <span class="nk-iv-scheme-label text-soft">Total Return</span>
                                        <span class="nk-iv-scheme-value amount">
                                            {{ $settings->currency == 'USD' ? '$' : $settings->currency }}
                                            {{ number_format(round($inv->i_return, 6)) }}</span>
                                    </div>
                                    <div class="nk-iv-scheme-amount-b nk-iv-scheme-order">
                                        <span class="nk-iv-scheme-label text-soft">Net Profit Earn</span>
                                        <span
                                            class="nk-iv-scheme-value amount">{{ $settings->currency == 'USD' ? '$' : $settings->currency }}
                                            {{ number_format($currentEarnings, 2) }}
                                        </span>
                                    </div>
                                </div>
                                <div class="nk-iv-scheme-more">
                                    <a class="btn btn-icon btn-lg btn-round btn-trans"
                                        href="{{ route('v2.investments.single', $inv->id) }}"><em
                                            class="icon ni ni-forward-ios"></em></a>
                                </div>

                            </div>
                        @endforeach

                        {{-- {{ $endInv->links() }} --}}

                        <!-- .nk-iv-scheme-list -->
                    </div>
                @endif
            </div>
        </div>
    </div>
    </div>
@endsection
@section('script')

    <script>
        var inv_dates = [];
        var inv_vals = [];

        var inv = '{!! json_encode($myInv) !!}';
        var js_inv = JSON.parse(inv);

        $.each(js_inv, function(k, val) {
            // $('#prnt').append(', ' +ky+": "+val['created_at']);
            var dt = moment(new Date(val["created_at"])).format("MM/YY"); //new Date(val['created_at']);
            inv_dates[k] = dt; // dt.getMonth() + '/'+ dt.getFullYear();
            inv_vals[k] = val["capital"];
        });



        var dummy_data = [123, 567, 869, 966, 960, 98]
        var dummy_dates = ['Jan', 'Feb', 'Mar', 'Apr', 'May']

        var _get_data = typeof inv_vals === '' ? eval(dummy_data) : inv_vals;
        var _get_label = typeof inv_dates === '' ? eval(dummy_data) : inv_dates;

        var chart_data = []

        var selectCanvas = document.getElementById("profitCM").getContext("2d");


        chart_data.push({
            label: "Investments ",
            tension: .4,
            backgroundColor: NioApp.hexRGB('#2f55d4', .3),
            borderWidth: 2,
            borderColor: NioApp.hexRGB('#2f55d4', .3),
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'transparent',
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: 'blue',
            pointBorderWidth: 2,
            pointHoverRadius: 4,
            pointHoverBorderWidth: 2,
            pointRadius: 4,
            pointHitRadius: 4,
            data: _get_data
        });


        var chart = new Chart(selectCanvas, {
            type: 'line',
            data: {
                labels: _get_label,
                datasets: chart_data
            },
            options: {
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                tooltips: {
                    enabled: true,
                    rtl: NioApp.State.isRTL,
                    callbacks: {
                        title: function title(tooltipItem, data) {
                            return false;
                        },
                        label: function label(tooltipItem, data) {
                            return data.datasets[tooltipItem.datasetIndex]['data'][tooltipItem['index']] + ' ' +
                                _get_data.dataUnit;
                        }
                    },
                    backgroundColor: '#fff',
                    titleFontSize: 11,
                    titleFontColor: '#6783b8',
                    titleMarginBottom: 4,
                    bodyFontColor: '#9eaecf',
                    bodyFontSize: 10,
                    bodySpacing: 3,
                    yPadding: 8,
                    xPadding: 8,
                    footerMarginTop: 0,
                    displayColors: false
                },
                scales: {
                    yAxes: [{
                        display: false,
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                    xAxes: [{
                        display: false,
                        ticks: {
                            reverse: NioApp.State.isRTL
                        }
                    }]
                }
            }
        });
    </script>

@endsection
@section('script')
    <script src="{{ asset('/js/app.js') }}"></script>
@endsection
