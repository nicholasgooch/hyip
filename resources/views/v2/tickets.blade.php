@extends('v2.layouts.main')
@section('title', 'Support Tickets')
@section('style')   
<style>
    .new_not{
	color:red; position: absolute;
	font-size: 10px !important;
    top: 2px;
    margin-left: -3px;
	
}

</style>
    
@endsection
@section('content')
<div class="nk-content nk-content-lg nk-content-fluid" id="app">
    <div class="container-xl wide-lg">
      <div class="nk-content-inner">
        <div class="nk-content-body">
          <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-content">
              <div class="nk-block-head-sub">
                <a href="{{route('v2.index')}}" class="back-to"
                  ><em class="icon ni ni-arrow-left"></em
                  ><span>Back to Overview</span></a
                >
              </div>
              <div class="nk-block-head-content">
                <h2 class="nk-block-title fw-normal">
                    Tickets

                </h2>
              </div>
            </div>
          </div>
          <div class="nk-block  mb-3">
            <div id="prnt"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-head-row">
                                <h4 class="card-title col-sm-12"  >{{ __('Open Ticket') }} 
                                    <span class="float-right"><a data-target="#open_ticket" data-toggle="modal" href="javascript:void(0)" class="btn btn-primary text-white"><i class="fas fa-plus-circle "></i>New Ticket</a></span>
                                </h4>
                            </div>
                             
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">                                        
                                <table  class=" display table table-striped table-hover" >
                                    <thead>
                                        <tr>                                                   
                                            <th>{{ __('Ticket ID') }}</th> 
                                            <th>{{ __('Title') }}</th>
                                            <th>{{ __('Status') }}</th>
                                            <th>{{ __('Action') }}</th>                                   
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @if(!empty($tickets))
                                            @foreach($tickets as $ticket)
                                                <tr>
                                                    <td>{{$ticket->ticket_id}}</td>
                                                    <td>{{$ticket->title}}</td>
                                                    <td>
                                                        @if($ticket->status == 0)
                                                            {{__('Closed')}}
                                                        @elseif($ticket->status == 1)
                                                            {{__('Open')}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a title="View Chat" href="{{route('v2.ticket.open', $ticket->id)}}" class="btn btn-warning">
                                                            <em class="icon ni ni-msg-circle-fill"></em>
                                                            @php($rd=0)
                                                            @foreach($ticket->comments as $comment)
                                                                @if($comment->state == 1 && $comment->sender == 'support')
                                                                    @php($rd = 1)
                                                                @endif
                                                            @endforeach
                                                            @if(isset($rd) && $rd == 1)
                                                                <em class="icon ni ni-circle-fill new_not"></em>
                                                                @php($rd = 0)
                                                            @endif
                                                        </a>
                                                        @if($ticket->status == 0)
                                                            <a title="Close Ticket" href="{{ route('v2.close.ticket', $ticket->id)}}" class="btn btn-warning">
                                                                <em class="icon ni ni-na"></em>
                                                            </a>
                                                        @endif
                                                        
                                                    </td>                                                                                 
                                                </tr>
                                            @endforeach
                                        @else
                                            
                                        @endif
                                    </tbody>
                                </table>
                                
                            </div>
                            {{$tickets->links()}}
                        </div>
                    </div>
                </div>
                
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
   <!-- Modal -->
   <div class="modal fade" id="open_ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Open a new ticket</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="text-danger">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" method="POST" role="form" action="{{ route('v2.ticket.create') }}" >
              @csrf
              <div class="form-group {{ $errors->has('amount') ? ' has-error' : '' }}">
                  <label class="control-label">{{ __('Title') }}</label>                            
                  <div class="input-group">
                      <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fa fa-pen-alt"></i></span>
                      </div>
                      <input type="text" class="form-control" name="title" value="" required autofocus>                    
                  </div>
              </div>
              <div class="form-group ">
                  <label class="control-label">{{ __('Message') }}</label>                            
                  <div class="input-group">                               
                      <textarea name="msg" class="form-control" required></textarea>                                                   
                  </div>
              </div>
              <div class="form-group">                                                                                                           
                  <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>  
              </div>
          </form>
        </div>
        
      </div>
    </div>
  </div>

@endsection