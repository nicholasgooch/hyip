@extends('v2.layouts.main')
@section('title', 'Ticket Chat')
@section('style')
<style>
    .chat_msg {
        position: relative;
        width: 80%;
        top: 0px;
        left: 10%;
        border-radius: 75px;
        background-color: #4083db !important;
        margin: 10px;
        color: white;
    }

    .chat_container {
        background-color: #F5F5F9;
        border: 1px solid #DDD;
        background-image: url('/img/chat_bg.jpg');
    }

    .chat_msg_container {
        height: 500px;
        margin-top: 10px;
    }

    .chat_bubble {
        max-width: 80%;
        min-width: 45%;
        border-radius: 10px;
    }

    .msg_entry {
        height: 30px;
        font-size: 13px;
    }

    .with_100 {
        min-width: 100%;
    }

    .avatar_chat {
        height: 30px;
        width: 30px;
    }

    /*========================== chat bubbles================================*/

    .talk-bubble {
        margin: 10px;
        display: inline-block;
        position: relative;
        max-width: 70%;
        min-width: 40%;
        height: auto;
        background-color: lightyellow;
    }

    .border {
        border: 8px solid #666;
    }

    .round {
        border-radius: 30px;
        -webkit-border-radius: 30px;
        -moz-border-radius: 30px;

    }

    /* Right triangle placed top left flush. */
    .tri-right.border.left-top:before {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: -40px;
        right: auto;
        top: -8px;
        bottom: auto;
        border: 32px solid;
        border-color: #666 transparent transparent transparent;
    }

    .tri-right.left-top:after {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: -20px;
        right: auto;
        top: 0px;
        bottom: auto;
        border: 22px solid;
        border-color: lightyellow transparent transparent transparent;
    }

    /* Right triangle, left side slightly down */
    .tri-right.border.left-in:before {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: -40px;
        right: auto;
        top: 30px;
        bottom: auto;
        border: 20px solid;
        border-color: #666 #666 transparent transparent;
    }

    .tri-right.left-in:after {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: -20px;
        right: auto;
        top: 38px;
        bottom: auto;
        border: 12px solid;
        border-color: lightyellow lightyellow transparent transparent;
    }

    /*Right triangle, placed bottom left side slightly in*/
    .tri-right.border.btm-left:before {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: -8px;
        right: auto;
        top: auto;
        bottom: -40px;
        border: 32px solid;
        border-color: transparent transparent transparent #666;
    }

    .tri-right.btm-left:after {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: 0px;
        right: auto;
        top: auto;
        bottom: -20px;
        border: 22px solid;
        border-color: transparent transparent transparent lightyellow;
    }

    /*Right triangle, placed bottom left side slightly in*/
    .tri-right.border.btm-left-in:before {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: 30px;
        right: auto;
        top: auto;
        bottom: -40px;
        border: 20px solid;
        border-color: #666 transparent transparent #666;
    }

    .tri-right.btm-left-in:after {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: 38px;
        right: auto;
        top: auto;
        bottom: -20px;
        border: 12px solid;
        border-color: lightyellow transparent transparent lightyellow;
    }

    /*Right triangle, placed bottom right side slightly in*/
    .tri-right.border.btm-right-in:before {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: auto;
        right: 30px;
        bottom: -40px;
        border: 20px solid;
        border-color: #666 #666 transparent transparent;
    }

    .tri-right.btm-right-in:after {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: auto;
        right: 38px;
        bottom: -20px;
        border: 12px solid;
        border-color: lightyellow lightyellow transparent transparent;
    }

    /*Right triangle, placed bottom right side slightly in*/
    .tri-right.border.btm-right:before {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: auto;
        right: -8px;
        bottom: -40px;
        border: 20px solid;
        border-color: #666 #666 transparent transparent;
    }

    .tri-right.btm-right:after {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: auto;
        right: 0px;
        bottom: -20px;
        border: 12px solid;
        border-color: lightyellow lightyellow transparent transparent;
    }

    /* Right triangle, right side slightly down*/
    .tri-right.border.right-in:before {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: auto;
        right: -40px;
        top: 30px;
        bottom: auto;
        border: 20px solid;
        border-color: #666 transparent transparent #666;
    }

    .tri-right.right-in:after {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: auto;
        right: -20px;
        top: 38px;
        bottom: auto;
        border: 12px solid;
        border-color: lightyellow transparent transparent lightyellow;
    }

    /* Right triangle placed top right flush. */
    .tri-right.border.right-top:before {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: auto;
        right: -40px;
        top: -8px;
        bottom: auto;
        border: 32px solid;
        border-color: #666 transparent transparent transparent;
    }

    .tri-right.right-top:after {
        content: ' ';
        position: absolute;
        width: 0;
        height: 0;
        left: auto;
        right: -20px;
        top: 0px;
        bottom: auto;
        border: 20px solid;
        border-color: lightyellow transparent transparent transparent;
    }

    /* talk bubble contents */
    .talktext {
        padding: 1em;
        text-align: left;
        line-height: 1.5em;
    }

    .talktext p {
        /* remove webkit p margins */
        -webkit-margin-before: 0em;
        -webkit-margin-after: 0em;
    }

</style>
@endsection
@section('content')
<div class="nk-content nk-content-lg nk-content-fluid" id="app">
    <div class="container-xl wide-lg">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-lg">
                    <div class="nk-block-head-content">
                        <div class="nk-block-head-sub">
                            <a href="{{route('v2.ticket')}}" class="back-to"><em
                                    class="icon ni ni-arrow-left"></em><span>Back to Tickets</span></a>
                        </div>
                        <div class="nk-block-head-content">
                            <h2 class="nk-block-title fw-normal">
                               Opened <b class="text-muted">{{$ticket_view->msg}}</b> Ticket

                            </h2>
                        </div>
                    </div>
                </div>
                <div class="nk-block  mb-3">
                    <div id="prnt"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-head-row">
                                        <div class="card-title col-sm-12">{{ __('Ticket Chat') }}
                                        </div>
                                    </div>

                                </div>
                                <div class="card-body ">
                                    <div class='p-2 text-center bg_white chat_msg'>
                                        <strong>Ticket Issue Messages</strong><br>
                                        Title: {{$ticket_view->msg}}
                                    </div>
                                    <div class="pl-3 pr-3 chat_container">
                                        <div id="chat_msg_container"
                                            class=" pt-1 chat_msg_container scroll scrollbar-inner ">
                                            @if(!empty($ticket_view))
                                            @foreach($comments as $comment)
                                            @if($comment->sender == "user")

                                            <div class="row col-sm-12 d-flex justify-content-end">
                                                <div class="talk-bubble tri-right right-top">
                                                    <div class="talktext">
                                                        <p>
                                                            {{$comment->message}}
                                                        </p>
                                                        <p class="font_10 float-left"><i>{{$comment->created_at}}</i>
                                                        </p>
                                                    </div>
                                                </div>
                                                <p class="mg_top_30">
                                                    <img src="{{$user->img }}" alt="..."
                                                        class="avatar_chat rounded-circle">
                                                </p>
                                            </div>
                                            @else
                                            <div class="row col-sm-12">
                                                <p class="mg_top_30">
                                                    <img src="{{$settings->site_logo}}" alt="..."
                                                        class="avatar_chat rounded-circle">
                                                </p>
                                                <div class="talk-bubble tri-right left-top">
                                                    <div class="talktext">
                                                        <p class="p-0">
                                                            {{$comment->message}}
                                                        </p>
                                                        <small
                                                            class="font_10 p-0 float-right"><i>{{$comment->created_at}}</i></small>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            @endforeach
                                            @endif
                                        </div>
                                        <div class="col-sm-12 mt-5">
                                            <form id="comment_form" class="form-horizontal" method="POST" role="form"
                                                action="{{ route('v2.ticket.comment') }}">
                                                <div
                                                    class="form-group {{ $errors->has('amount') ? ' has-error' : '' }}">
                                                    <div class="input-group">
                                                        <input id="ticket_id" type="hidden" class="form-control"
                                                            name="ticket_id" value="{{$ticket_view->id}}" required
                                                            autofocus>
                                                        <input type="text" id="msg_entry"
                                                            class="form-control form-control-lg" name="msg" autofocus
                                                            placeholder="Your Message">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <button type="submit" class="btn btn-primary"
                                                                    onclick="post_comment('user')"><em
                                                                        class="icon ni ni-send"></em></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">

                                                </div>
                                            </form>
                                            <audio id="buzzer" src="/sound/swiftly.mp3" type="audio/mp3"></audio>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    function load_chat() {
        var id = $('#ticket_id').val();
        $.ajax({
            url: "{!! route('v2.load_chat' ) !!}",
            type: 'get',
            data: {
                "id": id
            },
            dataType: 'json',
            success: function (result) {
                if (Object.keys(result).length > 0) {
                    var i = 0;
                    while (i < Object.keys(result).length) {
                        $('#chat_msg_container').append(
                            '<div class="row col-sm-12">\
                            <p class="mg_top_30">\
                               <img src="/img/logo.png" alt="..." class="avatar_chat rounded-circle">\
                            </p>\
                            <div class="talk-bubble tri-right left-top ">\
                              <div class="talktext">\
                                <p class="p-0">\
                                    ' + result[i].message + '\
                                </p>\
                                <small class="font_10 p-0 float-right"><i>' + result[i].created_at + '</i></small>\
                              </div>\
                            </div>\
                        </div>'
                        );
                        i = i + 1;
                    }
                    $('#buzzer').get(0).play();
                }

            },
            error: function (xhr) {

            }
        });
    }

    function post_comment(user) {
        // $('#p_loading').show();
        var data = new FormData(document.getElementById('comment_form'));
        $.ajax({
            url: $('#comment_form').attr('action'),
            type: "post",
            data: data,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result.toast_type == 'suc' && user == 'support') {
                    $('#chat_msg_container').append(
                        '<div class="row col-sm-12 d-flex justify-content-end">\
                        <div class="talk-bubble tri-right right-top ">\
                          <div class="talktext">\
                            <p class="p-0">\
                                ' + result.comment_msg + '\
                            </p>\
                            <small class="font_10 p-0"><i>' + result.comment_time + '</i></small>\
                          </div>\
                        </div>\
                        <p class="mg_top_30">\
                           <img src="'+{{$settings->site_logo}}+'" alt="..." class="avatar_chat rounded-circle">\
                        </p>             \
                    </div>'
                    );
                    $("#chat_msg_container").animate({
                        scrollTop: $('#chat_msg_container')[0].scrollHeight
                    }, 1000);
                    $('#msg_entry').val('');
                } else if (result.toast_type == 'suc' && user == 'user') {
                    $('#chat_msg_container').append(
                        '<div class="row col-sm-12 d-flex justify-content-end">\
                        <div class="talk-bubble tri-right right-top ">\
                          <div class="talktext">\
                            <p class="p-0">\
                                ' + result.comment_msg + '\
                            </p>\
                            <small class="font_10 p-0"><i>' + result.comment_time + '</i></small>\
                          </div>\
                        </div>\
                        <p class="mg_top_30">\
                           <img src="'+result.user_img + '" alt="..." class="avatar_chat rounded-circle">\
                        </p>\
                    </div>'
                    );
                    $("#chat_msg_container").animate({
                        scrollTop: $('#chat_msg_container')[0].scrollHeight
                    }, 1000);
                    $('#msg_entry').val('');
                } else {
                    $('#err').html(result.toast_msg)
                    $('#err').show().animate({
                        width: "30%"
                    }, "1000").delay(6000).fadeOut(1000);
                }

            },
            error: function (xhr) {

            }
        });

    };
    $(document).ready(function () {
        var timeout = 5000;
        setInterval(function () {
            load_chat();
            $("#chat_msg_container").animate({
                scrollTop: $('#chat_msg_container')[0].scrollHeight
            }, 1000);
        }, timeout);
    });
    $("#chat_msg_container").animate({
        scrollTop: $('#chat_msg_container')[0].scrollHeight
    }, 1000);
    $('#comment_form').submit(function (e) {
        e.preventDefault();
    });

</script>
@endsection
