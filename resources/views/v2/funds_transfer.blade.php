@extends('v2.layouts.main')
@section('title', 'Transfer Funds')
@section('content')
<div class="nk-content">
    <div class="container">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-lg">
                    <div class="nk-block-head-content">
                        <div class="nk-block-head-sub">
                            <a href="{{route('v2.index')}}" class="back-to"><em
                                    class="icon ni ni-arrow-left"></em><span>Back to Overview</span></a>
                        </div>
                        <div class="nk-block-head-content">
                            <h2 class="nk-block-title fw-normal">
                                Send Funds

                            </h2>
                        </div>
                    </div>
                </div>
                <div class="nk-block mb-3">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-bordered">
                                <div class="card-inner">
                                    <div class="card-title-group mb-1">
                                        <div class="card-title">
                                            <h6 class="title">Fund Transfer</h6>
                                            <p>Send Money to any user on the {{$settings->site_title}} platform free of charge. </p>
                                        </div>
                                    </div>
                                    <hr>
                                    @if(Session::has('err_send'))
                                    <div class="alert alert-danger">
                                        {{Session::get('err_send')}}
                                    </div>
                                    {{Session::forget('err_send')}}
                                    @endif
                                    <div class="">

                                        <form action="{{route('v2.funds_transfer.post')}}" method="post"
                                            enctype="multipart/form-data">
                                            <div class="form-group" align="left">
                                                <input type="hidden" class="regTxtBox" name="_token"
                                                    value="{{csrf_token()}}">
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text "><em
                                                                class="icon ni ni-user-alt-fill"></em></span>
                                                    </div>
                                                    <input type="text" class="form-control" name="usn" required
                                                        placeholder="Username">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group pad_top10">
                                                    <div class="input-group-prepend">
                                                        <span class=" input-group-text ">{{$settings->currency}}</span>
                                                    </div>
                                                    <input type="text" class="form-control" name="s_amt" required
                                                        placeholder="Enter amount you want to send">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <br><br>
                                                <button class="btn btn-primary btn-lg">{{ __('Send') }}</button>
                                                <br>
                                            </div>
                                        </form>
                                        <br><br>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-7 ">
                          <div class="card card-bordered">
                            <div class="card-inner">
                                <div class="card-title-group mb-1">
                                    <div class="card-title">
                                        <h6 class="title">Tranfer History</h6>
                                        <p>The overview of your transfered funds. 
                                        </p>
                                    </div>
                                </div>
                                <hr>
                                <?php
                                $trans = App\fund_transfer::where('from_usr',$user->username)->orwhere('to_usr', $user->username)->orderby('id','desc')->get();
                            ?>
                            <div class="table-responsive"><table id="basic-datatables" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>                
                                            <th>Sender</th>
                                            <th>Receiver</th>
                                            <th>Amount</th>
                                            <th>Date</th>                                                    
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @if(count($trans) > 0 )
                                            @foreach($trans as $activity)
                                                <tr>
                                                    <!-- <td></td> -->
                                                    <td>{{$activity->from_usr}}</td>
                                                    <td>{{$activity->to_usr}}</td>
                                                    <td>{{$settings->currency.' '.$activity->amt}}</td>   
                                                    <td>{{$activity->created_at}}</td> 
                                                </tr>
                                            @endforeach
                                        @else
                                            
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                                   
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
