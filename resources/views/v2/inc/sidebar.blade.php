<div class="card-inner">
    <div class="user-card">
        <div class="user-avatar bg-primary">

            @if($user->img == "")
            {{-- <img class="img-responsive" src="/img/any.png" > --}}
            {{substr(strtoupper($user->firstname),0,1)}}{{substr(strtoupper($user->lastname),0,1)}}
            @else
                <img class="img-responsive" src="{!! $user->img !!}" width="30" height="30">
            @endif
        </div>
        <div class="user-info">
            <span class="lead-text">{{$user->fullname}}</span>
            <span class="sub-text">{{$user->email}}</span>
            <span class="sub-text">{{$user->username}}</span>
        </div>
        <div class="user-action">
            <div class="dropdown">
                <a class="btn btn-icon btn-trigger mr-n2" data-toggle="dropdown" href="#"><em class="icon ni ni-more-v"></em></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <ul class="link-list-opt no-bdr">
                        <li><a href="javascript:void(0)"><label for="fileinput" class="inputfile" ><em class="icon ni ni-camera-fill" style="padding-right: 10px !important;"></em><span>Change Photo</span></label></a></li>
                        <li><a href="javascript:void(0)"  data-toggle="modal" data-target="#profile-edit"><em class="icon ni ni-edit-fill"></em><span>Update Profile</span></a></li>
                    </ul>
                    <form>
                        <input id="fileinput" type="file" style="display:none;"/>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- .user-card -->
</div><!-- .card-inner -->
<div class="card-inner">
    <div class="user-account-info py-0">
        <div class="user-balance">{{number_format(round($user->wallet),2)}} <small class="currency currency-btc">{{$settings->currency}}</small></div>
        <div class="user-balance-sub"><span>{{toBTC(round($user->wallet,2))}} <span class="currency currency-btc"></span>BTC</span></div>
        <div style="padding: 10px; text-align:center;" >Referral Link <br> <span style="color: #c60; font-size: 13px; word-wrap: break-word;"> {{request()->getHttpHost().__('/register/').$user->username}}</span></div>
    </div>
</div><!-- .card-inner -->
<div class="card-inner p-0">
    <ul class="link-list-menu">
        <li><a class="{{active('v2.user.profile')}}" href="{{route('v2.user.profile')}}"><em class="icon ni ni-user-fill-c"></em><span>Personal Infomation</span></a></li>
        <li><a class="{{active('v2.add.with.method')}}"  href="{{route('v2.add.with.method')}}"><em class="icon ni ni-opt-dot-fill"></em><span>Withdrawal Methods</span></a></li>
        <li><a href="{{route('v2.user.activities')}}" class="{{active('v2.user.activities')}}"><em class="icon ni ni-activity-round-fill"></em><span>Account Activity</span></a></li>
        <li><a href="{{route('v2.user.settings')}}" class="{{active('v2.user.settings')}}"><em class="icon ni ni-lock-alt-fill"></em><span>Security Settings</span></a></li>
        <li><a href="#"  data-toggle="modal" data-target="#profile-edit"><em class="icon ni ni ni-user-list-fill"></em><span>Update Profile</span></a></li>
    </ul>
</div><!-- .card-inner -->
