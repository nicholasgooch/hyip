<!-- @@ Profile Edit Modal @e -->
<div class="modal fade" role="dialog" id="profile-edit">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-lg">
                <h5 class="title">Update Profile</h5>
                <ul class="nk-nav nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#personal">Basic</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kyc">KYC</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#wallet">Add Wallet</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#bank">Add Bank</a>
                    </li>
                </ul><!-- .nav-tabs -->
                <div class="tab-content">
                    <div class="tab-pane active" id="personal">
                        <form class="" method="post" action="/user/update/profile" enctype="multipart/form-data">
                            <div class="row gy-4">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="firstname">{{ __('FirstName') }}</label>
                                        <input type="text" class="form-control form-control-lg" id="firstname"
                                            value="{{$user->firstname}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="lastname">{{ __('LastName') }}</label>
                                        <input type="text" class="form-control form-control-lg" id="lastname"
                                            value="{{$user->lastname}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="email">{{ __('Email') }}</label>
                                        <input type="text" class="form-control form-control-lg" id="email"
                                            value="{{$user->email}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="display-name">{{ __('UserName') }}</label>
                                        <input type="text" class="form-control form-control-lg" id="display-name"
                                            value="{{$user->username ?? ""}}" placeholder="Enter display name" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label" for="dob">{{ __('Date of Birth') }}</label>
                                        <input id="dob" type="text" class="form-control form-control-lg"
                                            value="{{$user->kyc->dob ?? ""}}" name="dob" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="country">{{ __('Country') }}</label>
                                        <select id="country" class="form-select form-select-lg" name="country">
                                            <?php 
                                                $country = App\country::orderby('name', 'asc')->get();
$phn_code = "";
                                            ?>
                                            @foreach($country as $c)
                                            @if($c->id == $user->country)
                                            @php($cs = $c->id)
                                            @php($phn_code = $c->phonecode)
                                                {{'selected'}}
                                                <option selected value="{{$c->id}}">{{$c->name}}</option>
                                            @else
                                            <option value="{{$c->id}}">{{$c->name}}</option>
                                            @endif
                                            @endforeach
                                            @if(!isset($cs))
                                                <option selected disabled>{{ __('Select Country') }}</option>
                                            @endif

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="state">{{ __('State/Province') }}</label>
                                        <select id="states" class="form-select form-select-lg" name="state" required>
                                            @if(isset($cs))
                                                <?php 
                                                                            $st = App\states::where('id', $user->state)->get();
                                                                        ?>
                                                @if(count($st) > 0)
                                                    <option selected value="{{$st[0]->id}}">{{$st[0]->name}}</option>
                                                @else
                                                    <option selected disabled>{{ __('Select State') }}</option>
                                                @endif

                                            @else
                                                <option selected disabled>{{ __('Select State') }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label" for="address">{{ __('Address') }}</label>
                                        <input id="adr" type="text" class="form-control form-control-lg"
                                            value="{{$user->address}}" name="adr" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="phone-no">{{ __('Phone Number') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span id="countryCode" class="input-group-text">
                                                    @if(isset($phn_code))
                                                        {{'+' . $phn_code}}
                                                    @else

                                                    @endif
                                                </span>
                                            </div>
                                            <input id="cCode" type="hidden" class="form-control form-control-lg"
                                                name="cCode" required>
                                            <input id="phone" type="text" class="form-control form-control-lg"
                                                value="{{str_replace('+' . $phn_code ?? "", '', $user->phone ?? "")}}"
                                                name="phone" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label" for="zip">{{ __('Zip Code') }}</label>
                                        <input id="zip" type="text" class="form-control form-control-lg"
                                            value="{{$user->kyc->zip ?? ""}}" name="zip" required>
                                    </div>
                                </div>
                                {{-- <div class="col-12">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="latest-sale">
                                        <label class="custom-control-label" for="latest-sale">Use full name to display
                                        </label>
                                    </div>
                                </div> --}}
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Update Profile</button>
                                        </li>
                                        <li>
                                            <a href="#" data-dismiss="modal" class="link link-light">Cancel</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>

                    </div><!-- .tab-pane -->

                    <div class="tab-pane" id="kyc">
                        @if ($user->kyc == null)
                            <div class="kyc-app wide-sm m-auto">
                                <div class="nk-block-head nk-block-head-lg wide-xs mx-auto">
                                    <div class="nk-block-head-content text-center">
                                        <h2 class="nk-block-title fw-normal">KYC Verification</h2>
                                        <div class="nk-block-des">
                                            <p>To comply with regulation each participant will have to go through indentity
                                                verification (KYC/AML) to prevent fraud causes. </p>
                                        </div>
                                    </div>
                                </div><!-- .nk-block-head -->
                                <div class="nk-block">
                                    <div class="card card-bordered">
                                        <div class="card-inner card-inner-lg">
                                            <div class="nk-kyc-app p-sm-2 text-center">
                                                <div class="nk-kyc-app-icon">
                                                    <em class="icon ni ni-files"></em>
                                                </div>
                                                <div class="nk-kyc-app-text mx-auto">
                                                    <p class="lead">You have not submitted your necessary documents to
                                                        verify your identity. In order to purchase our tokens, please verify
                                                        your identity.</p>
                                                </div>
                                                <div class="nk-kyc-app-action">
                                                    <a href="{{route('v2.kyc.form')}}" class="btn btn-lg btn-primary">Click
                                                        here to complete your KYC</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- .card -->

                                    <div class="text-center pt-4">
                                        <p>If you have any question, please contact our support team <a
                                                href="mailto:{{config('app.support_email')}}">{{config('app.support_email')}}</a>
                                        </p>
                                    </div>
                                </div> <!-- .nk-block -->
                            </div><!-- .kyc-app -->
                        @else
                            <form class="">
                                <div class="row gy-4">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="zipcode">{{ __('Zipcode') }}</label>
                                            <input type="text" class="form-control form-control-lg" id="zipcode"
                                                value="{{$user->kyc->zip ?? 'N\A'}}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="dob">{{ __('Date of Birth') }}</label>
                                            <input type="text" class="form-control form-control-lg" id="dob"
                                                value="{{$user->kyc->dob ?? 'N\A'}}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="dob">{{ __('ID Type') }}</label>
                                            <input type="text" class="form-control form-control-lg" id="dob"
                                                value="{{ strtoupper($user->kyc->id_type) ?? 'N\A'}}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6"></div>
                                    @if ($user->kyc->id_type == 'passport')
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6 ">


                                            <div class="nk-kycfm-upload">
                                                <h6 class="title nk-kycfm-upload-title">Passport File</h6>
                                                <div class="row align-items-center">
                                                    <div class="col-sm-12 d-none d-sm-block">
                                                        <div class="mx-md-4">
                                                            <img src="{{$user->kyc !== null ? "N\A" : $user->kyc->font_pics}}"
                                                                alt="passport image">
                                                        </div>
                                                    </div>


                                                </div>
                                            </div><!-- nk-kycfm-upload -->

                                        </div>
                                    @else
                                        <div class="col-md-6 ">


                                            <div class="nk-kycfm-upload">
                                                <h6 class="title nk-kycfm-upload-title">Front</h6>
                                                <div class="row align-items-center">
                                                    <div class="col-sm-12 d-none d-sm-block">
                                                        <div class="mx-md-4">
                                                            <img src="{{$user->kyc->font_pics}}" alt="ID Front">
                                                        </div>
                                                    </div>


                                                </div>
                                            </div><!-- nk-kycfm-upload -->

                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="nk-kycfm-upload">
                                                <h6 class="title nk-kycfm-upload-title">Back </h6>
                                                <div class="row align-items-center">
                                                    <div class="col-sm-12 d-none d-sm-block">
                                                        <div class="mx-md-4">
                                                            <img src="{{$user->kyc->back_pics}}" alt="ID Front">
                                                        </div>
                                                    </div>


                                                </div>
                                            </div><!-- nk-kycfm-upload -->
                                        </div>
                                    @endif
                                </div>
                            </form>
                        @endif
                    </div><!-- .tab-pane -->
                    <div class="tab-pane" id="wallet">
                        <div class="row gy-4">
                            <div class="col-md-6">
                                <?php 
                                            $wallets = App\banks::where('user_id', $user->id)->where('type', '!=', 'bank')->get();
                                            
                                        ?>

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Asset</th>
                                            <th scope="col">Coin Host</th>
                                            <th scope="col">Wallet Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($wallets as $index => $wallet)
                                            <tr>
                                                <th scope="row">{{round((int) $index + 1)}}</th>
                                                <td>{{$wallet->Account_name}}</td>
                                                <td data-toggle="tooltip" data-placement="top"
                                                    title="{{$wallet->Bank_Name}}">{{substr($wallet->Bank_Name, 0, 7)}}..
                                                </td>
                                                <td data-toggle="tooltip" data-placement="top"
                                                    title="{{$wallet->Account_number}}">
                                                    {{substr($wallet->Account_number, 0, 7)}}...
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <form id="wallet-form" method="post" action="{{route('v2.user.add_wallet')}}">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{$error}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="form-group">
                                        <label class="form-label" for="host">{{ __('Coin Host') }}</label>
                                        <input type="text" class="form-control form-control-lg" id="coin_host"
                                            value="{{old('coin_host')}}" name="coin_host"
                                            placeholder="Blockchain,Paxful">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="asset">{{ __('Asset') }}</label>
                                        <select name="asset_type" id="asset" class="form-select">
                                            <option value="btc">Bitcoin (BTC)</option>
                                            <option value="eth"> Ethereum (ETH)</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="coin_wallet">{{ __('Wallet Address') }}</label>
                                        <input type="text" class="form-control form-control-lg" id="coin_wallet"
                                            value="{{old('coin_wallet')}}" name="coin_wallet" placeholder="44243">
                                    </div>
                                    <div class="form-group">

                                        <input type="submit" class="btn btn-primary" value="Add Wallet">
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                    <div class="tab-pane" id="bank">
                        <div class="row gy-4">
                            <div class="col-md-6">
                                <?php 
                                        $banks = App\banks::where('user_id', $user->id)->where('type', 'bank')->get();
                                        
                                    ?>

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Account Name</th>
                                            <th scope="col">Account Number</th>
                                            <th scope="col">Bank Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($banks as $index => $bank)
                                            <tr>
                                                <th scope="row">{{round((int) $index + 1)}}</th>
                                                <td>{{$bank->Account_name}}</td>
                                                <td data-toggle="tooltip" data-placement="top"
                                                    title="{{$bank->Account_number}}">
                                                    {{substr($bank->Account_number, 0, 7)}}..
                                                </td>
                                                <td data-toggle="tooltip" data-placement="top" title="{{$bank->Bank_Name}}">
                                                    {{substr($bank->Bank_Name, 0, 7)}}...
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <form id="wallet-form" method="post" action="{{route('v2.user.add_bank')}}">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{$error}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="form-group">
                                        <label class="form-label" for="host">{{ __('Bank Name') }}</label>
                                        <input type="text" class="form-control form-control-lg" id="bank_name"
                                            value="{{old('bank_name')}}" name="bank_name" placeholder="Bank of America">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="account_name">{{ __('Account Name') }}</label>
                                        <input type="text" class="form-control form-control-lg" id="account_name"
                                            value="{{old('account_name')}}" name="account_name"
                                            placeholder="John Spencer">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label"
                                            for="account_number">{{ __('Account Number') }}</label>
                                        <input type="text" class="form-control form-control-lg" id="account_number"
                                            value="{{old('account_number')}}" name="account_number"
                                            placeholder="019********">
                                    </div>
                                    <div class="form-group">

                                        <input type="submit" class="btn btn-primary" value="Add Bank">
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div><!-- .tab-content -->
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div><!-- .modal -->