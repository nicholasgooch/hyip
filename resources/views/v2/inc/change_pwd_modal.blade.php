    <!-- @@ Profile Edit Modal @e -->
    <div class="modal fade" role="dialog" id="password-edit">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body ">
                    <div class="card">
                        <div class="card-inner">
                            <div class="card-title text-centered">

                             <h5 class="title">Update Password</h5>
                            </div>
                 
                            
                                <form class="" method="post" action="{{route('v2.profile.change_pwd.post')}}">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="row gy-4">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                         
                                            <label>{{ __('Old Password') }}</label>
                                            <input type="password" class="form-control" name="oldpwd" placeholder="Your current password" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label" for="new_password">{{ __('New Password') }}</label>
                                            <input type="password" class="form-control" name="newpwd" placeholder="New Password" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label" for="conf_password">{{ __('Confirm Password') }}</label>
                                            <input type="password" class="form-control form-control-lg" id="conf_password" name="cpwd">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-lg btn-primary" value="Submit">
                                        </div>
                                    </div>
                                </div>
                            </form>
                           
                        </div><!-- .tab-pane -->
                       
                        
                    </div><!-- .tab-content -->
                </div><!-- .modal-body -->
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- .modal -->