<div class="nk-header nk-header-fluid nk-header-fixed is-theme">
    <div class="container-xl wide-lg">
        <div class="nk-header-wrap">
            <div class="nk-menu-trigger mr-sm-2 d-lg-none">
                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="headerNav"><em
                        class="icon ni ni-menu"></em></a>
            </div>
            <div class="nk-header-brand">

                <a href="/" class="logo-link">
                    <img class="logo-light logo-img-lg hide_div" src="{{ $settings->site_logo }}"
                        srcset="{{ $settings->site_logo }} 2x" alt="logo" style="height: 50px !important;">
                    {{-- <img class="logo-dark logo-img-lg hide_div" src="{{$settings->site_logo}}" srcset="{{$settings->site_logo}} 2x" alt="logo-dark"heigth="100"> --}}

                </a>
            </div><!-- .nk-header-brand -->
            <div class="nk-header-menu" data-content="headerNav">
                <div class="nk-header-mobile">
                    <div class="nk-header-brand">
                        <a href="/" class="logo-link">
                            <img class="logo-light logo-img" src="{{ $settings->site_logo }}"
                                srcset="{{ $settings->site_logo }} 2x" alt="logo"
                                style="height: 50px !important;>
                                        {{-- <img class="logo-dark logo-img" src="{{$settings->site_logo}}" srcset="{{$settings->site_logo}} 2x" alt="logo-dark"> --}}

                                    </a>
                                </div>
                                <div class="nk-menu-trigger
                                mr-n2">
                            <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="headerNav"><em
                                    class="icon ni ni-arrow-left"></em></a>
                    </div>
                </div>
                @include('v2.partials.menu')
            </div><!-- .nk-header-menu -->
            <div class="nk-header-tools">
                <ul class="nk-quick-nav">

                    <li class="dropdown notification-dropdown">
                        <?php
                        $msgs = App\msg::orderby('id', 'DESC')->take(5)->get();
                        
                        ?>

                        <a href="#" class="dropdown-toggle nk-quick-nav-icon" data-toggle="dropdown">


                            @foreach ($msgs as $msg)
                                <?php
                                $rd = 0;
                                $str = explode(';', $msg->readers);
                                $receiver = explode(';', $msg->users);
                                if (in_array($user->username, $receiver) || empty($msg->users)) {
                                    if (!in_array($user->id, $str)) {
                                        $rd = 1;
                                    }
                                }
                                ?>
                                @if ($rd == 1)
                                    <div class="icon-status icon-status-info">
                                        <em class="icon ni ni-bell"></em>
                                    </div>
                                @else
                                    <div class="">
                                        <em class="icon ni ni-bell "></em>
                                    </div>
                                @endif
                            @endforeach
                        </a>
                        <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right dropdown-menu-s1">
                            <div class="dropdown-head">
                                <span class="sub-title nk-dropdown-title">Notifications</span>
                                <a href="#">Mark All as Read</a>
                            </div>
                            <div class="dropdown-body">


                                <div class="nk-notification">
                                    @foreach ($msgs as $msg)
                                        <?php
                                        $rd = 0;
                                        $str = explode(';', $msg->readers);
                                        $receiver = explode(';', $msg->users);
                                        if (in_array($user->username, $receiver) || empty($msg->users)) {
                                            if (!in_array($user->id, $str)) {
                                                $rd = 1;
                                            }
                                        }
                                        ?>
                                        @if ($rd == 1)
                                            <div class="nk-notification-item dropdown-inner">
                                                <div class="nk-notification-icon">
                                                    <em
                                                        class="icon icon-circle bg-warning-dim ni ni-curve-down-right"></em>
                                                </div>
                                                <div class="nk-notification-content">
                                                    <div class="nk-notification-text">{{ $msg->subject }}</div>
                                                    <div class="nk-notification-time">
                                                        {{ $msg->created_at->diffForHumans() }} </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach

                                </div><!-- .nk-notification -->
                            </div><!-- .nk-dropdown-body -->
                            <div class="dropdown-foot center">
                                <a href="#">View All</a>
                            </div>
                        </div>
                    </li><!-- .dropdown -->
                    <li class="hide-mb-sm">
                        <form id="logout-form" action="{{ route('auth.logout') }}" method="POST"
                            style="display: none;">
                            @csrf
                        </form>
                        <a href="#" class="nk-quick-nav-icon"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">

                            <em class="icon ni ni-signout"></em>
                        </a>
                    </li>
                    <li class="dropdown user-dropdown order-sm-first">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <div class="user-toggle">
                                <div class="user-avatar sm">
                                    @if ($user->img == '')
                                        {{-- <img class="img-responsive" src="/img/any.png" > --}}
                                        {{ substr(strtoupper($user->firstname), 0, 1) }}{{ substr(strtoupper($user->lastname), 0, 1) }}
                                    @else
                                        <img class="img-responsive" src="{!! $user->img !!}" width="30"
                                            height="30">
                                    @endif
                                </div>
                                <div class="user-info d-none d-xl-block">
                                    @if ($user->kyc_status == 0)
                                        <div class="user-status user-status-unverified">Unverified</div>
                                    @else
                                        <div class="user-status user-status-verified">Verified</div>
                                    @endif

                                    <div class="user-name dropdown-indicator">{{ substr($user->fullname, 0, 10) }}..
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1 is-light">
                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                <div class="user-card">
                                    <div class="user-avatar">

                                        @if ($user->img == '')
                                            <span>{{ substr(strtoupper($user->firstname), 0, 1) }}{{ substr(strtoupper($user->lastname), 0, 1) }}</span>
                                        @else
                                            <img class="img-responsive" src="{!! $user->img !!}" width="40"
                                                height="40">
                                        @endif
                                    </div>
                                    <div class="user-info">
                                        <span class="lead-text">{{ $user->firstname . ' ' . $user->lastname }}</span>
                                        <span class="sub-text">{{ $user->email }}</span>
                                        <span class="sub-text">{{ $user->usn }}</span>
                                    </div>
                                    <div class="user-action">
                                        <a class="btn btn-icon mr-n2" href="{{ route('v2.user.settings') }}"><em
                                                class="icon ni ni-setting"></em></a>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-inner user-account-info">
                                <h6 class="overline-title-alt">Account Balance</h6>
                                <div class="user-balance">{{ number_format(round($user->wallet, 6)) }} <small
                                        class="currency currency-usd">{{ $settings->currency }}</small></div>
                                {{-- <div class="user-balance-sub">Locked <span> {{number_format(round($currentEarning, 6))}} <span class="currency currency-usd">{{$settings->currency}}</span></span></div> --}}
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#modalDefault"
                                    class="link"><span>Withdraw Balance</span> <em
                                        class="icon ni ni-wallet-out"></em></a>
                            </div>
                            <div class="dropdown-inner">
                                <ul class="link-list">
                                    <li><a href="{{ route('v2.user.profile') }}"><em
                                                class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                    <li><a href="{{ route('v2.user.settings') }}"><em
                                                class="icon ni ni-setting-alt"></em><span>Account Setting</span></a>
                                    </li>
                                    <li><a href="{{ route('v2.user.activities') }}"><em
                                                class="icon ni ni-activity-alt"></em><span>Account Activity</span></a>
                                    </li>

                                    <li><a class="dark-switch" href="#"><em
                                                class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                </ul>
                            </div>
                            <div class="dropdown-inner">
                                <ul class="link-list">
                                    <li>
                                        <form id="logout-form" action="{{ route('auth.logout') }}" method="POST"
                                            style="display: none;">
                                            @csrf
                                        </form>
                                        <a href="#"
                                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            <i class="fas fa-arrow-left"></i>
                                            <p>Logout</p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li><!-- .dropdown -->
                </ul>

                <!-- .nk-quick-nav -->
            </div><!-- .nk-header-tools -->
        </div><!-- .nk-header-wrap -->
    </div><!-- .container-fliud -->
</div>
