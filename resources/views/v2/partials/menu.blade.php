<!-- Menu -->
<ul class="nk-menu nk-menu-main">
    <li class="nk-menu-item">
        <a href="{{route('v2.index')}}" class="nk-menu-link">
            <span class="nk-menu-text">Overview</span>
        </a>
    </li>

    <li class="nk-menu-item">
        <a href="{{ route('v2.investments.plan') }}" class="nk-menu-link">
            <span class="nk-menu-text">Invest</span>
        </a>
    </li>
    <li class="nk-menu-item">
        <a href="{{ route('v2.user.deposit') }}?c=deposit&t=invest" class="nk-menu-link">
            <span class="nk-menu-text">Deposit</span>
        </a>
    </li>
    <li class="nk-menu-item">
        <a href="{{ route('v2.investments') }}" class="nk-menu-link">
            <span class="nk-menu-text">Investments</span>
        </a>
    </li>

    <li class="nk-menu-item active has-sub">
        <a href="#" class="nk-menu-link nk-menu-toggle">
            <span class="nk-menu-text">Other</span>
        </a>

        <ul class="nk-menu-sub">
            <li class="nk-menu-item">
                <a href="{{ route('v2.user.profile') }}" class="nk-menu-link">
                    <span class="nk-menu-text">My Profile</span>
                </a>
            </li>
            <li class="nk-menu-item">
                <a href="{{ route('v2.user.transactions') }}" class="nk-menu-link">
                    <span class="nk-menu-text">Transactions</span>
                </a>
            </li>
            <li class="nk-menu-item">
                <a href="{{route('v2.affiliates')}}" class="nk-menu-link">
                    <span class="nk-menu-text">Affiliates</span>
                </a>
            </li>
            <li class="nk-menu-item">
                <a href="{{route('v2.funds_transfer')}}" class="nk-menu-link">
                    <span class="nk-menu-text">Transfer Funds</span>
                </a>
            </li>
            <li class="nk-menu-item">
                <a href="{{route('v2.ticket')}}" class="nk-menu-link">
                    <span class="nk-menu-text">Tickets</span>
                </a>
            </li>

        </ul>
    </li>
</ul>
