<div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-labelledby="modalDefault" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <form id="wd_formssss" action="{{ route('wallet.wd') }}" method="post">
                <div class="modal-body modal-body-md text-center">
                    <div class="nk-modal">
                        <h5 class="nk-modal-title">Withdraw Your Funds</h5>
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni  ni-wallet-out bg-success"></em>
                        <div class="nk-modal-text">


                            <ul class="pricing-features fw-bold mt-2 mb-2">
                                <li><span class="w-50">Available Balance</span> - <span
                                        class="ml-auto ">{{ $settings->currency === 'USD' ? '$' : $settings->currency }}
                                        {{ $user->wallet }}</span></li>
                                <li><span class="w-50">Max Withdrawal</span> - <span
                                        class="ml-auto">{{ $settings->currency === 'USD' ? '$' : $settings->currency }}
                                        {{ $settings->wd_limit }}</span></li>
                                <li><span class="w-50">Min Withdrawal</span> - <span
                                        class="ml-auto">{{ $settings->currency === 'USD' ? '$' : $settings->currency }}
                                        {{ $settings->min_wd }}</span></li>
                                <li><span class="w-50"> Withdrawal Fee</span> - <span class="ml-auto">
                                        {{ $settings->wd_fee }} %</span></li>
                                <li><span class="w-50"> Withdrawal Limit</span> - <span
                                        class="ml-auto">{{ $settings->currency === 'USD' ? '$' : $settings->currency }}
                                        {{ $settings->wd_limit }} </span></li>
                            </ul>

                        </div>
                        <div class="nk-modal-form">
                            <p class="fw-medium text-muted">{{ __('Enter amount and select Bank/Wallet Below') }} </p>
                            <div class="form-group" align="left">
                                <input type="hidden" class="form-control" name="_token" value="{{ csrf_token() }}">
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend ">
                                        <span
                                            class="input-group-text span_bg">{{ $settings->currency === 'USD' ? '$' : $settings->currency }}</span>
                                    </div>
                                    <input id="wd_amt" type="text" class="form-control" name="amt" required
                                        placeholder="Enter Amount to withdraw">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend ">
                                        <span class="input-group-text span_bg"><i class="fa fa-home"></i></span>
                                    </div>
                                    <select name="bank" class="form-control" required id="withClass"
                                        onchange="handleSelect">
                                        <option value="">---Select----</option>
                                        <?php
                                        $banks = App\banks::where('user_id', $user->id)->get();
                                        ?>
                                        @if (count($banks) > 0)
                                            @foreach ($banks as $bank)
                                                <option value="{{ $bank->id }}">
                                                    {{ $bank->Account_name . ' ' . $bank->Account_number . ' ' . $bank->Bank_Name }}
                                                </option>
                                            @endforeach
                                        @endif
                                        <option value="add">
                                            &#x2B; <span class="padding-left 5px;"></span> Add Bank/Wallet
                                        </option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="nk-modal-action">
                            <button type="submit" class="btn btn-lg btn-mw btn-primary">Confirm Withdrawal</button>
                            <div class="sub-text sub-text-alt mt-3 mb-4">This transaction will appear on your wallet
                                statement as {{ $settings->site_title }}.</div>
                            <a href="javascript:void(0)" class="link link-soft" data-dismiss="modal">Cancel and
                                return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modla-dialog -->
</div>


<script></script>
@section('script')
    <script type="text/javascript">
        $("#wallet_wd_close").click(function() {
            $("#wallet_wd").hide();
        });
        var handleSelect = function(elm) {
            console.log(elm)
            if (elm.value == 'add')
                window.open = "{{ route('v2.user.profile') }}";
        }
    </script>
@endsection
