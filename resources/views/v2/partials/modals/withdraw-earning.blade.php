<div
    class="modal fade"
    id="wd_earning_modal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="wd_earning_modal"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <form action="/user/wdraw/earning" method="post">
                <div class="modal-body modal-body-md text-center">
                    <div class="nk-modal">
                        <h5 class="nk-modal-title">{{ __('Withdraw Earnings') }} </h5>
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni  ni-wallet-out bg-success"></em>
                        <div class="nk-modal-text">
                            <h5>{{ __('Total Earning:') }} {{$settings->currency}} <span id="earned"></span></h5>  
                            <small>Days: <span id="days" class="text-danger" ></span></small>     
                        </div>
                        
                        <div class="nk-modal-form">

                            <div class="form-group" align="left">
                                <input type="hidden" class="form-control" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" class="form-control" name="_token" value="{{csrf_token()}}">
                                <input id="inv_id" type="hidden" class="form-control" name="p_id" value="">
                                <input id="ended" type="hidden" class="form-control" name="ended" value="">
                                
                            </div>
                            <div class="form-group">
                                <label>{{ __('Withdrawable Amount') }} </label>
                                <div class="input-group">
                                    <div class="input-group-prepend ">
                                        <span
                                            class="input-group-text span_bg">{{$settings->currency === 'USD' ? '$' : $settings->currency}}</span>
                                    </div>
                                    <input id="withdrawable_amt" type="text" value="" readonly
                                        class="bg_white form-control" name="amt" required>
                                </div>
                            </div>
                            <div class="nk-modal-action">
                                <button type="submit" class="btn btn-lg btn-mw btn-primary">Confirm Withdrawal</button>
                                <div class="sub-text sub-text-alt mt-3 mb-4">This transaction will appear on your wallet
                                    statement as {{$settings->site_title}}.</div>
                                <a href="javascript:void(0)" class="link link-soft" data-dismiss="modal">Cancel and return</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modla-dialog -->
</div>

