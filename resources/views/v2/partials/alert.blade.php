<div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
    <div class="toast-header">
        <strong class="mr-auto text-primary">{{__('Message')}}</strong>
        <small class="text-muted">just now</small>
        <button
            type="button"
            class="close"
            data-dismiss="toast"
            aria-label="Close"
        >
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
