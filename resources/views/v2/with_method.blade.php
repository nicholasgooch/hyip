@extends('v2.layouts.main')
@section('style')


@section('title', 'Settings')
@endsection
@section('content')
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-xl">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block">
                    <div class="card card-bordered">
                        <div class="card-aside-wrap">
                            <div class="card-inner card-inner-lg">
                                <div class="nk-block-head nk-block-head-lg">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h4 class="nk-block-title">Withdrawal Method</h4>
                                            <div class="nk-block-des">
                                                <p>Add bank accounts and wallet address to make withdrawals.</p>
                                            </div>
                                        </div>
                                        <div class="nk-block-head-content align-self-start d-lg-none">
                                            <a href="#" class="toggle btn btn-icon btn-trigger mt-n1" data-target="userAside"><em class="icon ni ni-menu-alt-r"></em></a>
                                        </div>
                                    </div>
                                </div><!-- .nk-block-head -->
                                <div class="nk-block">
                                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif

                                    <h6 class="card-title mb-5">Add Wallet</h6>
                                    <div class="row gy-4">
                                        <div class="col-md-6">
                                            <?php 
                                                $wallets = App\banks::where('user_id', $user->id)->where('type', '!=','bank')->get();
                                                
                                            ?>
    
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Asset</th>
                                                    <th scope="col">Coin Host</th>
                                                    <th scope="col">Wallet Address</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($wallets as $index => $wallet)
                                                    <tr>
                                                        <th scope="row">{{round((int)$index+1)}}</th>
                                                        <td>{{$wallet->Account_name}}</td>
                                                        <td data-toggle="tooltip" data-placement="top" title="{{$wallet->Bank_Name}}" >{{substr($wallet->Bank_Name,0,7)}}..</td>
                                                        <td data-toggle="tooltip" data-placement="top" title="{{$wallet->Account_number}}" >{{substr($wallet->Account_number,0,7)}}...</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-6">
                                            <form id="wallet-form" method="post" action="{{route('v2.user.add_wallet')}}">
                                               
                                                
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <div class="form-group">
                                                    <label class="form-label" for="host">{{ __('Coin Host') }}</label>
                                                    <input type="text" class="form-control form-control-lg" id="coin_host" value="{{old('coin_host')}}" name="coin_host" placeholder="Blockchain,Paxful">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="asset">{{ __('Asset') }}</label>
                                                <select name="asset_type" id="asset" class="form-select">
                                                    <option value="btc">Bitcoin (BTC)</option>
                                                    <option value="eth"> Ethereum (ETH)</option>
                                                </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="coin_wallet">{{ __('Wallet Address') }}</label>
                                                    <input type="text" class="form-control form-control-lg" id="coin_wallet" value="{{old('coin_wallet')}}" name="coin_wallet" placeholder="3J98t1WpEZ73CNmQviecrnyiWrnqRhWNLy">
                                                </div>
                                                <div class="form-group">
                                                
                                                    <input type="submit" class="btn btn-primary"value="Add Wallet">
                                                </div>
                                            </form>
                                        </div>
                                        
                                    </div>

                                    <hr>

                                    <h6 class="nk-block-title mb-5">Add Bank</h6>
                                    <div class="row gy-4">

                                        <div class="col-md-6">
                                            <?php 
                                                $banks = App\banks::where('user_id', $user->id)->where('type','bank')->get();
                                                
                                            ?>
        
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Account Name</th>
                                                    <th scope="col">Account Number</th>
                                                    <th scope="col">Bank Name</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($banks as $index => $bank)
                                                    <tr>
                                                        <th scope="row">{{round((int)$index+1)}}</th>
                                                        <td>{{$bank->Account_name}}</td>
                                                        <td data-toggle="tooltip" data-placement="top" title="{{$bank->Account_number}}" >{{substr($bank->Account_number,0,7)}}..</td>
                                                        <td data-toggle="tooltip" data-placement="top" title="{{$bank->Bank_Name}}" >{{substr($bank->Bank_Name,0,7)}}...</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-6">
                                            <form id="wallet-form" method="post" action="{{route('v2.user.add_bank')}}">
                                             
                                                
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <div class="form-group">
                                                    <label class="form-label" for="host">{{ __('Bank Name') }}</label>
                                                    <input type="text" class="form-control form-control-lg" id="bank_name" value="{{old('bank_name')}}" name="bank_name" placeholder="Bank of America">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="account_name">{{ __('Account Name') }}</label>
                                                    <input type="text" class="form-control form-control-lg" id="account_name" value="{{old('account_name')}}" name="account_name" placeholder="John Spencer">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="account_number">{{ __('Account Number') }}</label>
                                                    <input type="text" class="form-control form-control-lg" id="account_number" value="{{old('account_number')}}" name="account_number" placeholder="019********">
                                                </div>
                                                <div class="form-group">
                                                
                                                    <input type="submit" class="btn btn-primary"value="Add Bank">
                                                </div>
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div><!-- .nk-block -->
                            </div><!-- .card-inner -->
                            <div class="card-aside card-aside-left user-aside toggle-slide toggle-slide-left toggle-break-lg" data-content="userAside" data-toggle-screen="lg" data-toggle-overlay="true">
                                <div class="card-inner-group" data-simplebar>
                                   
                                   @include('v2.inc.sidebar')
                                </div><!-- .card-inner-group -->
                            </div><!-- card-aside -->
                        </div><!-- .card-aside-wrap -->
                    </div><!-- .card -->
                </div><!-- .nk-block -->
            </div>
        </div>
    </div>
</div>

@include('v2.inc.change_pwd_modal')
@endsection