@extends('v2.layouts.main')
@section('title', 'Transactions')
@section('content')
<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head">
                    <div class="nk-block-head-content">
                        <div class="nk-block-head-sub"><span>My Transactions</span></div>
                        <div class="nk-block-between-md g-4">
                            <div class="nk-block-head-content">
                                <h2 class="nk-block-title fw-normal">
                                    Transactions
                                </h2>
                                <div class="nk-block-des">
                                    <p>
                                        You have total ({{ (count($transactions)) }}) transaction(s).
                                    </p>
                                </div>
                            </div>
                            <!-- .nk-block-head-content -->
                            <div class="nk-block-head-content">
                                <ul class="nk-block-tools gx-3">
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#modalDefault"
                                            class="btn btn-primary"><span>Withdraw</span>
                                            <em class="icon ni ni-arrow-long-right d-none d-sm-inline-block"></em></a>
                                    </li>
                                    <li>
                                        <a href="{{ route('v2.investments.plan') }}"
                                            class="btn btn-white btn-light"><span>Invest More</span>
                                            <em class="icon ni ni-arrow-long-right d-none d-sm-inline-block"></em></a>
                                    </li>
                                    <li class="opt-menu-md dropdown">
                                        <a href="{{ route('v2.user.settings') }}"
                                            class="btn btn-white btn-light btn-icon"><em
                                                class="icon ni ni-setting"></em></a>

                                    </li>
                                </ul>
                            </div>
                            <!-- .nk-block-head-content -->
                        </div>
                    </div>
                </div>

                <div class="nk-block">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="card card-bordered card-full">
                                <div class="card-inner">
                                    <div class="card-title-group">
                                        <div class="card-title">
                                            <h6 class="title">
                                                <span class="mr-2">Transaction History</span>
                                                <p class="text-muted">all your transactions at your finger tips</p>
                                            </h6>
                                        </div>

                                    </div>
                                </div>
                                <!-- .card-inner -->
                                <div class="card-inner p-0 border-top">
                                    <div class="nk-tb-list nk-tb-orders">
                                        <div class="nk-tb-item nk-tb-head">
                                            <div class="nk-tb-col nk-tb-orders-type">
                                                <span>Type</span>
                                            </div>
                                            <div class="nk-tb-col"><span></span></div>
                                            <div class="nk-tb-col tb-col-sm">
                                                <span>Date</span>
                                            </div>
                                            <div class="nk-tb-col"><span></span></div>
                                            <div class="nk-tb-col tb-col-sm">
                                                <span>Method</span>
                                            </div>

                                            <div class="nk-tb-col tb-col-sm text-right">
                                                <span> Amount</span>
                                            </div>
                                            <div class="nk-tb-col text-right">
                                                <span>Action</span>
                                            </div>
                                        </div>
                                        <!-- .nk-tb-item -->
                                        @php
                                            $deposit_total = 0.00;
                                            $wd_total = 0.0;
                                        @endphp
                                        @foreach ($transactions as $transaction)
                                                                                    <div class="nk-tb-item">
                                                                                        @if ($transaction->getTable() === 'deposits')
                                                                                                                                        @php
                                                                                                                                            $deposit_total =
                                                                                                                                                intval($transaction->amount) + intval($deposit_total);
                                                                                                                                        @endphp
                                                                                                                                        <div class="nk-tb-col nk-tb-orders-type">
                                                                                                                                            <ul class="icon-overlap">
                                                                                                                                                @if ($transaction->bank == 'BTC')
                                                                                                                                                    <li>
                                                                                                                                                        <em class="bg-btc-dim icon-circle icon ni ni-sign-btc"></em>
                                                                                                                                                    </li>
                                                                                                                                                @elseif($transaction->bank == 'ETH')
                                                                                                                                                    <li>
                                                                                                                                                        <em class="bg-eth-dim icon-circle icon ni ni-sign-eth"></em>
                                                                                                                                                    </li>
                                                                                                                                                @else
                                                                                                                                                    <em class="bg-eth-dim icon-circle icon ni ni-building"></em>
                                                                                                                                                @endif

                                                                                                                                                <li>
                                                                                                                                                    <em
                                                                                                                                                        class="bg-success-dim icon-circle icon ni ni-arrow-down-left"></em>
                                                                                                                                                </li>
                                                                                                                                            </ul>
                                                                                                                                        </div>
                                                                                        @else
                                                                                                                                        @php
                                                                                                                                            $wd_total += (float) $transaction->amount;
                                                                                                                                        @endphp
                                                                                                                                        <div class="nk-tb-col nk-tb-orders-type">
                                                                                                                                            <ul class="icon-overlap">
                                                                                                                                                @if ($transaction->bank->type == 'btc')
                                                                                                                                                    <li>
                                                                                                                                                        <em class="bg-btc-dim icon-circle icon ni ni-sign-btc"></em>
                                                                                                                                                    </li>
                                                                                                                                                @elseif($transaction->bank->type == 'eth')
                                                                                                                                                    <li>
                                                                                                                                                        <em class="bg-btc-dim icon-circle icon ni ni-sign-eth"></em>
                                                                                                                                                    </li>
                                                                                                                                                @elseif($transaction->bank->type == 'bank')
                                                                                                                                                    <li>
                                                                                                                                                        <em
                                                                                                                                                            class="bg-btc-dim icon-circle icon ni ni-building-fill"></em>
                                                                                                                                                    </li>
                                                                                                                                                @endif
                                                                                                                                                <li>
                                                                                                                                                    <em
                                                                                                                                                        class="bg-purple-dim icon-circle icon ni ni-arrow-up-right"></em>
                                                                                                                                                </li>
                                                                                                                                            </ul>
                                                                                                                                        </div>
                                                                                        @endif
                                                                                        @if ($transaction->getTable() === 'deposits')
                                                                                            <div class="nk-tb-col">
                                                                                                <span class="tb-lead">Deposit</span>
                                                                                            </div>
                                                                                        @else
                                                                                            <div class="nk-tb-col">
                                                                                                <span class="tb-lead">Withdrawal</span>
                                                                                            </div>
                                                                                        @endif
                                                                                        <div class="nk-tb-col tb-col-sm">
                                                                                            <span
                                                                                                class="tb-sub">{{ $transaction->created_at->format('M D Y') }}</span>
                                                                                        </div>
                                                                                        @if ($transaction->getTable() == 'deposits')
                                                                                            <div class="nk-tb-col tb-col-xl">
                                                                                                <span class="tb-sub">
                                                                                                    @if ($transaction->bank == 'ETH')
                                                                                                    @elseif($transaction->bank == 'BTC')

                                                                                                    @elseif($transaction->bank == 'BANK')
                                                                                                    @endif
                                                                                                </span>
                                                                                            </div>
                                                                                        @else
                                                                                            <div class="nk-tb-col tb-col-xl">
                                                                                                <span class="tb-sub">{{ $transaction->account }}</span>
                                                                                            </div>
                                                                                        @endif
                                                                                        <div class="nk-tb-col tb-col-sm">
                                                                                            @if ($transaction->getTable() == 'deposits')
                                                                                                <span
                                                                                                    class="tb-sub text-primary">{{ $transaction->method == 'ETH' ? 'Ethereum' : 'Bitcoin' }}</span>
                                                                                            @else
                                                                                                <span
                                                                                                    class="tb-sub text-primary">{{ $transaction->method == 'ETH' ? 'Ethereum' : 'Bitcoin' }}</span>
                                                                                            @endif
                                                                                        </div>
                                                                                        <div class="nk-tb-col tb-col-sm text-right">
                                                                                            <span
                                                                                                class="tb-sub tb-amount"><span>{{ $settings->currency == 'USD' ? '$' : $settings->currency }}</span>{{ number_format(round($transaction->amount)) }}</span>
                                                                                        </div>
                                                                                        <div class="nk-tb-col text-right">
                                                                                            <span class="tb-sub tb-amount">
                                                                                                @if ($transaction->getTable() == 'deposits')
                                                                                                    @if ($transaction->status == 1)
                                                                                                        <span
                                                                                                            class="badge badge-sm badge-dim badge-outline-success d-none d-md-inline-flex">Approved</span>
                                                                                                    @elseif($transaction->status == 2)
                                                                                                        <span
                                                                                                            class="badge badge-sm badge-dim badge-outline-danger d-none d-md-inline-flex">Declined</span>
                                                                                                    @elseif($transaction->status == 0)
                                                                                                        <span
                                                                                                            class="badge badge-sm badge-dim badge-outline-warning d-none d-md-inline-flex">Pending</span>
                                                                                                    @endif
                                                                                                @else
                                                                                                    @if ($transaction->status == 'Approved')
                                                                                                        <span
                                                                                                            class="badge badge-sm badge-dim badge-outline-success d-none d-md-inline-flex">Approved</span>
                                                                                                    @elseif($transaction->status == 'Pending')
                                                                                                        <span
                                                                                                            class="badge badge-sm badge-dim badge-outline-warning d-none d-md-inline-flex">Pending</span>
                                                                                                    @elseif($transaction->status == 'Rejected')
                                                                                                        <span
                                                                                                            class="badge badge-sm badge-dim badge-outline-danger d-none d-md-inline-flex">Declined</span>
                                                                                                    @endif
                                                                                                @endif
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                        @endforeach
                                    </div>

                                </div>
                                <!-- .card-inner -->
                                <div class="card-inner">
                                    {{ $transactions->links() }}
                                </div>

                                <!-- .card-inner -->
                            </div>

                        </div>
                        <div class="col-lg-4">
                            <div class="card card-bordered ">
                                <div class="card-inner justify-center text-center">
                                    <div class="nk-iv-wg5">
                                        <div class="nk-iv-wg5-head">
                                            <h5 class="nk-iv-wg5-title">Investments</h5>
                                            <div class="nk-iv-wg5-subtitle">
                                                Invested so far <strong>{{ number_format($totalInvested) }}</strong>
                                                <span class="currency currency-usd">{{ $settings->currency }}</span>
                                            </div>
                                        </div>
                                        <div class="nk-iv-wg5-ck sm">
                                            <input type="text" class="knob-half" value="{{ $invs->count() }}"
                                                data-fgColor="#33d895" data-bgColor="#d9e5f7" data-thickness=".07"
                                                data-width="240" data-height="125" data-displayInput="false" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-bordered ">
                                <div class="card-inner justify-center text-center ">
                                    <div class="nk-iv-wg5">
                                        <div class="nk-iv-wg5-head">
                                            <h5 class="nk-iv-wg5-title">Deposits</h5>
                                            <div class="nk-iv-wg5-subtitle">
                                                Total Deposited so far
                                                <strong>{{ number_format($deposit_total, 2) }}</strong>
                                                <span class="currency currency-usd">{{ $settings->currency }}</span>
                                            </div>
                                        </div>
                                        <div class="nk-iv-wg5-ck sm">
                                            <input type="text" class="knob-half" value="{{ (float) count($deposits) }}"
                                                data-fgColor="#6576ff" data-bgColor="#d9e5f7" data-thickness=".06"
                                                data-width="300" data-height="155" maxValue="100"
                                                data-displayInput="false" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-bordered ">
                                <div class="card-inner justify-center text-center ">
                                    <div class="nk-iv-wg5">
                                        <div class="nk-iv-wg5-head">
                                            <h5 class="nk-iv-wg5-title">Withdrawals</h5>
                                            <div class="nk-iv-wg5-subtitle">
                                                Total Withdrawn so far
                                                <strong>{{ number_format($wd_total, 2) }}</strong>
                                                <span class="currency currency-usd">{{ $settings->currency }}</span>
                                            </div>
                                        </div>
                                        <div class="nk-iv-wg5-ck">
                                            <input type="text" class="knob-full" value="{{ $wd->count() }}"
                                                data-fgColor="#6576ff" data-bgColor="#d9e5f7" data-thickness=".06"
                                                data-width="300" data-height="155" data-displayInput="false" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div><!-- .nk-block -->
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(function () {
        $("#deposit").knob();
    });
</script>

@endsection