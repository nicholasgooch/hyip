@extends('v2.layouts.main')
@section('title', '2FA Verify')
@section('content')
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-xl">
        <div class="nk-content-inner">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="card card-bordered">
                        <div class="card-inner card-inner-lg">
                            <div class="nk-block-head nk-block-head-lg">
                                <div class="nk-block-between ">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Verify Two Factor Authentication</h4>
                                        <div class="nk-block-des text-center">
                                            <p>These settings are helps you keep your account secure.</p>
                                        </div>
                                    </div>
                                    <div class="nk-block-head-content align-self-start d-lg-none">
                                        <a href="#" class="toggle btn btn-icon btn-trigger mt-n1" data-target="userAside"><em class="icon ni ni-menu-alt-r"></em></a>
                                    </div>
                                </div>
                            </div><!-- .nk-block-head -->
                            <div class="nk-block  mb-3">
                                <div class="text-center">
                                    <div class="card-body">
                                        <p>Two factor authentication (2FA) strengthens access security by requiring two methods (also referred to as factors) to verify your identity. Two factor authentication protects against phishing, social engineering and password brute force attacks and secures your logins from attackers exploiting weak or stolen credentials.</p>
                
                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                
                                        Enter the pin from Google Authenticator app:<br/><br/>
                                        <form class="form-horizontal" action="{{ route('2faVerify') }}" method="POST">
                                            {{ csrf_field() }}
                                            <div class="form-group{{ $errors->has('one_time_password-code') ? ' has-error' : '' }}">
                                                <label for="one_time_password" class="control-label">One Time Password</label>
                                                <input id="one_time_password" name="one_time_password" class="form-control form-control-lg offset-md-3 col-md-6"  type="text" required/>
                                            </div>
                                            <button class="btn btn-primary" type="submit">Authenticate</button>
                                        </form>
                                    </div>
                                </div><!-- .card -->
                            </div><!-- .nk-block -->
                        </div><!-- .card-inner -->
                   
                </div><!-- .card -->
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
 
@endsection