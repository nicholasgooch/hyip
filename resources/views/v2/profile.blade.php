@extends('v2.layouts.main')
@section('title', 'Profile')
@section('style')

<style>
    .display_not{
	display: none;
}
</style>

@endsection
@section('content')
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-xl">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block">
                    <div class="card card-bordered">
                        <div class="card-aside-wrap">
                            <div class="card-inner card-inner-lg">
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if (session()->has('status') && session()->get('msgType') == 'suc')
                                    <div class="alert alert-success">
                                        <p>{{session()->get('status')}}</p>
                                    </div>
                                    {{session()->forget('status')}}
                                     {{session()->forget('msgType')}}
                                @endif
                                @if (session()->has('status') && session()->get('msgType') == 'err')
                                <div class="alert alert-danger">
                                    <p>{{session()->get('status')}}</p>
                                </div>
                                {{session()->forget('status')}}
                                 {{session()->forget('msgType')}}
                            @endif
                                <div class="nk-block-head nk-block-head-lg">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h4 class="nk-block-title">Personal Information</h4>
                                            <div class="nk-block-des">
                                                <p>Basic info, like your name and address, that you use on {{$settings->site_title}} Platform.</p>
                                            </div>
                                        </div>
                                        <div class="nk-block-head-content align-self-start d-lg-none">
                                            <a href="#" class="toggle btn btn-icon btn-trigger mt-n1" data-target="userAside"><em class="icon ni ni-menu-alt-r"></em></a>
                                        </div>
                                    </div>
                                </div><!-- .nk-block-head -->
                                <div class="nk-block">
                                    <div class="nk-data data-list">
                                        <div class="data-head">
                                            <h6 class="overline-title">Basics</h6>
                                        </div>
                                        <div class="data-item" data-toggle="modal" data-target="#profile-edit">
                                            <div class="data-col">
                                                <span class="data-label">First Name</span>
                                                <span class="data-value">{{ucfirst($user->firstname)}}</span>
                                            </div>
                                            <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                                        </div><!-- data-item -->
                                        <div class="data-item" data-toggle="modal" data-target="#profile-edit">
                                            <div class="data-col">
                                                <span class="data-label">Last Name</span>
                                                <span class="data-value">{{ucfirst($user->lastname)}}</span>
                                            </div>
                                            <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                                        </div><!-- data-item -->
                                        <div class="data-item" data-toggle="modal" data-target="#profile-edit">
                                            <div class="data-col">
                                                <span class="data-label">UserName</span>
                                                <span class="data-value">{{ucfirst($user->username)}}</span>
                                            </div>
                                            <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                                        </div><!-- data-item -->
                                        <div class="data-item">
                                            <div class="data-col">
                                                <span class="data-label">Email</span>
                                                <span class="data-value">{{ucfirst($user->email)}}</span>
                                            </div>
                                            <div class="data-col data-col-end"><span class="data-more disable"><em class="icon ni ni-lock-alt"></em></span></div>
                                        </div><!-- data-item -->
                                        <div class="data-item" data-toggle="modal" data-target="#profile-edit" >
                                            <div class="data-col">
                                                <span class="data-label">Phone Number</span>
                                                <span class="data-value">{{$user->phone == '' ? 'Not Added Yet' : $user->phone}}</span>
                                            </div>
                                            <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                                        </div><!-- data-item -->
                                        <div class="data-item" data-toggle="modal" data-target="#profile-edit" data-tab-target="#kyc">
                                            <div class="data-col">
                                                <span class="data-label">Date of Birth</span>
                                                <span class="data-value">{{is_null($user->kyc) ? 'N/A': $user->kyc->dob}}</span>
                                            </div>
                                            <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                                        </div><!-- data-item -->
                                      
                                    </div>
                                    
                                        <div class="nk-data data-list">
                                            <div class="data-head">
                                                <h6 class="overline-title">Location</h6>
                                            </div>
                                            <p class="text-danger">Please note: Updating your country for the first time will permanently set currency for your profile.</p>
                                          
                                        <div class="data-item" data-toggle="modal" data-target="#profile-edit" data-tab-target="#kyc">
                                            <div class="data-col">
                                                <span class="data-label">Country</span>
                                                
                                                <span class="data-value">{{$user->kyc->country->name ?? 'Not Added Yet' }}</span>
                                            </div>
                                            <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                                        </div><!-- data-item -->
                                        <div class="data-item" data-toggle="modal" data-target="#profile-edit" data-tab-target="#kyc">
                                            <div class="data-col">
                                                <span class="data-label">State</span>
                                                 <span class="data-value">{{$user->kyc->state->name ?? 'Not Added Yet'}}</span>
                                            </div>
                                            <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                                        </div><!-- data-item -->
                                        <div class="data-item" data-toggle="modal" data-target="#profile-edit" data-tab-target="#kyc">
                                            <div class="data-col">
                                                <span class="data-label">Address</span>
                                                <span class="data-value">{{$user->address == '' ? 'Not Added Yet' : $user->address}}</span>
                                            </div>
                                            <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                                        </div><!-- data-item -->
                                    
                                    </div><!-- data-list -->
                                   
                                </div><!-- .nk-block -->
                            </div>
                            <div class="card-aside card-aside-left user-aside toggle-slide toggle-slide-left toggle-break-lg" data-content="userAside" data-toggle-screen="lg" data-toggle-overlay="true">
                                <div class="card-inner-group" data-simplebar>
                                    
                                    @include('v2.inc.sidebar')
                                </div><!-- .card-inner-group -->
                            </div><!-- card-aside -->
                        </div><!-- .card-aside-wrap -->
                    </div><!-- .card -->
                </div><!-- .nk-block -->
            </div>
        </div>
    </div>
</div>
@include('v2.inc.update_profile')
@endsection
@section('script')
    <script>
        $(document).ready(function(){
	$('#country').on('change', function(){		
		var data = $('#country').val();
		$('#p_loading').show();

		$.ajax
		({
		    url: "/user/get/states/"+data,
		    type: "get",
		    // dataType: 'json',
		    data: data,
		    success:function(result)
		    {
		        var str = "";
		        var dt = JSON.parse(result);
                $('#states').html("<option selected disabled>Click to Choose State</option>");

		        for(var i = 0; i < dt.length; i++ )
		        {
		        	str = str + '<option value="'+dt[i].id+'">'+dt[i].name+'</option>';
		        }		                
		        $('#states').append(str);
		        // alert('here');
		    },
		    error: function (xhr) {	
		    	$('#p_loading').hide();	                
		        alert(xhr.responseText)		                
		    }
		   
		});   

		$.ajax
		({
		    url: "/user/get/countryCode/"+data,
		    type: "get",		            
		    data: data,
		    success:function(result)
		    { 
		        $('#cCode').val('+'+result);
		        $('#countryCode').html('+'+result);	
		        $('#p_loading').hide();	                
		    },
		    error: function (xhr) {	
		    	$('#p_loading').hide();	                
		        alert(xhr.responseText);		                
		    }
		   
		}); 

        if (data == 233) {
            $('#ssn-row').css({ 'display': 'block' })
        } else {
            $('#ssn-row').css({ 'display': 'none' })
        }

		// $('#p_loading').hide();       
	});
});
    </script>
@endsection