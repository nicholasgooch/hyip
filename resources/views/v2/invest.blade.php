@extends('v2.layouts.main')
@section('title', 'Invest')
@section('style')
<link
    rel="stylesheet"
    href="{{ asset('v2/assets/css/dashlite-crypto.css') }}"
/>

@endsection 

@section('content')
<div class="nk-content nk-content-lg nk-content-fluid" id="app">
    <div class="container-xl wide-lg">
      <div class="nk-content-inner">
        <div class="nk-content-body">
          <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-content">
              <div class="nk-block-head-sub">
                <a href="{{route('v2.investments.plan')}}" class="back-to"
                  ><em class="icon ni ni-arrow-left"></em
                  ><span>Back to plan</span></a
                >
              </div>
              <div class="nk-block-head-content">
                <h2 class="nk-block-title fw-normal">
                  Ready to get started?
                </h2>
              </div>
            </div>
          </div>
          <!-- nk-block-head -->
          
          <invest plan="{{$choosen_plan}}" user_id="{{$user->id}}" user="{{Auth::user()}}"></invest>
          <!-- .nk-block -->
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
    <script src="{{asset('/js/app.js')}}"></script>
@endsection