@extends('v2.layouts.main')
@section('title', '2FA Settings')
@section('content')
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-xl">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block">
                    <div class="card card-bordered">
                        <div class="card-aside-wrap">
                            <div class="card-inner card-inner-lg">
                                <div class="nk-block-head nk-block-head-lg">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h4 class="nk-block-title">Two Factor (2FA) Settings</h4>
                                            <div class="nk-block-des">
                                                <p>These settings are helps you keep your account secure.</p>
                                            </div>
                                        </div>
                                        <div class="nk-block-head-content align-self-start d-lg-none">
                                            <a href="#" class="toggle btn btn-icon btn-trigger mt-n1" data-target="userAside"><em class="icon ni ni-menu-alt-r"></em></a>
                                        </div>
                                    </div>
                                </div><!-- .nk-block-head -->
                                <div class="nk-block  mb-3">
                                    <div class="card card-bordered">
                                        <div class="">
                                            <div class="card-header"><strong>Two Factor Authentication</strong></div>
                                            <div class="card-body">
                                                <p>Two factor authentication (2FA) strengthens access security by requiring two methods (also referred to as factors) to verify your identity. Two factor authentication protects against phishing, social engineering and password brute force attacks and secures your logins from attackers exploiting weak or stolen credentials.</p>
                        
                                                @if (session('error'))
                                                    <div class="alert alert-danger">
                                                        {{ session('error') }}
                                                    </div>
                                                @endif
                                                @if (session('success'))
                                                    <div class="alert alert-success">
                                                        {{ session('success') }}
                                                    </div>
                                                @endif
                        
                                                @if($data['user']->loginSecurity == null)
                                                    <form class="form-horizontal" method="POST" action="{{ route('generate2faSecret') }}">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary">
                                                                Generate Secret Key to Enable 2FA
                                                            </button>
                                                        </div>
                                                    </form>
                                                @elseif(!$data['user']->loginSecurity->google2fa_enable)
                                                    1. Scan this QR code with your Google Authenticator App. Alternatively, you can use the code: <code>{{ $data['secret'] }}</code><br/>
                                                   {!!$data['google2fa_url'] !!}
                                                    <img src="{{$data['google2fa_url']}}" alt="">
                                                    <br/><br/>
                                                    2. Enter the pin from Google Authenticator app:<br/><br/>
                                                    <form class="form-horizontal" method="POST" action="{{ route('enable2fa') }}">
                                                        {{ csrf_field() }}
                                                        <div class="form-group{{ $errors->has('verify-code') ? ' has-error' : '' }}">
                                                            <label for="secret" class="control-label">Authenticator Code</label>
                                                            <input id="secret" type="text" class="form-control col-md-4" name="secret" required>
                                                            @if ($errors->has('verify-code'))
                                                                <span class="help-block">
                                                                <strong>{{ $errors->first('verify-code') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                        <button type="submit" class="btn btn-primary">
                                                            Enable 2FA
                                                        </button>
                                                    </form>
                                                @elseif($data['user']->loginSecurity->google2fa_enable)
                                                    <div class="alert alert-success">
                                                        2FA is currently <strong>enabled</strong> on your account.
                                                    </div>
                                                    <p>If you are looking to disable Two Factor Authentication. Please confirm your password and Click Disable 2FA Button.</p>
                                                    <form class="form-horizontal" method="POST" action="{{ route('disable2fa') }}">
                                                        {{ csrf_field() }}
                                                        <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                                            <label for="change-password" class="control-label">Current Password</label>
                                                                <input id="current-password" type="password" class="form-control col-md-4" name="current-password" required>
                                                                @if ($errors->has('current-password'))
                                                                    <span class="help-block">
                                                                <strong>{{ $errors->first('current-password') }}</strong>
                                                                </span>
                                                                @endif
                                                        </div>
                                                        <button type="submit" class="btn btn-primary ">Disable 2FA</button>
                                                    </form>
                                                @endif
                                            </div>
                                        </div>
                                       
                                    </div><!-- .card -->
                                </div><!-- .nk-block -->
                            </div><!-- .card-inner -->
                            <div class="card-aside card-aside-left user-aside toggle-slide toggle-slide-left toggle-break-lg" data-content="userAside" data-toggle-screen="lg" data-toggle-overlay="true">
                                <div class="card-inner-group" data-simplebar>
                                   
                                   @include('v2.inc.sidebar')
                                </div><!-- .card-inner-group -->
                            </div><!-- card-aside -->
                        </div><!-- .card-aside-wrap -->
                    </div><!-- .card -->
                </div><!-- .nk-block -->
            </div>
        </div>
    </div>
</div>

@include('v2.inc.update_profile')
@endsection