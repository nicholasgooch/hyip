@extends('v2.layouts.main')
@section('title', 'Activities')
@section('content')
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-xl">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block">
                    <div class="card card-bordered">
                        <div class="card-aside-wrap">
                            <div class="card-inner card-inner-lg">
                                <div class="nk-block-head nk-block-head-lg">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h4 class="nk-block-title">Account Activity</h4>
                                            <div class="nk-block-des">
                                                <p>Here is your last 20 login activities log. <span class="text-soft"><em class="icon ni ni-info"></em></span></p>
                                            </div>
                                        </div>
                                        <div class="nk-block-head-content align-self-start d-lg-none">
                                            <a href="#" class="toggle btn btn-icon btn-trigger mt-n1" data-target="userAside"><em class="icon ni ni-menu-alt-r"></em></a>
                                        </div>
                                    </div>
                                </div><!-- .nk-block-head -->

                                <div class="nk-block card card-bordered">
                                    <table class="table table-ulogs">
                                        <thead class="thead-light">
                                            <tr>
                                                <th class="tb-col-os"><span class="overline-title">Action <span class="d-sm-none">/ IP</span></span></th>
                                                {{-- <th class="tb-col-ip"><span class="overline-title">IP</span></th> --}}
                                                <th class="tb-col-time"><span class="overline-title">Time</span></th>
                                                <th class="tb-col-action"><span class="overline-title">&nbsp;</span></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($user->activities->take(20) as $item)
                                            <tr>
                                                <td class="tb-col-os">{{$item->action}}</td>
                                                {{-- <td class="tb-col-ip"><span class="sub-text">192.149.122.128</span></td> --}}
                                                <td class="tb-col-time"><span class="sub-text">{{$item->created_at->format('d m y')}}</span></td>
                                                <td class="tb-col-action"></td>
                                            </tr>
                                            @endforeach
                                           
                                        </tbody>
                                    </table>
                                </div><!-- .nk-block-head -->
                            </div><!-- .card-inner -->
                            <div class="card-aside card-aside-left user-aside toggle-slide toggle-slide-left toggle-break-lg" data-content="userAside" data-toggle-screen="lg" data-toggle-overlay="true">
                                <div class="card-inner-group" data-simplebar>
                                   
                                   @include('v2.inc.sidebar')
                                </div><!-- .card-inner-group -->
                            </div><!-- card-aside -->
                        </div><!-- .card-aside-wrap -->
                    </div><!-- .card -->
                </div><!-- .nk-block -->
            </div>
        </div>
    </div>
</div>

@endsection