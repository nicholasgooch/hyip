<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="{{ $settings->site_favicon }}">
    <!-- Page Title  -->
    <title> @yield('title') | {{ $settings->site_title }}</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="{{ asset('v2/assets/css/dashlite.css?ver=2.9.0') }}">

    <link rel="stylesheet" href="https:////cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    @livewireStyles
    @yield('style')
    <link id="skin-default" rel="stylesheet"
        href="{{ asset('v2/assets/css/skins/theme-' . config('app.portfolio_app_theme', 'red') . '.css?ver=2.9.0') }}">
    <style>
        .inputfile {
            font-size: 12px;
            font-weight: 500;
            color: #526484;
            transition: all .4s;
            line-height: 1.3rem;
            position: relative;
        }

        .inputfile:focus,
        .inputfile:hover {}
    </style>
    <style>
        @media screen and (max-width: 600px) {
            .hide_div {
                visibility: hidden;
                clear: both;
                float: left;
                margin: 10px auto 5px 20px;
                width: 28%;
                display: none;
            }
        }

        @media only screen and (max-width: 600px) {
            .hide-div {
                visibility: hidden;
                clear: both;
                float: left;
                margin: 10px auto 5px 20px;
                width: 28%;
                display: none;
            }
        }
    </style>
</head>

<body class="nk-body npc-invest bg-lighter ">
    <div class="nk-app-root">
        <!-- wrap @s -->
        <div class="nk-wrap ">
            <!-- main header @s -->
            @include('v2.partials.header')
            <!-- main header @e -->
            <!-- content @s -->
            @yield('content')
            <!-- content @e -->
            <!-- footer @s -->
            <div class="nk-footer nk-footer-fluid bg-lighter">
                <div class="container-xl wide-lg">
                    <div class="nk-footer-wrap">
                        <div class="nk-footer-copyright"> &copy; {{ date('Y') }} {{ $settings->site_title }}.</a>
                        </div>
                        <div class="nk-footer-links">
                            <ul class="nav nav-sm">
                                <li class="nav-item"><a class="nav-link"
                                        href="https://{{config('app.domain')}}/get-in-touch/">Contact</a></li>
                                <li class="nav-item"><a class="nav-link"
                                        href="https://{{config('app.domain')}}/privacy-policy/">Privacy</a></li>
                                <li class="nav-item"><a class="nav-link"
                                        href="https://{{config('app.domain')}}/faqs/">Help</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer @e -->
        </div>
        <!-- wrap @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->

    <script src="{{ asset('v2/assets/js/bundle.js?ver=2.9.0') }}"></script>
    @livewireScripts
    <script src="{{ asset('v2/assets/js/scripts.js?ver=2.9.0') }}"></script>

    <!-- <script src="{{ asset('v2/assets/js/charts/chart-crypto.js?ver=2.9.0') }}"></script> -->
    <script src="{{ asset('v2/assets/js/libs/jqvmap.js?ver=2.9.0') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"
        integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    @include('v2.partials.modals.withdraw-modal')
    @include('v2.partials.modals.withdraw-ref-earnings')
    <script>
        var color = "{!! config('app.portfolio_app_theme', 'red') !!}";
        console.log(color);
        var refBarChart = {
            labels: ["01 Nov", "02 Nov", "03 Nov", "04 Nov", "05 Nov", "06 Nov", "07 Nov", "08 Nov", "09 Nov", "10 Nov", "11 Nov", "12 Nov", "13 Nov", "14 Nov", "15 Nov", "16 Nov", "17 Nov", "18 Nov", "19 Nov", "20 Nov", "21 Nov", "22 Nov", "23 Nov", "24 Nov", "25 Nov", "26 Nov", "27 Nov", "28 Nov", "29 Nov", "30 Nov"],
            dataUnit: 'People',
            datasets: [{
                label: "Join",
                color: color,
                data: [110, 80, 125, 55, 95, 75, 90, 110, 80, 125, 55, 95, 75, 90, 110, 80, 125, 55, 95, 75, 90, 110, 80, 125, 55, 95, 75, 90, 75, 90]
            }]
        };

        function referStats(selector, set_data) {
            var $selector = selector ? $(selector) : $('.chart-refer-stats');
            $selector.each(function () {
                var $self = $(this),
                    _self_id = $self.attr('id'),
                    _get_data = typeof set_data === 'undefined' ? eval(_self_id) : set_data;

                var selectCanvas = document.getElementById(_self_id).getContext("2d");
                var chart_data = [];

                for (var i = 0; i < _get_data.datasets.length; i++) {
                    chart_data.push({
                        label: _get_data.datasets[i].label,
                        data: _get_data.datasets[i].data,
                        // Styles
                        backgroundColor: _get_data.datasets[i].color,
                        borderWidth: 2,
                        borderColor: 'transparent',
                        hoverBorderColor: 'transparent',
                        borderSkipped: 'bottom',
                        barPercentage: .5,
                        categoryPercentage: .7
                    });
                }

                var chart = new Chart(selectCanvas, {
                    type: 'bar',
                    data: {
                        labels: _get_data.labels,
                        datasets: chart_data
                    },
                    options: {
                        legend: {
                            display: false
                        },
                        maintainAspectRatio: false,
                        tooltips: {
                            enabled: true,
                            rtl: NioApp.State.isRTL,
                            callbacks: {
                                title: function title(tooltipItem, data) {
                                    return data.datasets[tooltipItem[0].datasetIndex].label;
                                },
                                label: function label(tooltipItem, data) {
                                    return data.datasets[tooltipItem.datasetIndex]['data'][tooltipItem['index']] + ' ' + _get_data.dataUnit;
                                }
                            },
                            backgroundColor: '#fff',
                            titleFontSize: 13,
                            titleFontColor: '#6783b8',
                            titleMarginBottom: 6,
                            bodyFontColor: '#9eaecf',
                            bodyFontSize: 12,
                            bodySpacing: 4,
                            yPadding: 10,
                            xPadding: 10,
                            footerMarginTop: 0,
                            displayColors: false
                        },
                        scales: {
                            yAxes: [{
                                display: false,
                                ticks: {
                                    beginAtZero: true
                                }
                            }],
                            xAxes: [{
                                display: false,
                                ticks: {
                                    reverse: NioApp.State.isRTL
                                }
                            }]
                        }
                    }
                });
            });
        } // init chart
        referStats();
    </script>

    @yield('script')
    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    </script>
    @if (Session::has('status') && Session::get('msgType') == 'suc')
        <script type="text/javascript">
            toastr.success('{{ Session::get('status') }}', 'Sucessful')
        </script>
        {{ Session::forget('status') }}
        {{ Session::forget('msgType') }}
    @elseif(Session::has('status') && Session::get('msgType') == 'err')
        <script type="text/javascript">
            toastr.error('{{ Session::get('status') }}', 'An Error Occoured')
        </script>
        {{ Session::forget('status') }}
        {{ Session::forget('msgType') }}
    @endif

    <script>
        {!! $settings->livechat_code !!}
    </script>
</body>

</html>