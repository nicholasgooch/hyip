@extends('v2.layouts.main')
@section('title', 'Notifications')
@section('style')

<style>
    .display_not{
	display: none;
}
</style>

@endsection
@section('content')
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-xl">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block">
                    <div class="card card-bordered" >
                        <div class="card-aside-wrap">
                                      
                            <div class="card card-bordered">
                                <div class="card-inner ">
                                    <div class="card-title-group">
                                        <div class="card-title">
                                            <h6 class="title">Notifications</h6>
                                        </div>
                                       
                                    </div>
                                </div>
                                <div class="card-inner">
                                    <div class="timeline">
                                        <h6 class="timeline-head">November, 2019</h6>
                                        <ul class="timeline-list">
                                            <li class="timeline-item">
                                                <div class="timeline-status bg-primary is-outline"></div>
                                                <div class="timeline-date">13 Nov <em class="icon ni ni-alarm-alt"></em></div>
                                                <div class="timeline-data">
                                                    <h6 class="timeline-title">Submited KYC Application</h6>
                                                    <div class="timeline-des">
                                                        <p>Re-submitted KYC Application form.</p>
                                                        <span class="time">09:30am</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="timeline-item">
                                                <div class="timeline-status bg-primary"></div>
                                                <div class="timeline-date">13 Nov <em class="icon ni ni-alarm-alt"></em></div>
                                                <div class="timeline-data">
                                                    <h6 class="timeline-title">Submited KYC Application</h6>
                                                    <div class="timeline-des">
                                                        <p>Re-submitted KYC Application form.</p>
                                                        <span class="time">09:30am</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="timeline-item">
                                                <div class="timeline-status bg-primary"></div>
                                                <div class="timeline-date">13 Nov <em class="icon ni ni-alarm-alt"></em></div>
                                                <div class="timeline-data">
                                                    <h6 class="timeline-title">Submited KYC Application</h6>
                                                    <div class="timeline-des">
                                                        <p>Re-submitted KYC Application form.</p>
                                                        <span class="time">09:30am</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="timeline-item">
                                                <div class="timeline-status bg-pink"></div>
                                                <div class="timeline-date">13 Nov <em class="icon ni ni-alarm-alt"></em></div>
                                                <div class="timeline-data">
                                                    <h6 class="timeline-title">Submited KYC Application</h6>
                                                    <div class="timeline-des">
                                                        <p>Re-submitted KYC Application form.</p>
                                                        <span class="time">09:30am</span>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- .card -->
                            











                            <div class="card-aside card-aside-left user-aside toggle-slide toggle-slide-left toggle-break-lg" data-content="userAside" data-toggle-screen="lg" data-toggle-overlay="true">
                                <div class="card-inner-group" data-simplebar>
                                    
  
                                    @include('v2.inc.sidebar')
                                </div><!-- .card-inner-group -->
                            </div><!-- card-aside -->
                        </div><!-- .card-aside-wrap -->
                    </div><!-- .card -->
                </div><!-- .nk-block -->
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')

@endsection