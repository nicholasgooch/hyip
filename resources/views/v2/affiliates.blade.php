@extends('v2.layouts.main')
@section('title', 'Referals')
@section('content')
<div class="nk-content nk-content-lg nk-content-fluid" id="app">
    <div class="container-xl wide-xxl">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-lg">
                    <div class="nk-block-head-content">
                        <div class="nk-block-head-sub">
                            <a href="{{route('v2.index')}}" class="back-to"><em
                                    class="icon ni ni-arrow-left"></em><span>Back to Overview</span></a>
                        </div>
                        <div class="nk-block-head-content">
                            <h2 class="nk-block-title fw-normal">
                                My Affiliates (Refferals)
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="nk-block">
                    <div class="card card-bordered">
                        <div class="nk-refwg">
                            <div class="nk-refwg-invite card-inner">
                                <div class="nk-refwg-head g-3">
                                    <div class="nk-refwg-title">
                                        <h5 class="title">Refer Us & Earn</h5>
                                        <div class="title-sub">Use the bellow link to invite your friends.</div>
                                    </div>
                                    {{-- <div class="nk-refwg-action">
                                        <a href="javascript:void(0)" class="btn btn-primary">Invite</a>
                                    </div> --}}
                                </div>
                                <div class="nk-refwg-url">
                                    <div class="form-control-wrap">
                                        <div class="form-clip clipboard-init" data-clipboard-target="#refUrl"
                                            data-success="Copied" data-text="Copy Link"><em
                                                class="clipboard-icon icon ni ni-copy"></em> <span
                                                class="clipboard-text">Copy Link</span></div>
                                        <div class="form-icon">
                                            <em class="icon ni ni-link-alt"></em>
                                        </div>
                                        <input type="text" class="form-control copy-text" id="refUrl"
                                            value="{{request()->getHttpHost() . __('/register/') . $user->username}}">
                                    </div>
                                </div>
                            </div>
                            <div class="nk-refwg-stats card-inner bg-lighter">
                                <div class="nk-refwg-group g-3">
                                    <div class="nk-refwg-name">
                                        <h6 class="title">My Referral <em class="icon ni ni-info" data-toggle="tooltip"
                                                data-placement="right" title="Referral Informations"></em></h6>
                                    </div>
                                    <div class="nk-refwg-info g-3">
                                        <div class="nk-refwg-sub">
                                            <div class="title">{{$refs->count()}}</div>
                                            <div class="sub-text">Total Joined</div>
                                        </div>
                                        <div class="nk-refwg-sub">
                                            <div class="title">
                                                {{$settings->currency == 'USD' ? '$' : $settings->currency}}
                                                {{ number_format(round($user->ref_bal, 6))}}
                                            </div>
                                            <div class="sub-text">Referral Earnings


                                            </div>
                                            <div class="sub-text">
                                                <a href="javascript:void(0);" data-toggle="modal"
                                                    data-target="#refEarnModal">Withdraw Earnings</a>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="nk-refwg-ck">
                                    <canvas class="chart-refer-stats" id="refBarChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div><!-- .card -->
                </div><!-- .nk-block -->
                <div class="nk-block  mb-3">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ __('My Affiliates') }}</h4>
                                </div>
                                <div class="card-body pb-0">
                                    <div class="table-responsive">
                                        <table id="basic-datatables" class="display table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <!-- <th data-field="state" data-checkbox="true"></th> -->
                                                    <th>{{ __('Name') }}</th>
                                                    <th>{{ __('Username') }}</th>
                                                    <th>{{ __('Investments') }}</th>
                                                    <th>{{ __('Date Registered') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
$refs = App\User::where('referal', $user->username)->orderby('id', 'desc')->get();
                                    ?>
                                                @if(count($refs) > 0)
                                                                                            @foreach($refs as $ref)
                                                                                                                                        <tr>
                                                                                                                                            <td>{{$ref->firstname}} {{$ref->lastname}}</td>
                                                                                                                                            <td>{{$ref->username}}</td>
                                                                                                                                            <td>
                                                                                                                                                <?php  
                                                                                                                                                                                                                                                                                                                                $ref_inv = App\investment::where('user_id', $ref->id)->get();
                                                                                                echo count($ref_inv);
                                                                                                                                                                                                                                                                                                                            ?>
                                                                                                                                            </td>
                                                                                                                                            <td>{{$ref->created_at->format('M-d-Y')}}</td>
                                                                                                                                        </tr>
                                                                                            @endforeach
                                                @else

                                                @endif
                                            </tbody>
                                        </table>
                                        <br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title"> {{ __('Earning from Affiliates') }} </h4>
                                </div>
                                <div class="card-body pb-0">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>{{ __('Date') }}</th>
                                                    <th>{{ __('Username') }}</th>
                                                    <th>{{ __('Name') }}</th>
                                                    <th>{{ __('Amount') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
$refs = App\ref::where('username', $user->username)->orderby('id', 'desc')->get();
                                        ?>
                                                @if(count($refs) > 0)
                                                                                            @foreach($refs as $ref)
                                                                                                                                        <?php
                                                                                                $usr = App\User::find($ref->user_id);
                                                                                                                                                                                                                                                                                                                        ?>
                                                                                                                                        <tr>
                                                                                                                                            <td>{{$ref->created_at->format('D-M-Y')}}</td>
                                                                                                                                            <td>{{$usr->username}}</td>
                                                                                                                                            <td>
                                                                                                                                                {{$usr->firstname . ' ' . $usr->lastname}}
                                                                                                                                            </td>
                                                                                                                                            <td>{{$ref->currency}} {{number_format($ref->amount, 2)}}</td>
                                                                                                                                        </tr>
                                                                                            @endforeach
                                                @else

                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function () {
        $('#basic-datatables').DataTable({
        });

        $('#multi-filter-select').DataTable({
            "pageLength": 5,
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var select = $('<select class="form-control"><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                });
            }
        });

        // Add Row
        $('#add-row').DataTable({
            "pageLength": 5,
        });

        var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

        $('#addRowButton').click(function () {
            $('#add-row').dataTable().fnAddData([
                $("#addName").val(),
                $("#addPosition").val(),
                $("#addOffice").val(),
                action
            ]);
            $('#addRowModal').modal('hide');

        });
    });
</script>
@endsection