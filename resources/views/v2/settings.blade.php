@extends('v2.layouts.main')
@section('style')
<style>
    .disabled {
        pointer-events: none;
        color: grey;
    }
</style>


@section('title', 'Settings')
@endsection
@section('content')
<div class="nk-content nk-content-fluid">
    <div class="container-xl wide-xl">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block">
                    <div class="card card-bordered">
                        <div class="card-aside-wrap">
                            <div class="card-inner card-inner-lg">
                                <div class="nk-block-head nk-block-head-lg">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h4 class="nk-block-title">Security Settings</h4>
                                            <div class="nk-block-des">
                                                <p>These settings are helps you keep your account secure.</p>
                                            </div>
                                        </div>
                                        <div class="nk-block-head-content align-self-start d-lg-none">
                                            <a href="#" class="toggle btn btn-icon btn-trigger mt-n1" data-target="userAside"><em class="icon ni ni-menu-alt-r"></em></a>
                                        </div>
                                    </div>
                                </div><!-- .nk-block-head -->
                                <div class="nk-block">
                                    @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if (session()->has('status') && session()->get('msgType') == 'suc')
                                    <div class="alert alert-success">
                                        <p>{{session()->get('status')}}</p>
                                    </div>
                                    {{session()->forget('status')}}
                                     {{session()->forget('msgType')}}
                                @endif
                                @if (session()->has('status') && session()->get('msgType') == 'err')
                                <div class="alert alert-danger">
                                    <p>{{session()->get('status')}}</p>
                                </div>
                                {{session()->forget('status')}}
                                 {{session()->forget('msgType')}}
                            @endif
                                    <div class="card card-bordered">
                                        <div class="card-inner-group">
                                            <div class="card-inner">
                                                <div class="between-center flex-wrap flex-md-nowrap g-3">
                                                    <div class="nk-block-text">
                                                        <h6>Save my Activity Logs</h6>
                                                        <p>You can save your all activity logs including unusual activity detected.</p>
                                                    </div>
                                                    <div class="nk-block-actions">
                                                        <ul class="align-center gx-3">
                                                            <li class="order-md-last">
                                                                <div class="custom-control custom-switch mr-n2">
                                                                    <input type="checkbox" class="custom-control-input" checked="" id="activity-log">
                                                                    <label class="custom-control-label" for="activity-log"></label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div><!-- .card-inner -->
                                            <div class="card-inner">
                                                <div class="between-center flex-wrap g-3">
                                                    <div class="nk-block-text">
                                                        <h6>Change Password</h6>
                                                        <p>Set a unique password to protect your account.</p>
                                                    </div>
                                                    <div class="nk-block-actions flex-shrink-sm-0">
                                                        <ul class="align-center flex-wrap flex-sm-nowrap gx-3 gy-2">
                                                            <li class="order-md-last">
                                                                <a href="#" data-toggle="modal" data-target="#password-edit" class="btn btn-primary">Change Password</a>
                                                            </li>
                                                            <li>
                                                                {{-- <em class="text-soft text-date fs-12px">Last changed: <span>Oct 2, 2019</span></em> --}}
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div><!-- .card-inner -->
                                            <div class="card-inner">
                                                <div class="between-center flex-wrap flex-md-nowrap g-3">
                                                    <div class="nk-block-text">
                                                        @if($data['user']->loginSecurity == null)
                                                           <h6>2FA Authentication <span class="badge badge-danger">Disabled</span></h6>
                                                        @else
                                                             @if (!$data['user']->loginSecurity->google2fa_enable )
                                                                <h6>2FA Authentication <span class="badge badge-danger">Disabled</span></h6>
                                                            @elseif($data['user']->loginSecurity->google2fa_enable)
                                                                <h6>2FA Authentication <span class="badge badge-success">Enabled</span></h6>
                                                            @endif
                                                        @endif
                                                        <p>Secure your account with 2FA security. When it is activated you will need to enter not only your password, but also a special code using app. You can receive this code by in mobile app. </p>
                                                    </div>
                                                    <div class="nk-block-actions">
                                                        @if(!$data['user']->loginSecurity == null)
                                                            @if ($data['user']->loginSecurity->google2fa_enable)
                                                                 <a href="{{route('v2.2fa_disable')}}" class="btn btn-lg btn-primary" >Disable</a>
                                                            @else
                                                                 <a href="{{route('v2.2fa.page')}}" class="btn btn-lg btn-primary" >Enable</a>
                                                            @endif
                                                        @else
                                                            <a id='myLink' href="{{route('v2.2fa.page')}}" class="btn btn-lg btn-primary " >Activate</a>
                                                        @endif


                                                    </div>




                                                </div>
                                            </div>
                                            <!-- .card-inner -->
                                        </div><!-- .card-inner-group -->
                                    </div><!-- .card -->
                                </div><!-- .nk-block -->
                            </div><!-- .card-inner -->
                            <div class="card-aside card-aside-left user-aside toggle-slide toggle-slide-left toggle-break-lg" data-content="userAside" data-toggle-screen="lg" data-toggle-overlay="true">
                                <div class="card-inner-group" data-simplebar>

                                   @include('v2.inc.sidebar')
                                </div><!-- .card-inner-group -->
                            </div><!-- card-aside -->
                        </div><!-- .card-aside-wrap -->
                    </div><!-- .card -->
                </div><!-- .nk-block -->
            </div>
        </div>
    </div>
</div>

@include('v2.inc.change_pwd_modal')
@endsection

@section('script')
<script>
    // var link = document.getElementById('myLink');
    // link.removeAttribute('href');
    // link.classList.add('disabled');
</script>
@endsection
