<?php
namespace Tests\Feature;

use App\User;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_can_view_login_form()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertViewIs('auth.login');
    }

    public function test_user_can_login_with_correct_credentials()
    {
        // Assuming you have a User factory set up
        $user = factory(User::class)->create([
            'pwd' => bcrypt($password = 'i-love-laravel'),
        ]);
        $response = $this->post('/', [
            'email' => $user->email,
            'password' => $password,
        ]);

        $this->assertAuthenticatedAs($user);
        $response->assertRedirect('/app/overview'); // Or wherever your application redirects users on login
    }

    public function test_user_cannot_login_with_incorrect_password()
    {
        $user = factory(User::class)->create([
            'pwd' => bcrypt('correct-password'),
        ]);

        $response = $this->from('/')->post('/', [
            'email' => $user->email,
            'pwd' => 'wrong-password',
        ]);

        $response->assertRedirect('/');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }
}
 