<?php

namespace Tests;

use Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        // Run specific Artisan commands
        $this->artisan('settings:register')
            ->expectsQuestion('Cloudlinary URL for the site logo', '')
            ->expectsQuestion('Cloudlinary URL for the site favicon', '')
            ->assertExitCode(0);
        Artisan::call("create:packages");
        // You can run any Artisan command this way
        Artisan::call('seed:country-state');
    }

}
